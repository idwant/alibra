export const pagesConstants = {
    'MAIN': '/login',
    'LOGIN': '/login'
};

export const userConstants = {
    'CURRENT_KEY': 'current',
    'AUTH_USER_HEADER_KEY': 'token',
    'AUTH_HEADER_KEY': 'Authorization'
};

export const AUTH_EVENTS = {
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
};