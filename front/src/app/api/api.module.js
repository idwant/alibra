import config from './api.config';
import UserManager from './services/UserManager.service';
import CourseManager from './services/CourseManager.service';
import UnitManager from './services/UnitManager.service';
import LessonManager from './services/LessonManager.service';
import LessonPartManager from './services/LessonPartManager.service';
import ExerciseManager from './services/ExerciseManager.service';
import ExerciseTemplateManager from './services/ExerciseTemplateManager.service';
import DictionaryManager from './services/DictionaryManager.service';
import WordManager from './services/WordManager.service';
import WordCardManager from './services/WordCardManager.service';
import WordCardCategoryManager from './services/WordCardCategoryManager.service';
import WordCardGroupManager from './services/WordCardGroupManager.service';
import GrammarManager from './services/GrammarManager.service';

import StudentDashboardManager from './services/StudentDashboardManager.service';
import StudentWordCardManger from './services/StudentWordCardManager.service';
import StudentManager from './services/StudentManager.service';
import StudentGrammarManager from './services/StudentGrammarManager.service';
import HintManager from './services/HintManager.service';
import StudentCourseManager from './services/StudentCourseManager.service';

let api = angular
    .module('api', ['ng-rest-api'])
    .config(config)
    .service('UserManager', UserManager)
    .service('CourseManager', CourseManager)
    .service('UnitManager', UnitManager)
    .service('LessonManager', LessonManager)
    .service('LessonPartManager', LessonPartManager)
    .service('ExerciseManager', ExerciseManager)
    .service('ExerciseTemplateManager', ExerciseTemplateManager)
    .service('DictionaryManager', DictionaryManager)
    .service('WordManager', WordManager)
    .service('WordCardManager', WordCardManager)
    .service('WordCardCategoryManager', WordCardCategoryManager)
    .service('WordCardGroupManager', WordCardGroupManager)
    .service('GrammarManager', GrammarManager)
    .service('HintManager', HintManager)

    .service('StudentDashboardManager', StudentDashboardManager)
    .service('StudentWordCardManager', StudentWordCardManger)
    .service('StudentManager', StudentManager)
    .service('StudentGrammarManager', StudentGrammarManager)
    .service('StudentCourseManager', StudentCourseManager)
    ;

export default api = api.name;
