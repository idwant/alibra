import User from './models/User';
import {UserSession} from './models/User';
import Unit from './models/Unit';
import Course from './models/Course';
import Lesson from './models/Lesson';
import LessonPart from './models/LessonPart';
import Exercise from './models/Exercise';
import ExerciseTemplate from './models/ExerciseTemplate';
import Dictionary from './models/Dictionary';
import Word from './models/Word';
import WordCard from './models/WordCard';
import WordCardCategory from './models/WordCardCategory';
import WordCardGroup from './models/WordCardGroup';
import Grammar from './models/Grammar';
import GrammarSection from './models/GrammarSection';
import GrammarTheme from './models/GrammarTheme';
import WordTranslation from './models/WordTranslation';
import UserWordCard from './models/UserWordCard';
import WordCardStat from './models/WordCardStat';
import Progress from './models/Progress';
import Feedback from './models/Feedback';
import Hint from './models/Hint';

function config(apiProvider, API_URL) {
    'ngInject';

    apiProvider.setBaseRoute(API_URL);

    apiProvider.endpoint('session')
        .route('sessions/:token')
        .model(UserSession)
        .addHttpAction('POST', 'login')
        .addHttpAction('POST', 'loginByToken', {params: {token: '@token'}})
    ;

    apiProvider.endpoint('user')
        .route('users/:id')
        .model(User)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('userCourse')
        .route('users/:id/courses')
        .model(User)
        .addHttpAction('POST', 'add', {params: {id: '@id'}})
        .addHttpAction('DELETE', 'remove', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('course')
        .route('courses/:id')
        .model(Course)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count'], params: {limit: 1000}})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
        .addHttpAction('DELETE', 'remove', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('unit')
        .route('units/:id')
        .model(Unit)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
        .addHttpAction('POST', 'addRelation', {params: {id: 'bind'}})
        .addHttpAction('POST', 'removeRelation', {params: {id: 'unbind'}})
        .addHttpAction('PATCH', 'sort', {params: {id: 'sort'}})
    ;

    apiProvider.endpoint('lesson')
        .route('lessons/:id')
        .model(Lesson)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
        .addHttpAction('POST', 'addRelation', {params: {id: 'bind'}})
        .addHttpAction('POST', 'removeRelation', {params: {id: 'unbind'}})
        .addHttpAction('PATCH', 'sort', {params: {id: 'sort'}})
    ;

    apiProvider.endpoint('grammar')
        .route('grammars/:id')
        .model(Grammar)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('grammarSection')
        .route('grammar-sections/:id')
        .model(Grammar)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('grammarTheme')
        .route('grammar-themes/:id')
        .model(Grammar)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('lessonPart')
        .route('lesson-parts/:id')
        .model(LessonPart)
        .addHttpAction('GET', 'query', {isArray: true,  headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}});

    apiProvider.endpoint('exercise')
        .route('exercises/:id')
        .model(Exercise)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
        .addHttpAction('POST', 'addRelation', {params: {id: 'bind'}})
        .addHttpAction('POST', 'removeRelation', {params: {id: 'unbind'}})
        .addHttpAction('PATCH', 'sort', {params: {id: 'sort'}})
    ;

    apiProvider.endpoint('exerciseTemplate')
        .route('exercise-templates/:name')
        .model(ExerciseTemplate)
        .addHttpAction('GET', 'query', {isArray: true})
        .addHttpAction('PATCH', 'patch', {params: {name: '@name'}})
    ;

    apiProvider.endpoint('dictionary')
        .route('dictionaries/:id')
        .model(Dictionary)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('dictionaryWord')
        .route('dictionaries/:id/words/:wordId')
        .model(Word)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count'], params: {id: '@id'}})
        .addHttpAction('POST', 'addWord', {params: {id: '@id', wordId: '@wordId'}})
        .addHttpAction('DELETE', 'removeWord', {params: {id: '@id', wordId: '@wordId'}})
    ;

    apiProvider.endpoint('word')
        .route('words/:id')
        .model(Word)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('wordTranslation')
        .route('word-translations/:id')
        .model(WordTranslation)
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('wordCard')
        .route('word-cards/:id')
        .model(WordCard)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('wordCardLesson')
        .route('word-cards/:id/lessons')
        .model(WordCard)
        .addHttpAction('POST', 'add', {params: {id: '@id'}})
        .addHttpAction('DELETE', 'remove', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('wordCardCategory')
        .route('word-card-categories/:id')
        .model(WordCardCategory)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
        .addHttpAction('GET', 'get', {params: {id: '@id'}})
        .addHttpAction('DELETE', 'remove', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('wordCardGroup')
        .route('word-card-groups/:id')
        .model(WordCardGroup)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
        .addHttpAction('PATCH', 'patch', {params: {id: '@id'}})
        .addHttpAction('DELETE', 'remove', {params: {id: '@id'}})
    ;

    apiProvider.endpoint('studentDashboardCourse')
        .route('students/dashboard/courses')
        .model(Course)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentDashboardUnit')
        .route('students/dashboard/units')
        .model(Unit)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentDashboardLesson')
        .route('students/dashboard/lessons')
        .model(Lesson)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentDashboardLessonPart')
        .route('students/dashboard/lesson-parts')
        .model(LessonPart)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentCourse')
        .route('students/courses/:id')
        .model(Course)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentExercise')
        .route('students/exercises')
        .model(Exercise)
        .addHttpAction('GET', 'query', {isArray: true})
    ;


    apiProvider.endpoint('studentExerciseAction')
        .route('students/exercises/:action')
        .addHttpAction('PATCH', 'start', {params: {action: 'start'}})
        .addHttpAction('PATCH', 'finish', {params: {action: 'finish'}})
    ;

    apiProvider.endpoint('studentWordCardGroup')
        .route('students/word-card-groups/:id')
        .model(WordCardGroup)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentWordCardCategory')
        .route('students/word-card-categories/:id')
        .model(WordCardCategory)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentWordCard')
        .route('students/word-cards/:action/:id')
        .model(WordCard)
        .addHttpAction('GET', 'getLearn', {isArray: true, params: {action: 'learn'}})
        .addHttpAction('PATCH', 'learn', {params: {id: '@id', action: 'learn'}})
        .addHttpAction('PATCH', 'postponeLearn', {params: {id: '@id', action: 'postpone-learn'}})
        .addHttpAction('PATCH', 'disable', {params: {id: '@id', action: 'disable'}})
    ;

    apiProvider.endpoint('studentUserWordCard')
        .route('students/word-cards/:action/:id')
        .model(UserWordCard)
        .addHttpAction('GET', 'getRepeat', {isArray: true, params: {action: 'repeat'}})
        .addHttpAction('GET', 'getReview', {isArray: true, params: {action: 'review'}})
        .addHttpAction('PATCH', 'repeat', {params: {id: '@id', action: 'repeat'}})
    ;

    apiProvider.endpoint('studentWordCardStat')
        .route('students/word-cards/stat')
        .model(WordCardStat)
        .addHttpAction('GET', 'query')
    ;

    apiProvider.endpoint('studentProgressStat')
        .route('students/courses/stat')
        .model(Progress)
        .addHttpAction('GET', 'query', {isArray: true, headersForReading: ['x-total-count']})
    ;

    apiProvider.endpoint('studentProgressStatExport')
        .route('students/courses/stat/:type')
        //.model(Progress)
        .addHttpAction('GET', 'export', {instantiateModel: false, params: {type: '@type'}})
    ;

    apiProvider.endpoint('studentFeedback')
        .route('students/feedbacks')
        .model(Feedback)
        .addHttpAction('POST', 'create')
    ;

    apiProvider.endpoint('studentGrammar')
        .route('students/grammars/:id')
        .model(Grammar)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentGrammarSection')
        .route('students/grammar-sections/:id')
        .model(GrammarSection)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('studentGrammarTheme')
        .route('students/grammar-themes/:id')
        .model(GrammarTheme)
        .addHttpAction('GET', 'query', {isArray: true})
    ;

    apiProvider.endpoint('hint')
        .route('students/hints')
        .model(Hint)
        .addHttpAction('GET', 'query', {isArray: true})
    ;
}

export default config;