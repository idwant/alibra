import Audio from './File/Audio';
import Image from './File/Image';
import WordTranslation from "./WordTranslation";

class Word {
    constructor({id, word, translations = [], transcription, image, audio} = {}) {
        this.id = id;
        this.word = word;
        this.transcription = transcription;
        this.image = new Image(image || {});
        this.audio = new Audio(audio || {});
        this.translations = translations.map((el) => new WordTranslation(el));
    }

    beforeSave() {
        this.image = this.image.id;
        this.audio = this.audio.id;
    }

    getFirstTranslation() {
        return this.translations.length > 0 ? this.translations[0] : null;
    }

    get translationsWords() {
        return this.translations.map(translation => translation.translation).join(', ');
    }
}

export default Word;