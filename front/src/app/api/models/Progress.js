import Course from './Course';
import Unit from './Unit';
import Lesson from './Lesson';
import LessonPart from './LessonPart';
import Dashboard from './Dashboard';

import Exercise from './Exercise';

class Progress {
    constructor({course_number, unit_number, lesson_number, lesson_part_number, course, unit, lesson, lesson_part, lesson_part_stat, exercise_type}) {
        this.course_numer = course_number;
        this.unit_number = unit_number;
        this.lesson_number = lesson_number;
        this.lesson_part_number = lesson_part_number;

        this.course = course ? new Course(course) : null;
        this.unit = unit ? new Unit(unit) : null;
        this.lesson = lesson ? new Lesson(lesson) : null;
        this.lesson_part = lesson_part ? new LessonPart(lesson_part) : null;
        this.lesson_part_stat = lesson_part_stat ? new Dashboard(lesson_part_stat) : null;
        this.exercise_type = exercise_type;
    }

    get isHomework() {
        return this.exercise_type === Exercise.TYPE_HOMEWORK;
    }

    get isDone() {
        return this.lesson_part_stat.isDone;
    }
}

export default Progress;