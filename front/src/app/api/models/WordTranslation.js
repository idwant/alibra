import Audio from './File/Audio';

class WordTranslation {
    constructor({id, translation, sample, sample_audio} = {}) {
        this.id = id;
        this.sample = sample;
        this.translation = translation;
        this.sample_audio = new Audio(sample_audio || {});
        this.is_deleted = false;
    }

    beforeSave() {
        this.sample_audio = this.sample_audio ? this.sample_audio.id : null;
        this.image = this.image ? this.image.id : null;
    }
}

export default WordTranslation;