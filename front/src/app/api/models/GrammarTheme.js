class GrammarTheme {
    constructor({id, name, section}) {
        this.id = id;
        this.name = name;
        this.section = section;

        this._grammars = null;
    }

    get grammars() {
        return this._grammars;
    }

    set grammars(grammars) {
        this._grammars = grammars;
    }
}

export default GrammarTheme;