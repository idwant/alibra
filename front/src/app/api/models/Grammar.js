import Video from './../models/File/Video';

class Grammar {
    constructor({id, name, content, is_demo, video, theme = {}}) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.is_demo = is_demo;
        this.theme = theme || {};
        this.video = new Video(video || {});
        this.section = this.theme.section || {};
    }

    validate() {
        let errors = [];

        if (!this.name) {
            errors.push('Не указано имя');
        }

        if (!this.content) {
            errors.push('Не указано содержание');
        }

        if (!this.theme || !this.theme.id) {
            errors.push('Не выбрана тема');
        }

        return errors;
    }

    beforeSave() {
        this.video = this.video.id;
        this.theme = this.theme.id;
    }
}

export default Grammar