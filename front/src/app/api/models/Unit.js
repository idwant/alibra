import Dashboard from './Dashboard';

class Unit {
    constructor({id, name, courses, dashboard}) {
        this.id = id;
        this.name = name;
        this.parents = this.courses = courses;
        this.dashboard = dashboard ? new Dashboard(dashboard) : null;
    }

    get isDone() {
        return this.dashboard ? this.dashboard.common.isDone : false;
    }

    get totalExercises() {
        return this.dashboard ? this.dashboard.common.total : 0;
    }

    get completedExercises() {
        return this.dashboard ? this.dashboard.common.completed : 0;
    }
}

export default Unit;