import Word from './Word';
import WordCardGroup from './WordCardGroup';
import Lesson from './Lesson';
import Audio from './File/Audio';
import Image from './File/Image';

class WordCard {
    constructor({id, word, audio, sample_audio, image, groups, sample, translation, transcription, lessons} = {}) {
        this.id = id;
        this.word = new Word(word);
        this.image = new Image(image || {});
        this.audio = new Audio(audio || {});
        this.sample_audio = new Audio(sample_audio || {});
        this.sample = sample;
        this.translation = translation;
        this.transcription = transcription;

        groups = groups || [];
        this.groups = groups.map(group => new WordCardGroup(group));

        lessons = lessons || [];
        this.lessons = lessons.map(lesson => new Lesson(lesson));

        // Работаем только с одной группой, хоть бэкенд и позволяет с несколькими
        this.group = this.groups.length ? this.groups[0] : new WordCardGroup({});
        this.category = this.group.category;
    }

    onChangeWord() {
        let firstTranslation = this.word.getFirstTranslation();

        this.image = this.word.image;
        this.audio = this.word.audio;
        this.transcription = this.word.transcription;

        this.sample = firstTranslation ? firstTranslation.sample : '';
        this.sample_audio = firstTranslation ? firstTranslation.sample_audio : new Audio({});
        this.translation = firstTranslation ? firstTranslation.translation : '';
    }

    beforeSave() {
        this.image = this.image.id;
        this.audio = this.audio.id;
        this.sample_audio = this.sample_audio.id;
        this.word = this.word.id;

        this.groups = this.group && this.group.id ? [this.group.id] : [];
    }

    addLesson(lesson) {
        let lessonIdx = this._searchLessonById(lesson.id);
        if (lessonIdx < 0) {
            this.lessons.push(lesson);
        }
    }

    removeLesson(lesson) {
        let lessonIdx = this._searchLessonById(lesson.id);
        if (lessonIdx >= 0) {
            this.lessons.splice(lessonIdx, 1);
        }
    }

    _searchLessonById(id) {
        return this.lessons.findIndex(function (item) {
            return item.id == id;
        });
    }
}

export default WordCard;