class Hint {
    constructor({name, text, user_attempts, max_attempts, is_show}) {
        this.name = name;
        this.text = text;
        this.user_attempts = user_attempts;
        this.max_attempts = max_attempts;
        this.closedInThisSession = false;
        this.is_show = is_show;
    }

    close() {
        this.user_attempts++;
        this.closedInThisSession = true;
    }

    needShow() {
        return (this.user_attempts < this.max_attempts) && !this.closedInThisSession;
    }

    isClosedInThisSession() {
        return this.closedInThisSession;
    }
}

export default Hint;