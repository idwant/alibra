import DashboardItem from './DashboardItem';

class Dashboard {
    constructor({common, group_task, homework, test}) {
        this.common = new DashboardItem(common || {});
        this.group_task = new DashboardItem(group_task || {});
        this.homework = new DashboardItem(homework || {});
        this.test = new DashboardItem(test || {});
    }

    get isDone() {
        return this.common.isDone;
    }

    get total() {
        return this.common.total;
    }

    get completed() {
        return this.common.complete;
    }

    get completedPercent() {
        return Math.floor(this.common.completed / this.common.total * 100);
    }

    get remainingExercises() {
        return this.common.wait + this.common.in_progress;
    }

    /**
     * Складывает показатели текущего Dashboard с другим
     * @param item
     */
    add(item) {
        this.common.add(item.common);
        this.group_task.add(item.group_task);
        this.homework.add(item.homework);
        this.test.add(item.test);
    }
}

export default Dashboard;