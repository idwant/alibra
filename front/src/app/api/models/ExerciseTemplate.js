class ExerciseTemplate {
    constructor({id, name, task_en, type, task_ru, audio} = {}) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.task_en = task_en;
        this.task_ru = task_ru;
        this.audio = audio;
    }
}

export default ExerciseTemplate;