import WordCardCategory from './WordCardCategory';

class WordCardGroup {
    constructor({id, name = '', description = '', translation =  '', category,
        learned_count = 0, ready_to_repeatition_count = 0,
        group_total_count = 0, total_count = 0,
        current_repetition_step = 0, min_repetition_step = 0, max_repetition_step = 0,
        max_repetition_date = '',

        total_to_repetition_count = 0,
        postponed_learn_count = 0} = {}
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.translation = translation;
        this.category = category ? new WordCardCategory(category) : null;

        // число изученных слов
        this.learned_count = learned_count;

        // число слов на изучении, которые были отложены
        this.postponed_learn_count = postponed_learn_count;

        // число слов, которое готово к повторению сейчас
        this.ready_to_repeatition_count = ready_to_repeatition_count;

        // число слов, которое находится на повторении
        this.total_to_repetition_count = total_to_repetition_count;

        // число слов, которое находится в группе на текущем уровне повторения
        this.group_total_count = group_total_count;

        // общее число слов в группе
        this.total_count = total_count;

        // число повторений, которое было совершено для группы, к которой принадлежит слово
        this.current_repetition_step = current_repetition_step;

        // минимальное число повторений в группе слов
        // на момент разработки данное поле должно быть равно current_repetition_step
        this.min_repetition_step = min_repetition_step;

        // максимальное число повторений
        this.max_repetition_step = max_repetition_step;

        // дата повторения
        this.max_repetition_date = max_repetition_date;
    }

    beforeSave() {
        this.category = this.category ? this.category.id : null;
    }

    /**
     * Возвращает true, если группа даже не начинали изучать, иначе - false
     */
    get notStarted() {
        return this.learned_count === 0;
    }

    /**
     * Возвращает true, если группа изучена частично
     */
    get partiallyLearned() {
        return (this.learned_count > 0) && !this.fullyLearned;
    }

    /**
     * Возвращает true, если группа полностью изучена, иначе - false
     */
    get fullyLearned() {
        return this.learned_count >= this.total_count;
    }

    /**
     * Возвращает число слов, которые повторены
     */
    get repeatedWordsCount() {
        return this.total_count - this.total_to_repetition_count;
    }

    /**
     * Возвращает true, если группа готова к повторению
     */
    get isReadyToRepetition() {
        let repetitionDate = Date.parse(this.max_repetition_date);
        let now = (new Date()).getTime();
        return this.fullyLearned && (this.ready_to_repeatition_count > 0 && repetitionDate <= now);
    }

    /**
     * Возвращает true, если группа и изучена и повторена полностью, иначе - false
     */
    get isDone() {
        return this.total_to_repetition_count === 0;
    }

    get hasAvailableToLearn() {
        return (this.total_count - this.learned_count - this.postponed_learn_count) > 0;
    }

    get title()
    {
        if (this.fullyLearned && this.isDone) {
            return 'Тема пройдена';
        } else if (this.fullyLearned) {
            return 'Повторение слов';
        } else if (this.notStarted) {
            return 'Тема не начата';
        }

        return 'Изучена часть слов';
    }

    get learnPercent() {
        return Math.round(this.learned_count / this.total_count * 100 );
    }
}

export default WordCardGroup;