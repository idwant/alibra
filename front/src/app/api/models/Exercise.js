import CompareParams from './ExerciseParams/CompareParams';
import DialogParams from './ExerciseParams/DialogParams';
import DragAndDropParams from './ExerciseParams/DragAndDropParams';
import FindCoupleParams from './ExerciseParams/FindCoupleParams';
import FreeFormParams from './ExerciseParams/FreeFormParams';
import ListenSayParams from './ExerciseParams/ListenSayParams';
import ListenWriteParams from './ExerciseParams/ListenWriteParams';
import ListenReadClickParams from './ExerciseParams/ListenReadClickParams';
import PictureParams from './ExerciseParams/PictureParams';
import ReadWriteParams from './ExerciseParams/ReadWriteParams';
import SelectParams from './ExerciseParams/SelectParams';
import PickOutParams from './ExerciseParams/PickOutParams';
import FiveCardsParams from './ExerciseParams/FiveCardsParams';
import SnowballParams from './ExerciseParams/SnowballExerciseParams';
import CrosswordParams from './ExerciseParams/CrosswordParams';
import Audio from '../../api/models/File/Audio';
import Image from '../../api/models/File/Image';
import Dictionary from '../models/Dictionary';
import Grammar from '../models/Grammar';
import Lesson from '../models/Lesson';

import UserExercise from './UserExercise';
import DragAndDropAudioParams from "./ExerciseParams/DragAndDropAudioParams";

class Exercise {
    constructor({template_type, task_audio, id, name, type, params = [], lessons, task_en, task_ru, dictionary, grammar, user_exercise, lesson} = {}) {
        this.id = id;
        this.name = name;
        this.type = this.getExerciseTypeByCode(type) || this.getExerciseTypes()[0];
        this.template_type = template_type;
        this.parents = this.lessons = lessons;
        this.params = params.map((param) => this._paramsFactory(template_type, param));
        this.params = this.params.length ? this.params : [this._paramsFactory(template_type, {})];
        this.task_en = task_en;
        this.task_ru = task_ru;
        this.task_audio = new Audio(task_audio || {});
        this.dictionary = dictionary ? new Dictionary(dictionary) : null;
        this.grammar = grammar ? new Grammar(grammar) : null;
        this.user_exercise = new UserExercise(user_exercise || {});
        this.lesson = lesson ? new Lesson(lesson) : null;

        this.params.forEach(p => p.result = null);
        if (this.user_exercise && this.user_exercise.completed && this.user_exercise.completed.result) {
            this.user_exercise.completed.result.forEach((r, i) => {
                if (this.params[i]) {
                    this.params[i].result = r.success  || null;
                    this.params[i].progress = r.progress || null;
                }
            });
        }
    }

    // если раунд упражнения внутри себя разделен на отдельные задания, которые нужно учитывать при расчете прогресса - true
    hasRoundsWithSteps() {
        const firstRound = this.params[0];
        return typeof firstRound.getStepsCount === 'function';
    }

    getRoundStepsCount() {
        return this.params.reduce((total, cur) => total += +cur.getStepsCount(), 0);
    }

    static createFromTemplate(exerciseTemplate) {
        return new Exercise({template_type: exerciseTemplate.type})
    }

    getCountRounds() {
        return this.params.length;
    }

    getRound(roundCode) {
        return this.params[roundCode];
    }

    getRoundsResults() {
        return this.params.map((param) => param.result);
    }

    addParam() {
        this.params.push(this._paramsFactory(this.template_type, {}));
    }

    deleteParam(param) {
        let paramIndex = this.params.indexOf(param);
        if (paramIndex != -1) {
            this.params.splice(paramIndex, 1);
        }
    }

    getExerciseTypes() {
        return [
            {code: 'group_task', title: 'Урок'},
            {code: 'homework', title: 'Домашнее задание'},
            {code: 'test', title: 'Тест'}
        ];
    }

    getExerciseTypeByCode(code) {
        return this.getExerciseTypes().filter(exerciseType => exerciseType.code == code).pop();
    }

    onAudioTaskLoaded(response) {
        this.task_audio = new Audio(response || {});
    }

    onAudioParamLoaded(param, paramName, response) {
        let paramIndex = this.params.indexOf(param);
        if (paramIndex != -1) {
            param[paramName] = new Audio(response || {});
        }
    }

    onImageParamLoaded(param, paramName, response) {
        let paramIndex = this.params.indexOf(param);
        if (paramIndex != -1) {
            param[paramName] = new Image(response || {});
        }
    }

    // Когда пользователь хочет еще раз пройти упражнение - сбрасываем значения
    tryAgain() {
        try {
            this.user_exercise.completed = false;
            this.params.forEach(e => {
                e.result = null;
                e.progress= null
            });
        } catch (e) {
            console.log(e.message);
        }
    }

    _paramsFactory(type = '', params = {}) {

        switch (type.toLowerCase()) {
            case 'compare':
                return new CompareParams(params);
            case 'dialog-exercise':
                return new DialogParams(params);
            case 'drag-and-drop':
                return new DragAndDropParams(params);
            case 'drag-and-drop-audio':
                return new DragAndDropAudioParams(params);
            case 'find-couple':
                return new FindCoupleParams(params);
            case 'free-form':
                return new FreeFormParams(params);
            case 'listen-say':
                return new ListenSayParams(params);
            case 'listen-write':
                return new ListenWriteParams(params);
            case 'listen-read-click':
                return new ListenReadClickParams(params);
            case 'picture':
                return new PictureParams(params);
            case 'read-write':
                return new ReadWriteParams(params);
            case 'select':
                return new SelectParams(params);
            case 'pick-out':
                return new PickOutParams(params);
            case 'five-cards':
                return new FiveCardsParams(params);
            case 'snowball':
                return new SnowballParams(params);
            case 'crossword':
                return new CrosswordParams(params);
            default:
                return {}
        }
    }

    beforeSave() {
        this.type = this.type ? this.type.code : null;
        this.grammar = this.grammar ? this.grammar.id : null;
        this.dictionary = this.dictionary ? this.dictionary.id : null;
        this.task_audio = this.task_audio.id;
        this.params.map((param) => {
            if (typeof param.beforeSave === 'function') {
                param.beforeSave();
            }
        });

    }

    validate() {
        let errors = [];

        if (!this.type) {
            errors.push('Не задан тип упражнения');
        }

        if (!this.name) {
            errors.push('Не задано название упражнения');
        }

        this.params.forEach((param) => {
            errors.push(...param.validate())
        });
        return errors;
    }
}

Exercise.TYPE_GROUP_TASK = 'group_task';
Exercise.TYPE_HOMEWORK = 'homework';
Exercise.TYPE_TEST = 'test';

export default Exercise;