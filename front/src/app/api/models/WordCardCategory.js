import Image from './File/Image';

class WordCardCategory {
    constructor({id, name = '', description = '', image, image_big, groups_count = 0, groups = []} = {}) {
        this.id = id;
        this.name = name;
        this.description = description;

        this.image = image ? new Image(image) : null;
        this.image_big = image_big ? new Image(image_big) : null;
        this.groups_count = groups_count;
        this.groups = groups;
    }

    beforeSave() {
        this.image = this.image ? this.image.id : null;
        this.image_big = this.image_big ? this.image_big.id : null;
    }
}

export default WordCardCategory;