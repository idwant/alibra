import WordCard from './WordCard';

class UserWordCard {
    constructor({id, known, word_card, possible_answers, repetition_count} = {}) {
        this.id = id;
        this.known = known;
        this.word_card = new WordCard(word_card) || null;
        this.possible_answers = possible_answers;
        this.repetition_count = repetition_count;
    }
}

export default UserWordCard;