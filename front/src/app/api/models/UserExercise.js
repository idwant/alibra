class UserExercise {
    constructor({id, start_date, end_date, status, completed}) {
        this.id = id || 0;
        this.start_date = start_date;
        this.end_date = end_date;
        this.status = status || UserExercise.STATUS_WAIT;
        this.completed = completed;
    }
}

UserExercise.STATUS_WAIT = 0;
UserExercise.STATUS_SUCCESS = 1;
UserExercise.STATUS_IN_PROGRESS = 2;
UserExercise.STATUS_FAIL = 3;

export default UserExercise;