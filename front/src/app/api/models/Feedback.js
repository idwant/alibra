import User from './../models/User';

class Feedback {
    constructor({id = 0, email = '', title = '', message = '', is_active = 1, created_at = '', user = null}) {
        this.id = id;
        this.email = email;
        this.title = title;
        this.message = message;
        this.is_active = is_active;
        this.created_at = created_at;

        this.user = user ? new User(user) : null;
    }
}

export default Feedback;