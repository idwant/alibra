class UserSession {
    constructor({token, user}) {
        this.token = token;
        this.user = new User(user);
    }
}

class User {
    constructor({id, email, password, roles,
        last_name, first_name, middle_name,
        phone, city, birthday,
        agreement_number, is_demo,
        teachers, students, employers, employees, courses
    } = {}) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.last_name = last_name || '';
        this.first_name = first_name || '';
        this.middle_name = middle_name || '';
        this.phone = phone;
        this.city = city;
        this.birthday = birthday;
        this.agreement_number = agreement_number;
        this.is_demo = is_demo;
        this.teachers = teachers;
        this.students = students;
        this.employers = employers;
        this.employees = employees;
        this.courses = courses;
    }

    get fio() {
        let name = this.last_name.trim() + ' ' + this.first_name.trim();
        let fio = name.trim() + ' ' + this.middle_name.trim();

        return fio.trim();
    }

    get coursesNames() {
        return this.courses
            .map((course) => course.name)
            .join(', ');
    }

    addCourse(course) {
        let courseIdx = this._searchCourseById(course.id);
        if (courseIdx < 0) {
            this.courses.push(course);
        }
    }

    removeCourse(course) {
        let courseIdx = this._searchCourseById(course.id);
        if (courseIdx >= 0) {
            this.courses.splice(courseIdx, 1);
        }
    }

    _searchCourseById(id) {
        return this.courses.findIndex(function (item) {
            return item.id == id;
        });
    }
}

export {UserSession};
export default User;