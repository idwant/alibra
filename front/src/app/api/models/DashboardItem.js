class DashboardItem {
    constructor({complete = 0, fail = 0, success = 0 , wait = 0, in_progress = 0}) {
        this.complete = complete;
        this.fail = fail || 0;
        this.in_progress = in_progress;
        this.success = success;
        this.wait = wait;
    }

    get isDone() {
        return this.wait === 0 && this.in_progress === 0;
    }

    get isInWork() {
        return !this.isDone && ((this.complete + this.in_progress) > 0);
    }

    get total() {
        return this.wait + this.complete + this.in_progress;
    }

    get completed() {
        return this.complete;
    }

    /**
     * Складывает показатели текущего элемента Dashboard с другим элементом
     * @param item
     */
    add(item) {
        this.complete += item.complete;
        this.fail += item.fail;
        this.in_progress += item.in_progress;
        this.success += item.success;
        this.wait += item.wait;
    }
}

export default DashboardItem;