import Dashboard from './Dashboard';

class LessonPart {
    constructor({id, name, type, exercises = [], dashboard}) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.exercises = exercises;
        this.dashboard = dashboard ? new Dashboard(dashboard) : null;
    }

    get isDone() {
        return this.dashboard ? this.dashboard.common.isDone : false;
    }

    get totalExercises() {
        return this.dashboard ? this.dashboard.common.total : 0;
    }

    get completedExercises() {
        return this.dashboard ? this.dashboard.common.completed : 0;
    }
}

export default LessonPart;