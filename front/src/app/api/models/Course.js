import Dashboard from './Dashboard';

class Course {
    constructor({id, name, is_phonetic, dashboard}) {
        this.id = id;
        this.name = name;
        this.is_phonetic = is_phonetic;
        this.dashboard = dashboard ? new Dashboard(dashboard) : null;
    }

    get isDone() {
        return this.dashboard ? this.dashboard.common.isDone : false;
    }

    get totalExercises() {
        return this.dashboard ? this.dashboard.common.total : 0;
    }

    get completedExercises() {
        return this.dashboard ? this.dashboard.common.completed : 0;
    }

    get isPhonetic() {
        return this.is_phonetic;
    }
}

export default Course;