import Dashboard from './Dashboard';

class Lesson {
    constructor({id, name, units, dashboard}) {
        this.id = id;
        this.name = name;
        this.parents = this.units = units;
        this.dashboard = dashboard ? new Dashboard(dashboard) : null;
    }

    get isDone() {
        return this.dashboard ? this.dashboard.common.isDone : false;
    }

    get isDoneGroupTask() {
        return this.dashboard ? this.dashboard.group_task.isDone : false;
    }

    get isDoneHomework() {
        return this.dashboard ? this.dashboard.homework.isDone : false;
    }

    get isDoneTest() {
        return this.dashboard ? this.dashboard.test.isDone : false;
    }

    get isInWorkHomework() {
        return this.dashboard ? this.dashboard.homework.isInWork : false;
    }

    get totalGroupTask() {
        return this.dashboard ? this.dashboard.group_task.total : false;
    }

    get totalHomework() {
        return this.dashboard ? this.dashboard.homework.total : false;
    }

    get totalTest() {
        return this.dashboard ? this.dashboard.test.total : false;
    }

    get completedGroupTask() {
        return this.dashboard ? this.dashboard.completed.total : false;
    }

    get completedHomework() {
        return this.dashboard ? this.dashboard.homework.total : false;
    }

    get completedTest() {
        return this.dashboard ? this.dashboard.test.total : false;
    }
}

export default Lesson;