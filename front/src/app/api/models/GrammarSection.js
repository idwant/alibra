class GrammarSection {
    constructor({id, name}) {
        this.id = id;
        this.name = name;

        this._themes = null;
    }

    get themes() {
        return this._themes;
    }

    set themes(themes) {
        this._themes = themes;
    }
}

export default GrammarSection;