import Image from '../File/Image';
import Audio from '../File/Audio';

class FindCoupleParams {
    constructor({couples = []}) {
        this.couples = couples.map((el) => new Couple(el));

        if(!this.couples.length) {
            this.couples.push(new Couple());
        }
        this.attempts = 0;
        this.maxAttempts = 3;
    }

    getStepsCount() {
        return this.couples.length;
    }

    addCouple() {
        this.couples.push(new Couple());
    }
    
    deleteCouple(couple) {
        let coupleIndex = this.couples.indexOf(couple);
        if (coupleIndex != -1) {
            this.couples.splice(coupleIndex, 1);
        }
    }

    beforeSave() {
        this.couples = this.couples.map((el) => el.beforeSave());
    }

    validate() {
        let errors = [];
        this.couples.map(el => errors.push(...el.validate()));
        return errors;
    }
}

class Couple {
    constructor({image_models = [], audio_models = [], words = []} = {}) {
        image_models = image_models || [];
        audio_models = audio_models || [];
        this.images = image_models.map(image_model => new Image(image_model || {}));
        this.audios = audio_models.map(audio_model => new Audio(audio_model || {}));
        this.words = words;

        this.elements = [...this.words, ...this.images, ...this.audios];
    }

    first() {
        return this.createCoupleItem(this.elements[0]);
    }

    second() {
        return this.createCoupleItem(this.elements[1]);
    }

    createCoupleItem(element) {
        if (element.constructor && element.constructor === Audio) {
            return new CoupleElement('audio', element)
        } else if (element.constructor && element.constructor === Image) {
            return new CoupleElement('image', element)
        } else {
            return new CoupleElement('text', element)
        }
    }

    beforeSave() {
        this.images = this.images.reduce((cur, image) => {
            if (image.id) {
                cur.push(image.id);
            }
            return cur;
        }, []);

        this.audios = this.audios.reduce((cur, audio) => {
            if (audio.id) {
                cur.push(audio.id);
            }
            return cur;
        }, []);

        return this;
    }

    validate() {
        let errors = [];

        if (this.words.length + this.images.length + this.audios.length !== 2) {
            errors.push('Введите по 2 элемента для каждой пары');
        }
        return errors;
    }
}

class CoupleElement {
    constructor(type, payload) {
        this.type = type;
        this.payload = payload;
    }

    isAudio() {
        return this.type === 'audio';
    }

    isImage() {
        return this.type === 'image';
    }

    isText() {
        return this.type === 'text';
    }
}


export default FindCoupleParams;