import Audio from '../File/Audio';

class SnowballExerciseParams {
    constructor({elements, exercise_text, audio_model} = {}) {
        this.elements = elements || [];
        this.exercise_text = exercise_text;
        this.audio = new Audio(audio_model || {});

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        this.audio = this.audio.id;
   }

    validate() {
        let errors = [];

        if (!this.elements.length) {
            errors.push('Не заданы варианты ответов');
        }

        if(!this.elements.filter(el => el.answer).length) {
            errors.push('Не выбран правильный вариант ответа')
        }

        return errors;
    }
}

export default SnowballExerciseParams;