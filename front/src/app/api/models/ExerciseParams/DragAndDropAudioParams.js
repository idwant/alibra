import Audio from './../File/Audio';

class DragAndDropAudioParams {
    constructor({questions = [{}]}) {
        this.questions = questions.map((el) => new Question(el));

        this.attempts = 0;
        this.hintHasShown = false;
        this.maxAttempts = 3;
    }

    addQuestion() {
        console.log('add');
        this.questions.push(new Question({}));
    }

    deleteQuestion(param) {
        let paramIndex = this.questions.indexOf(param);
        if (paramIndex != -1) {
            this.questions.splice(paramIndex, 1);
        }
    }

    beforeSave() {
        this.questions = this.questions.map((el) => el.beforeSave());
    }

    validate() {
        let errors = [];
        this.questions.map(el => errors.push(...el.validate()));

        return errors;
    }
}

class Question {
    constructor({audio_model, possible_answers = [], hint}) {
        this.audio = new Audio(audio_model || {});
        this.possible_answers = possible_answers;
        this.hint = hint;
    }

    beforeSave() {
        this.audio = this.audio.id;
        return this;
    }

    onAudioLoaded(response) {
        this.audio = new Audio(response || {});
    }

    validate() {
        let errors = [];

        if (!this.possible_answers.length) {
            errors.push('Не заданы варианты ответа');
        }

        return errors;
    }
}

export default DragAndDropAudioParams;