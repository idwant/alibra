import Audio from '../File/Audio';
import Image from './../File/Image';

class ListenReadClickParams {
    constructor({audio_model, hint, elements, question} = {}) {
        elements = elements || [];

        this.audio = new Audio(audio_model || {});
        this.hint = hint;
        this.question = question;
        this.elements = elements.map(element => {
            element.image = new Image(element.image_model || {});
            return element;
        });

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    getStepsCount() {
        return this.elements.filter(el => el.answer).length;
    }

    addImage(elementTag) {
        elementTag.image = new Image();
        return true;
    }

    beforeSave() {
        this.audio = this.audio.id;
        this.elements.map(element => element.image = element.image.id);
    }

    validate() {
        let errors = [];

        if(!this.question && !this.audio.id){
            errors.push('Необходимо задать либо текстовый вопрос, либо аудио раунда');
        }

        if (!this.elements.length) {
            errors.push('Не заданы варианты ответа');
        }

        if(!this.elements.filter(el => el.answer).length) {
            errors.push('Не выбран правильный вариант ответа')
        }

        return errors;
    }
}

export default ListenReadClickParams;