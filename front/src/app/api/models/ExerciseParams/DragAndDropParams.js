import Audio from '../File/Audio';
import Image from '../File/Image';

class DragAndDropParams {
    constructor({possible_answers = [], elements = [], audio_hint_model, image_model} = {}) {
        this.possible_answers = possible_answers;
        this.elements = elements || [];
        this.audio_hint = new Audio(audio_hint_model || {});
        this.image = new Image(image_model || {});

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    getStepsCount() {
        return this.elements.filter(el => el.skip).length;
    }

    beforeSave() {
        this.audio_hint = this.audio_hint.id;
        this.image = this.image.id;
    }

    toggleElementAsAnswer(elementTag) {
        if (elementTag.skip) {
            this.possible_answers.push({text: elementTag.text});
        } else {
            let index = null;
            let answer = this.possible_answers.find( (answer, tagIndex) => {index = tagIndex; return answer.text == elementTag.text});
            if (answer) {
                this.possible_answers.splice(index, 1);
            }
        }
    }

    checkAnswerRemoving(answerTag) {
        let index = null;
        let element = this.elements.find( (element, elementIndex) => {index = elementIndex; return (element.text == answerTag.text && element.skip)});
        if (element) {
            this.elements.splice(index, 1);
        }
    }

    validate() {
        let errors = [];
        if (!this.elements.length) {
            errors.push('Не заданы варианты ответа');
        }

        if(!this.elements.filter(el => el.skip).length) {
            errors.push('Не выбран правильный вариант ответа')
        }

        return errors;
    }
}

export default DragAndDropParams;