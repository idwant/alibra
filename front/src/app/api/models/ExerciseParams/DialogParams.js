import Audio from '../File/Audio';
import Image from '../File/Image';

class DialogParams {
    constructor({image_model, audio_model, hint, possible_answers, answers, audio_hint_model} = {}) {
        this.image = new Image(image_model || {});
        this.audio = new Audio(audio_model || {});
        this.answers = answers || [];
        this.audio_hint = new Audio(audio_hint_model || {});
        this.hint = hint;
        this.possible_answers = possible_answers || [];

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        this.audio = this.audio.id;
        this.image = this.image.id;
        this.audio_hint = this.audio_hint.id;
    }

    validate() {
        let errors = [];

        if (!this.possible_answers.length) {
            errors.push('Не заданы варианты ответов');
        }
        if (!this.answers.length) {
            errors.push('Не заданы ответы');
        }

        return errors;
    }
}

export default DialogParams;