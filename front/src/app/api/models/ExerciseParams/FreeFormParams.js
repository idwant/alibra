import Image from '../File/Image';
import Audio from '../File/Audio';

class FreeFormParams {
    constructor({image_models = [], question, possible_answers = [], hint, audio_hint_model}) {
        image_models = image_models || [];

        this.images = image_models.map(image_model => new Image(image_model || {}));
        this.possible_answers = possible_answers || [];
        this.audio_hint = new Audio(audio_hint_model || {});
        this.question = question;
        this.hint = hint;

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        let images = [];
        this.images.forEach(image => {
            if (image.id) {
                images.push(image.id);
            }
        });
        this.images = images;
        this.audio_hint = this.audio_hint.id;
    }

    /*addImage(){
        this.images.push(new Image());
    }

    removeImage(image) {
        let imageIndex = this.images.indexOf(image);
        if (imageIndex != -1) {
            this.images.splice(imageIndex, 1);
        }
    }*/

    validate() {
        let errors = [];

        if (!this.possible_answers.length) {
            errors.push('Не заданы проверочные слова');
        }

        return errors;
    }
}

export default FreeFormParams;