class PickOutParams {
    constructor({elements, hint} = {}) {
        this.elements = elements || [];
        this.hint = hint;

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    getStepsCount() {
        return this.elements.length;
    }

    validate() {
        let errors = [];
        if (!this.elements.length) {
            errors.push('Не заданы буквы, слова, фразы для упражнения');
        }

        if(!this.elements.filter(el => el.answer).length) {
            errors.push('Не выбран правильный вариант ответа')
        }

        return errors;
    }
}

export default PickOutParams;