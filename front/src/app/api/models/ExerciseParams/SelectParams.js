import Image from '../File/Image';

class SelectParams {
    constructor({elements, image_model} = {}) {
        this.elements = elements || [];
        this.image = new Image(image_model || {});

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    checkRemovingAnswer($tag, element) {
        let paramIndex = element.possible_answers.indexOf($tag);
        return !(paramIndex == 0 || element.possible_answers.length == 1);
    }

    getStepsCount() {
        return this.elements.filter(el => el.skip).length;
    }

    beforeSave() {
        this.image = this.image.id;
    }
    
    validate() {
        let errors = [];
        if (!this.elements.length) {
            errors.push('Не заданы варианты ответов');
        }

        if (!this.elements.filter(el => !!el.possible_answers).length) {
            errors.push('Не заданы варианты ответов');
        }

        if (this.elements.filter(el => el.skip && el.possible_answers && el.possible_answers.length < 2).length) {
            errors.push('Каждый список вариантов ответов должен содержать более 1 элемента');
        }

        return errors;
    }
}

export default SelectParams;