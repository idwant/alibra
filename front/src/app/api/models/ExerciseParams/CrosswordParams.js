import Image from './../File/Image';

class CrosswordParams {
    constructor({items = [], grid = [], legend} = {}) {
        this.items = items.map(el => new CrosswordItem(el));
        this.grid = grid;
        this.legend = legend;

        if (!this.items.length) {
            this.addItem();
        }

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    getStepsCount() {
        return this.items.length;
    }

    addItem(item = {}) {
        this.items.push(new CrosswordItem(item));
    }

    removeItem(item) {
        this.items.splice(this.items.indexOf(item), 1);
    }

    validate() {
        let errors = [];

        let isWordWithoutAnswerOrQuestion = (word) => {
            if (!word.answer) {
                return true;
            }
        };

        if (this.items.some(isWordWithoutAnswerOrQuestion)) {
            errors.push('Добавьте вопросы и ответы');
        }

        return errors;
    }

    beforeSave() {
        this.items = this.items.map(el => el.beforeSave());
    }
}

class CrosswordItem {
    constructor({question = '', answer = '', image_model}) {
        this.question = question;
        this.answer = answer;
        this.image = new Image(image_model || {});
    }

    onImageLoaded(response) {
        this.image = new Image(response || {});
    }

    beforeSave() {
        this.image = this.image.id;
        return this;
    }
}

export default CrosswordParams;