import Audio from '../File/Audio';
import Image from '../File/Image';

class ListenSayParams {
    constructor({answer, audio_hint_model, image_model, audio_model}) {
        this.answer = answer;
        this.audio_hint = new Audio(audio_hint_model || {});
        this.image = new Image(image_model || {});
        this.audio = new Audio(audio_model || {});

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        this.audio_hint = this.audio_hint.id;
        this.image = this.image.id;
        this.audio = this.audio.id;
    }

    validate() {
        let errors = [];
        if (!this.answer) {
            errors.push('Не задан ответ');
        }

        return errors;
    }
}

export default ListenSayParams;