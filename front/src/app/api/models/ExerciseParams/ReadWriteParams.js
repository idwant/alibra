import Image from '../File/Image';

class ReadWriteParams {
    constructor({elements = [], answer_bank, hint, image_model} = {}) {
        this.elements = elements;
        this.answer_bank = answer_bank;
        this.hint = hint;
        this.image = new Image(image_model || {});

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    getStepsCount() {
        return this.elements.filter(el => el.skip).length;
    }

    beforeSave() {
        this.image = this.image.id;
    }

    validate() {
        let errors = [];
        if (!this.elements.length) {
            errors.push('Не заданы варианты ответов');
        }

        return errors;
    }
}

export default ReadWriteParams;