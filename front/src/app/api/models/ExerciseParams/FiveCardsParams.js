import Image from './../File/Image';

class FiveCardsParams {
    constructor({cards = []}) {
        this.cards = cards.map((el) => new Card(el));

        if(!this.cards.length) {
            this.cards.push(...Array.from(new Array(5), () => new Card({})));
        }

        this.attempts = 0;
        this.hintHasShown = false;
        this.maxAttempts = 3;
    }

    hasOpenCard() {
        return !!this.cards.filter((c) => c.open).length;
    }

    beforeSave() {
        this.cards = this.cards.map((el) => el.beforeSave());
    }

    validate() {
        let errors = [];
        this.cards.map(el => errors.push(...el.validate()));

        return errors;
    }
}

class Card {
    constructor({image_model, answer, possible_answers = [], question, hint}) {
        this.image = new Image(image_model || {});
        this.answer = answer;
        this.question = question;
        this.possible_answers = possible_answers;
        this.hint = hint;
        this.open = null;
    }

    beforeSave() {
        this.image = this.image.id;
        return this;
    }

    onImageLoaded(response) {
        this.image = new Image(response || {});
    }

    validate() {
        let errors = [];

        if (!this.answer) {
            errors.push('Не задан ответ');
        }

        if (!this.possible_answers.length) {
            errors.push('Не заданы варианты ответа');
        }

        return errors;
    }
}

export default FiveCardsParams;