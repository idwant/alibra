import Audio from '../File/Audio';
import Image from '../File/Image';

class ListenWriteParams {
    constructor({question, answer, image_model, audio_model}) {
        this.question = question;
        this.answer = answer;
        this.image = new Image(image_model || {});
        this.audio = new Audio(audio_model || {});

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        this.audio = this.audio.id;
        this.image = this.image.id;
    }

    validate() {
        let errors = [];
        if (!this.answer) {
            errors.push('Не задан ответ');
        }

        return errors;
    }
}

export default ListenWriteParams;