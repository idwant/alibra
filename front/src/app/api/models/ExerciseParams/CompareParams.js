import Audio from '../File/Audio';
import Image from '../File/Image';

class CompareParams {
    constructor({image_model, answer, transcription, audio_hint_model, audio_answer_model, hint} = {}) {
        this.answer = answer;
        this.image = new Image(image_model || {});
        this.transcription = transcription || [];
        this.audio_hint = new Audio(audio_hint_model || {});
        this.audio_answer = new Audio(audio_answer_model || {});
        this.hint = hint;

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        this.audio_hint = this.audio_hint.id;
        this.image = this.image.id;
        this.audio_answer = this.audio_answer.id;
    }

    getTransription() {
        let transcription = '';
        this.transcription.forEach((el) => transcription += el.text);
        return transcription;
    }

    validate() {
        let errors = [];

        if (!this.answer) {
            errors.push('Не задан ответ');
        }

        if (!this.transcription.length) {
            errors.push('Не заданы знаки транскрипции');
        }

        return errors;
    }
}

export default CompareParams;