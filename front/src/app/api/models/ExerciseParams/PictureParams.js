import Audio from './../File/Audio';
import Image from './../File/Image';

class PictureParams {
    constructor({image_model, question, answer, audio_hint_model}) {
        this.image = new Image(image_model || {});
        this.answer = answer;
        this.audio_hint = new Audio(audio_hint_model || {});
        this.question = question;

        this.attempts = 0;
        this.maxAttempts = 3;
    }

    beforeSave() {
        this.audio_hint = this.audio_hint.id;
        this.image = this.image.id;

        return this;
    }

    validate() {
        let errors = [];

        if (!this.answer) {
            errors.push('Не задан ответ');
        }

        return errors;
    }
}

export default PictureParams;