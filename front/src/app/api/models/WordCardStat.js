class WordCardStat {
    constructor({total, learned, disabled, repeat, complete} = {}) {
        this.total = total;
        this.learned = learned;
        this.disabled = disabled;
        this.repeat = repeat;
        this.complete = complete;
    }

    get isLearnComplete() {
        return this.total <= this.learned;
    }
}

export default WordCardStat;