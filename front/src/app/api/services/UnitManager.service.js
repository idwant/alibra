import Unit from './../models/Unit';

class UnitManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({parent_id, query, limit, offset} = {}) {
        return this.api.unit.query({course_id: parent_id, query, limit, offset}).then(([units, headers]) => [units, headers['x-total-count']]);
    }

    remove(id) {
        return this.api.unit.remove({id});
    }

    addRelation(unit_id, course_id) {
        return this.api.unit.addRelation({unit_id, course_id});
    }

    removeRelation(unit_id, course_id) {
        return this.api.unit.removeRelation({unit_id, course_id});
    }

    sort(unit_id, course_id, position) {
        return this.api.unit.sort({unit_id, course_id, position});
    }

    getById(id) {
        return this.api.unit.get({id: id});
    }

    save(unit) {
        return (unit.id) ? this.api.unit.patch(unit) : this.api.unit.save(unit);
    }
}

export default UnitManager;