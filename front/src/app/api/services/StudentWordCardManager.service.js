class StudentWordCardManager {
    constructor(api) {
        'ngInject';
        this.api = api;
    }

    getCategories() {
        return this.api
            .studentWordCardCategory
            .query();
    }

    getCategoryById(id) {
        return this.api
            .studentWordCardCategory
            .get({id});
    }

    getGroups({category_id, group_id}) {
        return this.api
            .studentWordCardGroup
            .query({category_id, group_id});
    }

    getGroupById(id) {
        return this.api
            .studentWordCardGroup
            .get({id});
    }

    getLearnWordCards({group_id = null, lesson_id = null}) {
        return this.api
            .studentWordCard
            .getLearn({group_id, lesson_id});
    }

    getRepeatWordCards(group_id) {
        return this.api
            .studentUserWordCard
            .getRepeat({group_id});
    }

    getReviewWordCards(group_id) {
        return this.api
            .studentUserWordCard
            .getReview({group_id});
    }

    learn(card) {
        return this.api
            .studentWordCard
            .learn({id: card.id});
    }

    postponeLearn(card) {
        return this.api
            .studentWordCard
            .postponeLearn({id: card.id});
    }

    repeat(card) {
        return this.api
            .studentUserWordCard
            .repeat({id: card.id});
    }

    disable(card) {
        return this.api
            .studentWordCard
            .disable({id: card.id});
    }

    stat({course_id = null, unit_id = null, lesson_id = null}) {
        return this.api
            .studentWordCardStat
            .query({course_id, unit_id, lesson_id});
    }
}

export default StudentWordCardManager;