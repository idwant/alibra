class GrammarManager {
    constructor(api) {
        'ngInject';
        this.api = api;
    }

    getById(id){
        return this.api.grammar.get({id});
    }
    
    query({query, theme_id, limit, offset} = {}) {
        return this.api.grammar.query({query, theme: theme_id, limit, offset})
            .then(([grammars, headers]) => [grammars, headers['x-total-count']]);
    }

    save(grammar) {
        return (grammar.id) ? this.api.grammar.patch(grammar) : this.api.grammar.save(grammar);
    }

    remove(id) {
        return this.api.grammar.remove({id});
    }

    queryGrammarSections({query, limit, offset} = {}) {
        return this.api.grammarSection.query({query, limit, offset})
            .then(([grammars, headers]) => [grammars, headers['x-total-count']]);
    }

    saveGrammarSection(grammar) {
        return (grammar.id) ? this.api.grammarSection.patch(grammar) : this.api.grammarSection.save(grammar);
    }

    removeGrammarSection(id) {
        return this.api.grammarSection.remove({id});
    }

    queryGrammarThemes({query, section_id, limit, offset} = {}) {
        return this.api.grammarTheme.query({query, section: section_id, limit, offset})
            .then(([grammars, headers]) => [grammars, headers['x-total-count']]);
    }

    saveGrammarTheme(grammar) {
        return (grammar.id) ? this.api.grammarTheme.patch(grammar) : this.api.grammarTheme.save(grammar);
    }

    removeGrammarTheme(id) {
        return this.api.grammarTheme.remove({id});
    }
}

export default GrammarManager;