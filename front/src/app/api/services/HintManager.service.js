class HintManager {
    constructor(api) {
        'ngInject';
        this.api = api;
        this.hints = [];
    }

    loadHints(){
        if (!this.hints.length){
            return this.api.hint.query().then((hints) => this.hints = hints);
        }
        return Promise.resolve(this.hints);
    }

    closeHint(hint) {
        hint.close();
        this.api.hint.patch({'name': hint.name});
    }

    getHintByName(hintName) {
        return this.loadHints().then((hints) => {
            let el = hints.filter((hint) => hint.name === hintName);
            return el.length ? el[0] : null;
        });
    }
}

export default HintManager;