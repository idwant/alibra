import Lesson from './../models/Lesson';

class LessonManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({parent_id, query, limit, offset} = {}) {
        return this.api.lesson.query({unit_id: parent_id, query, limit, offset}).then(([lessons, headers]) => [lessons, headers['x-total-count']]);
    }

    addRelation(lesson_id, unit_id) {
        return this.api.lesson.addRelation({unit_id, lesson_id});
    }

    remove(id) {
        return this.api.lesson.remove({id});
    }

    removeRelation(lesson_id, unit_id) {
        return this.api.lesson.removeRelation({unit_id, lesson_id});
    }

    sort(lesson_id, unit_id, position) {
        return this.api.lesson.sort({unit_id, lesson_id, position});
    }
    
    getById(id) {
        return this.api.lesson.get({id: id});
    }

    save(lesson) {
        return (lesson.id) ? this.api.lesson.patch(lesson) : this.api.lesson.save(lesson);
    }
}

export default LessonManager;