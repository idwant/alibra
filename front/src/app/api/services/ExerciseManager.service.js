class ExerciseManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    getById(exerciseId) {
        return this.api.exercise.get({id: exerciseId});
    }

    query({parent_id, query, offset, limit = 15} = {}) {
        return this.api.exercise.query({lesson_id: parent_id, query, offset, limit});
    }

    setRelation(exercise_id, lesson_id, lesson_part_id) {
        return this.api.exercise.addRelation({exercise_id, lesson_id, lesson_part_id});
    }

    removeRelation(exercise_id, lesson_part_id) {
        return this.api.exercise.removeRelation({exercise_id, lesson_part_id});
    }

    sort(exercise_id, lesson_id, position) {
        return this.api.exercise.sort({exercise_id, lesson_id, position});
    }

    save(exercise) {
        return (exercise.id) ? this.api.exercise.patch(exercise) : this.api.exercise.save(exercise);
    }

    remove(exercise) {
        return this.api.exercise.remove(exercise);
    }

    getStudentExercises({course_id = null, unit_id = null, lesson_id = null, lesson_part_id = null, exercise_type = null}) {
        return this.api.studentExercise.query({course_id, unit_id, lesson_id, lesson_part_id, exercise_type})
    }

    startStudentExercise({exercise_id, lesson_id}) {
        return this.api.studentExerciseAction.start({exercise_id, lesson_id});
    }

    finishStudentExercise({exercise_id, lesson_id, completed}) {
        // result - массив объектов [{success: true},{success:false}]
        return this.api.studentExerciseAction.finish({exercise_id, lesson_id, completed});
    }
}

export default ExerciseManager;