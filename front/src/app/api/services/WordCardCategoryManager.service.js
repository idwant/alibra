class WordCardCategoryManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({name, limit, offset} = {}) {
        return this.api
            .wordCardCategory
            .query({name, limit, offset})
            .then(([cards, headers]) => [cards, headers['x-total-count']]);
    }

    getById(id) {
        return this.api.wordCardCategory.get({id: id});
    }

    save(category) {
        return (category.id) ? this.api.wordCardCategory.patch(category) : this.api.wordCardCategory.save(category);
    }

    remove(category) {
        return this.api.wordCardCategory.remove(category);
    }
}

export default WordCardCategoryManager;