import Word from './../models/Word';

class WordManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({query = {}, offset, limit} = {}) {
        return this.api.word.query(Object.assign(query, {offset, limit}));
    }

    getById(id) {
        return this.api.word.get({id: id});
    }

    save(word) {
        return (word.id) ? this.api.word.patch(word) : this.api.word.save(word);
    }

    remove(word) {
        return this.api.word.remove(word);
    }

    saveTranslation(word, wordTranslation) {
        wordTranslation.word = word.id;
        return (wordTranslation.id) ? this.api.wordTranslation.patch(wordTranslation) : this.api.wordTranslation.save(wordTranslation);
    }

    removeTranslation(wordTranslation) {
        return this.api.wordTranslation.remove(wordTranslation);
    }
}

export default WordManager;