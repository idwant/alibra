class WordCardGroupManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({category, name, limit, offset} = {}) {
        return this.api
            .wordCardGroup
            .query({category_id: category.id, name, limit, offset})
            .then(([cards, headers]) => [cards, headers['x-total-count']]);
    }

    save(group) {
        return (group.id) ? this.api.wordCardGroup.patch(group) : this.api.wordCardGroup.save(group);
    }

    remove(group) {
        return this.api.wordCardGroup.remove(group);
    }

    loadStudentGroups() {
        return this.api.studentWordCardGroup.query({});
    }
}

export default WordCardGroupManager;