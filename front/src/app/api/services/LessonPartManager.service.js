import LessonPart from './../models/LessonPart';

class LessonPartManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({parent_id , query, limit, offset}) {
        return this.api.lessonPart.query({lesson_id: parent_id, query, limit, offset});
    }

    remove(id) {
        return this.api.lessonPart.remove({id});
    }

    getById(id) {
        return this.api.lessonPart.get({id: id});
    }

    save(lesson_id, lessonPart) {
        return (lessonPart.id) ?
            this.api.lessonPart.patch(Object.assign({lesson_id}, lessonPart)):
            this.api.lessonPart.save(Object.assign({lesson_id}, lessonPart));
    }

   getLessonPartTypes() {
        return [
            'Phonetics',
            'Warm up',
            'Vocabulary',
            'Grammar intro',
            'Grammar games',
            'Skill-work games',
            'Listening',
            'Test',
            'Home work'
        ]
    }
}

export default LessonPartManager;