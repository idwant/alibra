class StudentManager {
    constructor(api) {
        'ngInject';
        this.api = api;
    }

    getProgressStat({course_id = null, unit_id = null, limit, offset}) {
        return this.api
            .studentProgressStat
            .query({course_id, unit_id, limit, offset})
            .then(([items, headers]) => [items, headers['x-total-count']]);
    }

    exportProgressStat({course_id = null, unit_id = null, type}) {
        console.log(3);
        return this.api
            .studentProgressStatExport
            .export({course_id, unit_id, type});
    }

    createFeedback(feedback) {
        return this.api
            .studentFeedback
            .create(feedback);
    }
}

export default StudentManager;