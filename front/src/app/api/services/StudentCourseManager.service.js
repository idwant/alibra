class StudentCourseManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({query} = {}) {
        return this.api.studentCourse.query({query});
    }

    getById(id) {
        return this.api.studentCourse.get({id: id});
    }
}

export default StudentCourseManager;