class DictionaryManager {
    constructor(api) {
        'ngInject';
        this.api = api;
    }

    query({query, offset} = {}) {
        return this.api.dictionary.query({query, offset});
    }

    getById(id) {
        return this.api.dictionary.get({id: id});
    }

    getWords({dictionaryId, word = '', limit = 15, offset = 0}) {
        return this.api.dictionaryWord.query({id: dictionaryId, word, limit, offset}).then(([words, headers]) => [words, headers['x-total-count']]);
    }

    addWord(dictionary, word) {
        return this.api.dictionaryWord.addWord({id: dictionary.id, wordId: word.id});
    }

    removeWord(dictionary, word) {
        return this.api.dictionaryWord.removeWord({id: dictionary.id, wordId: word.id});
    }

    save(dictionary) {
        return (dictionary.id) ? this.api.dictionary.patch(dictionary) : this.api.dictionary.save(dictionary);
    }

    remove(dictionary) {
        return this.api.dictionary.remove(dictionary);
    }
}

export default DictionaryManager;