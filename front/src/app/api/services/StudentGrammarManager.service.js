class StudentGrammarManager {
    constructor(api) {
        'ngInject';
        this.api = api;
    }

    getById(id){
        return this.api.studentGrammar.get({id});
    }

    query({query, theme_id, limit, offset} = {}) {
        return this.api
            .studentGrammar
            .query({query, theme: theme_id, limit, offset});
    }

    queryGrammarSections({query, limit, offset} = {}) {
        return this.api
            .studentGrammarSection
            .query({query, limit, offset});
    }

    queryGrammarThemes({query, section_id, limit, offset} = {}) {
        return this.api
            .studentGrammarTheme
            .query({query, section: section_id, limit, offset});
    }
}

export default StudentGrammarManager;