class CourseManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({query, limit, offset} = {}) {
        return this.api.course.query({query, limit, offset}).then(([courses, headers]) => [courses, headers['x-total-count']]);
    }

    getById(id) {
        return this.api.course.get({id: id});
    }

    save(course) {
        return (course.id) ? this.api.course.patch(course) : this.api.course.save(course);
    }

    remove(id) {
        return this.api.course.remove({id});
    }
}

export default CourseManager;