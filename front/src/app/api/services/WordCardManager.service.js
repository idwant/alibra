class WordCardManager {
    constructor($log, api) {
        'ngInject';

        this.$log = $log;
        this.api = api;
    }

    query({word, limit, offset} = {}) {
        return this.api
            .wordCard
            .query({word, limit, offset})
            .then(([cards, headers]) => [cards, headers['x-total-count']]);
    }

    getById(id) {
        return this.api.wordCard.get({id: id});
    }

    save(card) {
        return (card.id) ? this.api.wordCard.patch(card) : this.api.wordCard.save(card);
    }
    
    remove(card) {
        return this.api.wordCard.remove({id: card.id});
    }

    addLesson(card, lesson) {
        return this.api.wordCardLesson.add({id: card.id, lesson_id: lesson.id});
    }

    removeLesson(card, lesson) {
        return this.api.wordCardLesson.remove({id: card.id, lesson_id: lesson.id});
    }
}

export default WordCardManager;