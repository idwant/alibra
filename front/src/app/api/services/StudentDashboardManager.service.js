class StudentDashboardManager {
    constructor(api) {
        'ngInject';
        this.api = api;
    }

    getCourses() {
        return this.api
            .studentDashboardCourse
            .query();
    }

    getUnits(course) {
        return this.api
            .studentDashboardUnit
            .query({course_id: course.id});
    }

    getLessons(course, unit) {
        return this.api
            .studentDashboardLesson
            .query({course_id: course.id, unit_id: unit.id});
    }

    getLessonParts(course, unit, lesson = null, exercise_type = null) {
        return this.api
            .studentDashboardLessonPart
            .query({course_id: course.id, unit_id: unit.id, lesson_id: lesson ? lesson.id : null, exercise_type: exercise_type});
    }
}

export default StudentDashboardManager;