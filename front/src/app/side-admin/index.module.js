import config from './index.config';
import runBlock from './index.run';

import shared from './../shared/shared.module';
import components from './components/components.module';
import api from './../api/api.module';

//endpoints
import baseTemplate from './endpoints/base-template/base-template.module';
import login from './endpoints/login/login.module';
import exercise from './endpoints/exercise/exercise.module';
import user from './endpoints/user/user.module';
import course from './endpoints/course/course.module';
import unit from './endpoints/unit/unit.module';
import lesson from './endpoints/lesson/lesson.module';
import lessonPart from './endpoints/lesson-part/lesson-part.module';
import dictionary from './endpoints/dictionary/dictionary.module';
import word from './endpoints/word/word.module'
import wordCard from './endpoints/word-card/word-card.module'
import editor from './endpoints/editor/editor.module';
import grammar from './endpoints/grammar/grammar.module';
import mailings from './endpoints/mailings/mailings.module';
import report from './endpoints/report/report.module';

/////////////////////////////

angular.module('front', [
        'parameters.config',
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ui.select',
        'angular-cache',
        'angularFileUpload',
        'ngFileUpload',
        'ngTagsInput',
        'ui.sortable',
        'summernote',
        baseTemplate,
        shared,
        components,
        api,
        login,
        exercise,
        user,
        course,
        unit,
        lesson,
        lessonPart,
        dictionary,
        word,
        wordCard,
        editor,
        grammar,
        mailings,
        report
    ])
    .constant('toastr', toastr)
    .constant('moment', moment)
    .constant('alert', swal)
    .constant('bowser', bowser)
    .constant('localStorage', localStorage)

    .config(config)
    .run(runBlock)
;