import {debugEnabled} from './../../parameters.config';
import {pagesConstants} from './../constants';

function config($logProvider, $translateProvider, ru, $locationProvider,
                toastr, $httpProvider, CacheFactoryProvider, $urlRouterProvider, tagsInputConfigProvider, uiSelectConfig, $provide) {
    'ngInject';
    $logProvider.debugEnabled(true);

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: true
    });

    // AHTUNG мега-супер патч релоада ))) В base-template через ng-if а то у них косяк и
    // PR больше не принимают тк есть новая ветка 1  которая работает с компонентами но мы на нее не перезжаем
    $provide.decorator('$state', ($delegate, $rootScope, $timeout) => {
        'ngInject';

        let reload = $delegate.reload;

        $delegate.reload = () => {
            reload();
            $rootScope.pageReload = true;
            $timeout(() => $rootScope.pageReload = false);
        };
        return $delegate;
    });

    // Страница "по умолчанию"
    $urlRouterProvider.otherwise(pagesConstants.MAIN);

    // Настройки кэшера
    angular.extend(CacheFactoryProvider.defaults, {
        storageMode: 'localStorage',
        storeOnResolve: true
    });

    // Регистрируем интерсепторы для запросов
    $httpProvider.interceptors.push('AuthInterceptor');

    // Настраиваем поддержку мультиязычности (пока только русский)
    $translateProvider.translations('ru', ru);

    // Отключил sanitize так как используем константы
    $translateProvider.useSanitizeValueStrategy(false);
    $translateProvider.preferredLanguage('ru');

    // Конфигурация всплывающих сообщений
    toastr.options.closeButton = true;
    toastr.options.positionClass = 'toast-bottom-right';

    // Отключает добавление тегов через , по-умолчанию для tag-input
    tagsInputConfigProvider.setDefaults('tagsInput', {
        addOnComma : false,
        minLength : 1
    });

    uiSelectConfig.dropdownPosition = 'down';
}

export default config;
