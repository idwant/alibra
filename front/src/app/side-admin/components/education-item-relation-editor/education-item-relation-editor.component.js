class EducationItemRelationEditorController {
    constructor($timeout, alert, AlertResponse) {
        "ngInject";

        this.alert = alert;
        this.AlertResponse = AlertResponse;
        this.items = [];
        this.childs = [];
        this.currentPage = 1;
        this.countPerPage = 15;
        this.totalCount = 0;
        this.searchQuery = null;

        this.sortableOptions = {
            'update': (e, ui) => $timeout(() => this.onSort(ui)),
            'ui-floating': false
        };
    }

    $onInit() {
        this.current.query({limit: this.countPerPage})
            .then(([items, total]) => {
                this.items = items;
                this.totalCount = total;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    search(query, page = 1) {
        this.searchQuery = query;
        this.current.query({
            query,
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        }).then(([items, total]) => {
            this.items = items;
            this.totalCount = total;
        }).catch((response) => this.AlertResponse.fail(response));
    }

    loadChild(parent_id) {
        this.child.query({parent_id})
            .then(([items, total]) => this.childs[parent_id] = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addChild(current, parent_id) {
        if(!current || !current.id){
            this.alert('Сделайте выбор');
            return false;
        }
        this.child.addRelation(current.id, parent_id)
            .then(() => this.childs[parent_id].push(current))
            .catch((response) => this.AlertResponse.fail(response));
    }

    removeChild(current, parent_id) {
        current.isDeleted = true;
        this.child.removeRelation(current.id, parent_id)
            .catch((response) => this.AlertResponse.fail(response));
    }

    searchChildsToAdd(query) {
        this.child.query({query})
            .then(([items]) => this.childsToAdd = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    searchParentsToAdd(query) {
        return this.parent.query({query})
            .then(([items]) => this.parentsToAdd = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addParent(parent, current) {
        if(!parent || !parent.id){
            this.alert('Сделайте выбор');
            return false;
        }
        return this.current.addRelation(current.id, parent.id);
    }

    removeParent(parent, current_id) {
        return this.current.removeRelation(current_id, parent.id);
    }

    save(item, scope = {}) {
        this.current.save(item)
            .then((saved) => {Object.assign(item, saved); scope.edit = false;})
            .catch((response) => this.AlertResponse.fail(response));
    }

    remove(item) {
        item.isDeleted = true;
        this.current.remove(item.id).catch((response) => {
            item.isDeleted = false;
            this.AlertResponse.fail(response)
        });
    }

    onSort(ui) {
        let item = ui.item[0].attributes;
        let index = item.getNamedItem('data-index').value;
        let id = item.getNamedItem('data-id').value;
        let parent = item.getNamedItem('data-parent-id').value;

        this.child.sort(id, parent, index)
            .catch((response) => this.AlertResponse.fail(response));
    }

    goToPage(page) {
        this.currentPage = page;
        this.search(this.searchQuery, this.currentPage);
    }
}

export default {
    template: require('./education-item-relation-editor.html'),
    controller: EducationItemRelationEditorController,
    bindings: {
        parent: '<',
        current: '<',
        child: '<'
    }
};