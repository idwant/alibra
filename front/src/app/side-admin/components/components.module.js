import dialogExercise from './exercise/dialog-exercise/dialog-exercise.component';
import exerciseDirective from './exercise/exercise.directive';
import educationItemRelationEditor from './education-item-relation-editor/education-item-relation-editor.component';
import educationLessonRelationEditor from './education-lesson-relation-editor/education-lesson-relation-editor.component';
import dragAndDropExercise from './exercise/drag-and-drop-exercise/drag-and-drop-exercise.component';
import dragAndDropAudioExercise from './exercise/drag-and-drop-audio-exercise/drag-and-drop-audio-exercise.component';
import readWriteExercise from './exercise/read-write-exercise/read-write-exercise.component';
import crosswordExercise from './exercise/crossword-exercise/crossword-exercise.component';
import grammarSectionEditor from './grammar-section-editor/grammar-section-editor.component';

let components =
        angular.module('components', [])
            .directive('exercise', exerciseDirective)
            .component('educationItemRelationEditor', educationItemRelationEditor)
            .component('dialogExercise', dialogExercise)
            .component('educationLessonRelationEditor', educationLessonRelationEditor)
            .component('dragAndDropExercise', dragAndDropExercise)
            .component('dragAndDropAudioExercise', dragAndDropAudioExercise)
            .component('readWriteExercise', readWriteExercise)
            .component('crosswordExercise', crosswordExercise)
            .component('grammarSectionEditor', grammarSectionEditor)
    ;

export default components = components.name;
