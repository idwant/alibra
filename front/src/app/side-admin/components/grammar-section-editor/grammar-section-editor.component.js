class GrammarSectionEditorController {
    constructor($timeout, AlertResponse, GrammarManager, $state) {
        "ngInject";

        this.AlertResponse = AlertResponse;
        this.GrammarManager = GrammarManager;
        this.sections = [];
        this.themes = [];
        this.currentPage = 1;
        this.countPerPage = 30;
        this.totalCount = 0;
        this.searchQuery = null;
        this.$state = $state;

        this.sortableOptions = {
            'update': (e, ui) => $timeout(() => this.onSort(ui)),
            'ui-floating': false
        };
    }

    $onInit() {
        this.GrammarManager.queryGrammarSections({limit: this.countPerPage})
            .then(([items, total]) => {
                this.sections = items;
                this.totalCount = total;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    search(query, page = 1) {
        this.searchQuery = query;
        this.GrammarManager.queryGrammarSections({
            query,
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        }).then(([items, total]) => {
            this.sections = items;
            this.totalCount = total;
            console.log(total);
        }).catch((response) => this.AlertResponse.fail(response));
    }

    save(item, scope) {
        this.GrammarManager.saveGrammarSection(item)
            .then((saved) => {
                Object.assign(item, saved);
                scope.edit = false;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    remove(item) {
        item.isDeleted = true;
        this.GrammarManager.removeGrammarSection(item.id).catch((response) => {
            item.isDeleted = false;
            this.AlertResponse.fail(response);
        });
    }

    loadThemes(section_id) {
        this.GrammarManager.queryGrammarThemes({section_id, limit: 1000})
            .then(([items, total]) => this.themes[section_id] = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addTheme(theme, section_id, scope) {
        theme.section = section_id;
        this.GrammarManager.saveGrammarTheme(theme)
            .then((saved) => {
                Object.assign(theme, saved);
                scope.editChild = false;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    removeTheme(current, parent_id) {
        current.isDeleted = true;
        this.GrammarManager.removeGrammarTheme(current.id, parent_id)
            .catch((response) => this.AlertResponse.fail(response));
    }

    goToPage(page) {
        this.currentPage = page;
        this.search(this.searchQuery, this.currentPage);
    }

    goToTheme(theme_id){
        console.log(theme_id);
        this.$state.go('base.grammar', {theme_id});
    }
}

export default {
    template: require('./grammar-section-editor.html'),
    controller: GrammarSectionEditorController,
    bindings: {
        parent: '<',
        current: '<',
        child: '<'
    }
};