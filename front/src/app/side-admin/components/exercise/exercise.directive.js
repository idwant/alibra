import compareTemplate from './compare-exercise/compare-exercise.html';
import findCoupleTemplate from './find-couple-exercise/find-couple-exercise.html';
import freeFormExercise from './free-form-exercise/free-form-exercise.html';
import listenSayTemplate from './listen-say-exercise/listen-say-exercise.html';
import listenWriteTemplate from './listen-write-exercise/listen-write-exercise.html';
import listenReadClickTemplate from './listen-read-click-exercise/listen-read-click-exercise.html';
import pictureTemplate from './picture-exercise/picture-exercise.html';
import selectTemplate from './select-exercise/select-exercise.html';
import pickOutTemplate from './pick-out-exercise/pick-out-exercise.html';
import fiveCardsTemplate from './five-cards-exercise/five-cards-exercise.html';
import snowballTemplate from './snowball-exercise/snowball-exercise.html';
import exerciseHeader from './exercise-header.html';
import exerciseFooter from './exercise-footer.html';

class ExerciseController {
    constructor(GrammarManager, DictionaryManager) {
        'ngInject';
        this.grammars = [];
        this.dictionaries = [];
        this.GrammarManager = GrammarManager;
        this.DictionaryManager = DictionaryManager;
    }

    save() {
        this.callback();
    }

    getGrammars(query) {
        this.GrammarManager.query({query}).then(([grammars, headers]) => this.grammars = grammars);
    }

    getDictionaries(query) {
        this.DictionaryManager.query({query}).then(([dictionaries, headers]) => this.dictionaries = dictionaries);
    }
}

export default ($compile) => {
    'ngInject';

    return {
        controller: ExerciseController,
        link: (scope, element, attrs, $ctrl) => {
            scope.$watch(() => $ctrl.exercise, (exercise) => {
                if (!exercise) return;
                let template = exerciseHeader;
                switch (exercise.template_type.toLowerCase()) {
                    case 'compare':
                        template += compareTemplate;
                        break;
                    case 'dialog-exercise':
                        template += '<dialog-exercise exercise="$ctrl.exercise"></dialog-exercise>';
                        break;
                    case 'drag-and-drop':
                        template += '<drag-and-drop-exercise exercise="$ctrl.exercise"></drag-and-drop-exercise>';
                        break;
                    case 'drag-and-drop-audio':
                        template += '<drag-and-drop-audio-exercise exercise="$ctrl.exercise"></drag-and-drop-audio-exercise>';
                        break;
                    case 'find-couple':
                        template += findCoupleTemplate;
                        break;
                    case 'free-form':
                        template += freeFormExercise;
                        break;
                    case 'listen-say':
                        template += listenSayTemplate;
                        break;
                    case 'listen-write':
                        template += listenWriteTemplate;
                        break;
                    case 'listen-read-click':
                        template += listenReadClickTemplate;
                        break;
                    case 'picture':
                        template += pictureTemplate;
                        break;
                    case 'read-write':
                        template += '<read-write-exercise exercise="$ctrl.exercise"></read-write-exercise>';
                        break;
                    case 'select':
                        template += selectTemplate;
                        break;
                    case 'pick-out':
                        template += pickOutTemplate;
                        break;
                    case 'five-cards':
                        template += fiveCardsTemplate;
                        break;
                    case 'snowball':
                        template += snowballTemplate;
                        break;
                    case 'crossword':
                        template += '<crossword-exercise exercise="$ctrl.exercise"></crossword-exercise>';
                        break;
                    default:
                        return '';
                }
                template += exerciseFooter;
                element.html($compile(template)(scope));
            });
        },
        controllerAs: '$ctrl',
        bindToController: true,
        restrict: 'E',
        scope: {
            callback: '&',
            exercise: '<'
        }
    }
};