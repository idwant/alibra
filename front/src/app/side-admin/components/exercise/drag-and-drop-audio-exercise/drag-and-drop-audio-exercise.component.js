class DragAndDropAudioExerciseController {
    constructor() {
        "ngInject";
    }
}

export default {
    template: require('./drag-and-drop-audio-exercise.html'),
    controller: DragAndDropAudioExerciseController,
    bindings: {
        exercise: '<'
    }
};