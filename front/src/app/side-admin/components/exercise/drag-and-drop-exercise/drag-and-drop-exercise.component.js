import Audio from '../../../../api/models/File/Audio';

class DragAndDropExerciseController {
    constructor() {
        "ngInject";
    }
}

export default {
    template: require('./drag-and-drop-exercise.html'),
    controller: DragAndDropExerciseController,
    bindings: {
        exercise: '<'
    }
};