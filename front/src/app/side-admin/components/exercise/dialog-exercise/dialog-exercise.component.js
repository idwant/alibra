class DialogExerciseController {
    constructor() {
        "ngInject";
    }
}

export default {
    template: require('./dialog-exercise.html'),
    controller: DialogExerciseController,
    bindings: {
        exercise: '<'
    }
};