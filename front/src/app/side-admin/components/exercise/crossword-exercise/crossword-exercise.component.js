class CrosswordExerciseController {
    constructor($timeout, alert, $scope) {
        "ngInject";

        this.$timeout = $timeout;
        this.$scope = $scope;
        this.alert = alert;
        this.is_new = !this.exercise.params[0].grid.length;
        this.show_crossword = !this.is_new;
    }

    onGenerated(promise) {
        promise
            .then(({grid, legend}) => {
                this.exercise.params[0].grid = grid;
                this.exercise.params[0].legend = legend;
                this.$scope.$digest();
            })
            .catch((words) => {
                this.alert("Oops...", "Нет связи для слова " + words.pop().word, "error")
            })
    }

    generate() {
        if (this.exercise.params[0].validate().length) {
            return this.alert('Oops...', this.exercise.params[0].validate().pop(), 'error');
        }

        this.is_new = true;
        this.show_crossword = false;

        this.$timeout(() => {
            this.show_crossword = true;
        });
    }
}

export default {
    template: require('./crossword-exercise.html'),
    controller: CrosswordExerciseController,
    bindings: {
        exercise: '<'
    }
};