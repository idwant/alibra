import Audio from '../../../../api/models/File/Audio';

class ReadWriteExerciseController {
    constructor() {
        "ngInject";
    }
}

export default {
    template: require('./read-write-exercise.html'),
    controller: ReadWriteExerciseController,
    bindings: {
        exercise: '<'
    }
};