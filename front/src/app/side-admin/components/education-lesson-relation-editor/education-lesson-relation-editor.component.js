class EducationLessonRelationEditorController {
    constructor($timeout, UnitManager, LessonManager, LessonPartManager, ExerciseManager, alert, AlertResponse) {
        "ngInject";

        this.UnitManager = UnitManager;
        this.LessonManager = LessonManager;
        this.LessonPartManager = LessonPartManager;
        this.ExerciseManager = ExerciseManager;
        this.alert = alert;
        this.AlertResponse = AlertResponse;

        this.lessons = [];
        this.lesson_parts = [];
        this.exercises_to_add = [];
        this.lesson_part_types = LessonPartManager.getLessonPartTypes();
        this.currentPage = 1;
        this.countPerPage = 15;
        this.totalCount = 0;
        this.searchQuery = null;

        this.sortableOptions = {
            'update': (e, ui) => $timeout(() => this.onSort(ui)),
            'ui-floating': false
        };
    }

    $onInit() {
        this.LessonManager.query({limit: this.countPerPage}).then(([lessons, total]) => {
            this.lessons = lessons;
            this.totalCount = total;
        }).catch((response) => this.AlertResponse.fail(response));
    }

    searchLessons(query, page = 1) {
        this.searchQuery = query;
        this.LessonManager.query({
            query,
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        }).then(([lessons, total]) => {
            this.lessons = lessons;
            this.totalCount = total;
        }).catch((response) => this.AlertResponse.fail(response));
    }

    saveLesson(lesson, scope = {}) {
        this.LessonManager.save(lesson)
            .then((saved) => {
                Object.assign(lesson, saved);
                scope.edit = false;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    removeLesson(lesson) {
        lesson.isDeleted = true;
        this.LessonManager.remove(lesson.id)
            .catch((response) => {
                lesson.isDeleted = false;
                this.AlertResponse.fail(response);
            });
    }

    removeLessonPart(lesson_part) {
        lesson_part.isDeleted = true;
        this.LessonPartManager.remove(lesson_part.id)
            .catch((response) => {
                lesson_part.isDeleted = false;
                this.AlertResponse.fail(response);
            });

    }

    loadLessonParts(parent_id) {
        this.LessonPartManager.query({parent_id})
            .then(([lesson_parts]) => {
                this.lesson_parts[parent_id] = lesson_parts;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    saveLessonPart(lesson_id, lessonPart, scope = {}) {
        this.LessonPartManager.save(lesson_id, lessonPart)
            .then((saved) => {
                Object.assign(lessonPart, saved);
                scope.edit_lesson_part = false;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    searchExerciseToAdd(query) {
        this.ExerciseManager.query({query})
            .then(([exercises]) => this.exercises_to_add = exercises)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addExerciseRelation(exercise, lesson, lesson_part) {
        if (!exercise || !exercise.id) {
            return this.alert('Сделайте выбор');
        }
        this.ExerciseManager.setRelation(exercise.id, lesson.id, lesson_part.id)
            .then(() => lesson_part.exercises.push(exercise))
            .catch((response) => this.AlertResponse.fail(response));
    }

    removeExerciseRelation(exercise, lesson_part) {
        exercise.is_deleted = true;
        this.ExerciseManager.removeRelation(exercise.id, lesson_part.id)
            .catch((response) => {
                exercise.is_deleted = false;
                this.AlertResponse.fail(response);
            });
    }

    searchUnitsToAdd(query) {
        return this.UnitManager.query({query})
            .then(([items]) => this.unitsToAdd = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addUnit(unit, current) {
        if (!unit || !unit.id) {
            this.alert('Сделайте выбор');
            return false;
        }
        return this.LessonManager.addRelation(current.id, unit.id);
    }

    removeUnit(unit, current_id) {
        return this.LessonManager.removeRelation(current_id, unit.id);
    }

    onSort(ui) {
        let item = ui.item[0].attributes;
        let index = item.getNamedItem('data-index').value;
        let id = item.getNamedItem('data-id').value;
        let parent = item.getNamedItem('data-parent-id').value;

        this.ExerciseManager
            .sort(id, parent, index)
            .catch((response) => this.AlertResponse.fail(response));
    }

    goToPage(page) {
        this.currentPage = page;
        this.searchLessons(this.searchQuery, this.currentPage);
    }
}

export default {
    template: require('./education-lesson-relation-editor.html'),
    controller: EducationLessonRelationEditorController
};