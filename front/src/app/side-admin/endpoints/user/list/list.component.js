import template from './list.html';

class ListController {
    constructor($log, AlertResponse, UserManager) {
        "ngInject";

        this.AlertResponse = AlertResponse;
        this.UserManager = UserManager;

        this.filterFio = '';
        this.filterRole = '';

        this.users = [];
        this.currentPage = 1;
        this.countPerPage = 10;
        this.totalCount = 0;
        this.searchQuery = null;
    }


    $onInit() {
        this._loadUsers(this.filterFio, this.filterRole, this.countPerPage, this.currentPage - 1);
    }

    goToPage(page) {
        this.currentPage = page;
        this._loadUsers(this.filterFio, this.filterRole, this.countPerPage, this.currentPage - 1);
    }

    searchUsers() {
        this.currentPage = 1;
        this._loadUsers(this.filterFio, this.filterRole, this.countPerPage, this.currentPage - 1);
    }

    _loadUsers(fio, role, limit, page) {
        this.UserManager
            .query({fio, role, limit, offset: page * limit})
            .then(([users, total]) => {
                this.users = users;
                this.totalCount = total;
            }).catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: template,
    controller: ListController
};