function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.users', {
            url: '/users',
            template: '<list-users class="user"></list-users>'
        });

    $stateProvider
        .state('base.userEditor', {
            url: '/user/:id',
            template: '<user-editor class="user"></user-editor>'
        });
}

export default routeConfig;