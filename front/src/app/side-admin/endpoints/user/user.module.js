import routeConfig from './user.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('user', [])
    .config(routeConfig)
    .component('userEditor', editorComponent)
    .component('listUsers', listComponent)
    ;

export default module = module.name;