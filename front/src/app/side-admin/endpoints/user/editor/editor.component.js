import template from './editor.html';
import User from 'api/models/User';

class EditorController {
    constructor($log, $state, $stateParams, toastr, AlertResponse, UserManager, CourseManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.toastr = toastr;
        this.AlertResponse = AlertResponse;
        this.savedResult = '';

        this.UserManager = UserManager;
        this.CourseManager = CourseManager;

        this.id = parseInt($stateParams.id, 10);
        this.user = new User({id: 0});

        this.coursesToAdd = [];
        this.selectedCourseToAdd = '';
    }

    $onInit() {
        if (this.id) {
            this.UserManager
                .getById(this.id)
                .then(user => {
                    this.user = user;
                });
        }
    }

    save() {
        this.savedResult = '';
        return this.UserManager
            .save(this.user)
            .then((user) => {
                this.user = user;
                this.toastr.success('Сохранено');
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadCourses(query) {
        this.CourseManager
            .query({query})
            .then(([items]) => this.coursesToAdd = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addCourse(course) {
        if (course) {
            this.UserManager
                .addCourse(this.user, course)
                .then(() => {
                    this.user.addCourse(course);
                    this.toastr.success('Курс добавлен');
                })
                .catch((response) => this.AlertResponse.fail(response));

            this.selectedCourseToAdd = null;
        }
    }

    removeCourse(course) {
        if (course) {
            this.UserManager
                .removeCourse(this.user, course)
                .then(() => {
                    this.user.removeCourse(course);
                    this.toastr.success('Курс удалён');
                })
                .catch((response) => this.AlertResponse.fail(response));
        }
    }

    reload() {
        return this.$state.reload();
    }
}

export default {
    template: template,
    controller: EditorController
};