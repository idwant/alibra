import BaseTemplateController from './base-template.controller';
import template from './base-template.html';

function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
    // Абстрктное состояние - отрисовывает шаблон приложения с меню
        .state('base', {
            abstract: true,
            template: template,
            controller: BaseTemplateController.name,
            controllerAs: 'base'
        });
}

export default routeConfig;