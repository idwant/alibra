class BaseTemplateController {
    constructor($log) {
        'ngInject';

        this.$log = $log;
    }
}

export default BaseTemplateController;