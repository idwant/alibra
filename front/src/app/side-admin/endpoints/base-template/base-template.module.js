import routeConfig from './base-template.route'
import BaseTemplateController from './base-template.controller';

let module = angular
    .module('baseTemplate', [])
    .config(routeConfig)
    .controller(BaseTemplateController.name, BaseTemplateController)
    ;

export default module = module.name;