import template from './list.html';

class ListController {
    constructor(ExerciseManager) {
        "ngInject";

        this.ExerciseManager = ExerciseManager;

        this.currentPage = 1;
        this.countPerPage = 100;
        this.items = [];
        this.totalCount = 0;
    }

    $onInit() {
        this.search();
    }

    search(query, page = this.currentPage) {
        this.ExerciseManager.query({
            query,
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        }).then(([items, total]) => {
            this.items = items;
            this.totalCount = total['x-total-count'];
            console.log(this.totalCount);
        });
    }

    deleteExercise(exercise) {
        exercise.is_deleted = true;
        this.totalCount--;
        this.ExerciseManager.remove({id: exercise.id})
            .catch((response) => {
                exercise.is_deleted = false;
                this.totalCount++;
            });
    }
}

export default {
    template: template,
    controller: ListController
};