function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.exercises', {
            url: '/exercises',
            template: '<list-exercises class="exercise"></list-exercises>'
        });

    $stateProvider
        .state('base.exerciseEditor', {
            url: '/exercise/:id',
            template: '<exercise-editor class="exercise"></exercise-editor>'
        });
}

export default routeConfig;