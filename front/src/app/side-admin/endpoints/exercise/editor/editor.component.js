import Exercise from './../../../../api/models/Exercise';
import template from './editor.html';

class EditorController {
    constructor($scope, $state, $stateParams, alert, $filter, ExerciseManager, ExerciseTemplateManager) {
        "ngInject";

        this.alert = alert;
        this._$scope = $scope;
        this.$state = $state;
        this._$filter = $filter;
        this._ExerciseManager = ExerciseManager;
        this._ExerciseTemplateManager = ExerciseTemplateManager;

        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.templates = [];
        this.selectedComponent = null;
        this.savedResult = null;
    }

    $onInit() {
        this._ExerciseTemplateManager.query()
            .then((templates) => this.templates = templates)
            .then((templates) => this.isCreateMode ? this.onCreateMode(templates) : this.onUpdateMode(templates));

    }

    onCreateMode() {
        this._$scope.$watch(
            () => this.selectedComponent,
            (selectedComponent) => {selectedComponent ? this.exercise = Exercise.createFromTemplate(selectedComponent) : null;}
        );
    }

    onUpdateMode(templates) {
        this._ExerciseManager.getById(this.id)
            .then((exercise) => this.exercise = exercise)
            .then((exercise) =>
            this.selectedComponent = templates.filter(template => template.type === exercise.template_type).pop()
        );
    }

    save() {
        let errors = this.exercise.validate();
        if (errors.length > 0) {
            this.alert({type: 'error', title: errors.shift()});
        } else {
            return this._ExerciseManager.save(this.exercise)
                .then(() => this.savedResult = 'SUCCESS')
                .catch(response => {
                    if(response.data && response.data.message) {
                        this.alert({type: 'error', title: response.data.message});
                    } else {
                        this.savedResult = 'FAIL'
                    }
                });
        }
    }

    reload() {
        return this.$state.reload();
    }
}

export default {
    template: template,
    controller: EditorController
};