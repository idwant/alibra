import routeConfig from './exercise.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('lessons', [])
    .config(routeConfig)
    .component('exerciseEditor', editorComponent)
    .component('listExercises', listComponent)
    ;

export default module = module.name;