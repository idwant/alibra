import WordCardGroup from 'api/models/WordCardGroup';

class CreateCategoryController {
    constructor(toastr, AlertResponse, WordCardCategoryManager, WordCardGroupManager) {
        "ngInject";

        this.toastr = toastr;
        this.AlertResponse = AlertResponse;
        this.WordCardGroupManager = WordCardGroupManager;
        this.WordCardCategoryManager = WordCardCategoryManager;

        this.group = new WordCardGroup({});
    }

    $onInit() {

    }

    create() {
        if (this.group.name.trim() !== '') {
            this.WordCardGroupManager
                .save(this.group)
                .then((group) => {
                    this.group = new WordCardGroup({category: group.category});
                    this.toastr.success('Группа добавлена');
                })
                .catch((response) => this.AlertResponse.fail(response));
        }
    }

    loadCategories(name) {
        this.WordCardCategoryManager
            .query({name})
            .then(([categories]) => this.categoriesToSelect = categories)
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./create-group.html'),
    controller: CreateCategoryController,
    bindings: {
        isVisible: '='
    }
}