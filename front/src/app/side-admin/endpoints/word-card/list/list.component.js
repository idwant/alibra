import template from './list.html';

import WordCardGroup from 'api/models/WordCardGroup';
import WordCardCategory from 'api/models/WordCardCategory';

class ListController {
    constructor(toastr, AlertResponse, WordCardManager, WordCardGroupManager) {
        "ngInject";

        this.toastr = toastr;
        this.AlertResponse = AlertResponse;
        this.WordCardManager = WordCardManager;
        this.WordCardGroupManager = WordCardGroupManager;
        this.filterWord = '';

        this.cards = [];
        this.currentPage = 1;
        this.countPerPage = 10;
        this.totalCount = 0;
        this.searchQuery = null;

        this.newCategory = new WordCardCategory();

    }

    $onInit() {
        this._loadWordCards(this.filterWord, this.countPerPage, this.currentPage - 1);
    }

    goToPage(page) {
        this.currentPage = page;
        this._loadWordCards(this.filterWord, this.countPerPage, this.currentPage - 1);
    }

    searchWordCards() {
        this.currentPage = 1;
        this._loadWordCards(this.filterWord, this.countPerPage, this.currentPage - 1);
    }

    removeWordCard(card) {
        this.WordCardManager
            .remove(card)
            .then(() => {
                this.cards.splice(this.cards.indexOf(card), 1);
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadWordCards(word, limit, page) {
        this.WordCardManager
            .query({word, limit, offset: page * limit})
            .then(([cards, total]) => {
                this.cards = cards;
                this.totalCount = total;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: template,
    controller: ListController
};
