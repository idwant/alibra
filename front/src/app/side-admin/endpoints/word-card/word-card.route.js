function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.wordCards', {
            url: '/word-cards',
            template: '<list-word-cards class="word-card form-wrap"></list-word-cardss>'
        });

    $stateProvider
        .state('base.wordCardEditor', {
            url: '/word-card/:id',
            template: '<word-card-editor class="word-card form-wrap"></word-card-editor>'
        });

    $stateProvider
        .state('base.categories', {
            url: '/word-cards/categories',
            template: '<list-category class="form-wrap"></list-category>'
        });

    $stateProvider
        .state('base.categoryEditor', {
            url: '/word-cards/category/:id',
            template: '<category-editor class="form-wrap"></category-editor>'
        });
}

export default routeConfig;