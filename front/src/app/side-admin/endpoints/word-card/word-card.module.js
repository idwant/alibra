import routeConfig from './word-card.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';
import listCategoryComponent from './category/list-category.component';
import categoryEditor from './category/category-editor.component';

import createCategoryComponent from './category/create-category.component';
import createGroupComponent from './list/create-group.component';

let module = angular
    .module('wordCard', [])
    .config(routeConfig)
    .component('wordCardEditor', editorComponent)
    .component('listWordCards', listComponent)
    .component('createCategory', createCategoryComponent)
    .component('createGroup', createGroupComponent)
    .component('listCategory', listCategoryComponent)
    .component('categoryEditor', categoryEditor)
    ;

export default module = module.name;