import template from './editor.html';
import WordCard from 'api/models/WordCard';
import Image from 'api/models/File/Image';
import Audio from 'api/models/File/Audio';

class EditorController {
    constructor($log, $scope, $state, $stateParams, toastr, AlertResponse,
                WordCardManager, WordCardCategoryManager, WordCardGroupManager,
                WordManager, LessonManager
    ) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;
        this.$state = $state;
        this.toastr = toastr;
        this.AlertResponse = AlertResponse;
        this.savedResult = '';

        this.WordCardManager = WordCardManager;
        this.WordCardCategoryManager = WordCardCategoryManager;
        this.WordCardGroupManager = WordCardGroupManager;
        this.WordManager = WordManager;
        this.LessonManager = LessonManager;

        this.id = parseInt($stateParams.id, 10);
        this.card = new WordCard({id: 0});
        this.wordsToSelect = [];

        this.lessonsToAdd = [];
        this.selectedLessonToAdd = '';
    }

    $onInit() {
        this.registerWatcher();

        if (this.id) {
            this.WordCardManager
                .getById(this.id)
                .then(card => this.card = card);
        }
    }

    save() {
        this.savedResult = '';
        return this.WordCardManager
            .save(this.card)
            .then((card) => {
                this.card = card;
                this.toastr.success('Сохранено');
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadWords(word) {
        this.WordManager
            .query({query: {word: word}})
            .then(([words]) => this.wordsToSelect = words)
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadCategories(name) {
        this.categoriesToSelect = [];
        this.WordCardCategoryManager
            .query({name})
            .then(([categories]) => this.categoriesToSelect = categories)
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadGroups(name) {
        if (this.card.category) {
            this.groupsToSelect = [];

            this.WordCardGroupManager
                .query({category: this.card.category, name})
                .then(([groups]) => this.groupsToSelect = groups)
                .catch((response) => this.AlertResponse.fail(response));
        }
    }

    reload() {
        return this.$state.reload();
    }

    imageLoaded(response) {
        this.card.image = new Image(response);
    }

    audioLoaded(response) {
        this.card.audio = new Audio(response);
    }

    sampleAudioLoaded(response) {
        this.card.sample_audio = new Audio(response);
    }

    loadLessons(query) {
        this.LessonManager
            .query({query})
            .then(([items]) => this.lessonsToAdd = items)
            .catch((response) => this.AlertResponse.fail(response));
    }

    addLesson(lesson) {
        if (lesson) {
            this.WordCardManager
                .addLesson(this.card, lesson)
                .then(() => {
                    this.card.addLesson(lesson);
                    this.toastr.success('Урок добавлен');
                })
                .catch((response) => this.AlertResponse.fail(response));

            this.selectedLessonToAdd = null;
        }
    }

    removeLesson(lesson) {
        if (lesson) {
            this.WordCardManager
                .removeLesson(this.card, lesson)
                .then(() => {
                    this.card.removeLesson(lesson);
                    this.toastr.success('Урок удалён');
                })
                .catch((response) => this.AlertResponse.fail(response));
        }
    }

    onChangeWord() {
        this.card.onChangeWord();
    }

    registerWatcher() {
        this.$scope.$watch(() => this.card.category, (newCategory, oldCategory) => {
            if (!newCategory || (oldCategory && oldCategory.id !== newCategory.id)) {
                this.card.group = null;
            }
            this.loadGroups();
        });
    }
}

export default {
    template: template,
    controller: EditorController
};