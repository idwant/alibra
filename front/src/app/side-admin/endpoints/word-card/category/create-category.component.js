import WordCardCategory from 'api/models/WordCardCategory';
import Image from 'api/models/File/Image';

class CreateCategoryController {
    constructor(toastr, $state, AlertResponse, WordCardCategoryManager) {
        "ngInject";

        this.toastr = toastr;
        this.$state = $state;
        this.AlertResponse = AlertResponse;
        this.WordCardCategoryManager = WordCardCategoryManager;

        this.category = new WordCardCategory({});
    }

    $onInit() {

    }

    imageLoaded(response, type) {
        if (type == 'small') {
            this.category.image = new Image(response);
        } else if (type == 'big') {
            this.category.image_big = new Image(response);
        }
    }

    create() {
        this.WordCardCategoryManager
            .save(this.category)
            .then((category) => {
                this.category = new WordCardCategory();
                this.toastr.success('Категория сохранена добавлена');
                this.$state.reload();
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./create-category.html'),
    controller: CreateCategoryController,
    bindings: {
        isVisible: '='
    }
}