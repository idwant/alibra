import WordCardCategory from 'api/models/WordCardCategory';
import WordCardGroup from 'api/models/WordCardGroup';
import Image from 'api/models/File/Image';

class CategoryEditorController {
    constructor(toastr, AlertResponse, $state, $stateParams, WordCardCategoryManager, WordCardGroupManager) {
        "ngInject";

        this.WordCardCategoryManager = WordCardCategoryManager;
        this.WordCardGroupManager = WordCardGroupManager;
        this.toastr = toastr;
        this.$state = $state;
        this.AlertResponse = AlertResponse;

        this.id = parseInt($stateParams.id, 10);
        this.category = new WordCardCategory({});
        this.group = new WordCardGroup({});
    }

    $onInit() {
        this._loadCategory(this.id);
    }

    update() {
        return this.WordCardCategoryManager
            .save(this.category)
            .then((category) => {
                this.category = category;
                this.toastr.success('Сохранено');
                this.$state.go('base.categories');
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    imageLoaded(response, type) {
        if (type == 'small') {
            this.category.image = new Image(response);
        } else if (type == 'big') {
            this.category.image_big = new Image(response);
        }
    }

    removeGroup(group) {
        this.WordCardGroupManager
            .remove(group)
            .then(() => {
                this.$state.reload();
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    addGroup() {
        this.WordCardGroupManager
            .save(this.group)
            .then((group) => {
                this.group = new WordCardGroup({name: '', category: this.category});
                this.category.groups.push(group);
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadCategory(id) {
        this.WordCardCategoryManager
            .getById(id)
            .then((category) => {
                this.category = category;
                this.group = new WordCardGroup({name: '', category: category});
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}
export default {
    template: require('./category-editor.html'),
    controller: CategoryEditorController
}