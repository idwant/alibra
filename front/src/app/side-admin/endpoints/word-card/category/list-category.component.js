class ListCategoryController {
    constructor(WordCardCategoryManager, $state) {
        "ngInject";

        this.WordCardCategoryManager = WordCardCategoryManager;
        this.$state = $state;

        this.filterCategory = '';
        this.currentPage = 1;
        this.countPerPage = 10;
        this.totalCount = 0;
        this.categories = [];
    }

    $onInit() {
        this._loadWordCardCategory(this.filterCategory, this.countPerPage, this.currentPage - 1)
    }

    searchCategory() {
        this.currentPage = 1;
        this._loadWordCardCategory(this.filterCategory, this.countPerPage, this.currentPage - 1);
    }

    goToPage(page) {
        this.currentPage = page;
        this._loadWordCardCategory(this.filterCategory, this.countPerPage, this.currentPage - 1);
    }

    remove(category) {
        this.WordCardCategoryManager
            .remove(category)
            .then(() => {
                this.$state.reload();
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadWordCardCategory(name, limit, page) {
        this.WordCardCategoryManager
            .query({name, limit, offset: page * limit})
            .then(([category, total]) => {
                this.categories = category;
                this.totalCount = total;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./list-category.html'),
    controller: ListCategoryController
};