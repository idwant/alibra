function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.lessons', {
            url: '/lessons',
            template: '<list-lessons class="lesson form-wrap"></list-lessons>'
        });

    $stateProvider
        .state('base.lessonEditor', {
            url: '/lesson/:id',
            template: '<lesson-editor class="lesson form-wrap"></lesson-editor>'
        });
}

export default routeConfig;