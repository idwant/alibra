import template from './editor.html';

class EditorController {
    constructor($stateParams, $state, LessonManager) {
        "ngInject";
        this._LessonManager = LessonManager;
        this._$state = $state;
        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.lesson = {};

        this._activate();
    }

    _activate() {

        // TODO: DELETE
        this.countries = [ // Taken from https://gist.github.com/unceus/6501985
            { name: 'Afghanistan', code: 'AF' },
            { name: 'Aland Islands sdfsdfsdfsdfs asdfsadfsafsfsdf', code: 'AX' },
            { name: 'Albania', code: 'AL' },
            { name: 'Algeria', code: 'DZ' },
            { name: 'American Samoa', code: 'AS' },
            { name: 'Andorra', code: 'AD' },
            { name: 'Angola', code: 'AO' }
        ]

        if (!this.isCreateMode) {
            this._LessonManager.getById(this.id)
                .then((lesson) => {
                    this.lesson = lesson;
                });
        }
    };

    save() {
        let promise = this._LessonManager.save(this.lesson);

        return promise
            .then(() => {
                return this.savedResult = 'SUCCESS'
            })
            .catch(() => {
                return this.savedResult = 'FAIL'
            });
    }

    reload() {
        return this._$state.reload();
    }
}

export default {
    template: template,
    controller: EditorController
};
