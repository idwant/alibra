import template from './list.html';

class ListController {
    constructor(LessonManager) {
        "ngInject";
        this._LessonManager = LessonManager;
        this.lessons = [];

        this._activate();
    }

    _activate() {
        this._loadLessons();
    }

    _loadLessons() {
        return this._LessonManager.query()
            .then((lessons) => {
                return this.lessons = lessons;
            });
    }
}

export default {
    template: template,
    controller: ListController
};