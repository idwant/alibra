import routeConfig from './lesson.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('lesson', [])
    .config(routeConfig)
    .component('lessonEditor', editorComponent)
    .component('listLessons', listComponent)
    ;

export default module = module.name;