function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.grammarSections', {
            url: '/grammar-sections',
            template: '<grammar-sections class="grammar"></grammar-sections>'
        });

    $stateProvider
        .state('base.grammars', {
            url: '/grammars',
            template: '<list-grammar class="grammar"></list-grammar>'
        });

    $stateProvider
        .state('base.grammar', {
            url: '/grammars/:theme_id',
            template: '<list-grammar class="grammar"></list-grammar>'
        });

    $stateProvider
        .state('base.grammarEditor', {
            url: '/grammar/:id',
            template: '<grammar-editor class="grammar"></grammar-editor>',
        });
}

export default routeConfig;