import template from './sections.html';

class SectionsController {
    constructor() {
        "ngInject";
        this.query = '';
    }
}

export default {
    template: template,
    controller: SectionsController
};