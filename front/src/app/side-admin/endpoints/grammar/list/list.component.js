import template from './list.html';

class ListController {
    constructor($state, AlertResponse, GrammarManager) {
        "ngInject";

        this.AlertResponse = AlertResponse;

        this.theme_id = $state.params.theme_id;
        this.GrammarManager = GrammarManager;

        this.grammars = [];
        this.currentPage = 1;
        this.countPerPage = 30;
        this.totalCount = 0;
    }

    $onInit() {
        this.search('', this.currentPage);
    }

    search(query, page = 1) {
        this.GrammarManager.query({
            theme_id: this.theme_id,
            query,
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        })
        .then(([grammars, total]) => {
            this.grammars = grammars;
            this.totalCount = total;
        })
        .catch((response) => this.AlertResponse.fail(response));
    }

    remove(item) {
        item.isDeleted = true;
        this.GrammarManager
            .remove(item.id)
            .catch((response) => {
                item.isDeleted = false;
                this.AlertResponse.fail(response);
            });
    }

    goToPage(page) {
        this.currentPage = page;
        this
            .search(this.searchQuery, this.currentPage)
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: template,
    controller: ListController
};