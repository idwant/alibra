import routeConfig from './grammar.route';
import listComponent from './list/list.component';
import sectionsComponent from './sections/sections.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('grammar', [])
    .config(routeConfig)
    .component('grammarEditor', editorComponent)
    .component('listGrammar', listComponent)
    .component('grammarSections', sectionsComponent)
    ;

export default module = module.name;