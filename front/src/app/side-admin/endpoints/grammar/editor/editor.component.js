import template from './editor.html';
import Video from './../../../../api/models/File/Video';
import Grammar from './../../../../api/models/Grammar';

class EditorController {
    constructor(GrammarManager, $state, alert) {
        "ngInject";

        this.GrammarManager = GrammarManager;
        this.$state = $state;

        this.id = $state.params.id;
        this.sections = [];
        this.themes = [];
        this._section = null;
        this.alert = alert;
        this.theme = null;
        this.grammar = new Grammar({});
    }

    $onInit() {
        this.GrammarManager.queryGrammarSections({limit: 1000000}).then(([sections]) => this.sections = sections);
        if (this.id && this.id !== 'create') {
            this.GrammarManager.getById(this.id).then(grammar => {
                this.section = grammar.section;
                this.theme = grammar.theme;
                this.grammar = grammar;
            });
        }
    }

    onVideoLoaded(video) {
        this.grammar.video = new Video(video);
    }

    reload() {
        this.$state.reload();
    }

    save() {
        Object.assign(this.grammar, {theme: this.theme});

        let errors = this.grammar.validate();
        if (errors.length > 0) {
            this.alert({type: 'error', title: errors.shift()});
        } else {
            this.GrammarManager
                .save(this.grammar)
                .then((grammar) => {
                    Object.assign(this.grammar, grammar);
                    this.savedResult = 'SUCCESS'
                })
                .catch(() => this.savedResult = 'FAIL');
        }
    }

    get section() {
        return this._section;
    }

    set section(section) {
        this.themes = [];
        this.theme = null;
        this._section = section;
        this.GrammarManager
            .queryGrammarThemes({limit: 1000000, section_id: section.id})
            .then(([themes]) => this.themes = themes);

    }

}

export default {
    template: template,
    controller: EditorController
};