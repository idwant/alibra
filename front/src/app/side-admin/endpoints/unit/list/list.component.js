import template from './list.html';

class ListController {
    constructor(UnitManager) {
        "ngInject";
        this._UnitManager = UnitManager;
        this.units = [];

        this._activate();
    }

    _activate() {
        this._loadUnits();
    }

    _loadUnits() {
        return this._UnitManager.query()
            .then((units) => {
                return this.units = units;
            });
    }
}

export default {
    template: template,
    controller: ListController
};