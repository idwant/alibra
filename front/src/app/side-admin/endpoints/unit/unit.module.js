import routeConfig from './unit.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('unit', [])
    .config(routeConfig)
    .component('unitEditor', editorComponent)
    .component('listUnits', listComponent)
    ;

export default module = module.name;