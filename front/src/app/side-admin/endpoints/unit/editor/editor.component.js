import template from './editor.html';

class EditorController {
    constructor($stateParams, $state, UnitManager) {
        "ngInject";
        this._UnitManager = UnitManager;
        this._$state = $state;

        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.unit = {};

        this._activate();
    }

    _activate() {
        if(!this.isCreateMode) {
            this._UnitManager.getById(this.id)
                .then((unit) => {
                    this.unit = unit;
                });
        }
    };

    save() {
        let promise = this._UnitManager.save(this.unit);

        return promise
            .then(() => {
                return this.savedResult = 'SUCCESS'
            })
            .catch(() => {
                return this.savedResult = 'FAIL'
            });
    }

    reload() {
        return this._$state.reload();
    }
}

export default {
    template: template,
    controller: EditorController
};