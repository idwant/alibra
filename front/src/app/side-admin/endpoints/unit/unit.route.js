function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.units', {
            url: '/units',
            template: '<list-units class="unit"></list-units>'
        });

    $stateProvider
        .state('base.unitEditor', {
            url: '/unit/:id',
            template: '<unit-editor class="unit"></unit-editor>'
        });
}

export default routeConfig;