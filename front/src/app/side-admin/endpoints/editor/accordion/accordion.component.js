class AccordionController {
    constructor(CourseManager, UnitManager, LessonManager, ExerciseManager) {
        "ngInject";
        this.CourseManager = CourseManager;
        this.UnitManager = UnitManager;
        this.LessonManager = LessonManager;
        this.ExerciseManager = ExerciseManager;
    }
}

export default {
    template: require('./accordion.html'),
    controller: AccordionController
};