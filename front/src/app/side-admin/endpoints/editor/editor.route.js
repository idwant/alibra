function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.editor', {
            url: '/editor',
            template: '<editor-accordion class="editor-accordion"></editor-accordion>'
        });
}

export default routeConfig;