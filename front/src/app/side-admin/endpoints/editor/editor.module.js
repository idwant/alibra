import routeConfig from './editor.route';
import accordionComponent from './accordion/accordion.component';

let module = angular
    .module('accordion', [])
    .config(routeConfig)
    .component('editorAccordion', accordionComponent)
    ;

export default module = module.name;