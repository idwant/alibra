import routeConfig from './course.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('course', [])
    .config(routeConfig)
    .component('courseEditor', editorComponent)
    .component('listCourses', listComponent)
    ;

export default module = module.name;