import template from './editor.html';
import Exercise from './../../../../api/models/Exercise';

class EditorController {
    constructor($stateParams, $state, CourseManager) {
        "ngInject";
        this._CourseManager = CourseManager;
        this._$state = $state;
        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.course = {};

        this._activate();
    }

    _activate() {
        if(!this.isCreateMode) {
            this._CourseManager.getById(this.id)
                .then((course) => {
                    this.course = course;
                });
        }
    };

    save() {
        let promise = this._CourseManager.save(this.course);

        return promise
            .then(() => {
                return this.savedResult = 'SUCCESS'
            })
            .catch(() => {
                return this.savedResult = 'FAIL'
            });
    }

    reload() {
        return this._$state.reload();
    }
}

export default {
    template: template,
    controller: EditorController
};