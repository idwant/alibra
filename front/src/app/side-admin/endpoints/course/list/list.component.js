import template from './list.html';

class ListController {
    constructor(CourseManager) {
        "ngInject";
        this._CourseManager = CourseManager;
        this.courses = [];

        this._activate();
    }

    _activate() {
        this._CourseManager.query()
            .then((courses) => {
                return this.courses = courses;
            });
    }
}

export default {
    template: template,
    controller: ListController
};