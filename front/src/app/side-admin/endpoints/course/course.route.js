function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.courses', {
            url: '/courses',
            template: '<list-courses class="course form-wrap"></list-courses>'
        });

    $stateProvider
        .state('base.courseEditor', {
            url: '/course/:id',
            template: '<course-editor class="course form-wrap"></course-editor>'
        });
}

export default routeConfig;