function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.mailings', {
            url: '/mailings',
            template: '<list-mailings class="mailings"></list-mailings>'
        });

    $stateProvider
        .state('base.mailingsEditor', {
            url: '/mailings/:id',
            template: '<mailings-editor class="mailings"></mailings-editor>'
        });
}

export default routeConfig;