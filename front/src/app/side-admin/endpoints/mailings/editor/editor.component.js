import template from './editor.html';

class EditorController {
    constructor($log, $state, ExerciseManager, ExerciseTemplateManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.ExerciseManager = ExerciseManager;
        this.ExerciseTemplateManager = ExerciseTemplateManager;
        this.selectedComponent = null;
        this.savedResult = null;

        this.name = null;
        this.taskRu = null;
        this.taskEn = null;
        this.audio = null;
    }

    audioLoaded(response = []) {
        let firstEl = response.shift() || {};
        this.audio = firstEl.id || null;
    }

    save(exercise) {
        return this.ExerciseManager.save(exercise)
            .then(() => {
                return this.savedResult = 'SUCCESS'
            })
            .catch(() => {
                return this.savedResult = 'FAIL'
            });
    }

    reload() {
        return this.$state.reload();
    }

}

export default {
    template: template,
    controller: EditorController
};