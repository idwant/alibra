import routeConfig from './mailings.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('mailings', [])
    .config(routeConfig)
    .component('mailingsEditor', editorComponent)
    .component('listMailings', listComponent)
    ;

export default module = module.name;