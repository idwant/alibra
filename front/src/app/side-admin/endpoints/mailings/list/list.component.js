import template from './list.html';

class ListController {
    constructor($log, UserManager) {
        "ngInject";
        this.query = '';
    }
}

export default {
    template: template,
    controller: ListController
};