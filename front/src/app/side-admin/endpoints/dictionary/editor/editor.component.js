import template from './editor.html';
import Dictionary from '../../../../api/models/Dictionary';

class EditorController {
    constructor($stateParams, $state, DictionaryManager, WordManager, alert, toastr) {
        "ngInject";
        this.DictionaryManager = DictionaryManager;
        this.WordManager = WordManager;
        this.$state = $state;
        this.alert = alert;
        this.toastr = toastr;

        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.dictionary = new Dictionary();
        this.words = [];
        this.wordsToAdd = [];
        this.currentPage = 1;
        this.countPerPage = 30;
        this.totalCount = 0;
    }

    $onInit() {
        if(!this.isCreateMode) {
            this.DictionaryManager.getById(this.id).then(dictionary => this.dictionary = dictionary);
            this.getWords();
        }
    }


    getWords(filterWordQuery) {
        this.DictionaryManager.getWords({
            dictionaryId: this.id,
            word: filterWordQuery,
            limit: this.countPerPage,
            offset: (this.currentPage - 1) * this.countPerPage
        }).then(([words, totalCount]) => {
            this.words = words;
            this.totalCount = totalCount;
        });
    }

    addWord(word) {
        if (!word) {
            return;
        }
        this.DictionaryManager.addWord(this.dictionary, word)
            .then(() => {this.words.push(word)})
            .catch((response) => {
                let message = 'Неопознанная ошибка';
                if (response.data && response.data.message) {
                    message = response.data.message;
                }
                this.alert({type: 'error', title: message});
            })
        ;
    }

    removeWord(word) {
        word.is_deleted = true;
        this.totalCount--;
        this.DictionaryManager
            .removeWord(this.dictionary, word)
            .then(() => {
                let wordIndex = this.words.indexOf(word);
                if (wordIndex != -1) {
                    this.words.splice(wordIndex, 1);
                }
                if (!this.words.length && this.currentPage > 1) {
                    this.currentPage--;
                    this.getWords();
                }
            })
            .catch(() => word.is_deleted = false);
    }

    searchWordsToAdd(query) {
        this.WordManager.query({query: {word:query}}).then(([words]) => {
           this.wordsToAdd = words;
        });
    }

    save() {
        if (!this.dictionary.name) {
            this.alert({type: 'error', title: 'Введите название словаря'});
            return;
        }
        this.DictionaryManager.save(this.dictionary)
            .then((dictionary) => {
                this.dictionary = dictionary;
                this.toastr.success('Словарь сохранен');
            })
            .catch((response) => {
                let message = 'Неопознанная ошибка';
                if (response.data && response.data.message) {
                    message = response.data.message;
                }
                this.alert({type: 'error', title: message});
            });
    }
}

export default {
    template: template,
    controller: EditorController
};