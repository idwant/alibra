import template from './list.html';

class ListController {

    constructor(DictionaryManager) {
        "ngInject";

        this.DictionaryManager = DictionaryManager;
        this.currentPage = 1;
        this.countPerPage = 25;
        this.dictionaries = [];
        this.totalCount = 0;
    }

    $onInit() {
        this.search();
    }

    search(query, page = this.currentPage) {
        this.DictionaryManager.query({
            query,
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        }).then(([items, total]) => {
            this.dictionaries = items;
            this.totalCount = total['x-total-count'];
        });
    }

    deleteDictionary(dictionary) {
        dictionary.is_deleted = true;
        this.totalCount--;
        this.DictionaryManager.remove({id: dictionary.id})
            .catch((response) => {
                dictionary.is_deleted = false;
                this.totalCount++;
            });
    }
}

export default {
    template: template,
    controller: ListController
};