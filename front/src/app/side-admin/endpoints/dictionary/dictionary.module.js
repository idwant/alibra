import routeConfig from './dictionary.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('dictionary', [])
    .config(routeConfig)
    .component('dictionaryEditor', editorComponent)
    .component('listDictionaries', listComponent)
    ;

export default module = module.name;