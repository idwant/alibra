function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.dictionaries', {
            url: '/dictionaries',
            template: '<list-dictionaries class="dictionary form-wrap"></list-dictionaries>'
        });

    $stateProvider
        .state('base.dictionaryEditor', {
            url: '/dictionaries/:id',
            template: '<dictionary-editor class="dictionary form-wrap"></dictionary-editor>'
        });
}

export default routeConfig;