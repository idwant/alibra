function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.report', {
            url: '/report',
            template: '<list-report class="report"></list-report>'
        });

    $stateProvider
        .state('base.reportEditor', {
            url: '/report/:id',
            template: '<report-editor class="report"></report-editor>'
        });
}

export default routeConfig;