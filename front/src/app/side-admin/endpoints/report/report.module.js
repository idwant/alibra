import routeConfig from './report.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('report', [])
    .config(routeConfig)
    .component('reportEditor', editorComponent)
    .component('listReport', listComponent)
    ;

export default module = module.name;