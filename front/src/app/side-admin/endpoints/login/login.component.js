import template from './login.html';

class LoginController {
    constructor(UserManager, toastr, $location) {
        'ngInject';

        this.UserManager = UserManager;
        this.toastr = toastr;
        this.$location = $location;
    }

    $onInit() {
        // Выходим если сюда попал авторизированный пользователь
        this.UserManager.current().then(() => this.UserManager.logout(true)).catch(() => '');
    }

    auth(email, password) {
        this.UserManager.login(email, password)
            .then((msg) => {
                this.toastr.success(msg);
                return this.UserManager.current();
            })
            .then(() => this.$location.url('/editor'))
            .catch(({data}) => {
                this.toastr.error(data.message)
            });
    }

}

export default {
    template: template,
    controller: LoginController
};