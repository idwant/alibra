import template from './list.html';

class ListController {
    constructor(LessonPartManager, $state) {
        "ngInject";
        this._LessonPartManager = LessonPartManager;
        this._$state = $state;
        this.lessonParts = [];

        this._activate();
    }

    _activate() {
        this._loadLessonParts();
    }

    _loadLessonParts() {
        return this._LessonPartManager.query()
            .then((lessonParts) => {
                return this.lessonParts = lessonParts;
            });
    }
}

export default {
    template: template,
    controller: ListController
};