import LessonPartModel from './../../../../api/models/LessonPart';
import template from './editor.html';

class EditorController {
    constructor($stateParams, LessonPartManager, $state) {
        "ngInject";
        this._LessonPartManager = LessonPartManager;
        this._$state = $state;
        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.availableTypes = LessonPartModel.getAvailableTypes();
        this.lessonPart = {};

        this._activate();
    }

    _activate() {
        if(!this.isCreateMode) {
            this._LessonPartManager.getById(this.id)
                .then((lessonPart) => {
                    this.lessonPart = lessonPart;
                });
        }
    };

    save() {
        let promise = this._LessonPartManager.save(this.lessonPart);

        return promise
            .then(() => {
                return this.savedResult = 'SUCCESS'
            })
            .catch(() => {
                return this.savedResult = 'FAIL'
            });
    }

    reload() {
        return this._$state.reload();
    }
}

export default {
    template: template,
    controller: EditorController
};