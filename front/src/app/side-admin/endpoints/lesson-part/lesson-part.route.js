function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.lessonParts', {
            url: '/lessonParts',
            template: '<list-lesson-parts class="lesson-part"></list-lesson-parts>'
        });

    $stateProvider
        .state('base.lessonPartEditor', {
            url: '/lessonPart/:id',
            template: '<lesson-part-editor class="lesson-part"></lesson-part-editor>'
        });
}

export default routeConfig;