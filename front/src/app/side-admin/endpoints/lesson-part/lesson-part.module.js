import routeConfig from './lesson-part.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('lessonPart', [])
    .config(routeConfig)
    .component('lessonPartEditor', editorComponent)
    .component('listLessonParts', listComponent)
    ;

export default module = module.name;