import template from './editor.html';
import Word from 'api/models/Word';
import WordTranslation from 'api/models/WordTranslation';

class EditorController {
    constructor($stateParams, $state, WordManager, alert, toastr) {
        "ngInject";
        this.alert = alert;
        this.toastr = toastr;
        this.WordManager = WordManager;
        this.$state = $state;
        this.id = $stateParams.id;
        this.isCreateMode = (this.id && this.id.toLowerCase() === 'create') ? true : false;
        this.word = new Word();
    }

    $onInit() {
        if(!this.isCreateMode) {
            this.WordManager.getById(this.id).then(word => this.word = word);
        }
    }

    addTranslation() {
        this.word.translations.push(new WordTranslation());
    }

    removeTranslation(translation) {
        let translationIndex = this.word.translations.indexOf(translation);
        if (translation.id) {
            this.WordManager.removeTranslation(translation)
                .then( () => this.word.translations.splice(translationIndex, 1))
                .catch(response => this.showError())
        } else {
            this.word.translations.splice(translationIndex, 1);
        }
    }

    saveTranslation(translation) {
        if (!translation.translation) {
            this.alert({type: 'error', title: 'Введите перевод'});
            return;
        }
        this.WordManager.saveTranslation(this.word, translation)
            .then(wordTranslation => {
                this.toastr.success('Перевод сохранен');
                translation.id = wordTranslation.id;
            })
            .catch(response => this.showError());
    }

    save() {
        if (!this.word.word) {
            this.alert({type: 'error', title: 'Введите название слова'});
            return;
        }

        if (!this.word.transcription) {
            this.alert({type: 'error', title: 'Введите транскрипцию'});
            return;
        }

        this.WordManager.save(this.word)
            .then((word) => {
                this.word.id = word.id;
                this.toastr.success('Слово сохранено');
            })
            .catch(response => this.showError());
    }

    showError(response) {
        let message = 'Неопознанная ошибка';
        if (response && response.data && response.data.message) {
            message = response.data.message;
        }
        this.alert({type: 'error', title: message});
    }
}

export default {
    template: template,
    controller: EditorController
};