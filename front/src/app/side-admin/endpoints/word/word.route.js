function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('base.words', {
            url: '/words',
            template: '<list-words class="word form-wrap"></list-words>'
        });

    $stateProvider
        .state('base.wordEditor', {
            url: '/words/:id',
            template: '<word-editor class="word form-wrap"></word-editor>'
        });
}

export default routeConfig;