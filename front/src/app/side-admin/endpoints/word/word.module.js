import routeConfig from './word.route';
import listComponent from './list/list.component';
import editorComponent from './editor/editor.component';

let module = angular
    .module('word', [])
    .config(routeConfig)
    .component('wordEditor', editorComponent)
    .component('listWords', listComponent)
    ;

export default module = module.name;