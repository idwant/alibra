import template from './list.html';

class ListController {
    constructor(WordManager, $stateParams) {
        "ngInject";
        this.WordManager = WordManager;
        this.dictionaryId = $stateParams.dictionaryId;

        this.words = [];
        this.currentPage = 1;
        this.countPerPage = 25;
        this.totalCount = 0;
    }

    $onInit() {
        this.search();
    }

    search(query, page = this.currentPage) {
        this.WordManager.query({
            query : {word: query},
            limit: this.countPerPage,
            offset: (page - 1) * this.countPerPage
        }).then(([items, total]) => {
            this.words = items;
            this.totalCount = total['x-total-count'];
        });
    }

    deleteWord(word) {
        word.is_deleted = true;
        this.totalCount--;
        this.WordManager.remove({id: word.id})
            .catch((response) => {
                word.is_deleted = false;
                this.totalCount++;
            });
    }

}

export default {
    template: template,
    controller: ListController
};