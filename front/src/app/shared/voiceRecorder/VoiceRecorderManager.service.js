class VoiceRecorderManager {
    constructor(MediaRecorder, SpeechAnalyze, $rootScope) {
        'ngInject';
        this.MediaRecorderService = new MediaRecorder();
        this.SpeechAnalyze = new SpeechAnalyze();
        this.$rootScope = $rootScope;
        this.inProcess = false;
        this.onRecorded = null;
        this.onTextChanged = null;
    }

    // начинает запись звука, принимает callback-и которые нужно выполнить при завершении распознования звука
    start(onRecorded = null, onTextChanged = null) {
        if (!this.inProcess) {
            if (onRecorded && onTextChanged) {
               this.onRecorded = onRecorded;
               this.onTextChanged = onTextChanged;
            }
            this.inProcess = true;
            this.MediaRecorderService.startRecording();
            return this.SpeechAnalyze.startAnalyze();
        }
    }

    stop() {
        if(this.inProcess) {
            this.inProcess = false;
            this.MediaRecorderService.stopRecording();
            return Promise
                .all([Promise.resolve(this.MediaRecorderService.getSrc()), this.SpeechAnalyze.stopAnalyze()])
                .then(([src, text]) => {
                    if (this.onRecorded && this.onTextChanged) {
                        this.$rootScope.$apply(() => {
                            this.onRecorded({src});
                            this.onTextChanged({text});
                        });
                    }
                    return [src, text];
                });
        }
    }
}

export default VoiceRecorderManager;