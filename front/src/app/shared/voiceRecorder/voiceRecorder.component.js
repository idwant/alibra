import {APP_EVENTS} from './../../constants';

class VoiceRecorderController {
    constructor(VoiceRecorderManager) {
        'ngInject';
        this.VoiceRecorderManager = VoiceRecorderManager;

    }

    toggle() {
        this.VoiceRecorderManager.inProcess ? this.stop() : this.start();
    }

    start() {
        this.VoiceRecorderManager.start(this.onRecorded, this.onTextChanged);
    }

    stop() {
        this.VoiceRecorderManager.stop().catch((e) => console.log(e));
    }
}

export default {
    controller: VoiceRecorderController,
    template: `
            <div class="voice-recorder" ng-class="{'animate': $ctrl.VoiceRecorderManager.inProcess}" ng-click="$ctrl.toggle()"></div>
            <hint name="record_audio" close="$ctrl.VoiceRecorderManager.inProcess"></hint>
    `,
    bindings: {
        onRecorded: '&', // {src}
        onTextChanged: '&', // {text}
    }
}