class PleaseUseChrome {
    constructor(bowser) {
        'ngInject';

        this.bowser = bowser;
    }
}

export default {
    controller: PleaseUseChrome,
    template: require('./pleaseUseChrome.html')
}