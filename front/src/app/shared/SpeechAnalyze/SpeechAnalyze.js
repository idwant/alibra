class SpeechAnalyze {
    constructor() {
        let SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
        this.SpeechRecognition = SpeechRecognition;
        this.recognition = null;
        this.recognitionPromise = null;
        this.recognitionResult = '';
    }
    startAnalyze() {
        if (this.SpeechRecognition) {
            this.recognitionPromise = new Promise((resolve, reject) => {
                this.recognition = new this.SpeechRecognition();
                this.recognition.continuous = true; // не хотим чтобы когда пользователь прикратил говорить, распознование закончилось
                this.recognition.interimResults = false;  // не хотим видеть промежуточные результаты. Т.е. мы можем некоторое время видеть слова, которые еще не были откорректированы
                this.recognition.lang = 'en-US'; // устанавливаем локаль для распознавания
                this.recognitionResult = ''; // финальный результат распознования
                this.recognition.onresult = (event) => {
                    if (typeof(event.results) == 'undefined') {
                        resolve(this.recognitionResult);
                        this.stopAnalyze();
                        return;
                    }
                    for (var i = event.resultIndex; i < event.results.length; ++i) {
                        if (event.results[i].isFinal) {
                            this.recognitionResult += event.results[i][0].transcript;
                        }
                    }
                };

                this.recognition.onend = () => {
                    this.stopAnalyze();
                    console.log(this.recognitionResult);
                    resolve(this.recognitionResult);
                };

                this.recognition.onerror = (event) => {
                    if (event.error == 'not-allowed') {
                        if (event.timeStamp - start_timestamp < 100) {
                            reject(new Error('info_blocked'));
                        } else {
                            reject(new Error('info_denied'));
                        }
                    }
                    reject(new Error(event.error));
                };
                this.recognition.start();
            });
        }
    }

    stopAnalyze() {
        if (this.recognition) {
            this.recognition.stop();
            let resultPromise = this.recognitionPromise;
            this.recognition = null;
            this.recognitionPromise = null;
            return resultPromise;
        }
        return Promise.resolve();
    }

    getResult() {
        return this.recognitionResult;
    }


}

export default () => {
    'ngInject';
    return () => new SpeechAnalyze();
};