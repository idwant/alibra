import * as angularLocale from './dictionaries/angular-locale_ru';

// Constants
import {ru} from './dictionaries/ru';

// Directives
import clickOnce from './clickOnce/clickOnce.directive';
import confirm from './confirm/confirm';
import restrictInput from './restrictInput/restrictInput.directive';
import limitTo from './limitTo/limitTo.directive';

// Services
import AuthInterceptor from  './auth/auth.interceptor';
import Loader from './loader/loader.service';
import AlertResponse from './AlertResponse/AlertResponse.service';
import DataHelper from './DataHelper/DataHelper.service';
import VoiceRecorderManager from './voiceRecorder/VoiceRecorderManager.service';

// Factory
import MediaRecorder from './MediaRecorder/MediaRecorder';
import SpeechAnalyze from './SpeechAnalyze/SpeechAnalyze';

//components
import loadFile from './load-file/load-file.component';
import crossword from './crossword/crossword.component';
import textAngular from './textAngular/textAngular';
import voiceRecorder from './voiceRecorder/voiceRecorder.component';
import {audioPlayer, audioPlayerMin} from './audio-player/audio-player.component';
import hint from './hint/hint.component';
import imageCarousel from './image-carousel/image-carousel.component';
import pleaseUseChrome from './pleaseUseChrome/pleaseUseChrome.component';

let sharedComponents = angular.module('shared-components', [
        'ngProgress',
        'pascalprecht.translate'
    ])
        .constant('ru', ru)
        .service('AuthInterceptor', AuthInterceptor)
        .service('DataHelper', DataHelper)
        .service('Loader', Loader)
        .service('AlertResponse', AlertResponse)
        .service('VoiceRecorderManager', VoiceRecorderManager)
        .factory('MediaRecorder', MediaRecorder)
        .factory('SpeechAnalyze', SpeechAnalyze)
        .directive('clickOnce', () => new clickOnce)
        .directive('restrictInput', () => new restrictInput)
        .directive('confirm', confirm)
        .directive('limitTo', limitTo)
        .component('loadFile', loadFile)
        .component('crossword', crossword)
        .component('textAngular', textAngular)
        .component('voiceRecorder', voiceRecorder)
        .component('audioPlayer', audioPlayer)
        .component('audioPlayerMin', audioPlayerMin)
        .component('hint', hint)
        .component('imageCarousel', imageCarousel)
        .component('pleaseUseChrome', pleaseUseChrome)
    ;

export default sharedComponents = sharedComponents.name;

