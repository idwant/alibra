/* globals MediaRecorder, window */

class MediaRecorderController {
    constructor($sce) {
        this.$sce = $sce;
        this.mediaSource = new MediaSource();
        this.mimeType = 'audio/webm';
        this.recordedBlobs = [];
        this.mediaRecorder = null;
        this.sourceBuffer = null;
        this.stream = null;

        this.init({
            audio: true
        });
    }

    init(constraints) {
        // window.isSecureContext could be used for Chrome
        var isSecureOrigin = location.protocol === 'https:' || location.hostname === 'localhost';

        if (!isSecureOrigin) {
            // location.protocol = 'HTTPS';
        }

        this.mediaSource.addEventListener(
            'sourceopen',
            () => this.sourceBuffer = this.mediaSource.addSourceBuffer(this.mimeType),
            false
        );

        var getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia);

        return new Promise((resolve, reject) => {
            getUserMedia.call(navigator, constraints, resolve, reject);
        })
            .then((stream) => {
                this.stream = stream;
            })
            .catch((error) => {
                console.error('navigator.getUserMedia error: ', error)
            });
    }

    startRecording() {
        this.recordedBlobs = [];
        var options = {mimeType: this.mimeType};
        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
            console.log(options.mimeType + ' is not Supported');
            options = {mimeType: ''};
        }
        try {
            this.mediaRecorder = new MediaRecorder(this.stream, options);
        } catch (e) {
            console.error('Exception while creating MediaRecorder: ' + e);
            return;
        }

        this.mediaRecorder.ondataavailable = (event) => {
            if (event.data && event.data.size > 0) {
                this.recordedBlobs.push(event.data);
            }
        };

        this.mediaRecorder.start(10);
    }

    getSrc() {
        var superBuffer = new Blob(this.recordedBlobs, {type: this.mimeType});
        return this.$sce.trustAsResourceUrl(window.URL.createObjectURL(superBuffer));
    }

    stopRecording() {
        this.mediaRecorder.stop();
    }

    download(name = 'test') {
        var blob = new Blob(this.recordedBlobs, {type: this.mimeType});
        var url = window.URL.createObjectURL(blob);
        var a = document.createElement('a');
        var extension = '.webm';
        a.style.display = 'none';
        a.href = url;
        a.download = name + extension;
        document.body.appendChild(a);
        a.click();
        setTimeout(() => {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 100);
    }
}

export default ($sce) => {
    'ngInject';

    return () => new MediaRecorderController($sce);
}