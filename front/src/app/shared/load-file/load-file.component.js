import {userConstants} from './../../constants';
import template from './load-file.html';

/** @ngInject */
class LoadFileController {
    constructor(FileUploader, API_URL) {
        "ngInject";

        let initParams = {
            headers: {},
            url: API_URL + this.url
        };
        //initParams.headers[userConstants.AUTH_HEADER_KEY] = $http.defaults.headers.common[userConstants.AUTH_HEADER_KEY];

        this.loading = false;
        this.success = false;
        this.error = false;
        this.response = {};

        this.isArray = Array.isArray;
        this.uploader = new FileUploader(initParams);
        this.uploader.onAfterAddingFile = this.onAfterAddingFile.bind(this);
        this.uploader.onProgressItem = this.onProgressItem.bind(this);
        this.uploader.onSuccessItem = this.onSuccessItem.bind(this);
        this.uploader.onErrorItem = this.onErrorItem.bind(this);
    }

    repeat() {
        this.loading = false;
        this.success = false;
        this.error = false;
        this.response = {};
    };

    onAfterAddingFile(fileItem) {
        this.error = false;

        if (this.loading) {
            return;
        }

        if (this.params) {
            fileItem.url += '?' + $httpParamSerializerJQLike(this.params);
        }

        fileItem.upload();
    }

    onProgressItem(fileItem, progress) {
        this.loading = true;
    }

    onSuccessItem(item, response) {
        this.loading = false;
        this.success = true;
        this.response = response;

        if (this.callback) {
            this.callback({response: response});
        } else {
            if (this.file) {
                if (Array.isArray(this.file) && this.file.length) {
                    this.file.push(new this.file[0].constructor(response));
                } else {
                    this.file = new this.file.constructor(response);
                }
            }
        }

        if (this.successMessageTemplate) {
            this.successMessage = this.successMessageTemplate.replace(/%(.+?)%/gi, function (str, offset, s) {
                str = response[offset];
                return str;
            });
        }
    }

    clearFile(file){
        if (this.file) {
            if (Array.isArray(this.file)) {
                let fileIndex = this.file.indexOf(file);
                if (fileIndex != -1) {
                    this.file.splice(fileIndex, 1);
                }
            } else {
                this.file = new this.file.constructor();
            }
        }
        this.success = false;
    }

    onErrorItem(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
        this.errorMessage = response.error;
        this.loading = false;
        this.error = true;
    }
}

export default {
    template: template,
    controller: LoadFileController,
    bindings: {
        url: '@',
        params: '=',
        callback: '&?',
        successMessageTemplate: '@',
        label: '@',
        file: '='
    }
};

