import {Crossword} from './crossword';

class CrosswordController {
    constructor($element, $scope, $compile, $timeout) {
        'ngInject';
        this.$element = $element;
        this.$compile = $compile;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.grid = [];
        this.answers_grid = [];
        this.correct_answers = new Set();

        // Предыдущая оринтация (across, down) ввода - используется для перехода к след. букве
        this.prevType = {word: null, orientation: null};
    }

    $onChanges() {
        if (this.crosswordGrid && this.legend) {
            return this.onGrid(this.crosswordGrid, this.legend, !this.isGame)
        }
        if (this.items) {
            return this.generateCrossword();
        }
    }

    getTransformedItems() {
        let words = [];
        let clues = [];

        this.items.forEach((item) => {
            words.push(item.answer.toLowerCase());
            clues.push(item.question);
        });

        return {words, clues};
    }

    generateCrossword() {
        let {words, clues} = this.getTransformedItems();

        if (!words.length || !clues.length) return;

        // Create crossword object with the words and clues
        let cw = new Crossword(words, clues);

        // create the crossword grid (try to make it have a 1:1 width to height ratio in 10 tries)
        let tries = 50;
        let grid = cw.getSquareGrid(tries);

        // report a problem with the words in the crossword
        if (grid == null) {
            return this.onGenerated({promise: Promise.reject(cw.getBadWords())});
        }

        // make a nice legend for the clues
        let legend = cw.getLegend(grid);

        this.onGrid(grid, legend, true);

    }

    onGrid(grid, legend, show_answers) {
        this.grid = grid;
        this.legend = legend;
        this.$element.find('.crossword-wrapper').html(this.$compile(this.render(show_answers))(this.$scope));
        this.onGenerated({promise: Promise.resolve({grid: this.grid, legend: this.legend})});
    }

    onType(event, row, cell) {
        // таймаут добавлен что бы предотвратить перевод фокуса и пропуска клетки
        this.$timeout(() => {
            let realSymbol = this.grid[row][cell];
            let isRightAcrossWord = false;
            let isRightDownWord = false;
            let key = event.keyCode || event.charCode;
            this.answers_grid[row][cell]['old_value'] = angular.copy(this.answers_grid[row][cell]['value']);
            this.answers_grid[row][cell]['value'] = String.fromCharCode(key);

            if (realSymbol['across']) {
                let startCell = cell;
                while (!this.grid[row][startCell]['across']['is_start_of_word']) {
                    --startCell;
                }
                isRightAcrossWord = this.checkAnswer(row, startCell, 'across');
            }
            if (realSymbol['down']) {
                let startRow = row;
                while (!this.grid[startRow][cell]['down']['is_start_of_word']) {
                    --startRow;
                }
                isRightDownWord = this.checkAnswer(startRow, cell, 'down');
            }

            if (key == 8 || key == 46) return false;

            // код для расчета следующей позиции перехода
            let orientation = 'across';

            if (this.prevType.orientation && (
                    (realSymbol['across'] && this.prevType.word === realSymbol['across']['answer']) ||
                    (realSymbol['down'] && this.prevType.word === realSymbol['down']['answer'])
                )
            ) {
                orientation = this.prevType.orientation;
            } else if (realSymbol['across'] && realSymbol['down']) {
                orientation = realSymbol['down']['is_start_of_word'] ? 'down' : 'across';
            } else if (realSymbol['down']) {
                orientation = 'down';
            }

            if ((this.prevType.orientation === 'across' && isRightAcrossWord) || (this.prevType.orientation === 'down' && isRightDownWord)) {
                let word = this.grid[row][cell][orientation];
                let wordIndex = +$(`[${orientation}-index=${word.index}]`).attr('word-index');

                this.prevType = {word: null, orientation: null};

                $(`[word-index=${wordIndex + 1}]>input`).focus();

                return;
            }

            let element = $(`[r=${orientation === 'down' ? row + 1 : row}][c=${orientation === 'across' ? cell + 1 : cell}]`).find('input');
            if (element.length) {
                this.prevType.orientation = orientation;
                this.prevType.word = realSymbol[orientation]['answer'];
                element.focus();

                return;
            }

            // меняем вариант сдвига если не нашли элемент
            this.prevType.orientation = orientation = (orientation === 'across') ? 'down' : 'across';
            this.prevType.word = realSymbol[orientation] ? realSymbol[orientation]['answer'] : null;
            element = $(`[r=${orientation === 'down' ? row + 1 : row}][c=${orientation === 'across' ? cell + 1 : cell}]`).find('input');
            element.focus();

            return;
        });
    }

    checkAnswer(startRow, startCell, direction) {
        if (!this.grid[startRow] || !this.grid[startRow][startCell] || !this.grid[startRow][startCell][direction]) return;

        let word = this.grid[startRow][startCell][direction];
        let answer = word['answer'];
        let typedAnswer = '';
        let answerCorrect = false;

        if (direction === 'across') {
            for (let cell = startCell; cell < startCell + answer.length; cell++) {
                if (this.answers_grid[startRow] && this.answers_grid[startRow][cell] && this.answers_grid[startRow][cell]['value']) {
                    typedAnswer += this.answers_grid[startRow][cell]['value'];
                }
            }

            answerCorrect = answer.toLowerCase() === typedAnswer.toLowerCase();

            for (let cell = startCell; cell < startCell + answer.length; cell++) {
                if (this.answers_grid[startRow][cell]) {

                    this.answers_grid[startRow][cell]['correct'] = answerCorrect;
                    this.answers_grid[startRow][cell][direction] = {'correct': answerCorrect};

                    // проверка не пренадлежит ли сосесднему с другой ориентацией
                    if (this.answers_grid[startRow][cell]['down'] && this.answers_grid[startRow][cell]['down']['correct']) {
                        this.answers_grid[startRow][cell]['correct'] = true;
                    }
                }
            }
        }
        if (direction === 'down') {
            for (let row = startRow; row < startRow + answer.length; row++) {
                if (this.answers_grid[row] && this.answers_grid[row][startCell] && this.answers_grid[row][startCell]['value']) {
                    typedAnswer += this.answers_grid[row][startCell]['value'];
                }
            }

            answerCorrect = answer.toLowerCase() === typedAnswer.toLowerCase();

            for (let row = startRow; row < startRow + answer.length; row++) {
                if (this.answers_grid[row][startCell]) {

                    this.answers_grid[row][startCell]['correct'] = answerCorrect;
                    this.answers_grid[row][startCell][direction] = {'correct': answerCorrect};

                    // проверка не пренадлежит ли сосесднему с другой ориентацией
                    if (this.answers_grid[row][startCell]['across'] && this.answers_grid[row][startCell]['across']['correct']) {
                        this.answers_grid[row][startCell]['correct'] = true;
                    }
                }
            }
        }

        let index = -1;
        if (answerCorrect) {
            this.correct_answers.add(answer);
        } else {
            this.correct_answers.delete(answer);
        }

        let percent = this.correct_answers.size / this.items.length * 100;
        this.onProgress({correct_answers: [...this.correct_answers], percent: percent});

        return answerCorrect;
    }

    render(show_answers) {
        if (!this.grid) return;

        let html = [];
        let wordIndex = 1;
        html.push("<table class='crossword'>");
        for (let r = 0; r < this.grid.length; r++) {
            html.push("<tr>");
            for (let c = 0; c < this.grid[r].length; c++) {
                let cell = this.grid[r][c] || {};
                let is_start_of_word = false;
                let char = cell['char'];
                let css_class = "";

                if (cell == null) {
                    char = "&nbsp;";
                    css_class = "no-border";
                } else {
                    is_start_of_word = (cell['across'] && cell['across']['is_start_of_word']) || (cell['down'] && cell['down']['is_start_of_word']);
                }

                if (is_start_of_word) {
                    html.push(`<td class='${css_class} label_${wordIndex}' 
                                   word-index='${wordIndex}' 
                                   title='${r}, ${c}' 
                                   has-char='${!!char}' 
                                   down-index='${cell['down'] ? cell['down']['index'] : ''}'
                                   across-index='${cell['across'] ? cell['across']['index'] : ''}'
                                   r='${r}' 
                                   c='${c}'>
                                <span class="cell_index">${wordIndex}</span>`);
                    wordIndex++;
                } else {
                    html.push(`<td class='${css_class}' title='${r}, ${c}' has-char='${!!char}' r='${r}' c='${c}'>`);
                }

                if (char) {
                    this.answers_grid[r] = this.answers_grid[r] || {};
                    this.answers_grid[r][c] = {value: show_answers ? char : '', correct: false}; //char
                    html.push(`<input ng-model="$ctrl.answers_grid[${r}][${c}]['value']" 
                                      ng-disabled='!$ctrl.isGame'
                                      limit-to="1" 
                                      ng-class="{'correct': $ctrl.answers_grid[${r}][${c}]['correct']}" 
                                      ng-keyup="$ctrl.onType($event, ${r}, ${c})"/>`
                    );
                } else {
                    html.push("&nbsp;");
                }
            }
            html.push("</td></tr>");
        }
        html.push("</table>");
        return html.join("\n");
    }
}

export default {
    controller: CrosswordController,
    template: '<div class="crossword-wrapper"></div>',
    bindings: {
        items: '<',
        crosswordGrid: '<',
        legend: '<',
        isGame: '<',
        onGenerated: '&', // {grid: this.grid, legend: this.legend}
        onProgress: '&' // {correct_answers: this.correct_answers, percent: percent}
    }
}