class AudioPlayerController {
    constructor($timeout) {
        'ngInject';
        this.prevCongig = null;
        this.$timeout = $timeout;

        this.changed = true;
    }

    $onChanges(changes) {
        if (changes.src) {
            this.changed = false;
            this.$timeout(() => this.changed = true);
        }
    }
}

export const audioPlayer =  {
    controller: AudioPlayerController,
    template: require('./audio-player.html'),
    bindings: {
        src: '<', // $ctrl.src
    }
};

export const audioPlayerMin =  {
    controller: AudioPlayerController,
    template: require('./audio-player-min.html'),
    bindings: {
        src: '<', // $ctrl.src
    }
};