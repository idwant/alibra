class HintController {
    constructor(HintManager) {
        'ngInject';
        this.hint = null;
        this.HintManager = HintManager;
    }

    $onInit() {
        this.HintManager.getHintByName(this.name).then((hint) => {
            this.hint = hint;
        });
    }

    closeHint() {
        this.HintManager.closeHint(this.hint);
    }

    set close(v) {
        if(v) this.closeHint();
    }
}

export default {
    controller: HintController,
    template: require('./hint.html'),
    bindings: {
        name: '@',
        close: '<'
    }
};