export default (alert) => {
    "ngInject";

    return {
        restrict: 'A',
        priority: -1,
        scope: {
            ngClick: '&',
            confirm: '@',
        },
        link: (scope, elem) => {
            elem.bind('click', (evt) => {
                evt.preventDefault();
                evt.stopImmediatePropagation();

                let title = scope.confirm || 'Вы уверены?';

                alert({
                    title: title,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да",
                    cancelButtonText: "Нет"
                }, () => {
                    scope.ngClick();
                    scope.$apply();
                });
            })
        }
    }
}