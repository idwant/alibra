class NgSummernoteController {
    constructor($element, $scope, API_URL, Upload) {
        "ngInject";
        this.$scope = $scope;
        this.Upload = Upload;
        this.uploadUrl = API_URL + 'image/upload';
        this.config = {
            height: 150,
            toolbar: [
                ['magic', ['style']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture']]
            ]
        }
    }

    imageUpload(files) {

        if (files) {
            this.Upload.upload({
                url: this.uploadUrl,
                file: files[0]
            }).success((data) => {
                this.$scope.editor.summernote('insertImage', data.full_path, function ($image) {
                    $image.css('width', $image.width() / 3);
                    $image.attr('data-filename', 'retriever');
                });
            });
        }
    }
}

export default {
    template: `<summernote editor="editor" ng-model="$ctrl.ngModel" editable="editable" config="$ctrl.config" on-image-upload="$ctrl.imageUpload(files)"></summernote>`,
    controller: NgSummernoteController,
    bindings: {
        ngModel: '='
    }
};