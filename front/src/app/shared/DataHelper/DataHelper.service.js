class DataHelper {

    constructor() {
        this.possibleWords = this.getPossibleWords();

    }

    /*
     * Чистит строку: выполняет transofrm и заменяет в ней слова согласно справочника
     */
    clearStr(str) {
        let possibleWords = this.possibleWords;
        let transformedStr = this.transformStr(str);
        possibleWords.forEach((element) => {
            element.from.forEach((fromWord) => {
                let regExp = new RegExp(fromWord, "gi");
                transformedStr = transformedStr.replace(regExp, element.to);
            })
        });
        return transformedStr;
    }

    /*
     * Заменяет ’ на ', удаляет все символы, кроме ' a-z 0-9 и приводит результат к нижнему регистру
     */
    transformStr(str) {
        if (!str){
            str = '';
        }
        return str
            .replace(/<\/?[^>]+>/gi, ' ')
            .trim()
            .replace(/[’]/gi, "'")
            .replace(/[&nbsp]/gi, "")
            .replace(/(\s+)/g, " ")
            .replace(/[^a-z0-9'\s]/gi, "")
            .toLowerCase()
        ;
    }
    
    getPossibleWords() {
        const possibleWords = [
            // слова с двойным написанием
            {from: ["travelled"], to: "traveled"},
            {from: ["cancelling"], to: "canceling"},
            {from: ["dialled"], to: "dialed"},
            {from: ["programme"], to: "program"},
            {from: ["catalogue"], to: "catalog"},
            {from: ["monologue"], to: "monolog"},
            {from: ["dialogue"], to: "dialog"},
            {from: ["organisation"], to: "organization"},
            {from: ["analyse"], to: "analyze"},
            {from: ["defence"], to: "defense"},
            {from: ["licence"], to: "license"},
            {from: ["practice"], to: "practise"},
            {from: ["offence"], to: "offense"},
            {from: ["centre"], to: "center"},
            {from: ["theatre"], to: "theater"},
            {from: ["litre"], to: "liter"},
            {from: ["fibre"], to: "fiber"},
            {from: ["metre"], to: "meter"},
            {from: ["honour"], to: "honor"},
            {from: ["labour"], to: "labor"},
            {from: ["colour"], to: "color"},
            {from: ["splendour"], to: "splendor"},

            // сокращенные формы глаголов (в т.ч модальных)
            {from: ["don’t"], to: "do not"},
            {from: ["doesn’t"], to: "does not"},
            {from: ["didn’t"], to: "did not"},
            {from: ["won’t"], to: "will not"},
            {from: ["’ll", "shall"], to: " will"},
            {from: ["can’t"], to: "cannot"},
            {from: ["couldn’t"], to: "could not"},
            {from: ["mustn’t"], to: "must not"},
            {from: ["oughtn’t"], to: "ought not"},
            {from: ["’m"], to: " am"},
            {from: ["’re"], to: " are"},
            {from: ["aren’t"], to: "are not"},
            {from: ["isn’t"], to: "is not"},
            {from: ["wasn’t"], to: "was not"},
            {from: ["weren’t"], to: "were not"},
            {from: ["’ve"], to: " have"},
            {from: ["haven’t"], to: "have not"},
            {from: ["hasn’t"], to: "has not"},
            {from: ["hadn’t"], to: "had not"},
            {from: ["wouldn’t"], to: "would not"},
            {from: ["shouldn’t"], to: "should not"},

            // сокращенные формы глаголов (в т.ч модальных)
            // меняем в обратную сторону из-за того что правило замены одинаковое
            {from: [" had"], to: "’d"},
            {from: [" would"], to: "’d"},
            {from: [" is"], to: "’s"},
            {from: [" has"], to: "’s"},

            // цифры/числа
            {from: ["one million", "a million", "1 000 000"], to: "1000000"},
            {from: ["one thousand", "a thousand", "1 000"], to: "1000"},
            {from: ["one hundred", "a hundred"], to: "100"},
            {from: ["ninety"], to: "90"},
            {from: ["eighty"], to: "80"},
            {from: ["seventy"], to: "70"},
            {from: ["sixty"], to: "60"},
            {from: ["fifty"], to: "50"},
            {from: ["forty"], to: "40"},
            {from: ["thirty"], to: "30"},
            {from: ["twenty"], to: "20"},
            {from: ["nineteen"], to: "19"},
            {from: ["eighteen"], to: "18"},
            {from: ["seventeen"], to: "17"},
            {from: ["sixteen"], to: "16"},
            {from: ["fifteen"], to: "15"},
            {from: ["fourteen"], to: "14"},
            {from: ["thirteen"], to: "13"},
            {from: ["twelve"], to: "12"},
            {from: ["eleven"], to: "11"},
            {from: ["ten"], to: "10"},
            {from: ["nine"], to: "9"},
            {from: ["eight"], to: "8"},
            {from: ["seven"], to: "7"},
            {from: ["six"], to: "6"},
            {from: ["five"], to: "5"},
            {from: ["four"], to: "4"},
            {from: ["three"], to: "3"},
            {from: ["two"], to: "2"},
            {from: ["one"], to: "1"},
            {from: ["zero", "zeroh"], to: "0"},
        ];

        return possibleWords.map((el) => ({
            from: el.from.map(word => this.transformStr(word)),
            to: this.transformStr(el.to)
        }));
    }

    /*
     * Сравнивает содержимое 2 массивов, не учитывая порядок следования элементов в них
    */
    compareArrays(arr1, arr2) {
        if ( arr1.length !== arr2.length ) {
            return false;
        }
        for ( var i = arr1.length; i--; ) {
            if (arr2.indexOf(arr1[i]) === -1) {
                return false;
            }
        }
        return true;
    }
}

export default DataHelper;


