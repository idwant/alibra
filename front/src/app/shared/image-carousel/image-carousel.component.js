class ImageCarousel {
    constructor($timeout) {
        'ngInject';

        this.$timeout = $timeout;

        this.selected_image = null;
        this.imageContainer = null;
        this.imageContainerWith = 0;
        this.imageSize = 100;
        this.fromLeft = 0;

    }

    $onChanges() {
        if (this.images) {
            this.$timeout(() => {
                this.imageContainer = this.imageContainer || $('.image-carousel .card-imgs');
                this.selected_image = null;
                this.imageSize = 100;
                this.fromLeft = 0;

                Array.from(this.imageContainer.find('>div')).forEach(e => this.imageContainerWith += $(e).outerWidth());
            });
        }
    }

    scrollLeft() {
        this.fromLeft = this.imageContainer.scrollLeft();
        this.fromLeft = this.fromLeft >= this.imageSize ? this.fromLeft - this.imageSize : this.fromLeft;
        this.scroll();
    }

    scrollRight() {
        this.fromLeft = this.imageContainer.scrollLeft();
        this.fromLeft = (this.imageContainerWith - (this.imageContainer.outerWidth() + this.fromLeft) >= this.imageSize) ?
            this.fromLeft += this.imageSize : this.fromLeft;

        this.scroll();
    }

    scroll() {
        console.log(this.fromLeft);
        this.imageContainer.animate({scrollLeft: this.fromLeft}, 100);
    }
}

export default {
    controller: ImageCarousel,
    template: require('./image-carousel.html'),
    bindings: {
        images: '<'
    }
}