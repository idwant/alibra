function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('grammar', {
            url: '/grammar',
            template: '<grammar class="grammar site-wrapper"></grammar>'
        });
}

export default routeConfig;