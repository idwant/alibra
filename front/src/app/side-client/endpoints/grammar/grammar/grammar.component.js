import GrammarSection from 'api/models/GrammarSection';
import GrammarTheme from 'api/models/GrammarTheme';
import Grammar from 'api/models/Grammar';

class GrammarController {
    constructor($log, $q, AlertResponse, $uibModal, StudentGrammarManager) {
        "ngInject";

        this.$log = $log;
        this.$q = $q,
        this.AlertResponse = AlertResponse;
        this.$uibModal = $uibModal;
        this.StudentGrammarManager = StudentGrammarManager;

        this._section = null;
        this._theme = null;
        this._grammar = null;

        this.filterSections = [];
        this.filterThemes = [];
        this.filterGrammars = [];

        this.sections = [];
        this.themes = [];
        this.grammars = [];

        this.allSections = new GrammarSection({id: 0, name: 'All units'});
        this.allThemes = new GrammarTheme({id: 0, name: 'All categories'});
        this.allGrammars = new Grammar({id: 0, name: 'All topics'});
    }

    $onInit() {
        this.loadSections()
            .then(sections => this.sections = sections);
    }

    get section() {
        return this._section;
    }

    set section(section) {
        this._section = section;
        this.refreshFilterThemes()
            .then(themes => {
                this.theme = this.allThemes;
            });
    }

    get theme() {
        return this._theme;
    }

    set theme(theme) {
        this._theme = theme;
        this.refreshFilterGrammars()
            .then(grammars => {
                this.grammar = this.allGrammars;
            });
    }

    get grammar() {
        return this._grammar;
    }

    set grammar(grammar) {
        this._grammar = grammar;
    }
    
    refreshFilterSections(query) {
        this.loadSections(query)
            .then(sections => {
                this.filterSections = sections;
                this.filterSections.unshift(this.allSections);

                if (!this.section) {
                    this.section = this.allSections;
                }
            });
    }

    refreshFilterThemes(query) {
        return this.$q
            .resolve(this.section)
            .then (section => {
                const sectionId = section ? section.id : null;
                return this.loadThemes(sectionId, query);

            })
            .then (themes => {
                this.filterThemes = themes;
                this.filterThemes.unshift(this.allThemes);
            });
    }

    refreshFilterGrammars(query) {
        return this.$q
            .resolve(this.theme)
            .then(theme => {
                const themeId = theme ? theme.id : null;
                return this.loadGrammars(themeId, query);
            })
            .then(grammars => {
                this.filterGrammars = grammars;
                this.filterGrammars.unshift(this.allGrammars);
            });
    }

    loadSectionThemes(section) {
        if (!section.themes) {
            this.loadThemes(section.id)
                .then(themes => {
                    section.themes = themes;
                    section.showThemes = true;
                });
        } else {
            section.showThemes = !section.showThemes;
        }
    }

    loadThemeGrammars(theme) {
        if (!theme.grammars) {
            this.loadGrammars(theme.id)
                .then(grammars => {
                    theme.grammars = grammars;
                    theme.showGrammars = true;
                });
        } else {
            theme.showGrammars = !theme.showGrammars;
        }
    }

    showGrammar(grammar) {
        this.$uibModal.open({
            component: 'grammarDialog',
            windowClass: 'grammar-dialog-modal',
            backdropClass: 'back',
            windowTopClass: 'windowTopClass',
            openedClass: 'modal-opened',
            resolve: {
                grammar: function () {
                    return grammar;
                }
            }
        });
    }

    loadSections(query) {
        return this.StudentGrammarManager
            .queryGrammarSections({query, limit: 1000000});
    }

    loadThemes(sectionId, query) {
        return this.StudentGrammarManager
            .queryGrammarThemes({query, section_id: sectionId, limit: 1000000});
    }

    loadGrammars(themeId, query) {
        return this.StudentGrammarManager
            .query({query, theme_id: themeId});
    }

    compareItems(listItem, filterItem) {
        if (this._grammar && this._grammar.id !== 0) {
            if (listItem.constructor === GrammarSection) {
                return this._grammar.theme.section && this._grammar.theme && this._grammar.theme.section.id === listItem.id;
            }
            if (listItem.constructor === GrammarTheme) {
                return this._grammar.theme && this._grammar.theme.id == listItem.id;
            }
        }

        if (this._theme && this._theme.id !== 0) {
            //console.log(this._theme);
            if (listItem.constructor === GrammarSection) {
                //console.log(listItem);
                return this._theme.section && this._theme.section.id === listItem.id;
            }
        }
        return listItem && (!filterItem || listItem.id == filterItem.id || !filterItem.id);
    }
}

export default {
    template: require('./grammar.html'),
    controller: GrammarController
}