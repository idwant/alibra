class GrammarDialogController {
    constructor() {
        "ngInject";
    }

    $onInit() {
        this.grammar = this.resolve.grammar;
    }

    cancel() {
        this.modalInstance.close();
    }
}


export default {
    template: require('./grammar-dialog.html'),
    controller: GrammarDialogController,
    bindings: {
        modalInstance: '<',
        resolve: '<'
    }
};