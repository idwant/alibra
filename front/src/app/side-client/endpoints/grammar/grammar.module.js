import routeConfig from './grammar.route';
import GrammarDialogComponent from './grammar/grammar-dialog.component';
import grammar from './grammar/grammar.component';

let module = angular
    .module('grammar', [])
    .component('grammarDialog', GrammarDialogComponent)
    .config(routeConfig)
    .component('grammar', grammar)
    ;

export default module = module.name;