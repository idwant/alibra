import routeConfig from './cabinet.route';
import cabinetProgress from './cabinet/progress/progress.component';
import learnReport from './cabinet/progress/learn-report/learn-report.component';
import testReport from './cabinet/progress/test-report/test-report.component';

let module = angular
        .module('cabinet', [])
        .config(routeConfig)
        .component('cabinetProgress', cabinetProgress)
        .component('learnReport', learnReport)
        .component('testReport', testReport)
    ;

export default module = module.name;