function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('cabinetProgress', {
            url: '/cabinet/progress',
            template: '<cabinet-progress class="cabinet-progress site-wrapper"></cabinet-progress>'
        });
}

export default routeConfig;