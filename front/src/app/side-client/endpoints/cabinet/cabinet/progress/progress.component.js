class ProgressController {
    constructor($log, AlertResponse, StudentManager, StudentDashboardManager) {
        "ngInject";

        this.$log = $log;
        this.AlertResponse = AlertResponse;
        
        this.StudentManager = StudentManager;
        this.StudentDashboardManager = StudentDashboardManager;
        
        this.activeTab = 'learn';

        this.activeCourse = null;
    }

    $onInit() {
        this.loadCourse();
    }

    loadCourse() {
        return this.StudentDashboardManager
            .getCourses()
            .then(courses => this.activeCourse = courses.length > 0 ? courses[0] : null)
            .catch((response) => this.AlertResponse.fail(response));
    }
    
    exportCsv() {
        if (this.activeCourse) {
            this.StudentManager
                .exportProgressStat({course_id: this.activeCourse.id, type: 'csv'})
                .then(response => {
                    var result = new Blob(["\ufeff", response], {
                        type: 'text/csv;'
                    });

                    return saveAs(result, 'report.csv');
                });
        }
    }
}

export default {
    controller: ProgressController,
    template: require('./progress.html')
};