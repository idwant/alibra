class TestReportController {
    constructor($log, $scope, alert, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;

        this.alert = alert;
        this.AlertResponse = AlertResponse;
    }
    
}

export default {
    template: require('./test-report.html'),
    controller: TestReportController
}
