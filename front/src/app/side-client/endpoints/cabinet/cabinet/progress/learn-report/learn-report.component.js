class LearnReportController {
    constructor($log, $scope, alert, AlertResponse, StudentManager, StudentDashboardManager) {
        "ngInject";
        this.$log = $log;
        this.$scope = $scope;
        
        this.progressItems = [];
        this.currentPage = 1;
        this.countPerPage = 15;
        this.totalCount = 0;

        this.StudentManager = StudentManager;
        this.StudentDashboardManager = StudentDashboardManager;

        this.alert = alert;
        this.AlertResponse = AlertResponse;
    }

    $onChanges() {
        if(this.activeCourse) {
            this.reloadProgressItems();
        }
    }

    reloadProgressItems() {
        let limit = this.countPerPage;
        let page = this.currentPage - 1;
        this.StudentManager
            .getProgressStat({course_id: this.activeCourse.id, limit, offset: page * limit})
            .then(([items, total]) => {
                this.progressItems = items;
                this.totalCount = total;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./learn-report.html'),
    controller: LearnReportController,
    bindings: {
        activeCourse: '<'
    }
}
