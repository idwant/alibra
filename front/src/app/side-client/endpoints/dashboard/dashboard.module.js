import routeConfig from './dashboard.route';

import dashboard from './dashboard/dashboard.component';
import groupTaskParts from './dashboard/grouptask-parts/grouptask-parts.component';
import homeWorkParts from './dashboard/homework-parts/homework-parts.component';
import testParts from './dashboard/test-parts/test-parts.component';
import greetingDialog from "./dashboard/greeting-dialog/greeting-dialog.component";

let module = angular
    .module('dashboard', [])
    .config(routeConfig)
    .component('greetingDialog', greetingDialog)
    .component('dashboard', dashboard)
    .component('groupTaskParts', groupTaskParts)
    .component('homeWorkParts', homeWorkParts)
    .component('testParts', testParts)
    ;

export default module = module.name;
