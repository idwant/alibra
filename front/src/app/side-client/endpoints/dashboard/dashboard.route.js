function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('dashboard', {
            url: '/dashboard/:unitIndex',
            template: '<dashboard class="dashboard site-wrapper bgs"></dashboard>',
            params: {
                unitIndex: null
            }
        });
}

export default routeConfig;