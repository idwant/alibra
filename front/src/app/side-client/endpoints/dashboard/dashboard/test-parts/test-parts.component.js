class TestPartsController {
    constructor($log, $scope, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;

        this.AlertResponse = AlertResponse;
    }

    $onInit() {
        this.registerWatcher();
    }

    registerWatcher() {
        this.$scope.$watch(() => this.parts, () => {

        });
    }

    get unitName() {
        if (this.course.isPhonetic) {
            return  this.unitIndex == 0 ? 'Unit Ph' : ('Unit ' + this.unitIndex);
        }

        return 'Unit ' + (this.unitIndex + 1);
    }
}

export default {
    template: require('./test-parts.html'),
    controller: TestPartsController,
    bindings: {
        parts: '<',
        exerciseType: '<',
        course: '<',
        unit: '<',
        unitIndex: '<'
    }
}
