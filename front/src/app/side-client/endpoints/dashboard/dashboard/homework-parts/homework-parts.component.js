import LessonPart from 'api/models/LessonPart';
import Dashboard from 'api/models/Dashboard';

class HomeWorkPartsController {
    constructor($log, $scope, $state, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;
        this.$state = $state;

        this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.combinedPart = null;
        this.wordStat = null;
    }

    $onInit() {
        this.registerWatcher();
    }

    learnWords() {
        if (!this.wordStat.isLearnComplete) {
            this.$state.go('vocabularyLearnLesson', {lessonId: this.lesson.id});
        }
    }

    refreshParts() {
        let exercises = [];
        let dashboard = new Dashboard({});
        this.parts.forEach((part) => {
            if (part.dashboard) {
                dashboard.add(part.dashboard);
            }
            exercises.concat(part.exercises);
        });

        this.combinedPart = new LessonPart({
            id: 0,
            name: 'combinedPart',
            type: 'combined',
            exercises: [],
            dashboard: dashboard
        });
    }

    refreshWordCardStat() {
        if (this.lesson) {
            return this.StudentWordCardManager
                .stat({lesson_id: this.lesson.id})
                .then(stat => this.wordStat = stat)
                .catch((response) => this.AlertResponse.fail(response));
        }

        return Promise.resolve(null);
    }

    registerWatcher() {
        this.$scope.$watch(() => this.parts, () => {
            this.refreshParts();
        });

        this.$scope.$watch(() => this.lesson, () => {
            this.refreshWordCardStat();
        });
    }

}

export default {
    template: require('./homework-parts.html'),
    controller: HomeWorkPartsController,
    bindings: {
        parts: '<',
        animate: '<',
        exerciseType: '<',
        lessonIndex: '<',
        lesson: '<',
        unit: '<',
        unitIndex: '<',
        courseId: '<'
    }
}
