import Exercise from 'api/models/Exercise';

class DashboardController {
    constructor($log, AlertResponse, StudentDashboardManager, HintManager, $uibModal, $timeout, $state) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;

        this.AlertResponse = AlertResponse;
        this.StudentDashboardManager = StudentDashboardManager;
        this.HintManager = HintManager;
        this.$uibModal = $uibModal;
        this.$timeout = $timeout;

        this.courses = [];
        this.units = [];
        this.lessons = [];
        this.lessonParts = [];

        this.activeCourse = null;
        this.activeUnit = null;
        this.activeUnitIndex = parseInt($state.params.unitIndex || 0);
        this.activeLesson = null;
        this.activeLessonIndex = null;
        this._activeExerciseType = null;
    }


    $onInit() {
        this.reloadCourses()
            .then(() => {
                return this.HintManager.getHintByName('user-greeting');
            })
            .then((hint) => {
                if (hint && !hint.isClosedInThisSession()) {
                    this.$uibModal.open({
                        component: 'greetingDialog',
                        windowClass: 'greeting-modal',
                        backdropClass: 'greeting-backdrop'
                    })
                }
            })
        ;
    }

    reloadCourses() {
        return this.loadCourses()
            .then(courses => {
                this.activeCourse = courses.length > 0 ? courses[0] : null;
                return this.activeCourse ? this.reloadUnits(this.activeCourse) : [];
            })
    }

    reloadUnits(course) {
        this.loadUnits(course)
            .then(units => {
                this.activeUnit = units.length > this.activeUnitIndex
                    ? units[this.activeUnitIndex]
                    : units[0] ? units[0] : null;

                return this.activeUnit
                    ? this.reloadLessons(this.activeCourse, this.activeUnit, this.activeUnitIndex)
                    : [];
            })
    }

    reloadLessons(course, unit, unitIndex) {
        this.activeUnitIndex = unitIndex;

        this.loadLessons(course, unit)
            .then(lessons => {
                this.activeLesson = lessons.length > 0 ? lessons[0] : [];
                this.showTestsButton = true;

                return this.activeLesson
                    ? this.loadLessonParts(this.activeCourse, this.activeUnit, this.activeLesson, this.activeLessonIndex, Exercise.TYPE_HOMEWORK)
                    : [];
            })
            .then(lessonParts => {

            });
    }

    loadCourses() {
        return this.StudentDashboardManager
            .getCourses()
            .then(courses => {
                this.courses = courses;
                return courses;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadUnits(course) {
        return this.StudentDashboardManager
            .getUnits(course)
            .then(units => {
                this.activeCourse = course;
                this.units = units;
                return units;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadLessons(course, unit) {
        return this.StudentDashboardManager
            .getLessons(course, unit)
            .then(lessons => {
                this.activeCourse = course;
                this.activeUnit = unit;

                this.lessons = lessons;
                return lessons;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    loadLessonParts(course, unit, lesson = null, lessonIndex = null, exerciseType = null) {
        return this.StudentDashboardManager
            .getLessonParts(course, unit, lesson, exerciseType)
            .then(lessonParts => {
                this.activeCourse = course;
                this.activeUnit = unit;
                this.activeLesson = lesson;
                this.activeLessonIndex = lessonIndex;
                this.activeExerciseType = exerciseType;

                this.lessonParts = lessonParts;

                return lessonParts;
            })
            .catch((response) => this.AlertResponse.fail(response));

    }

    set activeExerciseType(type) {
        this._activeExerciseType = type;
        this.changeExerciseTypeAnimate = true;
        this.$timeout(() => this.changeExerciseTypeAnimate = false, 1000);
    }

    get activeExerciseType() {
        return this._activeExerciseType;
    }

    isSelectedGroupTask(lesson) {
        return this.activeLesson && lesson.id === this.activeLesson.id && this.isGroupTask();
    }

    isSelectedHomeWork(lesson) {
        return this.activeLesson && lesson.id === this.activeLesson.id && this.isHomeWork();
    }

    isSelectedTest() {
        return this.isTest();
    }

    isGroupTask() {
        return this.activeExerciseType === Exercise.TYPE_GROUP_TASK;
    }

    isHomeWork() {
        return this.activeExerciseType === Exercise.TYPE_HOMEWORK;
    }

    isTest() {
        return this.activeExerciseType == Exercise.TYPE_TEST;
    }
}

export default {
    template: require('./dashboard.html'),
    controller: DashboardController
};

