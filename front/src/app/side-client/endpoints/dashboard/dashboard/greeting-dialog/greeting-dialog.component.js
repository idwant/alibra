class GreetingDialogController {
    constructor(HintManager) {
        'ngInject';

        this.HintManager = HintManager;
    }

    $onInit() {
        this.modalInstance.closed
            .then(r => this.HintManager.getHintByName('user-greeting'))
            .then(hint => this.HintManager.closeHint(hint));
    }

    closeDialog() {
        this.modalInstance.close();
    }
}

export default {
    template: require('./greeting-dialog.html'),
    controller: GreetingDialogController,
    bindings: {
        modalInstance: '<'
    }
};