class GroupTaskPartsController {
    constructor($log, $scope, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;

        this.AlertResponse = AlertResponse;
    }

    $onInit() {
        this.registerWatcher();
    }

    registerWatcher() {
        this.$scope.$watch(() => this.parts, () => {

        });
    }
}

export default {
    template: require('./grouptask-parts.html'),
    controller: GroupTaskPartsController,
    bindings: {
        parts: '<',
        exerciseType: '<'
    }
}
