class ReadWriteExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper;
        this.showAnswers = false;
    }

    init() {
        this.activeRound = null;
    }

    $onChanges(changesObj) {
        if (changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    checkAnswer() {
        let result = true,
            successRoundSteps;

        this.activeRound.elements.forEach(e => {
                if (this.DataHelper.clearStr(e.text) !== this.DataHelper.clearStr(e.answer)) {
                    e.hasError = true;
                    result = false;
                }
            }
        );

        successRoundSteps = this.activeRound.elements
            .filter(e => e.skip && this.DataHelper.clearStr(e.text) === this.DataHelper.clearStr(e.answer))
            .length
        ;

        return {result, progress: {success_round_steps: successRoundSteps}};
    }

    setAnswerVisibility(isVisible = true) {
        this.showAnswers = isVisible;
    }
}

export default {
    template: require('./read-write-exercise.html'),
    controller: ReadWriteExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};