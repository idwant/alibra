class DialogExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper
    }

    init() {
        this.activeRound = null;
        this.result = null;
        this.audioRecord = null;
        this.text = null;
    }

    onRecorded({src}) {
        this.audioRecord = src;
    }

    onTextChanged({text}) {
        this.text = text;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    checkAnswer() {
        let result = this.DataHelper.clearStr(this.text);
        if(!result) return {result: false, progress: 0};

        result = !!this.activeRound.answers.filter(
            (answer) => result.indexOf(this.DataHelper.clearStr(answer.text)) !== -1
        ).length;

        return {result, progress: result ? 100 : 0};
    }
}

export default {
    template: require('./dialog-exercise.html'),
    controller: DialogExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};