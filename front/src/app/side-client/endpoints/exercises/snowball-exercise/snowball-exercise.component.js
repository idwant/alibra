class SnowballExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper
    }

    init() {
        this.activeRound = null;
        this.audioRecord = null;
        this.text = null;
    }

    $onChanges(changesObj) {
        if (changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    onRecorded({src}) {
        this.audioRecord = src;
    }

    onTextChanged({text}) {
        this.text = text;
    }

    checkAnswer() {
        let result = this.DataHelper.clearStr(this.text);
        if(!result) return {result: false, progress: 0};

        result = !!this.activeRound.elements.filter((element) => {
            if (element.answer){
                return result.indexOf(this.DataHelper.clearStr(element.text)) !== -1
            }
            return false;
        }).length;
        return {result, progress: result ? 100 : 0};
    }
}

export default {
    template: require('./snowball-exercise.html'),
    controller: SnowballExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};