import routeConfig from './exercises.route';
import baseExercise from './base-exercise/base-exercise.component';
import dialogExercise from './dialog-exercise/dialog-exercise.component';
import dictionaryDialog from './dictionary-dialog/dictionary-dialog.component';
import compareExercise from './compare-exercise/compare-exercise.component';
import listenSayExercise from './listen-say-exercise/listen-say-exercise.component';
import freeFormExercise from './free-form-exercise/free-form-exercise.component';
import crosswordExercise from './crossword-exercise/crossword-exercise.component';
import pictureExercise from './picture-exercise/picture-exercise.component';
import readWriteExercise from './read-write-exercise/read-write-exercise.component'
import snowballExercise from './snowball-exercise/snowball-exercise.component';
import dragAndDropExercise from './drag-and-drop-exercise/drag-and-drop-exercise.component';
import listenWriteExercise from './listen-write-exercise/listen-write-exercise.component';
import fiveCardsExercise from './five-cards-exercise/five-cards-exercise.component';
import selectExercise from './select-exercise/select-exercise.component';
import pickOutExercise from './pick-out-exercise/pick-out-exercise.component';
import listenReadClickExercise from './listen-read-click-exercise/listen-read-click-exercise.component';
import findCoupleExercise from './find-couple-exercise/find-couple-exercise.component';

let module = angular
    .module('exercises', [])
    .config(routeConfig)
    .component('baseExercise', baseExercise)
    .component('dialogExercise', dialogExercise)
    .component('dictionaryDialog', dictionaryDialog)
    .component('compareExercise', compareExercise)
    .component('listenSayExercise', listenSayExercise)
    .component('freeFormExercise', freeFormExercise)
    .component('crosswordExercise', crosswordExercise)
    .component('readWriteExercise', readWriteExercise)
    .component('pictureExercise', pictureExercise)
    .component('snowballExercise', snowballExercise)
    .component('dragAndDropExercise', dragAndDropExercise)
    .component('listenWriteExercise', listenWriteExercise)
    .component('selectExercise', selectExercise)
    .component('fiveCardsExercise', fiveCardsExercise)
    .component('pickOutExercise', pickOutExercise)
    .component('listenReadClickExercise', listenReadClickExercise)
    .component('findCoupleExercise', findCoupleExercise)
    ;

export default module = module.name;