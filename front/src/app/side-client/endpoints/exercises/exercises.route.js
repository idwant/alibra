function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('exercise', {
            url: '/exercise/:exerciseIndex?:unitId:unitIndex:courseId:lessonId:lessonPartId:lessonIndex:exerciseType',
            template: '<base-exercise></base-exercise>',
            params: {
                courseId: null,
                unitId: null,
                lessonId: null,
                lessonPartId: null,
                exerciseType: null,
                unitIndex: null,
                lessonIndex: null
            }
        })
}

export default routeConfig;