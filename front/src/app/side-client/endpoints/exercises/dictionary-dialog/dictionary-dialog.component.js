class DictionaryDialogController {
    constructor() {
        "ngInject";
        this.dictionary = null;
        this.words = [];

        this.totalCount = 0;
        this.activeWordIdx = 0;
    }

    $onInit() {
        this.words = this.resolve.words;
        this.dictionary = this.resolve.dictionary;
        this.totalCount = this.words.length;
    }

    next() {
        if (this.activeWordIdx === (this.words.length - 1)) {
            this.activeWordIdx = 0;
        } else {
            this.activeWordIdx++;
        }
    }

    prev() {
        if (this.activeWordIdx === 0) {
            this.activeWordIdx = this.words.length - 1;
        } else {
            this.activeWordIdx--;
        }
    }

    get activeWord() {
        if (this.activeWordIdx >= 0) {
            return this.words[this.activeWordIdx];
        }

        return null;
    }

    cancel() {
        this.modalInstance.close();
    }
}


export default {
    template: require('./dictionary-dialog.html'),
    controller: DictionaryDialogController,
    bindings: {
        modalInstance: '<',
        resolve: '<'
    }
};