class SelectExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper;
    }

    init() {
        this.activeRound = null;
        this.result = null;
        this.showAnswers = false;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    setAnswerVisibility(isVisible = true) {
        this.showAnswers = isVisible;
    }

    checkAnswer() {
        let result = true;
        let successRoundSteps = 0;
        this.activeRound.elements.filter(el => el.skip).forEach(e => {
                if (e.text !== (e.answer ? e.answer.text : '')) {
                    e.hasError = true;
                    result = false;
                } else {
                    successRoundSteps++;
                }
            }
        );

        return {result, progress: {success_round_steps: successRoundSteps}};
    }
}

export default {
    template: require('./select-exercise.html'),
    controller: SelectExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
}