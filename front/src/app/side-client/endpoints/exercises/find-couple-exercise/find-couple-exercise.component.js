class FindCoupleExerciseController {
    constructor(randomFilter) {
        "ngInject";
        this.init();
        this.randomFilter = randomFilter;
        this.colors = this.randomFilter([
            '#ff6170', '#ffbcc3', '#ff9ba5', '#ffb75b',
            '#fecb8c', '#fee0ba', '#feef88', '#fff7c9',
            '#9bd976', '#c0e7a8', '#ddf1ce', '#00aadd',
            '#7fceed', '#c2e8f5', '#6686b5', '#b4c3da',
            '#dae2ed', '#ff7fc9', '#ffb4df', '#ffdaef'
        ]);

        this.nextColorIndex = 0;
    }

    init() {
        this.activeRound = null;
        this.firstElements = [];
        this.secondElements = [];
        this.selectedFirst = null;
        this.selectedSecond = null;
        this.showErrors = false;
        this.showAnswers = false;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
            this.activeRound.couples.map((couple) => {
                let firstElement = couple.first();
                let secondElement = couple.second();
                let color = this.generateColor();
                firstElement.answerColor = secondElement.answerColor = color;
                firstElement.answer = secondElement;
                secondElement.answer = firstElement;
                firstElement.selectedAnswer = secondElement.selectedAnswer = null;
                this.firstElements.push(firstElement);
                this.secondElements.push(secondElement);
            });
            this.firstElements = this.randomFilter(this.firstElements);
            this.secondElements = this.randomFilter(this.secondElements);
        }
    }

    generateColor() {
        const colorsCount = this.colors.length;
        const color = this.colors[this.nextColorIndex];
        this.nextColorIndex = this.nextColorIndex === (colorsCount - 1) ? 0 : this.nextColorIndex + 1;
        return color;
    }

    selectWord(element, isFirst) {
        // если кликнули по тегу в режиме просмотра ответов - не делаем ничего
        if (this.showAnswers) {
            return;
        }
        this.showErrors = false;
        // если кликнули по тегу с уже связанным вариантом ответа - снимаем выледение с обоих
        if (element.selectedAnswer) {
            let answerElement = element.selectedAnswer;
            answerElement.selectedAnswer = null;
            element.selectedAnswer = null;
        } else {
            if (isFirst) {
                if (this.selectedFirst === element) {
                    // если кликнули два раза по одному и тому же элементу - снимаем выледение
                    this.selectedFirst = null;
                } else {
                    // если кликнули по элементу - выделяем его
                    this.selectedFirst = element;
                }
            } else {
                if (this.selectedSecond === element) {
                    this.selectedSecond = null;
                } else {
                    this.selectedSecond = element;
                }
            }
        }

        // если выбраны оба - сохраняем в теги выбранные связи и обнуляем ссылки
        if (this.selectedFirst && this.selectedSecond) {
            this.selectedFirst.selectedAnswer = this.selectedSecond;
            this.selectedSecond.selectedAnswer = this.selectedFirst;
            const color = this.generateColor();
            this.selectedFirst.color = color;
            this.selectedSecond.color = color;

            this.selectedFirst = null;
            this.selectedSecond = null;
        }
    }

    checkAnswer() {
        this.showErrors = true;
        // число правильно выбранных пах
        const successRoundSteps = this.firstElements.filter((element) => {
            return element.answer && element.selectedAnswer && element.answer === element.selectedAnswer
        }).length;

        // если для всех слов в колонке выбраны соответствующие правильные ответы result = true
        const result = successRoundSteps === this.firstElements.length;
        return {result, progress: {success_round_steps: successRoundSteps}};
    }

    setAnswerVisibility(isVisible = true) {
        this.showAnswers = isVisible;
    }
}

export default {
    template: require('./find-couple-exercise.html'),
    controller: FindCoupleExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};