class FreeFormExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.DataHelper = DataHelper;
        this.init();
    }

    init() {
        this.activeRound = null;
        this.result = null;
        this.answer = null;
        this.currentUserAnswer = null;
    }

    onRecorded({src}) {
        this.src = src;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    setAnswerVisibility(isVisible = true) {
        if (isVisible) {
            this.currentUserAnswer = this.answer;
            this.answer = this.activeRound.answer;
        } else {
            this.answer = this.currentUserAnswer;
        }
    }

    checkAnswer() {
        let result = this.DataHelper.clearStr(this.answer) === this.DataHelper.clearStr(this.activeRound.answer);
        return {result, progress: result ? 100 : 0};
    }
}

export default {
    template: require('./listen-write-exercise.html'),
    controller: FreeFormExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};