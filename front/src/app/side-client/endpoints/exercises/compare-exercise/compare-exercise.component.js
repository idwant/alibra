import Exercise from 'api/models/Exercise';

class CompareController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper;
    }

    init() {
        this.activeRound = null;
        this.result = null;
        this.audioRecord = null;
        this.text = null;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    onRecorded({src}) {
        this.audioRecord = src;
    }

    onTextChanged({text}) {
        this.text = text;
    }

    checkAnswer() {
        let result = this.DataHelper.clearStr(this.text) === this.DataHelper.clearStr(this.activeRound.answer);
        return {result, progress: result ? 100 : 0};
    }
}

export default {
    template: require('./compare-exercise.html'),
    controller: CompareController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};