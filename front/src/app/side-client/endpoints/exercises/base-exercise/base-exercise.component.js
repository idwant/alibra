import Exercise from 'api/models/Exercise';
import UserExercise from  'api/models/UserExercise';
import {APP_EVENTS} from './../../../../constants';

class BaseExerciseController {
    constructor($log, $element, $compile, $state, $scope, $timeout, alert, $uibModal,
                ExerciseManager, VoiceRecorderManager, StudentCourseManager, DictionaryManager
    ) {
        'ngInject';

        this.$log = $log;
        this.$element = $element;
        this.$compile = $compile;
        this.$scope = $scope;
        this.$state = $state;
        this.$timeout = $timeout;
        this.ExerciseManager = ExerciseManager;
        this.StudentCourseManager = StudentCourseManager;
        this.VoiceRecorderManager = VoiceRecorderManager;
        this.DictionaryManager = DictionaryManager;
        this.alert = alert;
        this.$uibModal = $uibModal;

        this.exerciseIndex = parseInt($state.params.exerciseIndex || 0);
        this.courseId = $state.params.courseId;
        this.unitId = $state.params.unitId;
        this.unitIndex = parseInt($state.params.unitIndex || 0);
        this.lessonId = $state.params.lessonId;
        this.lessonIndex = parseInt($state.params.lessonIndex || 0);
        this.lessonPartId = $state.params.lessonPartId;
        this.exerciseType = $state.params.exerciseType;

        this.exercises = [];
        this.exercisesCopy = [];
        this.exercise = null;
        this.currentRound = -1;
        this.exerciseAnimate = false;
        this.roundAnimate = false;
        this.viewParams = null;
        this.exerciseInited = false;
        this.words = [];
    }

    $onInit() {
        if ((!this.courseId && !this.unitId && !this.lessonId && !this.lessonPartId) || !this.exerciseType) {
            return this.$state.go('dashboard');
        }

        if(this.courseId){
            this.StudentCourseManager
                .getById(this.courseId)
                .then(course => {this.course = course});
        }

        this.ExerciseManager
            .getStudentExercises({
                course_id: this.courseId,
                unit_id: this.unitId,
                lesson_id: this.lessonId,
                lesson_part_id: this.lessonPartId,
                exercise_type: this.exerciseType
            })
            .then(exercises => {
                this.exercises = exercises;
                this.exercisesCopy = angular.copy(exercises);

                //первый невыполненный
                if (!this.exerciseIndex) {
                    this.exercises.some((exercise, index) => {

                        if (exercise.user_exercise.status != UserExercise.STATUS_SUCCESS) {
                            this.exerciseIndex = index;

                            return true;
                        }
                    });
                }

                this.goToExercise(this.exerciseIndex, false);
            });
    }

    goToExercise(index, storeHistory = true) {
        this.words = [];
        this.$element.find('#exercise').empty();

        this.currentRound = 0;
        this.exercise = this.exercises[index];
        this.exerciseInited = false;
        this.exerciseIndex = index + 1;

        console.log(this.exercise);

        if (!this.exercise) {
            return this.$state.go('dashboard');
        }

        if(this.exercise.dictionary) {
            this.DictionaryManager
                .getWords({dictionaryId: parseInt(this.exercise.dictionary.id)})
                .then(([words, headers]) => this.words = words);
        }

        this.$state.go('exercise', Object.assign(this.$state.params, {
            exerciseIndex: index,
            lessonId: this.lessonId
        }), storeHistory ? {notify: false} : {location: "replace", notify: false});

        if (!this.exercise.user_exercise.completed) {
            this.ExerciseManager.startStudentExercise({
                exercise_id: this.exercise.id,
                lesson_id: this.exercise.lesson.id
            });
        }

        this.initViewParams();
        this.onExerciseChange();
        this.$timeout(() => this.compileExerciseTemplate());
        this.$timeout(() => this.exerciseInited = true, 100);
    }

    nextRound() {
        if (this.currentRound < this.exercise.getCountRounds()) {
            this.currentRound++;
        }
        this.onRoundChange();
    }

    previousRound() {
        if (this.currentRound > 0) {
            this.currentRound--;
        }
        this.onRoundChange();
    }

    onRoundComplete({result, progress}) {
        const currentRound = this.exercise.params[this.currentRound];
        // если в раунде есть этапы, сохраняем прогресс
        if (this.exercise.hasRoundsWithSteps()) {
            currentRound.progress = {
                success_round_steps: progress.success_round_steps,
                total_round_steps: currentRound.getStepsCount()
            }
        }

        currentRound.result = result;
        if (result) {
            this.alert({title: "Good job!", text: "Round completed!", type: "success"}, () => this.$scope.$apply(() => {

                if (this.currentRound === this.exercise.params.length - 1) {
                    return this.onExerciseComplete();
                }

                this.nextRound();
            }));
        } else if (result === false) {
            const modalCb = (next) => {
                if (!next) {
                    return;
                }

                if (this.currentRound === this.exercise.params.length - 1) {
                    return this.onExerciseComplete();
                }

                this.nextRound();
            };

            if (this.exercise.hasRoundsWithSteps()) {
                swal({
                    title: "Result",
                    text: `
                        <div class="result-msg-block">
                            <div class="result-msg">
                                <div class="result-line"><div class="sa-icon sa-success"><span class="sa-line sa-tip"></span><span class="sa-line sa-long"></span></div> Correct: ${progress.success_round_steps}</div>
                                <div class="result-line"><div class="sa-icon sa-error"><span class="sa-x-mark"><span class="sa-line sa-left"></span><span class="sa-line sa-right"></span></span></div> Wrong: ${currentRound.getStepsCount() - progress.success_round_steps}</div>
                            </div>
                        </div>
                    `,
                    showCancelButton: true,
                    confirmButtonText: "Next",
                    cancelButtonText: "Try again",
                    html: true
                }, modalCb);
            } else {
                swal({
                    title: "You can do it better!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonText: "Next",
                    cancelButtonText: "Try again",
                }, modalCb);
            }

        }
    }

    onRoundChange() {
        // принудительно заканчиваем запись звука при смене раунда
        this.VoiceRecorderManager.stop();

        this.roundAnimate = true;
        this.$timeout(() => this.roundAnimate = false, 500);
    }

    onExerciseComplete() {
        let percent = 0;
        // если раунд упражнения разделен на задания, которые надо считать -
        // считаем прогресс упражнения по прогрессу всех выполненных заданий в раундах
        if (this.exercise.hasRoundsWithSteps()) {
            percent = (
                this.exercise.params.reduce((total, cur) => total += cur.progress ? +cur.progress.success_round_steps : 0, 0) /
                this.exercise.getRoundStepsCount() * 100
            );
        } else {
            percent = this.exercise.params.reduce((total, cur) => total += +cur.result, 0) / this.exercise.params.length * 100;
        }

        let result = this.exercise.params.map(r => {
            return {success: !!r.result, progress: r.progress ? r.progress : null}
        });

        this.exercise.user_exercise.completed = {
            percent,
            result
        };

        this.ExerciseManager.finishStudentExercise({
            exercise_id: this.exercise.id,
            lesson_id: this.exercise.lesson.id,
            completed: this.exercise.user_exercise.completed
        });
    }

    onExerciseChange() {
        this.exerciseAnimate = true;
        this.$timeout(() => this.exerciseAnimate = false, 500);
    }

    initViewParams() {
        this.viewParams = {
            showRoundNavigation: true,
            showTaskBtn: false,
            showTask: false
        };

        this.setCompletedClass();
    }

    setCompletedClass() {
        this.$timeout(() => {
            $('.pagination-page').removeClass('completed');
            this.exercises.forEach((e, index) => e.user_exercise.completed ? $('#exercise_' + (index + 1)).addClass('completed') : null);
        });
    }

    tryAgain() {
        this.exercises[this.exerciseIndex - 1] = Object.assign(
            new Exercise(),
            angular.copy(this.exercisesCopy[this.exerciseIndex - 1])
        );

        this.exercises[this.exerciseIndex - 1].tryAgain();

        this.goToExercise(this.exerciseIndex - 1);
    }

    showGrammar(grammar) {
        this.$uibModal.open({
            component: 'grammarDialog',
            windowClass: 'grammar-dialog-modal',
            resolve: {
                grammar: function () {
                    return grammar;
                }
            }
        });
    }

    showDictionary(words, dictionary) {
        this.$uibModal.open({
            component: 'dictionaryDialog',
            windowClass: 'dictionary-dialog-modal',
            resolve: {
                words: function () {
                    return words;
                },
                dictionary: function() {
                    return dictionary;
                }
            }
        });
    }

    compileExerciseTemplate() {
        let template = '';

        switch (this.exercise.template_type.toLowerCase()) {
            case 'compare':
                template += `<compare-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </compare-exercise>`;
                break;
            case 'dialog-exercise':
                template += `<dialog-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </dialog-exercise>`;
                break;
            case 'listen-say':
                template += `<listen-say-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </listen-say-exercise>`;
                break;
            case 'free-form':
                template += `<free-form-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </free-form-exercise>`;
                break;
            case 'crossword':
                template += `<crossword-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </crossword-exercise>`;
                break;
            case 'read-write':
                template += `<read-write-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </read-write-exercise>`;
                break;
            case 'drag-and-drop':
                template += `<drag-and-drop-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </drag-and-drop-exercise>`;
                break;
            case 'listen-write':
                template += `<listen-write-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-round-complete="$ctrl.onRoundComplete({result, progress})">
                            </listen-write-exercise>`;
                break;
            case 'five-cards':
                this.viewParams.showRoundNavigation = false;
                template += `<five-cards-exercise exercise="$ctrl.exercise"
                                               round="$ctrl.currentRound"
                                               ng-class="{'round_animate': $ctrl.roundAnimate}"
                                               on-exercise-complete="$ctrl.onExerciseComplete(result)"
                                               next-round="$ctrl.nextRound()">
                            </five-cards-exercise>`;
                break;
            case 'picture':
                template += `
                    <picture-exercise exercise="$ctrl.exercise"
                                    round="$ctrl.currentRound"
                                    ng-class="{'round_animate': $ctrl.roundAnimate}"
                                    on-round-complete="$ctrl.onRoundComplete({result, progress})">
                    </picture-exercise>`;
                break;
            case 'snowball':
                template += `
                     <snowball-exercise exercise="$ctrl.exercise" 
                                    round="$ctrl.currentRound"
                                    ng-class="{'round_animate': $ctrl.roundAnimate}"
                                    on-round-complete="$ctrl.onRoundComplete({result, progress})">
                    </snowball-exercise>`;
                break;
            case 'select':
                template += `
                    <select-exercise exercise="$ctrl.exercise"
                                    round="$ctrl.currentRound"
                                    ng-class="{'round_animate': $ctrl.roundAnimate}"
                                    on-round-complete="$ctrl.onRoundComplete({result, progress})">
                    </select-exercise>`;
                break;
            case 'pick-out':
                template += `
                    <pick-out-exercise exercise="$ctrl.exercise"
                                    round="$ctrl.currentRound"
                                    ng-class="{'round_animate': $ctrl.roundAnimate}"
                                    on-round-complete="$ctrl.onRoundComplete({result, progress})">
                    </pick-out-exercise>`;
                break;
            case 'listen-read-click':
                template += `
                    <listen-read-click-exercise exercise="$ctrl.exercise"
                                    round="$ctrl.currentRound"
                                    ng-class="{'round_animate': $ctrl.roundAnimate}"
                                    on-round-complete="$ctrl.onRoundComplete({result, progress})">
                    </listen-read-click-exercise>`;
                break;
            case 'find-couple':
                template += `
                    <find-couple-exercise exercise="$ctrl.exercise"
                                    round="$ctrl.currentRound"
                                    ng-class="{'round_animate': $ctrl.roundAnimate}"
                                    on-round-complete="$ctrl.onRoundComplete({result, progress})">
                    </find-couple-exercise>`;
                break;
            default:
                return '';
        }

        // Создаем изолированный скоуп, для каждого темплейта, чтобы избежать зависимости упражнений от друг друга
        let scope = this.$scope.$new(true);
        this.$scope.$watch(
            "$ctrl.currentRound",
            (newValue) => {
                scope.$ctrl.currentRound = newValue;
            }
        );

        scope.$ctrl = {
            exercise: this.exercise,
            currentRound: this.currentRound,
            onRoundComplete: this.onRoundComplete.bind(this),
            onExerciseComplete: this.onExerciseComplete.bind(this),
            nextRound: this.nextRound.bind(this),
            roundAnimate: this.roundAnimate
        };

        this.$element.find('#exercise').html(this.$compile(template)(scope));
    }

    isHomeWork() {
        return (this.exerciseType === Exercise.TYPE_HOMEWORK);
    }

    isTest() {
        return (this.exerciseType === Exercise.TYPE_TEST);
    }
}

export default {
    template: require('./base-exercise.html'),
    controller: BaseExerciseController
}