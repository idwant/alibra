class FreeFormExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.DataHelper = DataHelper;
        this.init();
    }

    init() {
        this.activeRound = null;
        this.result = null;
        this.answer = null;
    }

    onRecorded({src}) {
        this.src = src;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    checkAnswer() {
        let possibleWords =  this.activeRound.possible_answers.map((answer) => {
            return this.DataHelper.clearStr(answer.text);
        });

        if (possibleWords.length === 0) {
            return {result: true, progress: 100};
        }

        if (!this.answer) {
            this.answer = ''
        }

        let intersectionWords = possibleWords.filter((word) => {
            return this.DataHelper.clearStr(this.answer).indexOf(word) !== -1;
        });

        let result = !!intersectionWords.length;

        return {result, progress: result ? 100 : 0};
    }
}

export default {
    template: require('./free-form-exercise.html'),
    controller: FreeFormExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};