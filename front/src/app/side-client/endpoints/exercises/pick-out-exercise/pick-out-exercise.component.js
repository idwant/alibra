class PickOutExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.DataHelper = DataHelper;
        this.init();
    }

    init() {
        this.activeRound = null;
        this.selectedElements = [];
        this.showErrors = false;
        this.showAnswers = false;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    toggleElement(element) {
        this.showErrors = false;
        const elementIndex = this.selectedElements.indexOf(element);
        if (elementIndex === -1) {
            this.selectedElements.push(element);
        } else {
            this.selectedElements.splice(elementIndex, 1);
        }
    }

    isSelected(element) {
        return this.selectedElements.indexOf(element) !== -1;
    }

    setAnswerVisibility(isVisible = true) {
        this.showAnswers = isVisible;
    }

    checkAnswer() {
        this.showErrors = true;
        const answers = this.activeRound.elements.filter(e => e.answer);
        const result = this.DataHelper.compareArrays(
            answers,
            this.selectedElements
        );

        let successRoundSteps = this.activeRound.elements
            .reduce((total, el) => {
                if((el.answer && this.selectedElements.includes(el)) || (!el.answer && !this.selectedElements.includes(el))) {
                    total++
                }
                return total;
            }, 0)
        ;
        successRoundSteps = successRoundSteps > 0 ? successRoundSteps : 0;

        return {result, progress: {success_round_steps: successRoundSteps}};
    }
}

export default {
    template: require('./pick-out-exercise.html'),
    controller: PickOutExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
}