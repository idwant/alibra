class FiveCardsExerciseController {
    constructor(DataHelper, $timeout) {
        "ngInject";
        this.DataHelper = DataHelper;
        this.$timeout = $timeout;

        this.progress = 0;
        this.activeRoundIndex = null;

        this.init();
    }

    init() {
        this.activeRoundIndex = this.round;
        this.activeRound = this.exercise.getRound(this.round);
        this.lock = false;
        this.cardWasOpened = false;
    }

    $doCheck() {
        if (this.activeRoundIndex !== this.round) {
            this.init();
        }
    }

    checkAnswer(card) {
        if (card.open) {
            let result = card.selected_answer ? this.DataHelper.clearStr(card.selected_answer.text) === this.DataHelper.clearStr(card.answer) : false;
            let progress = result ? this.activeRound.hintHasShown ? 0.5 : 1 : 0;

            return {result, progress};
        }
    }

    skip(card) {
        return this.onRoundComplete({result: false, progress: 0}, card, true);
    }

    onRoundComplete({result, progress}, card, skip) {
        if (skip || (card.open && (result || (this.activeRound.attempts >= this.activeRound.maxAttempts)))) {
            card.open = false;
            this.lock = true;
            this.progress += progress;
            this.activeRound.result = progress === 1;

            if (this.round === 4) {
                this.onExerciseComplete({result: true});
            }

            this.$timeout(() => this.nextRound(), 700);
        }
    }
}

export default {
    template: require('./five-cards-exercise.html'),
    controller: FiveCardsExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onExerciseComplete: '&',
        nextRound: '&'
    }
};
