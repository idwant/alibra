class CrosswordExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper;
        this.isGame = true;
    }

    init() {
        this.activeRound = null;
        this.percent = 0;
        this.correct_answers = [];
    }

    onProgress({correct_answers, percent}) {
        this.percent = percent;
        this.correct_answers = correct_answers;
    }

    $onChanges(changesObj) {
        if (changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    setAnswerVisibility(isVisible) {
        this.isGame = !isVisible;
    }

    // Небольшой костыль проверки/получения картинки по слову что бы не перегенеривать кроссворды
    getImageByWord(word) {
        let image;
        this.activeRound.items.forEach(i => {
            i.answer.toLowerCase() == word.toLowerCase() ? image = i.image : null
        });

        return image;
    }

    checkAnswer() {
        let result = +this.percent === 100;
        return {result, progress: {success_round_steps: this.correct_answers.length}};
    }
}

export default {
    template: require('./crossword-exercise.html'),
    controller: CrosswordExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};