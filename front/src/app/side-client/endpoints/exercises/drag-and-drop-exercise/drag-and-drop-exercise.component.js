class ReadWriteExerciseController {
    constructor(DataHelper, randomFilter) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper;
        this.randomFilter = randomFilter;
        this.possible_answers = [];
        this.showAnswers = false;
    }

    init() {
        this.activeRound = null;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
            this.possible_answers = this.randomFilter(Object.assign([], this.activeRound.possible_answers));
        }
    }

    checkAnswer() {
        let result = true,
            successRoundSteps;

        this.activeRound.elements.forEach(e => {
                if (e.text !== (e.answer[0] ? e.answer[0].text : '')) {
                    e.hasError = true;
                    result = false;
                }
            }
        );

        successRoundSteps = this.activeRound.elements
            .filter(e => e.skip && e.text === (e.answer[0] ? e.answer[0].text : ''))
            .length
        ;

        return {result, progress: {success_round_steps: successRoundSteps}};
    }

    setAnswerVisibility(isVisible = true) {
        this.showAnswers = isVisible;
    }
}

export default {
    template: require('./drag-and-drop-exercise.html'),
    controller: ReadWriteExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};