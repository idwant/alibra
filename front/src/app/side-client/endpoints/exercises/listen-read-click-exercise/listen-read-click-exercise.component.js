class ListenReadClickExerciseController {
    constructor() {
        "ngInject";
        this.init();
    }

    init() {
        this.activeRound = null;
        this.selectedElement = null;
        this.showErrors = false;
        this.showAnswers = false;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    toggleElement(element) {
        this.showErrors = false;
        this.selectedElement = element;
    }

    isSelected(element) {
        return this.selectedElement === element;
    }

    setAnswerVisibility(isVisible = true) {
        this.showAnswers = isVisible;
    }

    checkAnswer() {
        this.showErrors = true;
        const result = this.activeRound.elements.filter(e => e.answer).includes(this.selectedElement);

        return {result, progress: {success_round_steps: result ? 1 : 0}};
    }
}

export default {
    template: require('./listen-read-click-exercise.html'),
    controller: ListenReadClickExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
}