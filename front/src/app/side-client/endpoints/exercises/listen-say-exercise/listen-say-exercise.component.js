class ListenSayExerciseController {
    constructor(DataHelper) {
        "ngInject";
        this.init();
        this.DataHelper = DataHelper;
    }

    init() {
        this.activeRound = null;
        this.result = null;
        this.src = null;
        this.text = null;
    }

    onRecorded({src}) {
        this.src = src;
    }

    onTextChanged({text}) {
        this.text = text;
    }

    $onChanges(changesObj) {
        if(changesObj.round) {
            this.init();
            this.activeRound = this.exercise.getRound(parseInt(changesObj.round.currentValue));
        }
    }

    checkAnswer() {
        let result = this.DataHelper.clearStr(this.activeRound.answer) === this.DataHelper.clearStr(this.text);
        return {result, progress: result ? 100 : 0};
    }
}

export default {
    template: require('./listen-say-exercise.html'),
    controller: ListenSayExerciseController,
    bindings: {
        exercise: '<',
        round: '<',
        onRoundComplete: '&'
    }
};