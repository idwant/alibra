class GroupsController {
    constructor($log, $state, $stateParams, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;

        this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.category = null;
        this.categoryId = $stateParams.categoryId;

        this.groups = [];

        this.contentType = 'vocabulary';
    }

    $onInit() {
        this._loadCategory()
            .then(() => {
                return this._loadGroups();
            });
    }

    filterGroups(actual, expected) {
        return actual > expected;
    }

    _loadCategory() {
        return this.StudentWordCardManager
            .getCategoryById(this.categoryId)
            .then(category => this.category = category)
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadGroups() {
        return this.StudentWordCardManager
            .getGroups({category_id: this.categoryId})
            .then(groups => this.groups = groups)
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./vocabulary-groups.html'),
    controller: GroupsController
};