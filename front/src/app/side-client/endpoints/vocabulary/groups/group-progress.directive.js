const COLOR_ORANGE = 'e8a72d';
const COLOR_GREEN = '618811';
const COLOR_BLUE = '4a83ca';
const COLOR_CRIMSON = 'EC3B83';
const COLOR_RED = 'cb2228';

const CLASS_LEARN_NOT_STARTED = 'learn-not-started';
const CLASS_LEARN_IN_PROGRESS = 'learn-in-progress';
const CLASS_REPEAT_IN_PROGRESS = 'repeat-in-progress';
const CLASS_REPEAT_OVERDUE = 'repeat-overdue';
const CLASS_DONE = 'done';

class GroupProgressController {
    constructor($log, $scope, $state, moment, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.group = $scope.group;
        
        //Исользуется для вывода заголовка в контент
        this.contentType = $scope.contentType;
        
        this.$state = $state;

        this.moment = moment;
    }

    $onInit(change) {
    }

    get progressValue() {
        if (this.group.notStarted) {
            return this.group.total_count;
        } else if (this.group.partiallyLearned) {
            return this.group.learned_count;
        } else if (this.group.fullyLearned) {
            return this.group.learned_count;
        }

        return this.group.total_count;
    }

    get progressTotal() {
        return this.group.total_count;
    }

    get progressColor() {

        if (this.group.notStarted) {
            return COLOR_ORANGE;
        } else if (this.group.partiallyLearned) {
            return COLOR_BLUE;
        } else if (this.group.fullyLearned) {
            if (this.group.isDone) {
                return COLOR_GREEN;
            } else {
                if (this.group.isReadyToRepetition) {
                    return COLOR_RED;
                }
                return COLOR_CRIMSON;
            }
        }

        return COLOR_ORANGE;
    }

    get progressClass() {
        if (this.group.notStarted) {
            return CLASS_LEARN_NOT_STARTED;
        } else if (this.group.partiallyLearned) {
            return CLASS_LEARN_IN_PROGRESS;
        } else if (this.group.fullyLearned) {
            if (this.group.isDone) {
                return CLASS_DONE;
            } else {
                if (this.group.isReadyToRepetition) {
                    return CLASS_REPEAT_OVERDUE;
                }
                return CLASS_REPEAT_IN_PROGRESS;
            }
        }

        return CLASS_LEARN_NOT_STARTED;
    }

    get progressTitle() {
        if (this.group.notStarted || this.group.partiallyLearned) {
            return this.group.learnPercent + '%';
        } else if (this.group.fullyLearned) {
            return `${this.group.min_repetition_step}/${this.group.max_repetition_step}`;
        }
    }

    learn(group) {
        if (!group.fullyLearned) {
            this.$state.go('vocabularyLearnGroup', {'wordGroupId': group.id});
        } else if (this.group.isReadyToRepetition) {
            this.$state.go('vocabularyRepetitionWords', {'wordGroupId': group.id, 'contentType': this.contentType})
        } else {
            this.$state.go('vocabularyReview', {'wordGroupId': group.id});
        }
    }
}

export default () => ({
    template: require('./group-progress.html'),
    controller: GroupProgressController,
    controllerAs: '$ctrl',
    replace: true,
    scope: {
        group: '<',
        contentType: '<'
    }
});