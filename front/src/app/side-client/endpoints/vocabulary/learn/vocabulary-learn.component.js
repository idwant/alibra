import Exercise from 'api/models/Exercise';

class LearnController {
    constructor($log, $state, $stateParams, alert, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.$stateParams = $stateParams;

        this.alert = alert;
        this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.groupId = $state.params.wordGroupId;
        this.group = null;
        this.lessonId = $state.params.lessonId;

        //для перехода на домашнее задание после изучения
        this.exerciseType = Exercise.TYPE_HOMEWORK;

        this.cards = [];
        this.totalCount = 0;
        this.activeCard = null;
        this.showTranslate = false;
        this.loaded = false;
        this.postponeLearnCount = 0;
    }

    $onInit() {
        this._loadGroup()
            .then(() => this._loadCards())
            .then(() => this.loaded = true);
    }

    knowIt() {
        this.StudentWordCardManager
            .learn(this.activeCard)
            .then(card => this._nextCard())
            .catch((response) => this.AlertResponse.fail(response));
    }

    repeatAgain() {
        this.StudentWordCardManager
            .postponeLearn(this.activeCard)
            .then(card => {
                this.postponeLearnCount++;
                this._nextCard();
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    disable() {
        this.StudentWordCardManager
            .disable(this.activeCard)
            .then(card => this._nextCard())
            .catch((response) => this.AlertResponse.fail(response));
    }

    get learnDone() {
        if (this.group) {
            return (this.postponeLearnCount === 0) && (this.group.learn_postpone_count === 0);
        }

        return false;
    }

    _nextCard() {
        this.activeCard = this.cards.length > 0 ? this.cards.shift() : null;
        this.showTranslate = false;
    }

    _loadGroup() {
        return this.StudentWordCardManager
            .getGroups({group_id: this.groupId})
            .then(groups => this.group = groups[0])
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadCards() {
        return this.StudentWordCardManager
            .getLearnWordCards({group_id: this.groupId, lesson_id: this.lessonId})
            .then(cards => {
                this.cards = cards;
                this.totalCount = cards.length;
                this.cards.forEach((card, idx) => card.idx = idx);
                this._nextCard();
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./vocabulary-learn.html'),
    controller: LearnController
};

