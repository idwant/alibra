class CategoriesController {
    constructor($log, $state, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;

        this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.categories = [];
    }

    $onInit() {
        this._loadCategories();
    }

    _loadCategories() {
        this.StudentWordCardManager
            .getCategories()
            .then(categories => this.categories = categories)
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./vocabulary-categories.html'),
    controller: CategoriesController
};

