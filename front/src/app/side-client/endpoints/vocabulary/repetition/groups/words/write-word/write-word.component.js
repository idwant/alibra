class WriteWordController {
    constructor($log, $scope, alert, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;

        this.alert = alert;
        this.AlertResponse = AlertResponse;
        
        this.maxAttempts = 2;
        this.startNew();
    }

    $onInit() {
        this.registerWatcher();
    }

    check() {
        if (this.enteredWord.toUpperCase() === this.card.word.word.toUpperCase()) {
            if (this.showInfo) {
                if (this.isIncorrect) {
                    this.onFail().then(() => this.$scope.$broadcast('repetition:done'));
                } else {
                    this.onSuccess().then(() => this.$scope.$broadcast('repetition:done'));
                }
                return;
            }
            this.showInfo = true;
        } else if (this.enteredWord.trim() !== '') {
            this.attempt++;
        }

        this.$scope.$broadcast('repetition:done');
    }


    get isIncorrect() {
        return this.attempt >= this.maxAttempts;
    }

    startNew() {
        this.enteredWord = '';
        this.showInfo = false;
        this.attempt = 0;
    }

    registerWatcher() {
        this.$scope.$watch(() => this.userCard, () => {
            if (this.userCard) {
                this.card = this.userCard.word_card;
                this.startNew();
            }
        });
    }
}

export default {
    template: require('./write-word.html'),
    controller: WriteWordController,
    bindings: {
        userCard: '=',
        onSuccess: '&',
        onFail: '&'
    }
}