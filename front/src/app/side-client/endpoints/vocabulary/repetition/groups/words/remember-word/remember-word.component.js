class RememberWordController {
    constructor($log, $rootScope, $scope, alert, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;

        this.alert = alert;
        this.AlertResponse = AlertResponse;

        this.card = null;

        this.startNew();
    }

    $onInit() {
        this.registerWatcher();
    }

    correct() {
        this.onSuccess().then(() => this.$scope.$broadcast('repetition:done'));
    }

    wrong() {
        this.onFail().then(() => this.$scope.$broadcast('repetition:done'));
    }

    startNew() {
        this.showAnswer = false;
    }

    registerWatcher() {
        this.$scope.$watch(() => this.userCard, () => {
          if (this.userCard) {
              this.card = this.userCard.word_card;
              this.startNew();
          }
      })
    }
}

export default {
    template: require('./remember-word.html'),
    controller: RememberWordController,
    bindings: {
        userCard: '=',
        onSuccess: '&',
        onFail: '&'
  }
}
