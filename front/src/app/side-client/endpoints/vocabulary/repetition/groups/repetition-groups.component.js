class RepetitionGroupsController {
    constructor($log, $state, $stateParams, $scope, alert, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.$scope = $scope;

        this.alert = alert;
        this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.groups = [];
        this.isLoaded = false;
        this.isFullyGroup = false;
        this.contentType = 'revision-calendar';
    }

    $onInit() {
        this._loadGroups();
    }

    _loadGroups() {
        this.StudentWordCardManager
            .getGroups({})
            .then(groups => {
                this.isFullyGroup = groups.filter(group => group.fullyLearned) ? true : false;
                this.groups = groups.filter(group => group.isReadyToRepetition);
                this.isLoaded =true;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    filterGroups(actual, expected) {
        return actual > expected;
    }

}

export default {
    template: require('./repetition-groups.html'),
    controller: RepetitionGroupsController,
}