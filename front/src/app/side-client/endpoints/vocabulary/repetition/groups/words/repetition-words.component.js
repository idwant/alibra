class RepetitionWordsController {
    constructor($log, $state, $stateParams, $scope, alert, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.$scope = $scope;

        this.alert = alert;
        this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.groupId = $state.params.wordGroupId;
        this.contentType = $state.params.contentType;

        this.cards = [];
        this.group = [];
        this.activeCard = null;
        this.activeTab = 'find';
        this.repeatDone = false;
        this.contentTitle = '';
    }

    $onInit() {
        this._loadCards();
        this._loadGroup();

        switch(this.contentType) {
            case 'vocabulary':
                this.contentTitle = 'Vocabulary';
                break;
            case 'revision-calendar':
                this.contentTitle = 'Revision Calendar';
                break;
            default: break;
        }
    }

    completeCard() {
        return this.StudentWordCardManager
            .repeat(this.activeCard)
            .then(card => {
                this._nextCard();
            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    _nextCard() {
        this.activeCard = this.cards.length > 0 ? this.cards.shift() : null;
        this.repeatDone = !this.activeCard;
    }

    _loadCards() {
        this.StudentWordCardManager
            .getRepeatWordCards(this.groupId)
            .then(cards => {
                this.cards = cards;
                this._nextCard();

            })
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadGroup() {
        this.StudentWordCardManager
            .getGroupById(this.groupId)
            .then(group => this.group = group)
            .catch((response) => this.AlertResponse.fail(response));
    }

    get nextRepetitionDate() {
        moment.locale('ru');
        let repetitionDate = moment(this.group.max_repetition_date);
        return repetitionDate.format('Do MMMM');
    }
}

export default {
    template: require('./repetition-words.html'),
    controller: RepetitionWordsController
}