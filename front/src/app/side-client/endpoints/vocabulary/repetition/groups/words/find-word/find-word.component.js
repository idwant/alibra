class FindWordController {
    constructor($log, $scope, alert, AlertResponse, $interval) {
        "ngInject";

        this.$log = $log;
        this.$scope = $scope;

        this.alert = alert;
        this.AlertResponse = AlertResponse;

        this.maxAttempts = 2;
        this.startNew();
        this.$interval = $interval;
    }

    $onInit() {
        this.registerWatcher();
    }

    check() {
        if (this.activeIndex === this.correctIndex) {
            if (this.isIncorrect) {
                this.onFail().then(() => this.$scope.$broadcast('repetition:done'));
            } else {
                this.onSuccess().then(() => this.$scope.$broadcast('repetition:done'));
            }
            return;
        }

        this.attempt++;
        this.$scope.$broadcast('repetition:done');
    }

    get isIncorrect() {
        return this.attempt >= this.maxAttempts
    }

    refreshPossibleAnswers() {
        this.correctIndex = this.randomInteger(0, 2);

        this.possibleAnswers = [];
        for (let i = 0; i < 3; i++) {
            if (i == this.correctIndex) {
                this.possibleAnswers.push(this.card.word);
            } else {
                this.possibleAnswers.push(this.userCard.possible_answers[i]);
            }
        }
    }

    startNew() {
        this.attempt = 0;
        this.activeIndex = -1;
        this.correctIndex = null;
    }

    registerWatcher() {
        this.$scope.$watch(() => this.userCard, () => {
            if (this.userCard) {
                this.card = this.userCard.word_card;
                this.startNew();
                this.refreshPossibleAnswers();
            }
        });
    }

    randomInteger(min, max) {
        var rand = min - 0.5 + Math.random() * (max - min + 1);
        rand = Math.round(rand);
        return rand;
    }
}

export default {
    template: require('./find-word.html'),
    controller: FindWordController,
    bindings: {
        userCard: '=',
        onSuccess: '&',
        onFail: '&'
    }
}