class RepetitionController {
    constructor($log, $state, $stateParams, $scope, moment, AlertResponse, StudentWordCardManager) {
        "ngInject";

        this.$log = $log;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.$scope = $scope;
        this.moment = moment;

            this.AlertResponse = AlertResponse;
        this.StudentWordCardManager = StudentWordCardManager;

        this.groupId = $state.params.wordGroupId;
        this.cards = [];
        this.totalCount = 0;

        this.activeCardIdx = -1;
    }

    $onInit() {
        this._loadGroup()
            .then(() => this._loadCards());
    }

    next() {
        if (this.activeCardIdx === (this.cards.length - 1)) {
            this.activeCardIdx = 0;
        } else {
            this.activeCardIdx++;
        }
    }

    prev() {
        if (this.activeCardIdx === 0) {
            this.activeCardIdx = this.cards.length - 1;
        } else {
            this.activeCardIdx--;
        }
    }

    get activeCard() {
        if (this.activeCardIdx >= 0) {
            return this.cards[this.activeCardIdx].word_card;
        }

        return null;
    }

    get nextRepetitionDate() {
        let repetitionDate = moment(this.group.max_repetition_date);
        return repetitionDate.format('Do MMMM');
    }

    _loadGroup() {
        return this.StudentWordCardManager
            .getGroups({group_id: this.groupId})
            .then(groups => this.group = groups.length > 0 ? groups[0] : null)
            .catch((response) => this.AlertResponse.fail(response));
    }

    _loadCards() {
        return this.StudentWordCardManager
            .getReviewWordCards(this.groupId)
            .then(cards => {
                this.cards = cards;
                this.totalCount = this.cards.length;
                this.activeCardIdx = 0;
            })
            .catch((response) => this.AlertResponse.fail(response));
    }
}

export default {
    template: require('./vocabulary-review.html'),
    controller: RepetitionController
}