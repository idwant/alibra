function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('vocabulary', {
            url: '/vocabulary',
            template: '<vocabulary-categories class="vocabulary-categories site-wrapper"></vocabulary-categories>'
        })
        .state('vocabularyGroups', {
            url: '/vocabulary/group/:categoryId',
            template: '<vocabulary-groups class="vocabulary-groups site-wrapper"></vocabulary-groups>',
            params: {
                categoryId: null
            }
        })
        .state('vocabularyLearnLesson', {
            url: '/vocabulary/learn-lesson/:lessonId',
            template: '<vocabulary-learn class="vocabulary-learn site-wrapper"></vocabulary-learn>',
            params: {
                lessonId: null
            }
        })
        .state('vocabularyLearnGroup', {
            url: '/vocabulary/learn-group/:wordGroupId',
            template: '<vocabulary-learn class="vocabulary-learn site-wrapper"></vocabulary-learn>',
            params: {
                wordGroupId: null
            }
        })
        .state('vocabularyRepetitionGroups', {
            url: '/vocabulary/repetition/groups/',
            template: '<vocabulary-repetition-groups class="vocabulary-repetition site-wrapper"></vocabulary-repetition-groups>',
        })
        .state('vocabularyRepetitionWords', {
            url: '/vocabulary/repetition/groups/words/:wordGroupId?:contentType',
            template: '<vocabulary-repetition-words class="vocabulary-repetition site-wrapper"></vocabulary-repetition-words>',
            params: {
                wordGroupId: null
            }
        })
        .state('vocabularyReview', {
            url: '/vocabulary/review/:wordGroupId',
            template: '<vocabulary-review class="vocabulary-review site-wrapper"></vocabulary-review>',
            params: {
                wordGroupId: null
            }
        });

}

export default routeConfig;