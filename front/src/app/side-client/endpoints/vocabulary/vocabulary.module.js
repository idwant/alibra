import routeConfig from './vocabulary.route';
import vocabularyCategories from './categories/vocabulary-categories.component';
import vocabularyGroups from './groups/vocabulary-groups.component';
import vocabularyLearn from './learn/vocabulary-learn.component';
import vocabularyReview from './review/vocabulary-review.component';

import vocabularyRepetitionFindWord from './repetition/groups/words/find-word/find-word.component';
import vocabularyRepetitionRememberWord from './repetition/groups/words/remember-word/remember-word.component';
import vocabularyRepetitionWriteWord from './repetition/groups/words/write-word/write-word.component';

import vocabularyRepetitionGroups from './repetition/groups/repetition-groups.component';
import vocabularyRepetitionWords from './repetition/groups/words/repetition-words.component';

import groupProgress from './groups/group-progress.directive';

let module = angular
    .module('vocabulary', [])
    .config(routeConfig)
    .component('vocabularyCategories', vocabularyCategories)
    .component('vocabularyGroups', vocabularyGroups)
    .component('vocabularyLearn', vocabularyLearn)
    .component('vocabularyRepetitionGroups', vocabularyRepetitionGroups)
    .component('vocabularyRepetitionWords', vocabularyRepetitionWords)
    .component('vocabularyReview', vocabularyReview)
    .component('vocabularyRepetitionFindWord', vocabularyRepetitionFindWord)
    .component('vocabularyRepetitionRememberWord', vocabularyRepetitionRememberWord)
    .component('vocabularyRepetitionWriteWord', vocabularyRepetitionWriteWord)

    .directive('groupProgress', groupProgress)
    ;

export default module = module.name;