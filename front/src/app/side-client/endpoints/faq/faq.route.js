function routeConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('faq', {
            url: '/faq',
            template: '<faq class="faq site-wrapper"></faq>'
        });
}

export default routeConfig;