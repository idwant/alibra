import routeConfig from './faq.route';
import faq from './faq/faq.component';
import VideoDialog from './faq/video-dialog.component';

let module = angular
    .module('faq', [])
    .config(routeConfig)
    .component('faqVideoDialog', VideoDialog)
    .component('faq', faq)
    ;

export default module = module.name;