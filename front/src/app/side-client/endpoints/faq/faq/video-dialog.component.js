class VideoDialogController {
    constructor($log) {
        "ngInject";

        this.$log = $log;

        this.config = {
            sources: [
                {src: "files/video/Alibra_method.mp4", type: "video/mp4"}
            ]
        };
    }

    cancel() {
        this.modalInstance.close();
    }
}

export default {
    template: require('./video-dialog.html'),
    controller: VideoDialogController,
    bindings: {
        modalInstance: '<'
    }
};