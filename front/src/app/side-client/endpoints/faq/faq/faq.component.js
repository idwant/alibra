import QuestionCollection from './question-collection';

class FaqController {
    constructor($log, $uibModal, AlertResponse) {
        "ngInject";

        this.$log = $log;
        this.$uibModal = $uibModal;

        this.AlertResponse = AlertResponse;

        this.search = '';
        this.questions = QuestionCollection.getAllItems();
    }

    $onInit() {
    }

    showVideo() {
        this.$uibModal.open({component: 'faqVideoDialog', windowClass: 'video-modal'});
    }
}

export default {
    template: require('./faq.html'),
    controller: FaqController
};