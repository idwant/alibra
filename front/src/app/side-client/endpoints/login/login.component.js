import template from './login.html';

class LoginController {
    constructor(UserManager, toastr, $location, $state) {
        'ngInject';

        this.UserManager = UserManager;
        this.toastr = toastr;
        this.$location = $location;
        this.$state = $state;
        this.token = $state.params.token;
    }

    $onInit() {
        // Выходим если сюда попал авторизированный пользователь
        this.UserManager.current().then(() => this.UserManager.logout(true)).catch(() => '');
        if(this.token) {
            this.authByToken(this.token);
        }
    }

    auth(email, password) {
        this.UserManager.login(email, password)
            .then((msg) => {
                this.toastr.success(msg);
                return this.UserManager.current();
            })
            .then(() => this.$location.url('/dashboard/'))
            .catch(({data}) => {
                this.toastr.error(data.message)
            });
    }

    authByToken(token) {
        this.UserManager.loginByToken(token)
            .then(() => this.$state.go('dashboard'))
            .catch(() => this.$state.go('login'));
    }
}

export default {
    template: template,
    controller: LoginController
};