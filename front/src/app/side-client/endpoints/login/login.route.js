function routeConfig($stateProvider) {
    'ngInject';
    $stateProvider
        .state('login', {
            url: '/login',
            template: '<login class="site-wrapper login-page bgs"></login>'
        })
        .state('tokenLogin', {
            url: '/login/:token',
            template: '<login class="site-wrapper login-page bgs"></login>',
            params: {
                token: null
            }
        });
}

export default routeConfig;