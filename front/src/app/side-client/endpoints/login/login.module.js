import routeConfig from './login.route';
import loginComponent from './login.component';


let login = angular
    .module('login', [])
    .config(routeConfig)
    .component('login', loginComponent)
    ;

export default login = login.name;