import {pagesConstants, AUTH_EVENTS} from './../constants';

function runBlock($rootScope, $filter, toastr, $window, UserManager, $location, $state, bowser) {
    'ngInject';

    $rootScope.bowser = bowser;

    UserManager.loadUserCredentials().catch(() => {
        console.log('not user');
    });

    // Проставление в title
    $rootScope.$on('$stateChangeSuccess', function (event, toState) {
        $window.document.title = toState.title ? toState.title : 'Alibra School';
    });

    // $rootScope.$on("$stateChangeStart", function (event, next, nextParams, from, fromParams) {
    //     if (fromParams.skipSomeAsync) {
    //         return;
    //     }
    //
    //     if (next.url !== pagesConstants.LOGIN && next.url !== pagesConstants.LOGOUT && next.name !== 'dashboard') {
    //         event.preventDefault();
    //
    //         UserManager.current()
    //             .then(() => {
    //                 fromParams.skipSomeAsync = true;
    //                 $state.go(next.name, angular.merge(nextParams, {getParams: $location.search()}));
    //             })
    //             .catch(() => $state.go('login'));
    //     }
    // });

    $rootScope.$on(AUTH_EVENTS.notAuthorized, function () {
        toastr.error($filter('translate')('AUTH REQUIRED'));
        $location.path(pagesConstants.LOGIN);
    });

    $rootScope.$on(AUTH_EVENTS.notAuthenticated, function () {
        UserManager.current().then(function () {
            toastr.error($filter('translate')('SESSION LOST'));
            UserManager.logout();
        });

        $location.path(pagesConstants.LOGIN);
    });

}

export default runBlock;
