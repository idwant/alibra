/* global malarkey:false, toastr:false, moment:false */
import config from './index.config';
import runBlock from './index.run';

import shared from './../shared/shared.module'
import loginPage from './endpoints/login/login.module';
import api from './../api/api.module';

import components from './components/components.module';
import dashboard from './endpoints/dashboard/dashboard.module';
import exercises from './endpoints/exercises/exercises.module';
import vocabulary from './endpoints/vocabulary/vocabulary.module';
import faq from './endpoints/faq/faq.module';
import grammar from './endpoints/grammar/grammar.module';
import cabinet from './endpoints/cabinet/cabinet.module';

angular.module('front', [
        'parameters.config',
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ui.select',
        'angular-cache',
        'angularFileUpload',
        'ngTagsInput',
        'ui.sortable',
        'angular-svg-round-progressbar',
        'com.2fdevs.videogular',
        'com.2fdevs.videogular.plugins.controls',
        'contenteditable',
        'dndLists',
        'duScroll',
        shared,
        components,
        api,
        loginPage,
        exercises,
        dashboard,
        vocabulary,
        faq,
        grammar,
        cabinet
    ])
    .constant('toastr', toastr)
    .constant('moment', moment)
    .constant('alert', swal)
    .constant('localStorage', localStorage)
    .constant('bowser', bowser)

    .config(config)
    .run(runBlock)
;