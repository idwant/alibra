import checkExercise from './check-exercise/check-exercise.component';
import footer from './footer/footer';
import header from './header/header';
import contentNavi from './content-navi/content-navi'
import randomFilter from './random-filter/random-filter';
import adoptTextFilter from './adop-text-filter/adop-text-filter';
import unsafeFilter from './unsafe-filter/unsafe-filter';

import feedback from './feedback/feedback.component';
import FeedbackDialogController from './feedback/feedback-dialog.controller';

let components =
        angular.module('components', [])
            .controller('FeedbackDialogController', FeedbackDialogController)

            .component('checkExercise', checkExercise)
            .component('footer', footer)
            .component('header', header)
            .component('feedback', feedback)
            .component('contentNavi', contentNavi)
            .filter('random',  randomFilter)
            .filter('adoptText', adoptTextFilter)
            .filter('unsafe',  unsafeFilter)
    ;

export default components = components.name;