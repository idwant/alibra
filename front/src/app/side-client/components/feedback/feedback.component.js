class FeedbackController {
    constructor($log, $uibModal, AlertResponse) {
        'ngInject';
        this.$log = $log;
        this.$uibModal = $uibModal;
        this.AlertResponse = AlertResponse;
    }

    $onInit() {

    }

    showDialog() {
        this.$uibModal.open({
            template: require('./feedback-dialog.html'),
            controller: 'FeedbackDialogController',
            controllerAs: '$ctrl',
            windowClass: 'feedback-modal'
        });
    }
}

export default {
    controller: FeedbackController,
    template: require('./feedback.html'),
    bindings: {
        title: '@'
    }
};