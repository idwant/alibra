import Feedback from 'api/models/Feedback';

class FeedbackDialogController {
    constructor($log, $scope, $uibModalInstance, AlertResponse, StudentManager) {
        'ngInject';

        this.$log = $log;
        this.$scope = $scope;
        this.$uibModalInstance = $uibModalInstance;
        this.AlertResponse = AlertResponse;

        this.StudentManager = StudentManager;

        this.feedback = new Feedback({});

        this.formSended = false;
    }

    send() {
        this.StudentManager
            .createFeedback(this.feedback)
            .then(feedback => this.formSended = true)
            .catch(response => this.AlertResponse.fail(response))
            .finally(() => this.$scope.$broadcast('done'));
    }

    cancel() {
        this.$uibModalInstance.close();
    }
}

export default FeedbackDialogController;