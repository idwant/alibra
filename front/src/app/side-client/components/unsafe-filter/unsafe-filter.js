export default function unsafeFilter($sce) {
    'ngInject';
    return function(val) {
        return $sce.trustAsHtml(val);
    };
}
