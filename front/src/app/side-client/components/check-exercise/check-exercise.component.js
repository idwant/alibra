import {APP_EVENTS} from './../../../constants';

class CheckExerciseController {

    constructor($rootScope, VoiceRecorderManager) {
        'ngInject';
        this.result = false;
        this.$rootScope = $rootScope;
        this.VoiceRecorderManager = VoiceRecorderManager;

        // состояние курсора и клавиши мыши над Глазком
        this.isMouseOver = false;
        this.isMouseDown = false;
    }

    check($event) {
        $event.preventDefault();
        $event.stopPropagation();

        // принудительно заканчиваем запись звука при проверке упражнения
        let recognitionPromise = this.VoiceRecorderManager.stop();
        if (recognitionPromise) {
            recognitionPromise
                .then(() => {
                    this.checkRound();
                })
                .catch((e) => {
                    this.checkRound();
                });
        } else {
            this.checkRound();
        }
    }

    checkRound() {
        this.result = this.checkAnswer();
        this.round.attempts++;
        return this.onRoundComplete(this.result);
    }

    updateAnswerVisibility(isMouseOver, isMouseDown) {
        if (isMouseOver && isMouseDown && (!this.isMouseOver || !this.isMouseDown)) {
            this.setAnswerVisibility({isVisible: true});
        }

        if (this.isMouseOver && this.isMouseDown && (!isMouseOver || !isMouseDown)) {
            this.setAnswerVisibility({isVisible: false});
        }

        this.isMouseDown = isMouseDown;
        this.isMouseOver = isMouseOver;

        if (!this.isMouseOver) {
            this.isMouseDown = false;
        }

    }

    $onChanges() {
        this.showHint = false;
    }
}

export default  {
    controller: CheckExerciseController,
    template: require('./check-exercise.html'),
    bindings: {
        checkAnswer: '&', // return {result: true|false, progress: any}
        setAnswerVisibility: '&?',
        onRoundComplete: '&',
        exercise: '<',
        round: '<',
    }
}