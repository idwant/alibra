export default function randomFilter(orderByFilter) {
    'ngInject';
    return function(elements) {
        return orderByFilter(elements, () => 0.5 - Math.random());
    };
}
