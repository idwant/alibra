class HeaderController {
    constructor(UserManager, WordCardGroupManager, HintManager) {
        'ngInject';

        this.UserManager = UserManager;
        this.WordCardGroupManager = WordCardGroupManager;
        this.HintManager = HintManager;
        this.user = {};
        this.notices = [];
    }

    $onInit() {
        this.UserManager.current().then(user => {
            this.user = user;
        });

        this.HintManager.getHintByName('group_notice').then(hint => {
            if(hint.is_show) {
                this.WordCardGroupManager.loadStudentGroups().then(groups => {
                    let availableGroups = 0;
                    groups.forEach(function(group) {
                        if(group.hasAvailableToLearn) {
                            availableGroups++;
                        }
                    });

                    this.notices.push({
                        'text': hint.text.replace('%availableGroups%', availableGroups),
                        'hint': hint
                    });
                });
            }
        });
    }

    logout() {
        this.UserManager.logout();
    }

    closeNotice(index, hint) {
        this.HintManager.closeHint(hint);
        this.notices.splice(index, 1);
    }
}

export default {
    template: require('./header.html'),
    controller: HeaderController
}