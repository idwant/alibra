export default () => {
    'ngInject';

    return (element) => {
        if(typeof element === 'string')
            return element.replace(/\\\\/g, '<br>');
        return '';
    };
}
