<?php

namespace CoreDomain\Services\Task;

interface TaskServiceInterface
{
    /** 
     * @param array $params
     * @return mixed
     */
    public function execute(array $params);

    /**
     * Метод возвращает id сервива в контейнере
     * 
     * Пример: 
     * private $serviceName = 'app.service.mailer';
     * public function getServiceName()
     * {
     *    return $this->serviceName;
     * }
     * 
     * @return string
     */
    public function getServiceName();
}