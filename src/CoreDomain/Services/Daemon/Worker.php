<?php

namespace CoreDomain\Services\Daemon;

abstract class Worker
{
    const DEFAULT_MAX_PROCESSES = 20;
    // Комманды демону
    const COMMAND_TERMINATE = 'terminate';
    const COMMAND_PAUSE = 'pause';
    const COMMAND_RUN = 'run';
    // Статусы мастер-процесса
    const STATUS_RUN = 0;
    const STATUS_STOP = 1;
    const STATUS_PAUSE = 2;
    // Время ожидания (в секундах) между проверкой на поступление новых задач
    const SLEEP = 1;
    // Лимит памяти для демона
    const MEMORY_LIMIT = 300;

    // Текущий статус мастер-процесса
    protected $masterStatus;
    // Список исполняющихся дочерних процессов
    protected $runningProcesses = array();
    // Список завершённых дочерних процессов
    protected $finishedProcesses = array();
    //корневая папка проекта
    protected $appDir = '';
    
    protected $workerName = 'default';
    protected $pidFileName = 'worker.pid';
    protected $cmdFileName = 'worker.cmd';

    /**
     * Инициализация работы мастер-процесса
     */
    public function initWorker()
    {
        // Проверяем наличие расширений, требуемых для работы демона
        if (!extension_loaded('pcntl') || !extension_loaded('posix')) {
            exit('ERROR: Extensions PCNTL & POSIX must be installed and enabled...');
        }
        // Проверяем, запущен ли демон, если да, то новый запускать не нужно
        if ($this->checkExistingMasterProcess()) {
            exit('already running' . PHP_EOL);
        }
        if(!file_exists($this->getRuntimePath())) {
            mkdir($this->getRuntimePath(), 0777, true);
        }
        // Отвязываем демона от консоли
        fclose(STDIN);
        fclose(STDOUT);
        fclose(STDERR);
        // Переводим запись ошибок и сообщений в файлы
        $STDIN = fopen('/dev/null', 'r');
        $STDOUT = fopen($this->getRuntimePath() . DIRECTORY_SEPARATOR . 'runtime.log', 'a+');
        $STDERR = fopen($this->getRuntimePath() . DIRECTORY_SEPARATOR . 'error.log', 'a+');
        // Создаем дочерний поток
        $childPid = pcntl_fork();
        // Ошибка создания дочернего потока
        if ($childPid == -1) {
            die ('error');
            // Убиваем основной поток
        } else if ($childPid) {
            exit;
        }
        // Ставим дочерний поток основным
        posix_setsid();
        // Сохраняем его PID для потомков
        $this->setExistingMasterProcess(posix_getpid());
        // Вешаем обработчик на сигналы, получаемые от ОС и дочерних процессов
        pcntl_signal(SIGTERM, array($this, 'masterSignalListener'));
        pcntl_signal(SIGCHLD, array($this, 'masterSignalListener'));
        declare(ticks = 1);
        register_tick_function('pcntl_signal_dispatch');
        // Запускаем мастер управления задачами
        $this->startMaster();
        $this->runMaster();
    }

    /**
     * Описание цикла работы мастер-процесса
     */
    protected function runMaster()
    {
        while (true) {
            // Обрабатываем команды из файла
            $command = $this->getCommand();
            switch ($command) {
                case self::COMMAND_TERMINATE:
                    $this->stopMaster();
                    file_put_contents($this->getCmdFile(), '');
                    break;
                case self::COMMAND_PAUSE:
                    $this->pauseMaster();
                    file_put_contents($this->getCmdFile(), '');
                    break;
                case self::COMMAND_RUN:
                    $this->startMaster();
                    file_put_contents($this->getCmdFile(), '');
                    break;
                default:
                    file_put_contents($this->getCmdFile(), '');
                    break;
            }
            // Выполняем действие согласно текущему статусу
            switch ($this->masterStatus) {
                case self::STATUS_PAUSE:
                    $this->initWorkerPause();
                    $this->finishJobs();
                    break;
                case self::STATUS_RUN:
                    // Проверяем возможность запуска дополнительного процесса
                    if ($this->canLaunchJob()) {
                        // Запуск дочерних процессов на исполнение
                        $this->launchJobs($this->canLaunchJobsCount());
                    }
                    // Обрабатываем завершённые процессы
                    $this->finishJobs();
                    break;
                case self::STATUS_STOP:
                    $this->setExistingMasterProcess(0);
                    exit;
            }

            //если первышен лимит памяти, ставим демон на паузу, дожидаемся завершения всех дочерних процессов
            //и убиваем демона
            if(!$this->checkMemory()) {
                $this->pauseMaster();
            }

            sleep(self::SLEEP);
        }
    }

    /**
     * Возвращает статус готовности основного процесса к запуску дополнительных дочерних процессов
     * @return boolean
     */
    protected function canLaunchJob()
    {
        return $this->canLaunchJobsCount() > 0;
    }

    /**
     * Возвращает кол-во дочерних процессов, которые могут быть запущены
     * @return boolean
     */
    protected function canLaunchJobsCount()
    {
        return $this->getMaxProcesses() - count($this->runningProcesses);
    }

    /**
     * Пакетный запуск задач на выполнение
     * @param int $count - максимальное кол-во задач, которые можно запустить
     */
    protected function launchJobs($count)
    {
        foreach ($this->getTasksToLaunch($count) as $task) {
            $this->launchJob($task);
        }
    }

    /**
     * Порождение дочернего процесса и запуск его на выполнение
     * @param $task
     * @return bool
     */
    protected function launchJob($task)
    {

        $this->closeResource();
        
        $pid = pcntl_fork();
        
        $this->openResource();        
        
        // Ошибка создания дочернего процесса
        if ($pid == -1) {
            return false;
        // Создан дочерний объект - управление у основного
        } else if ($pid) {
            $this->runningProcesses[$pid] = $task;
            unset($task);
        // Запуск работы дочернего объекта
        } else {
            pcntl_signal(SIGTERM, array($this, 'childSignalListener'));
            $this->runTask($task);
            exit;
        }
        return true;
    }

    /**
     * Возвращает задачи для запуска
     * @param $count
     * @return array
     */
    abstract protected function getTasksToLaunch($count);

    /**
     * Запуск задачи на исполнение
     * @param $task 
     */
    abstract protected function runTask($task);
    
    /**
     * Завершает завершённые процессы
     */
    protected function finishJobs()
    {
        $keys = array_keys($this->finishedProcesses);
        foreach($keys as $key) {
            $this->finishJob($key);
        }
    }

    /**
     * Завершает задачу
     * @param $pid
     * @return bool
     */
    protected function finishJob($pid)
    {
        if(!array_key_exists($pid, $this->finishedProcesses)) {
            return false;
        }
        $job = $this->finishedProcesses[$pid];
        $this->finishTask($job['task'], $job['status']);
        unset($this->finishedProcesses[$pid]);
        return true;
    }

    /**
     * Завершение задачи
     * @param $task 
     * @param $status
     */
    abstract protected function finishTask($task, $status);
    
    /**
     * Слушатель сигналов для мастер-процесса, получаемых от ОС
     * @param $signo  - код сигнала
     * @param $pid    - ID процесса
     * @param $status - статус процесса
     */
    public function masterSignalListener($signo, $pid = null, $status = null)
    {
        switch ($signo) {
            // Завершение процесса
            case SIGTERM:
                $this->stopMaster();
                break;
            // Остановка процесса
            case SIGSTOP:
                $this->pauseMaster();
                break;
            // Возобновление процесса
            case SIGCONT:
                $this->startMaster();
                break;
            // Получение сигналов от дочерних процессов
            case SIGCHLD:
                if (!$pid) {
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                // Обрабатываем все сигналы по порядку
                while ($pid > 0) {
                    $this->childStatusHandler($pid, $status);
                    // Получаем следующий сигнал
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                break;
            // Остальные сигналы
            default:
                break;
        }
    }

    public function childSignalListener($signo, $pid = null, $status = null)
    {
        switch ($signo) {
            case SIGTERM :
                posix_kill(posix_getpid(), SIGKILL);
                break;
        }
    }

    protected function childStatusHandler($pid, $status)
    {
        if (!isset($this->runningProcesses[$pid])) {
            return false;
        }
        
        $this->finishedProcesses[$pid] = [
            'task' => $this->runningProcesses[$pid],
            'status' => $status
        ];        

        unset($this->runningProcesses[$pid]);
        return true;
    }

    /**
     * Запуск основного процесса
     */
    protected function startMaster()
    {
        $this->masterStatus = self::STATUS_RUN;
    }

    /**
     * Остановка основного процесса
     */
    protected function stopMaster()
    {
        $this->masterStatus = self::STATUS_STOP;
    }

    /**
     * Приостановка основного процесса
     */
    protected function pauseMaster()
    {
        $this->masterStatus = self::STATUS_PAUSE;
    }

    public function getMaxProcesses()
    {
        return self::DEFAULT_MAX_PROCESSES;
    }

    /**
     * Возвращает имя, где будет храниться PID основного процесса
     * @return string
     */
    public function getPidFileName()
    {
        return $this->pidFileName;
    }

    /**
     * Возвращает имя файла, куда можно будет писать команды для основного процесса
     *
     * @return string
     */
    public function getCmdFileName()
    {
        return $this->cmdFileName;
    }

    /**
     * Возвращает путь к pid-файлу демона
     *
     * @return string
     */
    protected function getPidFile()
    {
        return $this->getRuntimePath() . DIRECTORY_SEPARATOR . $this->getPidFileName();
    }

    /**
     * Возвращает путь к cmd файлу
     * @return string
     */
    protected function getCmdFile()
    {
        return $this->getRuntimePath() . DIRECTORY_SEPARATOR . $this->getCmdFileName();
    }

    protected function getRuntimePath()
    {
        return $this->appDir . '/logs/daemon/' . $this->getName();
    }

    protected function getCommand()
    {
        if(file_exists($this->getCmdFile())) {
            return trim(file_get_contents($this->getCmdFile()));
        }
        return '';
    }

    /**
     * Проверяет, существует ли уже работающая версия демона
     * @return boolean
     */
    public function checkExistingMasterProcess()
    {
        $pid = $this->getPidParentProcess();
        
        if ($pid && is_numeric($pid)) {
            return posix_kill($pid, 0);
        }
        
        return false;
    }

    /**
     * Фиксирует ID основного процесса в pid-файле, предотвращает запуск второй копии демона
     *
     * @param integer $pid - ID процесса для записи
     */
    protected function setExistingMasterProcess($pid)
    {
        $file = $this->getPidFile();
        if (!is_file($file)) {
            fopen($file, 'w');
        }
        file_put_contents($file, $pid);
    }

    /**
     * Возвращает имя демона
     * @return string
     */
    protected function getName()
    {
        return $this->workerName;
    }

    /**
     * Метод проверяет на превышение лимита выделеной памяти
     * @return bool
     */
    protected function checkMemory()
    {
        $currentUseMemory = memory_get_usage(true)/1024/1024;

        if($currentUseMemory < self::MEMORY_LIMIT) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает pid родительского процесса
     * @return bool|string
     */
    protected function getPidParentProcess()
    {
        $file = $this->getPidFile();
        if (is_file($file)) {
            $pid = file_get_contents($file);
            if ($pid && is_numeric($pid)) {
                return $pid;
            }
        }
        return false;
    }

    /**
     * Открывает соединение с ресурсом
     */
    abstract protected function openResource();

    /**
     * Закрывает соединение с ресурсом
     */
    abstract protected function closeResource();

    /**
     * Метод вызывается при постановке демона на паузу
     */
    abstract protected function initWorkerPause();
}