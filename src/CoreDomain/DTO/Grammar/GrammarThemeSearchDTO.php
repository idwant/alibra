<?php
namespace CoreDomain\DTO\Grammar;

class GrammarThemeSearchDTO
{
    public $query;
    public $section;
    public $isDemo;
    public $limit = 10;
    public $offset = 0;

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}