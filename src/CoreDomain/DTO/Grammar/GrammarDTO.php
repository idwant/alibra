<?php

namespace CoreDomain\DTO\Grammar;


class GrammarDTO
{
    public $id;

    public $title;
    public $content;
    public $theme;
    public $video;
    public $isDemo;
}