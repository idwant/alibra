<?php
namespace CoreDomain\DTO\Grammar;

class GrammarSectionSearchDTO
{
    public $query;
    public $isDemo;
    public $limit = 10;
    public $offset = 0;

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}