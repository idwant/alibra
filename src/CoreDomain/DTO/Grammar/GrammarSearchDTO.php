<?php
namespace CoreDomain\DTO\Grammar;

class GrammarSearchDTO
{
    public $id;
	public $query;
	public $title;
	public $content;
    public $isDemo;
	public $theme;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}