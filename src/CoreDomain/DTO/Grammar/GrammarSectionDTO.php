<?php

namespace CoreDomain\DTO\Grammar;


class GrammarSectionDTO
{
    public $id;
    public $name;
}