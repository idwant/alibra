<?php

namespace CoreDomain\DTO\Grammar;


class GrammarThemeDTO
{
    public $id;

    public $name;
    public $section;
}