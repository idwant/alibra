<?php

namespace CoreDomain\DTO\Task;


class TaskQueueDTO
{
    public $id;
    
    public $service;
    public $params;    
    public $status;
    public $blockId;
    public $attempts;
    public $created;
    public $dateStart;    
}