<?php
namespace CoreDomain\DTO\Common;

class FeedbackDTO
{
    public $email;
    public $title;
    public $message;
}