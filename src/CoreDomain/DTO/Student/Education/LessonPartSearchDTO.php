<?php

namespace CoreDomain\DTO\Student\Education;


class LessonPartSearchDTO
{
	public $id;
	public $query;
	public $userId;
	public $courseId;
	public $unitId;
	public $lessonId;
	public $type;
	public $limit;
	public $offset;
}