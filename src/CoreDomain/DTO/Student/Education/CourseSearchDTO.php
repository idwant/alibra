<?php

namespace CoreDomain\DTO\Student\Education;

class CourseSearchDTO
{
    public $id;
    public $query;
    public $userId;
    public $progress;
    public $limit;
    public $offset;
    public $isDemo;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}