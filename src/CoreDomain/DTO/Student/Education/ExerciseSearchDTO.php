<?php
namespace CoreDomain\DTO\Student\Education;


class ExerciseSearchDTO
{
    public $id;
    public $query;
    public $userId;
    public $courseId;
    public $unitId;
    public $lessonId;
    public $lessonPartId;
    public $exerciseType;
    public $limit;
    public $offset;
    public $isDemo;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function getCourseId()
    {
        return $this->courseId;
    }

    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
        return $this;
    }

    public function getUnitId()
    {
        return $this->unitId;
    }

    public function setUnitId($unitId)
    {
        $this->unitId = $unitId;
        return $this;
    }

    public function getLessonId()
    {
        return $this->lessonId;
    }

    public function setLessonId($lessonId)
    {
        $this->lessonId = $lessonId;
        return $this;
    }

    public function getLessonPartId()
    {
        return $this->lessonPartId;
    }

    public function setLessonPartId($lessonPartId)
    {
        $this->lessonPartId = $lessonPartId;
        return $this;
    }

    public function getExerciseType()
    {
        return $this->exerciseType;
    }

    public function setExerciseType($exerciseType)
    {
        $this->exerciseType = $exerciseType;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function getIsDemo()
    {
        return $this->isDemo;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}