<?php
namespace CoreDomain\DTO\Student\Education;


class ExerciseStatusDTO
{
    public $exerciseId;
    public $lessonId;
    public $completed;
}