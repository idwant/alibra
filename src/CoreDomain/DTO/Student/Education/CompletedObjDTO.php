<?php
namespace CoreDomain\DTO\Student\Education;


class CompletedObjDTO
{
    public $percent;
    public $result;
}