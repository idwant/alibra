<?php

namespace CoreDomain\DTO\Dashboard;

class DashboardSearchDTO
{
	public $courseId;
	public $unitId;
	public $lessonId;
	public $userId;
	public $exerciseType;
    public $isDemo;

    public function getCourseId()
    {
        return $this->courseId;
    }

    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
        return $this;
    }

    public function getUnitId()
    {
        return $this->unitId;
    }

    public function setUnitId($unitId)
    {
        $this->unitId = $unitId;
        return $this;
    }

    public function getLessonId()
    {
        return $this->lessonId;
    }

    public function setLessonId($lessonId)
    {
        $this->lessonId = $lessonId;
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function getExerciseType()
    {
        return $this->exerciseType;
    }

    public function setExerciseType($exerciseType)
    {
        $this->exerciseType = $exerciseType;
        return $this;
    }

    public function getIsDemo()
    {
        return $this->isDemo;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}