<?php

namespace CoreDomain\DTO\Dashboard;

use CoreDomain\DTO\Education\LessonPartStatDTO;

class DashboardItemDTO extends LessonPartStatDTO
{
    // Эти поля есть в родительском классе, но удалять их нельзя, так как
    // будут проблемы с сериализацией
    public $common;
    public $groupTask;
    public $homework;
    public $test;
}