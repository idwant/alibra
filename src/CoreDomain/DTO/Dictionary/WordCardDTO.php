<?php

namespace CoreDomain\DTO\Dictionary;


class WordCardDTO
{
    public $id;
    public $image;
    public $audio;
    public $sampleAudio;
    public $sample;
    public $translation;
    public $transcription;
    public $isDeleted;
    public $exercise;
    public $user;
    public $word;
    public $groups;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getAudio()
    {
        return $this->audio;
    }

    public function setAudio($audio)
    {
        $this->audio = $audio;
        return $this;
    }

    public function getSampleAudio()
    {
        return $this->sampleAudio;
    }

    public function setSampleAudio($sampleAudio)
    {
        $this->sampleAudio = $sampleAudio;
        return $this;
    }

    public function getSample()
    {
        return $this->sample;
    }

    public function setSample($sample)
    {
        $this->sample = $sample;
        return $this;
    }

    public function getTranslation()
    {
        return $this->translation;
    }

    public function setTranslation($translation)
    {
        $this->translation = $translation;
        return $this;
    }

    public function getTranscription()
    {
        return $this->transcription;
    }

    public function setTranscription($transcription)
    {
        $this->transcription = $transcription;
        return $this;
    }

    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    public function getExercise()
    {
        return $this->exercise;
    }

    public function setExercise($exercise)
    {
        $this->exercise = $exercise;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getWord()
    {
        return $this->word;
    }

    public function setWord($word)
    {
        $this->word = $word;
        return $this;
    }

    public function getGroups()
    {
        return $this->groups;
    }

    public function setGroups($groups)
    {
        $this->groups = $groups;
        return $this;
    }
}