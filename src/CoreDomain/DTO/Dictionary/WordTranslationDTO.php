<?php

namespace CoreDomain\DTO\Dictionary;

class WordTranslationDTO
{
	public $id;
	public $word;
	public $translation;
	public $sample;
	public $sampleAudio;

    public function getWord()
    {
        return $this->word;
    }

    public function setWord($word)
    {
        $this->word = $word;
        return $this;
    }

    public function getTranslation()
    {
        return $this->translation;
    }

    public function setTranslation($translation)
    {
        $this->translation = $translation;
        return $this;
    }

    public function getSample()
    {
        return $this->sample;
    }

    public function setSample($sample)
    {
        $this->sample = $sample;
        return $this;
    }

    public function getSampleAudio()
    {
        return $this->sampleAudio;
    }

    public function setSampleAudio($sampleAudio)
    {
        $this->sampleAudio = $sampleAudio;
        return $this;
    }
}