<?php

namespace CoreDomain\DTO\Dictionary;


class WordDTO
{
    public $id;

    public $word;
    public $transcription;
    public $image;
    public $audio;

    public function getWord()
    {
        return $this->word;
    }

    public function setWord($word)
    {
        $this->word = $word;
        return $this;
    }

    public function getTranscription()
    {
        return $this->transcription;
    }

    public function setTranscription($transcription)
    {
        $this->transcription = $transcription;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getAudio()
    {
        return $this->audio;
    }

    public function setAudio($audio)
    {
        $this->audio = $audio;
        return $this;
    }


}