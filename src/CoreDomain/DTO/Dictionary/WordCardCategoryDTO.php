<?php

namespace CoreDomain\DTO\Dictionary;

class WordCardCategoryDTO
{
    public $image;
    public $image_big;
    public $name;
    public $description;
    public $translation;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getImageBig()
    {
        return $this->image_big;
    }

    public function setImageBig($image_big)
    {
        $this->image_big = $image_big;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getTranslation()
    {
        return $this->translation;
    }

    public function setTranslation($translation)
    {
        $this->translation = $translation;
        return $this;
    }
}