<?php

namespace CoreDomain\DTO\Education;

class LessonDTO
{
    public $id;
    public $name;
}