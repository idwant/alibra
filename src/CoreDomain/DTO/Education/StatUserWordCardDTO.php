<?php

namespace CoreDomain\DTO\Education;

class StatUserWordCardDTO
{
    public $total;
    public $learned;
    public $disabled;
    public $repeat;
    public $complete;

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function getLearned()
    {
        return $this->learned;
    }

    public function setLearned($learned)
    {
        $this->learned = $learned;
        return $this;
    }

    public function getDisabled()
    {
        return $this->disabled;
    }

    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
        return $this;
    }

    public function getRepeat()
    {
        return $this->repeat;
    }

    public function setRepeat($repeat)
    {
        $this->repeat = $repeat;
        return $this;
    }

    public function getComplete()
    {
        return $this->complete;
    }

    public function setComplete($complete)
    {
        $this->complete = $complete;
        return $this;
    }
}