<?php

namespace CoreDomain\DTO\Education;

class LessonPartStatDTO
{
    public $common;
    public $groupTask;
    public $homework;
    public $test;

    public function updateInfo(array $item)
    {
        $this->common = [
            'wait' => $item['wait'],
            'success' => $item['success'],
            'fail' => $item['fail'],
            'complete' => $item['complete'],
            'in_progress' => $item['in_progress']
        ];

        $this->groupTask = [
            'wait' => $item['group_task_wait'],
            'success' => $item['group_task_success'],
            'fail' => $item['group_task_fail'],
            'complete' => $item['group_task_complete'],
            'in_progress' => $item['group_task_in_progress']
        ];

        $this->homework = [
            'wait' => $item['homework_wait'],
            'success' => $item['homework_success'],
            'fail' => $item['homework_fail'],
            'complete' => $item['homework_complete'],
            'in_progress' => $item['homework_in_progress']
        ];

        $this->test = [
            'wait' => $item['test_wait'],
            'success' => $item['test_success'],
            'fail' => $item['test_fail'],
            'complete' => $item['test_complete'],
            'in_progress' => $item['test_in_progress']
        ];
    }

    public function getCommonCompletedPercent() {
        return floor($this->common['complete'] / $this->getCommonTotal() * 100);
    }

    public function getCommonTotal()
    {
        return $this->common['wait'] + $this->common['complete'] + $this->common['in_progress'];
    }

    public function remainingExercises() {
        return $this->common['wait'] + $this->common['in_progress'];
    }
}