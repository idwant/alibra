<?php

namespace CoreDomain\DTO\Education;

class ExerciseSortDTO
{
    public $lessonId;
    public $exerciseId;
    public $position;
}