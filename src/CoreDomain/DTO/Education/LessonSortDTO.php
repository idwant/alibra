<?php

namespace CoreDomain\DTO\Education;

class LessonSortDTO
{
    public $lessonId;
    public $unitId;
    public $position;
}