<?php

namespace CoreDomain\DTO\Education;

class ExerciseRequestDTO
{
    public $type;
    public $templateType;
    public $name;
    public $taskEn;
    public $taskRu;
    public $taskAudio;
    public $grammar;
    public $dictionary;
    public $params;
}