<?php


namespace CoreDomain\DTO\Education;


class HintDTO
{
    public $name;
    public $attempts;
    public $user;
}