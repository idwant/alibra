<?php
namespace CoreDomain\DTO\Education;

class SearchUserProgressDTO
{
    public $courseId;
    public $userId;

    public $isDemo;

    public $limit;
    public $offset;

    public function getCourseId()
    {
        return $this->courseId;
    }

    public function setCourseId($courseId)
    {
        $this->courseId = $courseId;
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function getIsDemo()
    {
        return $this->isDemo;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}