<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class FreeFormTemplateDTO extends ExerciseTemplateDTO
{
    public $question;
    public $possibleAnswers;
    public $hint;
    public $images;
    public $imageModels;
    public $audioHint;
    public $audioHintModel;

    public function getDependencyFields()
    {
        return array(
            'imageModels' => array(
                'repository' => 'image',
                'field' => 'images',
                'value' => $this->images,
                'type' => 'array'
            ),
            'audioHintModel' => array(
                'repository' => 'audio',
                'field' => 'audioHintModel',
                'value' => $this->audioHint
            )
        );
    }

    /**
     * @return mixed
     */
    public function getImageModels()
    {
        return $this->imageModels;
    }

    /**
     * @return mixed
     */
    public function getAudioHintModel()
    {
        return $this->audioHintModel;
    }
}