<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class CrosswordTemplateDTO extends ExerciseTemplateDTO
{
    public $grid;
    public $legend;
    public $items;

    public function getDependencyFields()
    {
        return array(
            'items' => array(
                'included' => true,
                'field' => $this->items
            ),
        );
    }
}