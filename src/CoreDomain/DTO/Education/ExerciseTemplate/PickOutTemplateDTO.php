<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class PickOutTemplateDTO extends ExerciseTemplateDTO
{
    public $elements;
    public $hint;

    public function getDependencyFields()
    {
        return array();
    }
}