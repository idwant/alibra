<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class CrosswordItemDTO
{
    public $question;
    public $answer;
    public $image;
    public $imageModel;

    public function getDependencyFields()
    {
        return array(
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}