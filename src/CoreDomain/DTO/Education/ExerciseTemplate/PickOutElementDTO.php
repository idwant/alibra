<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;


class PickOutElementDTO
{
    public $text;
    public $answer;
}