<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;


class SelectElementDTO
{
    public $text;
    public $skip;
    public $possibleAnswers;
}