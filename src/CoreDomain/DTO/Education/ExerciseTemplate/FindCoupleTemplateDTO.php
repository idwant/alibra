<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class FindCoupleTemplateDTO extends ExerciseTemplateDTO
{
    public $couples;

    public function getDependencyFields()
    {
        return array(
            'couples' => array(
                'included' => true,
                'field' => $this->couples
            ),
        );
    }
}