<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class ReadWriteTemplateDTO extends ExerciseTemplateDTO
{
    public $elements;
    public $hint;
    public $image;
    public $imageModel;
    public $answerBank;

    public function getDependencyFields()
    {
        return array(
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}