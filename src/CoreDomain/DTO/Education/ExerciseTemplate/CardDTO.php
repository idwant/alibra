<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;


class CardDTO
{
    public $question;
    public $possibleAnswers;
    public $answer;
    public $image;
    public $imageModel;
    public $hint;

    public function getDependencyFields()
    {
        return array(
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}
