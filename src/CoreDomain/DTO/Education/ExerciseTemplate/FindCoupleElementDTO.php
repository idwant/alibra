<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class FindCoupleElementDTO
{
    public $images;
    public $imageModels;
    public $audios;
    public $audioModels;
    public $words;

    public function getDependencyFields()
    {
        return array(
            'imageModels' => array(
                'repository' => 'image',
                'field' => 'images',
                'value' => $this->images,
                'type' => 'array'
            ),
            'audioModels' => array(
                'repository' => 'audio',
                'field' => 'audios',
                'value' => $this->audios,
                'type' => 'array'
            ),
        );
    }

    /**
     * @return mixed
     */
    public function getImageModels()
    {
        return $this->imageModels;
    }

    /**
     * @return mixed
     */
    public function getAudioModels()
    {
        return $this->audioModels;
    }
}