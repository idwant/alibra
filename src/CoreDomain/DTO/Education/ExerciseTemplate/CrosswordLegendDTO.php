<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class CrosswordLegendDTO
{
    public $across;
    public $down;
}