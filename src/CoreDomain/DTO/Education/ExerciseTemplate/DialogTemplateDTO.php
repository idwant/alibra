<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class DialogTemplateDTO extends ExerciseTemplateDTO
{
    public $possibleAnswers;
    public $answers;
    public $audioHint;
    public $audioHintModel;
    public $audio;
    public $audioModel;
    public $image;
    public $imageModel;
    public $hint;

    public function getDependencyFields()
    {
        return array(
            'audioHintModel' => array(
                'repository' => 'audio',
                'field' => 'audioHint',
                'value' => $this->audioHint
            ),
            'audioModel' => array(
                'repository' => 'audio',
                'field' => 'audio',
                'value' => $this->audio
            ),
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getAudioHintModel()
    {
        return $this->audioHintModel;
    }

    /**
     * @return mixed
     */
    public function getAudioModel()
    {
        return $this->audioModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}