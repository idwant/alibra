<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;


abstract class ExerciseTemplateDTO
{
    /**
     * @return ExerciseTemplateDTO
     */
    public function getClass()
    {
        return get_called_class();
    }

    public static function getDeserializationGroup()
    {
        return array(
            'api_exercise_create',
        );
    }

    public static function getValidationGroup()
    {
        return array(
            'api_exercise_create'
        );
    }

    public static function getSerializationGroup()
    {
        return array(
            'api_exercise_create',
        );
    }

    abstract public function getDependencyFields();
}