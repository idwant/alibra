<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class DragAndDropTemplateDTO extends ExerciseTemplateDTO
{
    public $elements;
    public $possibleAnswers;
    public $audioHint;
    public $audioHintModel;
    public $image;
    public $imageModel;

    public function getDependencyFields()
    {
        return array(
            'audioHintModel' => array(
                'repository' => 'audio',
                'field' => 'audioHintModel',
                'value' => $this->audioHint
            ),
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getAudioHintModel()
    {
        return $this->audioHintModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}