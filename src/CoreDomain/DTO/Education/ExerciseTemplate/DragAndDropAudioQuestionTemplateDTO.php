<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class DragAndDropAudioQuestionTemplateDTO
{
    public $hint;
    public $possibleAnswers;
    public $audio;
    public $audioModel;

    public function getDependencyFields()
    {
        return array(
            'audioModel' => array(
                'repository' => 'audio',
                'field' => 'audio',
                'value' => $this->audio
            )
        );
    }

    /**
     * @return mixed
     */
    public function getAudioModel()
    {
        return $this->audioModel;
    }
}