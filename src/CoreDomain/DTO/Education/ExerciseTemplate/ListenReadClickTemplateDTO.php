<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class ListenReadClickTemplateDTO extends ExerciseTemplateDTO
{
    public $audio;
    public $audioModel;
    public $image;
    public $imageModel;
    public $hint;
    public $question;
    public $elements;

    public function getDependencyFields()
    {
        return array(
            'audioModel' => array(
                'repository' => 'audio',
                'field' => 'audio',
                'value' => $this->audio
            ),
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            ),
            'elements' => array(
                'included' => true,
                'field' => $this->elements
            ),
        );
    }

    /**
     * @return mixed
     */
    public function getAudioModel()
    {
        return $this->audioModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }


}