<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class FiveCardsTemplateDTO extends ExerciseTemplateDTO
{
    public $cards;

    public function getDependencyFields()
    {
        return array(
            'cards' => array(
                'included' => true,
                'field' => $this->cards
            ),
        );
    }
}