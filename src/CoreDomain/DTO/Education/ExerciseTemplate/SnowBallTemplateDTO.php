<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class SnowBallTemplateDTO extends ExerciseTemplateDTO
{
    public $exerciseText;
    public $elements;
    public $audio;
    public $audioModel;

    public function getDependencyFields()
    {
        return array(
            'audioModel' => array(
                'repository' => 'audio',
                'field' => 'audioModel',
                'value' => $this->audio
            )
        );
    }

    /**
     * @return mixed
     */
    public function getAudioModel()
    {
        return $this->audioModel;
    }
}