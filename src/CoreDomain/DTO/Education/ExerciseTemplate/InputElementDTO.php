<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;


class InputElementDTO
{
    public $text;
    public $skip;
}