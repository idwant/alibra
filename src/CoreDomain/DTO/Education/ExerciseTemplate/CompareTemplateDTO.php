<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class CompareTemplateDTO extends ExerciseTemplateDTO
{
    public $answer;
    public $transcription;
    public $audioHint;
    public $audioHintModel;
    public $audioAnswer;
    public $audioAnswerModel;
    public $image;
    public $imageModel;
    public $hint;

    public function getDependencyFields()
    {
        return array(
            'audioHintModel' => array(
                'repository' => 'audio',
                'field' => 'audioHint',
                'value' => $this->audioHint
            ),
            'audioAnswerModel' => array(
                'repository' => 'audio',
                'field' => 'audioAnswer',
                'value' => $this->audioAnswer
            ),
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            ),
        );
    }

    /**
     * @return mixed
     */
    public function getAudioHintModel()
    {
        return $this->audioHintModel;
    }

    /**
     * @return mixed
     */
    public function getAudioAnswerModel()
    {
        return $this->audioAnswerModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}