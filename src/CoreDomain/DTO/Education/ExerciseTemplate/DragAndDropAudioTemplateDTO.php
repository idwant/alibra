<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class DragAndDropAudioTemplateDTO extends ExerciseTemplateDTO
{
    public $questions;

    public function getDependencyFields()
    {
        return array(
            'questions' => array(
                'included' => true,
                'field' => $this->questions
            ),
        );
    }
}