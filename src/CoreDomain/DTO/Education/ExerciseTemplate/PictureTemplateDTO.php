<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class PictureTemplateDTO extends ExerciseTemplateDTO
{
    public $answer;
    public $image;
    public $imageModel;
    public $audioHint;
    public $audioHintModel;
    public $question;

    public function getDependencyFields()
    {
        return array(
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            ),
            'audioHintModel' => array(
                'repository' => 'audio',
                'field' => 'audioHint',
                'value' => $this->audioHint
            ),
        );
    }

    /**
     * @return mixed
     */
    public function getAudioHintModel()
    {
        return $this->audioHintModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }

}