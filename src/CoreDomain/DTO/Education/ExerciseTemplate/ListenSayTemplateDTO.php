<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class ListenSayTemplateDTO extends ExerciseTemplateDTO
{
    public $answer;
    public $audio;
    public $audioModel;
    public $image;
    public $imageModel;
    public $audioHint;
    public $audioHintModel;

    public function getDependencyFields()
    {
        return array(
            'audioModel' => array(
                'repository' => 'audio',
                'field' => 'audioModel',
                'value' => $this->audio
            ),
            'audioHintModel' => array(
                'repository' => 'audio',
                'field' => 'audioHintModel',
                'value' => $this->audioHint
            ),
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'imageModel',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getAudioModel()
    {
        return $this->audioModel;
    }

    /**
     * @return mixed
     */
    public function getAudioHintModel()
    {
        return $this->audioHintModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}