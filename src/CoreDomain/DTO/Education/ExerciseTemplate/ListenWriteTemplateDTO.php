<?php

namespace CoreDomain\DTO\Education\ExerciseTemplate;

class ListenWriteTemplateDTO extends ExerciseTemplateDTO
{
    public $answer;
    public $question;
    public $audio;
    public $audioModel;
    public $image;
    public $imageModel;

    public function getDependencyFields()
    {
        return array(
            'audioModel' => array(
                'repository' => 'audio',
                'field' => 'audio',
                'value' => $this->audio
            ),
            'imageModel' => array(
                'repository' => 'image',
                'field' => 'image',
                'value' => $this->image
            )
        );
    }

    /**
     * @return mixed
     */
    public function getAudioModel()
    {
        return $this->audioModel;
    }

    /**
     * @return mixed
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }
}