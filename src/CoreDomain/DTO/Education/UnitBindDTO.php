<?php

namespace CoreDomain\DTO\Education;

class UnitBindDTO
{
    public $courseId;
    public $unitId;
}