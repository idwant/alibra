<?php
namespace CoreDomain\DTO\Education;

use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\LessonPart;
use CoreDomain\Model\Education\Unit;

class ProgressDTO
{
    public $courseNumber;
    public $unitNumber;
    public $lessonNumber;
    public $lessonPartNumber;

    public $course;
    public $unit;
    public $lesson;
    public $lessonPart;
    public $lessonPartStat;
    public $exerciseType;

    public function getCourseNumber()
    {
        return $this->courseNumber;
    }

    public function setCourseNumber($courseNumber)
    {
        $this->courseNumber = $courseNumber;
        return $this;
    }

    public function getUnitNumber()
    {
        return $this->unitNumber;
    }

    public function setUnitNumber($unitNumber)
    {
        $this->unitNumber = $unitNumber;
        return $this;
    }

    public function getLessonNumber()
    {
        return $this->lessonNumber;
    }

    public function setLessonNumber($lessonNumber)
    {
        $this->lessonNumber = $lessonNumber;
        return $this;
    }

    public function getLessonPartNumber()
    {
        return $this->lessonPartNumber;
    }

    public function setLessonPartNumber($lessonPartNumber)
    {
        $this->lessonPartNumber = $lessonPartNumber;
        return $this;
    }

    /**
     * @return Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    public function setCourse($course = null)
    {
        $this->course = $course;
        return $this;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnit(Unit $unit = null)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return Lesson
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    public function setLesson(Lesson $lesson = null)
    {
        $this->lesson = $lesson;
        return $this;
    }

    /**
     * @return LessonPart
     */
    public function getLessonPart()
    {
        return $this->lessonPart;
    }

    public function setLessonPart(LessonPart $lessonPart = null)
    {
        $this->lessonPart = $lessonPart;
        return $this;
    }

    /**
     * @return LessonPartStatDTO
     */
    public function getLessonPartStat()
    {
        return $this->lessonPartStat;
    }

    public function setLessonPartStat(LessonPartStatDTO $lessonPartStat)
    {
        $this->lessonPartStat = $lessonPartStat;
        return $this;
    }

    public function getExerciseType()
    {
        return $this->exerciseType;
    }

    public function setExerciseType($exerciseType)
    {
        $this->exerciseType = $exerciseType;
        return $this;
    }
}