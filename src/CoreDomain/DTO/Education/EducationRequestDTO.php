<?php

namespace CoreDomain\DTO\Education;

class EducationRequestDTO
{
    public $course;
    public $unit;
    public $lesson;
    public $lessonPart;
    public $exercise;

    public $number;
}