<?php

namespace CoreDomain\DTO\Education;

class LessonBindDTO
{
    public $lessonId;
    public $unitId;
}