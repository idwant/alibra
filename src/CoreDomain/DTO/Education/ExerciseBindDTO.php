<?php

namespace CoreDomain\DTO\Education;

class ExerciseBindDTO
{
    public $exerciseId;
    public $lessonPartId;
}