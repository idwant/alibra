<?php

namespace CoreDomain\DTO\Education;

class LessonPartDTO
{
    public $id;

    public $name;
    public $type;
    public $lessonId;
}