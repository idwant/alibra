<?php

namespace CoreDomain\DTO\Education;

class CourseDTO
{
    public $id;
    public $name;
}