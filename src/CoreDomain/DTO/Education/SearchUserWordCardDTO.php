<?php
namespace CoreDomain\DTO\Education;

class SearchUserWordCardDTO
{
    public $currentUserId;
    public $userId;

    public $wordId;
    public $wordCardId;
    public $groupId;
    public $lessonId;

    public $known;
    public $disabled;
    public $repetitionCount;
    public $repetitionDate;
    public $learnDate;

    public $isDemo;
    
    public function getWordId()
    {
        return $this->wordId;
    }

    public function setWordId($wordId)
    {
        $this->wordId = $wordId;
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function getCurrentUserId()
    {
        return $this->currentUserId;
    }

    public function setCurrentUserId($currentUserId)
    {
        $this->currentUserId = $currentUserId;
        return $this;
    }

    public function getGroupId()
    {
        return $this->groupId;
    }

    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
        return $this;
    }

    public function getLessonId()
    {
        return $this->lessonId;
    }

    public function setLessonId($lessonId)
    {
        $this->lessonId = $lessonId;
    }

    public function getKnown()
    {
        return $this->known;
    }

    public function setKnown($known)
    {
        $this->known = $known;
        return $this;
    }

    public function getDisabled()
    {
        return $this->disabled;
    }

    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
        return $this;
    }

    public function getRepetitionCount()
    {
        return $this->repetitionCount;
    }

    public function setRepetitionCount($repetitionCount)
    {
        $this->repetitionCount = $repetitionCount;
        return $this;
    }

    public function getRepetitionDate()
    {
        return $this->repetitionDate;
    }

    public function setRepetitionDate($repetitionDate)
    {
        $this->repetitionDate = $repetitionDate;
        return $this;
    }

    public function getWordCardId()
    {
        return $this->wordCardId;
    }

    public function setWordCardId($wordCardId)
    {
        $this->wordCardId = $wordCardId;
        return $this;
    }

    public function getLearnDate()
    {
        return $this->learnDate;
    }

    public function setLearnDate($learnDate)
    {
        $this->learnDate = $learnDate;
        return $this;
    }

    public function getIsDemo()
    {
        return $this->isDemo;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}