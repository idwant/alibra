<?php

namespace CoreDomain\DTO\Education;


abstract class ExerciseTemplateDTO
{
    public $name;
    public $taskEn;
    public $taskRu;

    public $audio;
    public $audioModel;
    public $type;
}