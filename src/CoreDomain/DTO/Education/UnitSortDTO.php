<?php

namespace CoreDomain\DTO\Education;

class UnitSortDTO
{
    public $courseId;
    public $unitId;
    public $position;
}