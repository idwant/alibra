<?php

namespace CoreDomain\DTO\Delivery;


class DeliveryTemplateDTO
{
	public $id;
	public $name;
	public $type;
	public $userTypes;
	public $startDate;
	public $endDate;
	public $repeatCount;
	public $repeatStepMinute;
	public $user;

	public $message;
}