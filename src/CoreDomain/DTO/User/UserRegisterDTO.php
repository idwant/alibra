<?php

namespace CoreDomain\DTO\User;

use CoreDomain\Model\User\User;

class UserRegisterDTO
{
    private $email;
    private $password;
    private $roles = [];
    private $token;

    private $lastName;
    private $firstName;
    private $middleName;
    private $phone;
    private $city;
    private $birthday;
    private $agreementNumber;
    private $isDemo = false;

    public function __construct($email, $password, array $roles = [], $token = null,
           $lastName = null, $firstName = null, $middleName = null, $phone = null,
           $city = null, $birthday = null, $agreementNumber = null, $isDemo = false)
    {
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
        $this->token = $token;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->phone = $phone;
        $this->city = $city;
        $this->birthday = $birthday;
        $this->isDemo = $isDemo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles ?: [User::ROLE_OFFLINE_STUDENT];
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getAgreementNumber()
    {
        return $this->agreementNumber;
    }

    /**
     * @return mixed
     */
    public function getIsDemo()
    {
        return $this->isDemo;
    }
}
