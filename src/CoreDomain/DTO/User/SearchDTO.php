<?php

namespace CoreDomain\DTO\User;

class SearchDTO
{
    public $fio;
    public $role;
    public $limit;
    public $offset;

    public function getFio()
    {
        return $this->fio;
    }

    public function setFio($fio)
    {
        $this->fio = $fio;
        return $this;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }
}