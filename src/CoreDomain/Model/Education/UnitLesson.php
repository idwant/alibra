<?php
namespace CoreDomain\Model\Education;

class UnitLesson
{
    private $id;

    private $position;
    private $createdAt;

    private $unit;
    private $lesson;
    private $wordCards;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }
    
    public function updateInfo(Unit $unit, Lesson $lesson, $position)
    {
        $this->unit = $unit;
        $this->lesson = $lesson;
        $this->position = $position;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getLesson()
    {
        return $this->lesson;
    }

    public function getUnit()
    {
        return $this->unit;
    }


    public function getPosition()
    {
        return $this->position;
    }
    
    public function setNumber($number)
    {
        $this->number = $number;
    }
}