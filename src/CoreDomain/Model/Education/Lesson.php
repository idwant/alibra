<?php
namespace CoreDomain\Model\Education;

use CoreDomain\Model\Dictionary\WordCard;

class Lesson
{
    private $id;

    private $name;
    private $isDeleted;
    private $unitLesson;
    private $lessonExercise;
    private $lessonPart;
    private $wordCards;

    private $dashboard;

    public function __construct()
    {
        $this->isDeleted = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function updateInfo($name)
    {
        $this->name = $name;
    }

    public function getUnits()
    {
        $units = [];

        if($this->unitLesson) {
            foreach ($this->unitLesson as $unitLesson) {
                $units[] = $unitLesson->getUnit();
            }
        }

        return $units;
    }

    /**
     * @return WordCard[]
     */
    public function getWordCards()
    {
        return $this->wordCards;
    }

    public function addWordCard(WordCard $wordCard)
    {
        $this->wordCards[] = $wordCard;
        return $this;
    }

    public function getLessonParts()
    {
        return $this->lessonPart;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    public function getDashboard()
    {
        return $this->dashboard;
    }

    public function setDashboard($dashboard)
    {
        $this->dashboard = $dashboard;
    }

    public function getExercises($existExercises)
    {
        $exercises = [];

        if($this->lessonExercise) {
            foreach ($this->lessonExercise as $lessonExercise) {
                $exercise = $lessonExercise->getExercise();
                if(in_array($exercise, $existExercises)) {
                    $exercise = clone $exercise;
                }
                $exercise->setLesson($this);
                $exercises[] = $exercise;
            }
        }

        return $exercises;
    }
}