<?php
namespace CoreDomain\Model\Education;


class Hint
{
    private $id;

    private $text;
    private $name;
    private $maxAttempts;
    private $timeout;
    private $userHint;

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMaxAttempts()
    {
        return $this->maxAttempts;
    }

    /**
     * @param mixed $maxAttempts
     */
    public function setMaxAttempts($maxAttempts)
    {
        $this->maxAttempts = $maxAttempts;
    }

    /**
     * @return integer
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param integer $timeout
     * @return $this
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }

    public function getUserAttempts()
    {
        $attempts = 0;
        if($this->userHint) {
            foreach ($this->userHint as $userHint) {
                $attempts = $userHint->getAttempts();
            }
        }

        return $attempts;
    }

    public function isShow()
    {
        $showDate = null;
        if($this->userHint) {
            /** @var UserHint $userHint */
            foreach ($this->userHint as $userHint) {
                $showDate = $userHint->getShowAgainDate();
            }
        }

        if($showDate) {
            $nowDate = new \DateTime();
            return $nowDate->setTime(0,0,0) >= $showDate->setTime(0,0,0);
        }
        else {
            return true;
        }
    }

}