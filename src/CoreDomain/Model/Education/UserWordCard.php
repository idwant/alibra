<?php
namespace CoreDomain\Model\Education;

use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\User\User;

class UserWordCard
{
    const NUMBER_POSSIBLE_ANSWERS = 20;
    const REPETITION_COUNT = 7;

    private $id;

    private $known;
    private $disabled;

    private $learnDate;

    private $user;
    private $wordCard;
    
    private $repetitionDate;
    private $repetitionRate;
    private $repetitionCount;

    private $possibleAnswers;

    public function __construct(User $user, WordCard $wordCard, $repetitionRate = 'P7D')
    {
        $this->user = $user;
        $this->wordCard = $wordCard;
        $this->known = false;
        $this->disabled = false;
        $this->repetitionCount = 0;
        $this->learnDate = new \DateTime();
        $this->repetitionDate = new \DateTime();
        $this->repetitionRate = $repetitionRate;
    }

    /**
     * @return WordCard
     */
    public function getWordCard()
    {
        return $this->wordCard;
    }

    /**
     * @return string
     */
    public function getRepetitionRate()
    {
        return $this->repetitionRate;
    }

    /**
     * @return int
     */
    public function getRepetitionCount()
    {
        return $this->repetitionCount;
    }

    /**
     * @param int $repetitionCount
     * @return UserWordCard
     */
    public function setRepetitionCount($repetitionCount)
    {
        $this->repetitionCount = $repetitionCount;
        return $this;
    }

    public function increaseRepetitionCount()
    {
        $repetitionDate = (new \DateTime('now'))->add(new \DateInterval($this->getRepetitionRate()));
        
        $repetitionCount = $this->getRepetitionCount();
        $this->setRepetitionDate($repetitionDate)->setRepetitionCount(++$repetitionCount);
    }

    /**
     * @return bool
     */
    public function isKnown()
    {
        return $this->known;
    }

    /**
     * @param boolean $known
     * @return UserWordCard
     */
    public function setKnown($known)
    {
        if ($known) {
            $repetitionDate = (new \DateTime('now'))->add(new \DateInterval($this->getRepetitionRate()));
            $this->setRepetitionDate($repetitionDate);
        }

        $this->known = $known;
        return $this;
    }

    public function getDisabled()
    {
        return $this->disabled;
    }

    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    /**
     * @return \DateTime
     */
    public function getRepetitionDate()
    {
        return $this->repetitionDate;
    }

    /**
     * @param \DateTime $repetitionDate
     * @return UserWordCard
     */
    public function setRepetitionDate($repetitionDate)
    {
        $this->repetitionDate = $repetitionDate;
        return $this;
    }

    /**
     * @param Word[] $possibleAnswers
     */
    public function setPossibleAnswers($possibleAnswers)
    {
        $this->possibleAnswers = $possibleAnswers;
    }

    /**
     * @return Word[]
     */
    public function getPossibleAnswers()
    {
        return $this->possibleAnswers;
    }

    /**
     * @return \DateTime
     */
    public function getLearnDate()
    {
        return $this->learnDate;
    }

    /**
     * @param \DateTime $learnDate
     * @return $this
     */
    public function setLearnDate($learnDate)
    {
        $this->learnDate = $learnDate;
        return $this;
    }
}