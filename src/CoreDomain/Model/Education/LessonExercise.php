<?php
namespace CoreDomain\Model\Education;

class LessonExercise
{
    private $id;

    private $position;
    private $createdAt;

    private $lesson;
    private $exercise;
    private $lessonPart;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function updateInfo(Lesson $lesson, Exercise $exercise, LessonPart $lessonPart, $position)
    {
        $this->exercise = $exercise;
        $this->lesson = $lesson;
        $this->position = $position;
        $this->lessonPart = $lessonPart;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    /**
     * @return mixed
     */
    public function getExercise()
    {
        return $this->exercise;
    }

    /**
     * @return mixed
     */
    public function getLessonPart()
    {
        return $this->lessonPart;
    }
}