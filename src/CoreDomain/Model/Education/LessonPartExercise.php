<?php
namespace CoreDomain\Model\Education;

class LessonPartExercise
{
    private $id;

    private $number;
    private $createdAt;

    private $exercise;

    public function __construct(Exercise $exercise, $number)
    {
        $this->createdAt = new \DateTime();
        $this->exercise = $exercise;
        $this->number = $number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }
}