<?php
namespace CoreDomain\Model\Education;

class CourseUnit
{
    private $id;

    private $createdAt;

    private $course;
    private $unit;
    private $position;
    private $isDemo;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isDemo = false;
    }

    public function updateInfo(Course $course, Unit $unit, $position)
    {
        $this->course = $course;
        $this->unit = $unit;
        $this->position = $position;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function getCourse()
    {
        return $this->course;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function isIsDemo()
    {
        return $this->isDemo;
    }
}