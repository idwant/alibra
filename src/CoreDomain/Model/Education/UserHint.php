<?php

namespace CoreDomain\Model\Education;


use CoreDomain\Model\User\User;

class UserHint
{
    private $id;

    private $attempts;
    private $showAgainDate;
    private $user;
    private $hint;

    public function __construct(User $user, Hint $hint, $attempts = 0)
    {
        $this->user = $user;
        $this->hint = $hint;
        $this->attempts = $attempts;
    }

    /**
     * @return int
     */
    public function getAttempts()
    {
        return $this->attempts;
    }

    /**
     * @param int $attempts
     */
    public function setAttempts($attempts)
    {
        $this->attempts = $attempts;
    }

    public function incrementAttempts()
    {
        $this->attempts++;
    }

    public function setShowAgainDate()
    {
        $date = new \DateTime();
        $this->showAgainDate = $date->setTimestamp(time() + $this->hint->getTimeout());
    }

    /**
     * @return \DateTime
     */
    public function getShowAgainDate()
    {
        return $this->showAgainDate;
    }
}