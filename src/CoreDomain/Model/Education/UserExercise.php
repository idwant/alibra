<?php
namespace CoreDomain\Model\Education;

use CoreDomain\Model\User\User;
use Symfony\Component\Validator\Constraints\Date;

class UserExercise
{
    const STATUS_WAIT = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_FAIL = 3;

    private $id;

    private $startDate;
    private $endDate;
    private $status;
    private $completed;

    private $user;
    private $exercise;
    private $userLesson;

    public function __construct()
    {
        $this->status = self::STATUS_WAIT;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function getCompleted()
    {
        return ($this->completed) ? $this->completed : null;
    }

    public function updateInfo(User $user, Exercise $exercise, UserLesson $userLesson)
    {
        $this->user = $user;
        $this->exercise = $exercise;
        $this->userLesson = $userLesson;

    }

    public function setCompleted($completed = null)
    {
        $this->completed = $completed;
    }

    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    public function setEndDate(\DateTime $endDate = null)
    {
        $this->endDate = $endDate;
    }

    public function updateStatus($status = self::STATUS_WAIT)
    {
        $this->status = $status;
    }
}