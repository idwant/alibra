<?php
namespace CoreDomain\Model\Education;

class Unit
{
    private $id;

    private $name;
    private $isDeleted;
    private $courseUnit;
    private $unitLesson;

    private $dashboard;

    public function __construct()
    {
        $this->isDeleted = false;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUnits()
    {
        $units = [];
        if($this->courseUnit) {
            foreach ($this->courseUnit as $courseUnit) {
                $units[] = $courseUnit->getUnit();
            }
        }

        return $units;
    }

    public function updateInfo($name)
    {
        $this->name = $name;
    }

    public function getLessons()
    {
        $lessons = [];

        if($this->unitLesson) {
            foreach ($this->unitLesson as $unitLesson) {
                $lessons[] = $unitLesson->getLesson();
            }
        }

        return $lessons;
    }

    public function getCourses()
    {
        $courses = [];

        if($this->courseUnit) {
            foreach ($this->courseUnit as $courseUnit) {
                if($courseUnit->getCourse() && !$courseUnit->getCourse()->getIsDeleted()) {
                    $courses[] = $courseUnit->getCourse();
                }
            }
        }

        return $courses;
    }

    public function getDashboard()
    {
        return $this->dashboard;
    }

    public function setDashboard($dashboard)
    {
        $this->dashboard = $dashboard;
    }
}