<?php
namespace CoreDomain\Model\Education;

class Course
{
    private $id;
    private $name;
    private $isDeleted;
    private $isPhonetic;
    private $courseUnit;

    private $users;

    private $dashboard;

    public function __construct()
    {
        $this->isDeleted = false;
        $this->isPhonetic = false;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    public function getIsPhonetic()
    {
        return $this->isPhonetic;
    }

    public function setIsPhonetic($isPhonetic)
    {
        $this->isPhonetic = $isPhonetic;
        return $this;
    }

    public function updateInfo($name)
    {
        $this->name = $name;
    }

    public function getUnits()
    {
        $units = [];
        if($this->courseUnit) {
            foreach ($this->courseUnit as $courseUnit) {
                $units[] = $courseUnit->getUnit();
            }
        }

        return $units;
    }

    public function getDashboard()
    {
        return $this->dashboard;
    }

    public function setDashboard($dashboard)
    {
        $this->dashboard = $dashboard;
    }
}