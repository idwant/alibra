<?php
namespace CoreDomain\Model\Education;

class LessonPart
{
    private $id;

    private $name;
    private $type;
    private $lesson;
    private $lessonExercise;
    private $isDeleted;

    private $dashboard;

    public function __construct()
    {
        $this->isDeleted = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function updateInfo($name, $type, Lesson $lesson)
    {
        $this->name = $name;
        $this->type = $type;
        $this->lesson = $lesson;
    }

    public function delete()
    {
        $this->isDeleted = true;
        $this->name .= (new \DateTime())->format('_YmdHis');
    }

    public function getLesson()
    {
        return $this->lesson;
    }
    
    public function getExercise()
    {
        $exercises = [];

        if($this->lessonExercise) {
            foreach ($this->lessonExercise as $lessonExercise) {
                $exercises[] = $lessonExercise->getExercise();
            }
        }

        return $exercises;
    }

    public function getDashboard()
    {
        return $this->dashboard;
    }

    public function setDashboard($dashboard)
    {
        $this->dashboard = $dashboard;
    }
}