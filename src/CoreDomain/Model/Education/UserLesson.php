<?php
namespace CoreDomain\Model\Education;

use CoreDomain\Model\User\User;
use Symfony\Component\Validator\Constraints\Date;

class UserLesson
{
    const STATUS_WAIT = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_FAIL = 3;

    private $id;

    private $startDate;
    private $endDate;
    private $status;

    private $user;
    private $lesson;

    public function __construct()
    {
        $this->status = self::STATUS_WAIT;
    }

    public function getId()
    {
        return $this->id;
    }

    public function updateInfo(User $user, Lesson $lesson)
    {
        $this->user = $user;
        $this->lesson = $lesson;
    }

    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    public function setEndDate(\DateTime $endDate = null)
    {
        $this->endDate = $endDate;
    }

    public function updateStatus($status = self::STATUS_WAIT)
    {
        $this->status = $status;
    }
}