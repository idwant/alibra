<?php
namespace CoreDomain\Model\Education;

use CoreDomain\DTO\Education\ExerciseRequestDTO;
use CoreDomain\Model\Dictionary\Dictionary;
use CoreDomain\Model\File\Audio;
use CoreDomain\Model\Grammar\Grammar;

class Exercise
{
    const TYPE_GROUP_TASK = 'group_task';
    const TYPE_HOMEWORK = 'homework';
    const TYPE_TEST = 'test';

    private $id;

    private $name;
    private $taskEn;
    private $taskRu;
    private $isDeleted;
    private $params;
    private $type;

    public $template;
    private $grammars;
    private $grammar;
    private $dictionary;
    private $taskAudio;
    private $lessonExercise;

    private $userExercise;
    private $lesson;

    public function getTemplateName()
    {
        return $this->template->getName();
    }

    public function getTemplateType()
    {
        return $this->template->getType();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function getUserExercise()
    {
        return $this->userExercise;
    }

    public function setUserExercise(UserExercise $userExercise)
    {
        $this->userExercise = $userExercise;
    }

    public function getLesson()
    {
        return $this->lesson;
    }

    public function setLesson(Lesson $lesson)
    {
        $this->lesson = $lesson;
    }

    public function updateInfo(ExerciseRequestDTO $exerciseRequestDTO, $audio, $grammar, $dictionary, $params, ExerciseTemplate $exerciseTemplate, $isDeleted = false)
    {
        $this->name = $exerciseRequestDTO->name;
        $this->taskEn = $exerciseRequestDTO->taskEn;
        $this->taskRu = $exerciseRequestDTO->taskRu;
        $this->dictionary = $dictionary;
        $this->grammar = $grammar;
        $this->taskAudio = $audio;
        $this->params = $params;
        $this->type = $exerciseRequestDTO->type;
        $this->template = $exerciseTemplate;
        $this->isDeleted = $isDeleted;
    }

    /**
     * @param mixed $dictionary
     */
    public function setDictionary(Dictionary $dictionary = null) {
        $this->dictionary = $dictionary;
        return $this;
    }


}