<?php
namespace CoreDomain\Model\Education;

class ExerciseTemplate
{
    private $id;

    private $name;
    private $type;
    private $taskEn;
    private $taskRu;
    private $params;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTaskEn()
    {
        return $this->taskEn;
    }

    /**
     * @return mixed
     */
    public function getTaskRu()
    {
        return $this->taskRu;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }


    public function updateInfo($name, $taskRu, $taskEn, $params)
    {
        $this->name = $name;
        $this->taskRu = $taskRu;
        $this->taskEn = $taskEn;
        $this->params = $params;
    }
}