<?php

namespace CoreDomain\Model\Task;

class TaskQueue
{
    private $id;
    private $service;
    private $params;
    private $status;
    private $blockId;
    private $attempts;
    private $created;
    private $dateStart;

    public function __construct(
        $service,
        $params = [],
        TaskStatus $status,
        $attempts = 1
    ) {
        $this->service = $service;
        $this->params = $params;
        $this->status = $status;
        $this->attempts = $attempts;
        $this->created = new \DateTime('now');
        $this->dateStart = new \DateTime('now');
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $blockId
     */
    public function setBlockId($blockId)
    {
        $this->blockId = $blockId;
    }

    /**
     * @param TaskStatus $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    
}