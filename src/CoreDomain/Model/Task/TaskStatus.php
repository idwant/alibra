<?php

namespace CoreDomain\Model\Task;


class TaskStatus
{
    const TASK_WAITE = 1;
    const TASK_IN_PROCESS = 2;
    const TASK_EXECUTED = 3;

    private $id;
    private $name;
    private $title;
    private $description;
}