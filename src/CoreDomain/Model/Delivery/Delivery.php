<?php
namespace CoreDomain\Model\Delivery;

class Delivery
{
    const SEND_STATUS = 0;

    private $id;

    private $type;
    private $userTypes;
    private $deliveryDate;
    private $sendedDate;
    private $message;
    private $sendStatus;

    private $user;
    private $template;

    public function __construct()
    {
        $this->sendStatus = self::SEND_STATUS;
    }
}