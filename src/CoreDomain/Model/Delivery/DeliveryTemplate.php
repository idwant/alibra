<?php
namespace CoreDomain\Model\Delivery;

use AppBundle\Services\Helpers\DeliveryTemplateHelper;
use CoreDomain\DTO\Delivery\DeliveryTemplateDTO;

class DeliveryTemplate
{
    const REPEAT_COUNT = 1000000;
    const REPEAT_STEP_MINUTE = 1440;
    const SENT_COUNT = 0;
    const IS_ACTIVE = 1;

    private $id;
    private $name;
    private $type;
    private $userTypes;
    private $startDate;
    private $endDate;
    private $repeatCount;
    private $repeatStepMinute;
    private $message;
    private $sentCount;
    private $isActive;

    private $user;

    public function __construct()
    {
        $this->sentCount = self::SENT_COUNT;
        $this->isActive = self::IS_ACTIVE;
    }

    public function updateInfo(DeliveryTemplateDTO $dto, $user)
    {
        $this->name = $dto->name;
        $this->type = DeliveryTemplateHelper::getDeliveryTemplateTypeByName($dto->type);
        $this->userTypes = $dto->userTypes;
        $this->startDate = $dto->startDate;
        $this->endDate = $dto->endDate;

        if (!$dto->repeatCount) {
            $dto->repeatCount = self::REPEAT_COUNT;
        }
        if (!$dto->repeatStepMinute) {
            $dto->repeatStepMinute = self::REPEAT_STEP_MINUTE;
        }

        $this->repeatCount = $dto->repeatCount;
        $this->repeatStepMinute = $dto->repeatStepMinute;
        $this->message = $dto->message;

        $this->user = $user;
    }
}