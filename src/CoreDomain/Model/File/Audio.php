<?php
namespace CoreDomain\Model\File;

class Audio extends File
{
    public $id;
    public $path;
    public $name;
    public $originalName;

    public function getUploadDir()
    {
        return 'audio/';
    }

    public function getValidationGroup()
    {
        return 'audio_upload';
    }
}