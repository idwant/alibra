<?php

namespace CoreDomain\Model\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class File
{
    const TYPE_AUDIO = 'audio';
    const TYPE_VIDEO = 'video';
    const TYPE_IMAGE = 'image';

    protected $id;
    protected $path;
    protected $createdAt;
    protected $originalName;
    protected $name;

    protected $file;

    private $fullPath;

    public function __construct(UploadedFile $file = null)
    {
        $this->file = $file;
        $this->originalName = $file->getClientOriginalName();
        $this->name = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
        $this->path = '/files/'.$this->getUploadDir();
        $this->createdAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return null
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/files/' . $this->getUploadDir();
    }

    public function setFullPath($host)
    {
        if (!$this->fullPath) {
            $this->fullPath = $host . $this->path;
        }

        return $this->fullPath;
    }

    public function getFullPath()
    {
        return !$this->fullPath
            ? $this->path . $this->name
            : $this->fullPath . $this->name;
    }

    public static function getAvailableTypes()
    {
        return [static::TYPE_AUDIO, static::TYPE_VIDEO, static::TYPE_IMAGE];
    }

    public abstract function getUploadDir();

    public abstract function getValidationGroup();
}