<?php
namespace CoreDomain\Model\File;

class Video extends File
{
    public $id;
    public $path;
    public $name;
    public $originalName;

    public function getUploadDir()
    {
        return 'video/';
    }

    public function getValidationGroup()
    {
        return 'video_upload';
    }
}