<?php
namespace CoreDomain\Model\Grammar;

use CoreDomain\Model\File\Video;

class Grammar
{
    private $id;

    private $title;
    private $content;
    private $theme;
    private $video;
    private $isDeleted;
    private $isDemo;

    public function updateInfo($title, $content, GrammarTheme $theme, Video $video = null, $isDeleted = false, $isDemo = false)
    {
        $this->title = $title;
        $this->content = $content;
        $this->theme = $theme;
        $this->video = $video;
        $this->isDeleted = $isDeleted;
        $this->isDemo = $isDemo;
    }

    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
}