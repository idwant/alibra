<?php
namespace CoreDomain\Model\Grammar;

class GrammarTheme
{
    private $id;

    private $name;

    private $section;
    private $grammars;

    public function getId()
    {
        return $this->id;
    }

    public function updateInfo($name, GrammarSection $section)
    {
        $this->name = $name;
        $this->section = $section;
    }
}