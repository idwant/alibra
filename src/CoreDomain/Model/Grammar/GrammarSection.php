<?php
namespace CoreDomain\Model\Grammar;

class GrammarSection
{
    private $id;

    private $name;

    private $themes;

    public function getId()
    {
        return $this->id;
    }

    public function updateInfo($name)
    {
        $this->name = $name;
    }
}