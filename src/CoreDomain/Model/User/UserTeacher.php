<?php

namespace CoreDomain\Model\User;

class UserTeacher
{
    private $user;
	private $teacher;

	public function getUser()
	{
		return $this->user;
	}

	public function getTeacher()
	{
		return $this->teacher;
	}

	public function updateInfo(User $user, User $teacher)
	{
		$this->user = $user;
		$this->teacher = $teacher;
	}
}