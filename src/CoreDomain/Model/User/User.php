<?php

namespace CoreDomain\Model\User;

use CoreDomain\DTO\User\ProfileDTO;
use CoreDomain\Model\Education\Course;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class User implements AdvancedUserInterface
{

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPERADMIN = 'ROLE_SUPERADMIN';

    const ROLE_TEACHER = 'ROLE_TEACHER';
    const ROLE_CORPORATE_USER = 'ROLE_CORPORATE_USER';
    const ROLE_OFFLINE_STUDENT = 'ROLE_OFFLINE_STUDENT';
    const ROLE_ONLINE_STUDENT = 'ROLE_ONLINE_STUDENT';
    const ROLE_METHODOLOGIST = 'ROLE_METHODOLOGIST';

    private $id;
    private $email;
    private $password;
    private $salt;
    private $roles;

    private $lastName;
    private $firstName;
    private $middleName;
    private $phone;
    private $city;
    private $birthday;
    private $agreementNumber;

    private $status;
    private $lastActivityDate;
    private $lastAuthDate;

    private $settings;

    private $teachers;
    private $students;
    private $employers;
    private $employees;
    private $courses;
    private $confirmationToken;
    private $authenticationToken;
    private $userHint;

    // Not mapped, текущая сессия пользователя, инициализируется в ApiProvider
    private $session;

    private $isDemo = false;

    public function __construct($email, Password $password, array $roles = [])
    {
        $this->email = $email;
        $this->password = $password->getPassword();
        $this->salt = $password->getSalt();
        if(!$roles) {
            $roles = [self::ROLE_USER];
        }
        $this->roles = $roles;
        $this->isDemo = false;
    }

    public function login()
    {
        $this->lastAuthDate = new \DateTime();
        return new UserSession($this);
    }

    public function logout()
    {
        if($this->session instanceof UserSession) {
            $this->session->finish();
        }
    }

    public function changePassword(Password $password)
    {
        $this->password = $password->getPassword();
        $this->salt = $password->getSalt();
    }

    public function updateProfile(ProfileDTO $dto)
    {
        $this->email = $dto->email;
        $this->lastName = $dto->lastName;
        $this->firstName = $dto->firstName;
        $this->middleName = $dto->middleName;
        $this->phone = $dto->phone;
        $this->city = $dto->city;
        $this->birthday = $dto->birthday;
        $this->roles = $dto->roles;
        $this->agreementNumber = $dto->agreementNumber;
        $this->isDemo = $dto->isDemo;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getTeachers()
    {
        return $this->teachers;
    }

    public function getEmployers()
    {
        return $this->employers;
    }

    public function getStudents()
    {
        return $this->students;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return true;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param mixed $session
     */
    public function setSession(UserSession $session)
    {
        $this->session = $session;
    }

    /**
     * @return mixed
     */
    public function getAuthenticationToken()
    {
        return $this->authenticationToken;
    }

    /**
     * @param mixed $authenticationToken
     */
    public function setAuthenticationToken($authenticationToken)
    {
        $this->authenticationToken = $authenticationToken;
        return $this;
    }

    public function setTeachers($teachers)
    {
        $this->teachers = $teachers;
    }

    public function setStudents($students)
    {
        $this->students = $students;
    }

    public function setEmployers($employers)
    {
        $this->employers = $employers;
    }

    public function hasRole($role)
    {
        return in_array($role, $this->roles);
    }

    public static function getAvailableRoles()
    {
        return [
            self::ROLE_USER,
            self::ROLE_ADMIN,
            self::ROLE_SUPERADMIN,
            self::ROLE_TEACHER,
            self::ROLE_CORPORATE_USER,
            self::ROLE_METHODOLOGIST,
            self::ROLE_OFFLINE_STUDENT,
            self::ROLE_ONLINE_STUDENT
        ];
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getFirstName()
    {
        return $this->lastName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getMiddleName()
    {
        return $this->middleName;
    }

    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
        return $this;
    }

    public function getLastActivityDate()
    {
        return $this->lastActivityDate;
    }
    public function setLastActivityDate($lastActivityDate)
    {
        $this->lastActivityDate = $lastActivityDate;
        return $this;
    }

    public function getLastAuthDate()
    {
        return $this->lastAuthDate;
    }
    public function setLastAuthDate($lastAuthDate)
    {
        $this->lastAuthDate = $lastAuthDate;
    }

    public function getAgreementNumber()
    {
        return $this->agreementNumber;
    }

    public function setAgreementNumber($agreementNumber)
    {
        $this->agreementNumber = $agreementNumber;
        return $this;
    }

    /**
     * @return Course[]
     */
    public function getCourses()
    {
        return $this->courses;
    }

    public function getActiveCourses()
    {
        if ($this->courses) {
            return array_values(array_filter($this->courses->getValues(), function (Course $course) {
                return !$course->getIsDeleted();
            }));
        }

        return [];
    }

    public function setCourses($courses)
    {
        $this->courses = $courses;
        return $this;
    }

    public function getIsDemo()
    {
        return $this->isDemo;
    }

    public function setIsDemo($isDemo)
    {
        $this->isDemo = $isDemo;
        return $this;
    }
}
