<?php
namespace CoreDomain\Model\Common;

use CoreDomain\DTO\Common\FeedbackDTO;
use CoreDomain\Model\User\User;

class Feedback
{
    private $id;

    private $email;
    private $title;
    private $message;
    private $isActive;
    private $createdAt;

    private $user;

    public function __construct()
    {
        $this->isActive = true;
        $this->createdAt = new \DateTime();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function isIsActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user = null)
    {
        $this->user = $user;
        return $this;
    }

    public function updateInfo(FeedbackDTO $feedbackDTO)
    {
        $this
            ->setEmail($feedbackDTO->email)
            ->setTitle($feedbackDTO->title)
            ->setMessage($feedbackDTO->message);

        return $this;
    }
}