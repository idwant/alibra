<?php
namespace CoreDomain\Model\Dictionary;

use CoreDomain\DTO\Dictionary\WordCardDTO;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\File\Audio;
use CoreDomain\Model\File\Image;
use Doctrine\Common\Collections\ArrayCollection;

class WordCard
{
    private $id;

    private $sample;
    private $translation;
    private $transcription;
    private $isDeleted;

    private $word;
    private $image;
    private $audio;
    private $sampleAudio;

    private $groups;
    private $lessons;

    public function updateInfo(
        WordCardDTO $wordCardDTO,
        Word $word,
        Audio $audio = null,
        Audio $sampleAudio = null,
        Image $image = null,
        $isDeleted = false
    ) {
        $this->sample = $wordCardDTO->sample;
        $this->word = $word;
        $this->image = $image;
        $this->audio = $audio;
        $this->sampleAudio = $sampleAudio;
        $this->translation = $wordCardDTO->translation;
        $this->transcription = $wordCardDTO->transcription;
        $this->isDeleted = $isDeleted;
    }

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getWordCardGroups()
    {
        return $this->groups->getValues();
    }

    public function addWordCardGroup(WordCardGroup $group)
    {
        $this->groups->add($group);
        return $this;
    }

    public function removeWordCardGroup(WordCardGroup $group)
    {
        $this->groups->removeElement($group);
        return $this;
    }

    public function getLessons()
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson)
    {
        $this->lessons[] = $lesson;
        return $this;
    }

    public function getActiveLessons()
    {
        if ($this->lessons) {
            return array_values(array_filter($this->lessons->getValues(), function (Lesson $lesson) {
                return !$lesson->isIsDeleted();
            }));
        }

        return [];
    }
}