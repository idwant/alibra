<?php

namespace CoreDomain\Model\Dictionary;

use CoreDomain\Model\File\Audio;

class WordTranslation
{
	private $id;

	private $word;
	private $translation;
	private $sample;
	private $isDeleted;
	private $sampleAudio;

	public function __construct()
	{
		$this->isDeleted = false;
	}

	public function getId()
	{
		return $this->id;
	}

    public function getWord()
    {
        return $this->word;
    }

    public function getTranslation()
    {
        return $this->translation;
    }

    public function getSample()
    {
        return $this->sample;
    }

    public function getSampleAudio()
    {
        return $this->sampleAudio;
    }

	public function isDeleted() {
		return $this->isDeleted;
	}

	public function updateInfo(
		Word $word,
	    $translation,
	    $sample,
        Audio $sampleAudio = null)
	{
		$this->word = $word;
		$this->translation = $translation;
		$this->sample = $sample;
		$this->sampleAudio = $sampleAudio;
	}
}