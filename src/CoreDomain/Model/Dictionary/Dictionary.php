<?php
namespace CoreDomain\Model\Dictionary;

class Dictionary
{
    private $id;
    private $name;
    private $isDeleted;
    private $words;

    public function updateInfo($name)
    {
        $this->name = $name;
        $this->isDeleted = false;
    }

    public function addWord(Word $word)
    {
        $this->words[] = $word;
    }

    public function deleteWord(Word $word)
    {
        $this->words->removeElement($word);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getWords()
    {
        return $this->words;
    }

    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}