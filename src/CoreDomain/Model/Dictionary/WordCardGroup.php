<?php

namespace CoreDomain\Model\Dictionary;

use CoreDomain\DTO\Dictionary\WordCardGroupDTO;
use Doctrine\Common\Collections\ArrayCollection;

class WordCardGroup
{
    private $id;
    private $name;
    private $description;
    private $translation;
    private $isDeleted;

    private $wordCards;
    private $category;

    public function __construct()
    {
        $this->wordCards = new ArrayCollection();
        $this->isDeleted = false;
    }

    public function addWordCard(WordCard $wordCard)
    {
        $this->wordCards->add($wordCard);
        return $this;
    }

    public function removeWordCard(WordCard $wordCard)
    {
        $this->wordCards->removeElement($wordCard);
        return $this;
    }

    public function updateInfo(WordCardGroupDTO $groupDTO, WordCardCategory $category)
    {
        $this->name = $groupDTO->name;
        $this->translation = $groupDTO->translation;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }
}