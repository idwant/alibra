<?php

namespace CoreDomain\Model\Dictionary;

use CoreDomain\DTO\Dictionary\WordCardCategoryDTO;
use CoreDomain\Model\File\Image;

class WordCardCategory
{
    private $id;
    private $name;
    private $description;
    private $translation;
    private $isDeleted;

    private $groups;
    private $image;
    private $imageBig;

    private $groupsCount;

    public function __construct()
    {
        $this->isDeleted = false;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function updateInfo(WordCardCategoryDTO $categoryDTO, Image $image = null, Image $imageBig = null)
    {
        $this->name = $categoryDTO->name;
        $this->description = $categoryDTO->description;
        $this->translation = $categoryDTO->translation;
        $this->image = $image;
        $this->imageBig = $imageBig;
    }

    /**
     * @return boolean
     */
    public function isIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function setImageBig($imageBig)
    {
        $this->imageBig = $imageBig;
        return $this;
    }

    public function setGroupsCount($groupsCount)
    {
        $this->groupsCount = $groupsCount;
    }

    public function getGroupsCount()
    {
        if ($this->groupsCount != 0) {
            return $this->groupsCount;
        }

        return count(array_filter($this->groups->getValues(), function ($group) {
            return !$group->isIsDeleted();
        }));
    }


    public function getActiveGroups()
    {
        if($this->groups) {
            return array_values(array_filter($this->groups->getValues(), function (WordCardGroup $wordCardGroup) {
                return !$wordCardGroup->isIsDeleted();
            }));
        }

        return [];
    }
}