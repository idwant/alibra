<?php

namespace CoreDomain\Model\Dictionary;


class Word
{
    private $id;

    private $word;
    private $transcription;
    private $isDeleted;
    private $image;
    private $audio;

    private $translations;

    public function __construct()
    {
        $this->isDeleted = false;
        $this->translations = [];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTranscription()
    {
        return $this->transcription;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getTranslations()
    {
        $wordTranslations = [];
        if ($this->translations) {
            foreach($this->translations as $translation) {
                if (!$translation->isDeleted()) {
                    $wordTranslations[] = $translation;
                }
            }
        }

        usort($wordTranslations, function(WordTranslation $translation1, WordTranslation $translation2) {
            return ($translation1->getId() < $translation2->getId()) ? -1 : 1;
        });

        return $wordTranslations;
    }

    public function updateInfo($word, $transcription,
                               $image, $audio)
    {
        $this->word = $word;
        $this->transcription = $transcription;
        $this->image = $image;
        $this->audio = $audio;
    }

    public function setTranslations(array $translations)
    {
        $this->translations = $translations;
    }
}