<?php

namespace CoreDomain\Repository\Delivery;

use CoreDomain\Model\Delivery\DeliveryTemplate;

interface DeliveryTemplateRepositoryInterface
{
	public function findAll();
	public function findOneById($id);
	public function addAndSave(DeliveryTemplate $deliveryTemplate);
	public function deleteById($id);
}