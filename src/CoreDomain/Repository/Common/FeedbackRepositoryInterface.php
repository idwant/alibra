<?php
namespace CoreDomain\Repository\Common;

use CoreDomain\Model\Common\Feedback;

interface FeedbackRepositoryInterface
{
    public function addAndSave(Feedback $feedback);
}