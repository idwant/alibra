<?php

namespace CoreDomain\Repository\Task;

interface TaskStatusRepositoryInterface
{
    public function findOneById($id);
}