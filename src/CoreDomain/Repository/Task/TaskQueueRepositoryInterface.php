<?php

namespace CoreDomain\Repository\Task;

use CoreDomain\Model\Task\TaskQueue;

interface TaskQueueRepositoryInterface
{
    /**
     * Метод достает одну задачу ждущую выполнения
     * @return TaskQueue
     */
    public function findOneForExecute();

    public function addAndSave(TaskQueue $task);

    /**
     * Снимает блокировку с задачи
     * @param TaskQueue $task
     * @return mixed
     */
    public function unlock(TaskQueue $task);
}