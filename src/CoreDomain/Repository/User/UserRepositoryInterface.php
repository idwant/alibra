<?php

namespace CoreDomain\Repository\User;

use CoreDomain\DTO\User\SearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\User\User;

interface UserRepositoryInterface
{
    /** @return \CoreDomain\Model\User\User */
    public function findOneByEmail($email);
    public function findOneById($id);
    public function findOneByToken($token);
    /** @return \CoreDomain\Model\User\User */
    public function findOneByAuthToken($token);

    public function search(SearchDTO $searchDTO, $onlyCount = false);

    public function add(User $user);
    public function addAndSave(User $user);

    public function getUserCourseByLesson(Lesson $lesson, User $user);

    public function addTeacherToUser(User $user, User $teacher);
    public function addEmployerToUser(User $user, User $employer);
    public function addStudentToTeacher(User $teacher, User $student);
    public function addEmployeeToEmployer(User $employer, User $employee);
    public function addCourse(User $user, Course $course);

    public function deleteTeacherByUser(User $user, User $teacher);
    public function deleteEmployerByUser(User $user, User $employer);
    public function deleteStudentByTeacher(User $teacher, User $student);
    public function deleteEmployeeByEmployer(User $employer, User $employee);
    public function deleteCourse(User $user, Course $course);
}
