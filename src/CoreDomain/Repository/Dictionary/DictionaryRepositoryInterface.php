<?php

namespace CoreDomain\Repository\Dictionary;



use CoreDomain\Model\Dictionary\Dictionary;

interface DictionaryRepositoryInterface
{
    public function search($query, $onlyCount = false, $limit = null, $offset = null);
    public function findOneById($id);
    public function findOneByName($name);

    public function addAndSave(Dictionary $dictionary);
    public function deleteById($id);
}