<?php

namespace CoreDomain\Repository\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardDTO;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Education\Lesson;

interface WordCardRepositoryInterface
{
    public function addAndSave(WordCard $card);

    public function findAll();
    public function findOneById($id);
    public function deleteById($id);
    public function search(SearchWordCardDTO $searcDTO, $onlyCount = false);

    public function addLesson(WordCard $card, Lesson $lesson);
    public function deleteLesson(WordCard $card, Lesson $lesson);
}