<?php

namespace CoreDomain\Repository\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;
use CoreDomain\Model\Dictionary\WordCardGroup;
use CoreDomain\Model\User\User;

interface WordCardGroupRepositoryInterface
{
    public function addAndSave(WordCardGroup $group);

    public function findOneById($id);
    public function findAllByUser(User $user);
    public function findAllWithStatByUser(SearchWordCardGroupDTO $searchDTO);

    public function search(SearchWordCardGroupDTO $searchDTO, $onlyCount = false);
}