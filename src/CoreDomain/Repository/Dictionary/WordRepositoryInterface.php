<?php

namespace CoreDomain\Repository\Dictionary;


use CoreDomain\DTO\Dictionary\WordDTO;
use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\Dictionary;
use CoreDomain\Model\Dictionary\WordTranslation;

interface WordRepositoryInterface
{
    /**
     * @param $id
     * @return Word|null
     */
    public function findOneById($id);
    public function findAllByDictionary(Dictionary $dictionary, $word = null, $onlyCount = false, $limit = null, $offset = null);
    public function findDictionaryWord($dictionaryId, $wordId);
    public function findWordTranslationById($id);
    public function search(WordDTO $wordDTO, $onlyCount = false, $limit = null, $offset = null);

    public function addAndSave(Word $word);
    public function addAndSaveWordTranslation(WordTranslation $wordTranslation);

    public function setDeletedById($id);
    public function deleteWordTranslation($wordTranslationId);

    public function translateWord($word);
    public function getRandomWords($num = 3, $wordId = null);
}