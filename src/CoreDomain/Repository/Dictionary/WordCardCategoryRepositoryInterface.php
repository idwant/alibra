<?php

namespace CoreDomain\Repository\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardCategoryDTO;
use CoreDomain\Model\Dictionary\WordCardCategory;

interface WordCardCategoryRepositoryInterface
{
    public function addAndSave(WordCardCategory $category);

    public function findOneById($id);

    public function search(SearchWordCardCategoryDTO $searchDTO, $onlyCount = false);
}