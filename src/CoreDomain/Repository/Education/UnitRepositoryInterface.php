<?php

namespace CoreDomain\Repository\Education;


use CoreDomain\DTO\Education\UnitBindDTO;
use CoreDomain\DTO\Education\UnitSortDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\CourseUnit;
use CoreDomain\Model\Education\Unit;

interface UnitRepositoryInterface
{
    public function findAllByDTO(UnitSearchDTO $unitSearchDTO, $onlyCount = false);

    /**
     * @param $id
     * @return Unit|null
     */
    public function findOneById($id);
    
    public function findOneByName($name);
    public function findCourseUnitByDTO(UnitBindDTO $unitBindDTO);

    public function addAndSave(Unit $unit);
    public function addAndSaveCourseUnit(CourseUnit $courseUnit);

    public function setDeletedById($id);
    public function findMaxPosition($courseId);
    public function sort(UnitSortDTO $unitSortDTO);
    public function decrementPositions(Course $course, $from = NULL);
    public function search(UnitSearchDTO $unitSearchDTO);
}