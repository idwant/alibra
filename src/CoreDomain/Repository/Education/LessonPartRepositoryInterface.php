<?php

namespace CoreDomain\Repository\Education;

use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonPartDTO;
use CoreDomain\Model\Education\LessonPart;
use CoreDomain\Model\Education\Lesson;

interface LessonPartRepositoryInterface
{
    public function findAllByDTO(EducationRequestDTO $dto);

    /**
     * @param $id
     * @return LessonPart
     */
    public function findOneById($id);
    
    public function findByLessonId($lessonId);
    public function findByName(LessonPartDTO $lessonPartDTO);

    public function addAndSave(LessonPart $lessonPart);
}