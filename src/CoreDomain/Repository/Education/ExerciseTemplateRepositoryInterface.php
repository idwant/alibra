<?php

namespace CoreDomain\Repository\Education;


use CoreDomain\Model\Education\ExerciseTemplate;

interface ExerciseTemplateRepositoryInterface
{
    /** @return \CoreDomain\Model\Education\ExerciseTemplate */
    public function findOneById($id);
    public function findOneByType($type);

    public function addAndSave(ExerciseTemplate $exerciseTemplate);
}