<?php

namespace CoreDomain\Repository\Education;


use CoreDomain\DTO\Dashboard\DashboardSearchDTO;
use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\ExerciseBindDTO;
use CoreDomain\DTO\Education\ExerciseSortDTO;
use CoreDomain\DTO\Education\SearchUserProgressDTO;
use CoreDomain\DTO\Student\Education\ExerciseSearchDTO;
use CoreDomain\DTO\Student\Education\LessonPartSearchDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Exercise;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\LessonExercise;
use CoreDomain\Model\Education\LessonPart;
use CoreDomain\Model\Education\Unit;
use CoreDomain\Model\Education\UserExercise;
use CoreDomain\Model\Education\UserLesson;
use CoreDomain\Model\User\User;

interface ExerciseRepositoryInterface
{
    public function findLessonExerciseByDTO(ExerciseBindDTO $exerciseBindDTO);
    public function findLessonExerciseByLessonPart(LessonPart $lessonPart);
    public function findLessonExercise(Lesson $lesson, Exercise $exercise);

    public function findOneById($id);
    public function findOneByName($name);
    public function deleteById($id);
    public function addAndSave(Exercise $entity);
    public function addAndSaveLessonExercise(LessonExercise $lessonExercise);

    public function findMaxPosition($unitId);
    public function sort(ExerciseSortDTO $exerciseSortDTO);
    public function decrementPositions(Lesson $lesson, $from = NULL);
    public function search(ExerciseSearchDTO $exerciseSearchDTO);

    public function findCoursesStats(DashboardSearchDTO $searchDTO);
    public function findUnitsStats(DashboardSearchDTO $searchDTO);
    public function findLessonsStats(DashboardSearchDTO $searchDTO);
    public function findLessonPartsStats(DashboardSearchDTO $searchDTO);
    public function findUserExercise(Exercise $exercise, User $user, UserLesson $userLesson);

    public function getUserProgresStat(SearchUserProgressDTO $searchProgressDTO, $onlyCount = false);

    public function addAndSaveUserExercise(UserExercise $userExercise);
}