<?php

namespace CoreDomain\Repository\Education;

use CoreDomain\DTO\Education\SearchUserWordCardDTO;
use CoreDomain\DTO\Education\StatUserWordCardDTO;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Education\UserWordCard;
use CoreDomain\Model\User\User;

interface UserWordCardRepositoryInterface
{
    public function save(UserWordCard $userWordCard);
    public function addAndSave(UserWordCard $userWordCard);

    /**
     * @param $id
     * @return UserWordCard
     */
    public function findOneById($id);

    /**
     * @param SearchUserWordCardDTO $searchDTO
     * @return WordCard[]
     */
    public function searchWordCards(SearchUserWordCardDTO $searchDTO);

    /**
     * @param SearchUserWordCardDTO $searchDTO
     * @return StatUserWordCardDTO[]
     */
    public function statWordCards(SearchUserWordCardDTO $searchDTO);

    /**
     * @param SearchUserWordCardDTO $searchDTO
     * @return UserWordCard[]
     */
    public function searchUserWordCards(SearchUserWordCardDTO $searchDTO);

    public function findAllByUser(User $user);
}