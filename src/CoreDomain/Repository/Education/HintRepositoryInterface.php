<?php

namespace CoreDomain\Repository\Education;


use CoreDomain\Model\Education\UserHint;
use CoreDomain\Model\User\User;

interface HintRepositoryInterface
{
    public function findWithUser(User $user);
    public function findOneByName($name);
    public function findUserHint(User $user,$name);

    public function addAndSaveUserHint(UserHint $userHint);
}