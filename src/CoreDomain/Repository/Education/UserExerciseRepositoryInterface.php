<?php

namespace CoreDomain\Repository\Education;

interface UserExerciseRepositoryInterface
{
    public function findOneByUserAndLessonAndExercise($userId, $lessonId, $exerciseId);
}