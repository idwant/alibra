<?php

namespace CoreDomain\Repository\Education;


use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonBindDTO;
use CoreDomain\DTO\Education\LessonSortDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\Model\Education\Unit;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\UnitLesson;
use CoreDomain\Model\Education\UserLesson;
use CoreDomain\Model\User\User;

interface LessonRepositoryInterface
{
    /**
     * @param $id
     * @return Lesson
     */
    public function findOneById($id);
    public function findOneByName($name);
    public function findAllByDTO(LessonSearchDTO $lessonSearchDTO, $onlyCount = false);
    public function findByDTO(EducationRequestDTO $dto);
    public function findUnitLessonByDTO(LessonBindDTO $lessonBindDTO);

    public function addAndSave(Lesson $lesson);
    public function addAndSaveUnitLesson(UnitLesson $unitLesson);
    public function addAndSaveUserLesson(UserLesson $unitLesson);

    public function setDeletedById($id);
    public function findMaxPosition($unitId);
    public function sort(LessonSortDTO $lessonSortDTO);
    public function decrementPositions(Unit $unit, $from = NULL);
    public function search(LessonSearchDTO $lessonSearchDTO);
    public function findUserLesson(Lesson $lesson, User $user);
}