<?php

namespace CoreDomain\Repository\Education;

use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use CoreDomain\Model\Education\Course;

interface CourseRepositoryInterface
{
    public function findAllByDTO(CourseSearchDTO $courseSearchDTO, $onlyCount = false);

    /**
     * @param $id
     * @return Course|null
     */
    public function findOneById($id);
    
    public function findOneByName($name);

    public function addAndSave(Course $course);
    public function setDeletedById($id);
    public function search(CourseSearchDTO $courseSearchDTO);
}