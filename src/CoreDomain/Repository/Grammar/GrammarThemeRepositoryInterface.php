<?php

namespace CoreDomain\Repository\Grammar;


use CoreDomain\DTO\Grammar\GrammarThemeSearchDTO;
use CoreDomain\Model\Grammar\GrammarSection;
use CoreDomain\Model\Grammar\GrammarTheme;

interface GrammarThemeRepositoryInterface
{
    public function search(GrammarThemeSearchDTO $searchDTO, $onlyCount);
    public function findAllBySection(GrammarSection $grammarSection);
    public function findOneById($id);

    public function addAndSave(GrammarTheme $grammarTheme);
    public function deleteById($id);
}