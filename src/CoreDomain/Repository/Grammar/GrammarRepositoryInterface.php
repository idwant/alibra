<?php

namespace CoreDomain\Repository\Grammar;


use CoreDomain\DTO\Grammar\GrammarSearchDTO;
use CoreDomain\Model\Grammar\Grammar;
use CoreDomain\Model\Grammar\GrammarTheme;

interface GrammarRepositoryInterface
{
    public function findOneById($id);
    public function search(GrammarSearchDTO $grammarSearchDTO, $onlyCount = false, $limit = null, $offset = null);
    public function findAllByTheme(GrammarTheme $grammarTheme);
    public function addAndSave(Grammar $grammar);
    public function deleteById($id);

    public function searchByTitle($grammar);
}