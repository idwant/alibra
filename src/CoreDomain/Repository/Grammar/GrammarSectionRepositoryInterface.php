<?php

namespace CoreDomain\Repository\Grammar;


use CoreDomain\DTO\Grammar\GrammarSectionSearchDTO;
use CoreDomain\Model\Grammar\GrammarSection;

interface GrammarSectionRepositoryInterface
{
    public function search(GrammarSectionSearchDTO $searchDTO);
    public function findOneById($id);
    public function findOneByName($name);

    public function addAndSave(GrammarSection $grammarSection);
    public function deleteById($id);
}