<?php

namespace AppBundle\Command;

use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;

use CoreDomain\Model\Dictionary\Dictionary;

use CoreDomain\Repository\Dictionary\DictionaryRepositoryInterface;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadDictionaryCommand extends ContainerAwareCommand
{

	protected function configure()
	{
		$this
			->setName('dictionary:upload')
			->setDescription('Upload dictionary');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		/** @var EntityManagerInterface $em */
		$em = $this->getContainer()->get('doctrine')->getManager();
		/** @var Connection $connection */
		$connection = $em->getConnection();

		$connection->beginTransaction();

		try {
			$dictionaryRepository = $this->getContainer()->get('app.repository.dictionary');

			//Получаем список групп
			$listWordCardGroups = $this->getWordCardGroups();

			//Создаем Словари и слова к ним
			foreach ($listWordCardGroups as $itemGroup) {

				$category = $itemGroup->getCategory();
				
				$nameDictionary = $category->getName()." - ".$itemGroup->getName();

				echo "Dictionary=" . $nameDictionary . PHP_EOL;

				//Проверяем на наличие словоря, иначе создаем новый
				$isDictionary = $dictionaryRepository->findOneByName($nameDictionary);
				$dictionary = empty($isDictionary) ? $this->createDictionary($nameDictionary) : $isDictionary;

				//Добавляем слова в словарь
				$dictionaryRepository->addWords($dictionary, $itemGroup);
			}

			$connection->commit();
		} catch (\Exception $e) {
			$connection->rollBack();

			throw $e;
		}
	}

	/**
	 * @return Dictionary
	 */
	private function createDictionary($name)
	{
		/** @var DictionaryRepositoryInterface $dictionaryRepository */
		$dictionaryRepository = $this->getContainer()->get('app.repository.dictionary');

		$dictionary = new Dictionary();
		$dictionary->setName($name);
		$dictionary->setIsDeleted(false);

		$dictionaryRepository->addAndSave($dictionary);

		return $dictionary;
	}

	private function getWordCardGroups()
	{
		$wordCardGroupRepository =  $this->getContainer()->get('app.repository.dictionary.word_card_group');
		return $wordCardGroupRepository->search(new SearchWordCardGroupDTO);
	}
}