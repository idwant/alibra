<?php

namespace AppBundle\Command;

use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Education\Lesson;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class BindWordCardToLessonCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('word_card:bind')
            ->setDescription('Bind word card to lesson from CSV')
            ->setDefinition(array(
                new InputArgument('path', InputArgument::REQUIRED, 'Path')
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');

        $handle = fopen($path, 'r');
        $header = array_flip(fgetcsv($handle, 2000, ';'));

        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var Connection $connection */
        $connection = $em->getConnection();

        $connection->beginTransaction();

        try {
            $category = null;
            $theme = null;
            $lesson = null;
            $words = [];
            $i = 0;
            while ($row = fgetcsv($handle, 2000, ';')) {
                if($row[$header['theme']] && $row[$header['theme']] == '-') {
                    continue;
                }
                if(!empty($row[$header['lesson']])) {
                    if($lesson) {
                        echo 'Всего слов по уроку '.$lesson->getName().': ' .count($words).PHP_EOL.PHP_EOL;
                        foreach ($words as $word) {
                            $this->addWord($lesson, $word, $em);
                        }
                    }
                    $words = [];
                    $theme = null;
                    $lesson = $this->findLesson($row[$header['lesson']].'l');
                    if(!$lesson) {
                        echo 'Урок ' . $row[$header['lesson']] . ' не найден'.PHP_EOL;
                    }
                    else {
                        echo 'Урок: ' . $row[$header['lesson']];
                    }
                }
                if(!$lesson) {
                    continue;
                }

                if(!empty($row[$header['category']])) {
                    $category = $row[$header['category']];
                    echo PHP_EOL.'Категория: '.$category;
                }

                if(!empty($row[$header['theme']])) {
                    $theme = quotemeta($row[$header['theme']]);
                    echo 'Тема: '.$theme;
                }

                if($theme) {
                    $newWords = $this->findWord($theme, $category);
                    $words = array_merge($words, $newWords);
                    //echo PHP_EOL.'Найдено слов по теме '.$theme.': '.count($newWords);
                }
                else {
                    throw new \Exception('Тема не задана');
                }

                echo ++$i .PHP_EOL;
            }
            if($lesson) {
                echo 'Всего слов по уроку '.$lesson->getName().': ' .count($words).PHP_EOL.PHP_EOL;
                foreach ($words as $word) {
                    $this->addWord($lesson, $word, $em);
                }
            }

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();

            throw $e;
        }

        fclose($handle);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();
        if (!$input->getArgument('path')) {
            $question = new Question('Please enter a path:');
            $question->setValidator(function($path) {
                if (empty($path)) {
                    throw new \Exception('Path can not be empty');
                }
                return $path;
            });
            $questions['path'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    private function findWord($theme, $category = '')
    {
        return $this->getContainer()->get('app.repository.dictionary.word_card')
            ->findByCategoryAndTheme($theme, $category);
    }

    private function findLesson($name)
    {
        return $this->getContainer()->get('app.repository.education.lesson')
            ->findOneByName($name);
    }

    /**
     * @param Lesson $lesson
     * @param WordCard $word
     * @param EntityManagerInterface $em
     */
    private function addWord($lesson, $word, $em)
    {
        $lesson->addWordCard($word);
        $word->addLesson($lesson);

        $em->persist($lesson);
        $em->persist($word);

        $em->flush();
    }
}

