<?php

namespace AppBundle\Command;

use CoreDomain\Model\File\File;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class UploadFileToEntityCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('file:upload-to-entity')
            ->setDescription('Upload file to Entity')
            ->setDefinition(array(
                new InputArgument('path', InputArgument::REQUIRED, 'Path'),
                new InputArgument('type', InputArgument::REQUIRED, 'File type'),
                new InputArgument('table', InputArgument::REQUIRED, 'Table'),
                new InputArgument('setter', InputArgument::REQUIRED, 'Setter'),
                new InputArgument('id', InputArgument::REQUIRED, 'Id')
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $type = $input->getArgument('type');
        $table = $input->getArgument('table');
        $setter = $input->getArgument('setter');
        $id = $input->getArgument('id');

        echo $table . PHP_EOL;

        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var Connection $connection */
        $connection = $em->getConnection();

        $entityRepository = $this->getTableRepository($table);
        if (!$entityRepository) {
            throw new \Exception('Table repository not found');
        }

        if (!method_exists($entityRepository, 'findOneById')) {
            throw new \Exception('Table repository does not have \'findOneById\' method');
        }

        if (!method_exists($entityRepository, 'addAndSave')) {
            throw new \Exception('Table repository does not have \'addAndSave\' method');
        }

        if (filter_var($path, FILTER_VALIDATE_URL) !== false) {

        } else if (!file_exists($path)) {
            throw new \Exception('File does not found');
        }

        $fileManager = $this->getContainer()->get('app.manager.file');

        $connection->beginTransaction();
        try {
            $file = $fileManager->downloadFile($path, $type);
            $entity = $entityRepository->findOneById($id);

            if (!$entity) {
                throw new \Exception('Entity not foud');
            }

            $entity->$setter($file);
            $entityRepository->addAndSave($entity);

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();

            throw $e;
        }
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();
        if (!$input->getArgument('path')) {
            $question = new Question('Please enter a path:');
            $question->setValidator(function($path) {
                if (empty($path)) {
                    throw new \Exception('Path can not be empty');
                }
                return $path;
            });

            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('path', $answer);
        }

        $type = $this->getHelper('dialog')->select(
            $output,
            'Please select file type',
            File::getAvailableTypes(),
            null
        );
        $input->setArgument('type', File::getAvailableTypes()[$type]);

        $tables = ['word_card_category'];
        $table = $this->getHelper('dialog')->select(
            $output,
            'Please select table',
            $tables,
            null
        );
        $input->setArgument('table', $tables[$table]);

        if (!$input->getArgument('setter')) {
            $question = new Question('Please enter a file setter-function name:');
            $question->setValidator(function($name) {
                if (empty($name)) {
                    throw new \Exception('Please enter a file setter-function name');
                }
                return $name;
            });
            $questions['setter'] = $question;

            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('setter', $answer);
        }

        if (!$input->getArgument('id')) {
            $question = new Question('Please enter a record id:');
            $question->setValidator(function($name) {
                if (empty($name)) {
                    throw new \Exception('Record id can not be empty');
                }
                return $name;
            });
            $questions['id'] = $question;

            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('id', $answer);
        }
    }

    private function getTableRepository($table)
    {
        switch ($table) {
            case 'word_card_category':
                return $this->getContainer()
                    ->get('app.repository.dictionary.word_card_category');
        }

        return null;
    }
}