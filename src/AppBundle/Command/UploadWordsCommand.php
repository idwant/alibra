<?php

namespace AppBundle\Command;

use CoreDomain\DTO\Dictionary\WordCardCategoryDTO;
use CoreDomain\DTO\Dictionary\WordCardDTO;
use CoreDomain\DTO\Dictionary\WordCardGroupDTO;
use CoreDomain\DTO\Dictionary\WordDTO;
use CoreDomain\DTO\Dictionary\WordTranslationDTO;
use CoreDomain\Model\Dictionary\Dictionary;
use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\WordCardCategory;
use CoreDomain\Model\Dictionary\WordCardGroup;
use CoreDomain\Model\File\File;
use CoreDomain\Repository\Dictionary\DictionaryRepositoryInterface;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadWordsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('word:upload')
            ->setDescription('Upload words from CSV')
            ->setDefinition(array(
                new InputArgument('path', InputArgument::REQUIRED, 'Path')
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');

        $handle = fopen($path, 'r');
        $header = array_flip(fgetcsv($handle, 2000, ';'));

        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var Connection $connection */
        $connection = $em->getConnection();

        $connection->beginTransaction();

        try {
            $dictionary = $this->createDictionary();

            $category = null;
            $group = null;
            $i = 0;
            while ($row = fgetcsv($handle, 2000, ';')) {
                if (!empty($row[$header['category']])) {
                    $category = $this->createWordCardCategory($header, $row);
                } else if (!empty($row[$header['group']])) {
                    $group = $this->createWordCardGroup($category, $header, $row);
                } else if (!empty($row[$header['word']])) {
                    $word = $this->createWord($dictionary, $group, $header, $row);
                } else {
                    throw new \Exception('Некорректная ситуация: пустые группа, тема и слово.');
                }

                echo ++$i . PHP_EOL;
            }

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();

            throw $e;
        }

        fclose($handle);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();
        if (!$input->getArgument('path')) {
            $question = new Question('Please enter a path:');
            $question->setValidator(function($path) {
                if (empty($path)) {
                    throw new \Exception('Path can not be empty');
                }
                return $path;
            });
            $questions['path'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @return Dictionary
     */
    private function createDictionary()
    {
        /** @var DictionaryRepositoryInterface $dictionaryRepository */
        $dictionaryRepository = $this->getContainer()->get('app.repository.dictionary');

        $dictionary = new Dictionary();
        $dictionary->setName('Uploaded dictionary');

        return $dictionary;
    }

    /**
     * @param Dictionary $dictionary
     * @param WordCardGroup $group
     * @param $header
     * @param $row
     * @return Word
     */
    private function createWord(Dictionary $dictionary, WordCardGroup $group, $header, $row)
    {
        $wordManager = $this->getContainer()->get('app.dictionary.word');
        $wordCardManager = $this->getContainer()->get('app.dictionary.word_card');
        $fileManager = $this->getContainer()->get('app.manager.file');

        $image = $fileManager->downloadFile($row[$header['image']], 'image');
        $audio = $fileManager->downloadFile($row[$header['audio']], 'audio');
        $sampleAudio = $fileManager->downloadFile($row[$header['sample_audio']], 'audio');

        $wordDTO = new WordDTO();
        $wordDTO
            ->setWord($row[$header['word']])
            ->setImage($image ? $image->getId() : null)
            ->setAudio($audio ? $audio->getId() : null)
            ->setTranscription($row[$header['transcription']]);

        /** @var Word $word */
        $word = $wordManager->addWord($wordDTO);

        $translationDTO = new WordTranslationDTO();
        $translationDTO
            ->setWord($word->getId())
            ->setTranslation($row[$header['translation']])
            ->setSample($row[$header['sample']])
            ->setSampleAudio($sampleAudio ? $sampleAudio->getId() : null);

        $translation = $wordManager->addTranslation($translationDTO);

        $dictionary->addWord($word);

        $wordCardDTO = new WordCardDTO();
        $wordCardDTO
            ->setImage($image ? $image->getId() : null)
            ->setAudio($audio ? $audio->getId() : null)
            ->setSampleAudio($sampleAudio ? $sampleAudio->getId() : null)
            ->setSample($translation->getSample())
            ->setTranslation($translation->getTranslation())
            ->setTranscription($word->getTranscription())
            ->setWord($word->getId())
            ->setGroups([$group->getId()]);

        $wordCard = $wordCardManager->save($wordCardDTO);

        return $word;
    }

    /**
     * @param WordCardCategory $category
     * @param array $header
     * @param array $row
     * @return WordCardGroup
     */
    private function createWordCardGroup(WordCardCategory $category, array $header, array $row)
    {
        $groupManager = $this->getContainer()->get('app.dictionary.word_card_group');

        $groupDTO = new WordCardGroupDTO();
        $groupDTO
            ->setName($row[$header['group']])
            ->setTranslation($row[$header['translation']])
            ->setCategory($category->getId());

        return $groupManager->save($groupDTO);
    }

    /**
     * @param array $header
     * @param array $row
     * @return WordCardCategory
     */
    private function createWordCardCategory(array $header, array $row)
    {
        $categoryManager = $this->getContainer()->get('app.dictionary.word_card_category');
        $fileManager = $this->getContainer()->get('app.manager.file');

        $categoryDTO = new WordCardCategoryDTO();
        $categoryDTO
            ->setName($row[$header['category']])
            ->setTranslation($row[$header['translation']]);

        if (!empty($row[$header['image']])) {
            $image = $fileManager->downloadFile($row[$header['image']], 'image');
            $categoryDTO->setImage($image ? $image->getId() : null);
        }

        return $categoryManager->save($categoryDTO);
    }
}