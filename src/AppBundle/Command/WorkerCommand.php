<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('worker:start')
            ->setDescription('Worker start')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $worker = $this->getContainer()->get('app.service.daemon.task_worker');
            $worker->initWorker();
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }
}