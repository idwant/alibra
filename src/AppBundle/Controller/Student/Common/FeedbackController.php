<?php
namespace AppBundle\Controller\Student\Common;

use CoreDomain\DTO\Common\FeedbackDTO;
use CoreDomain\Exception\ValidationException;
use JMS\Serializer\DeserializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class FeedbackController extends Controller
{
    /**
     * @Rest\Post("/feedbacks")
     * @Rest\View(serializerGroups={"api_feedback_get", "api_user_search"}, statusCode=201)
     * @ParamConverter(
     *     "feedbackDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_feedback_create"},
     *         "validator"={"groups"={"api_feedback_create"}}
     *     }
     * )
     */
    public function createAction(FeedbackDTO $feedbackDTO, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.managers.feedback')->save($feedbackDTO, $this->getUser());
    }
}