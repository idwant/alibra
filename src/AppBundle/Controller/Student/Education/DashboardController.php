<?php

namespace AppBundle\Controller\Student\Education;

use CoreDomain\DTO\Dashboard\DashboardSearchDTO;
use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use CoreDomain\DTO\Student\Education\LessonPartSearchDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\User\User;
use JMS\Serializer\DeserializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CoreDomain\Exception\ValidationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    /**
     * @Rest\Get("/dashboard/courses")
     * @Rest\View(serializerGroups={"api_dashboard_get"}, statusCode=200)
     */
    public function getCoursesAction(Request $request)
    {
        $searchDTO = $this->getDashboardSearchDTO($request, ['api_dashboard_courses']);
        $courses = $this->get('app.managers.dashboard')->getCourses($searchDTO);

        $user = $this->getUser();

        // Костылище!! Для демо пользователей по быстрому вырубаем флаг фонетики
        if ($user->getIsDemo()) {
            /** @var Course $course */
            foreach ($courses as $course) {
                $course->setIsPhonetic(false);
            }
        }

        return $courses;
    }

    /**
     * @Rest\Get("/dashboard/units")
     * @Rest\View(serializerGroups="api_dashboard_get", statusCode=200)
     */
    public function getUnitsAction(Request $request)
    {
        $searchDTO = $this->getDashboardSearchDTO($request, ['api_dashboard_units']);
        return $this->get('app.managers.dashboard')->getUnits($searchDTO);
    }

    /**
     * @Rest\Get("/dashboard/lessons")
     * @Rest\View(serializerGroups="api_dashboard_get", statusCode=200)
     */
    public function getLessonsAction(Request $request)
    {
        $searchDTO = $this->getDashboardSearchDTO($request, ['api_dashboard_lessons']);
        return $this->get('app.managers.dashboard')->getLessons($searchDTO);
    }

    /**
     * @Rest\Get("/dashboard/lesson-parts")
     * @Rest\View(serializerGroups="api_dashboard_get", statusCode=200)
     */
    public function getLessonPartsAction(Request $request)
    {
        $searchDTO = $this->getDashboardSearchDTO($request, ['api_dashboard_lesson_parts']);
        return $this->get('app.managers.dashboard')->getLessonParts($searchDTO);
    }

    /**
     * Создаёт объект DashboardSearchDTO на основании Request
     * @param Request $request
     * @param array $serializationGroups
     * @return DashboardSearchDTO
     */
    private function getDashboardSearchDTO(Request $request, array $serializationGroups)
    {
        $user = $this->getUser();

        $searchDTO= $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            DashboardSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups($serializationGroups)
        );
        $searchDTO->setUserId($user->getId());

        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $searchDTO;
    }
}