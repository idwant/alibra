<?php

namespace AppBundle\Controller\Student\Education;

use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\Model\User\User;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class LessonController extends Controller
{
    /**
     * @Rest\Get("/lessons")
     * @Rest\View(serializerGroups="api_student_lesson_search", statusCode=200)
     */
    public function listAction(Request $request)
    {
        if ($this->getUser()) {
            /* @var $searchDTO LessonSearchDTO */
            $searchDTO = $this->get('serializer')->deserialize(
                json_encode($request->query->all()),
                LessonSearchDTO::class,
                'json',
                DeserializationContext::create()->setGroups(['api_student_lesson_search'])
            );

            $user = $this->getUser();
            $searchDTO->userId = $user->getId();
            if ($user->getIsDemo()) {
                $searchDTO->setIsDemo($user->getIsDemo());
            }

            return $this->get('app.managers.user.student')->getLessons($searchDTO);
        }

        throw new \Exception('User is not authorized');
    }
}