<?php

namespace AppBundle\Controller\Student\Education;


use CoreDomain\DTO\Education\HintDTO;
use CoreDomain\Exception\LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class HintController extends Controller
{
    /**
     * @Rest\GET("/hints")
     * @Rest\View(serializerGroups="api_user_hint", statusCode=200)
     */
    public function userHintsAction()
    {
        if($this->getUser()) {
            return $this->get('app.manager.hint')->getHintsWithUser($this->getUser());
        }
        else {
            throw new LogicException('Пользователь не авторизован');
        }
    }

    /**
     * @Rest\PATCH("/hints")
     * @Rest\View(serializerGroups="api_user_hint", statusCode=200)
     * @ParamConverter(
     *     "hintDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_user_hint"},
     *          "validator"={"groups"={"api_user_hint"}}
     *     }
     * )
     */
    public function updateAction(HintDTO $hintDTO)
    {
        if($this->getUser()) {
            return $this->get('app.manager.hint')->updateUserHint($this->getUser(), $hintDTO);
        }
        else {
            throw new LogicException('Пользователь не авторизован');
        }
    }
}