<?php

namespace AppBundle\Controller\Student\Education;

use CoreDomain\DTO\Student\Education\ExerciseFinishDTO;
use CoreDomain\DTO\Student\Education\ExerciseSearchDTO;
use CoreDomain\DTO\Student\Education\ExerciseStartDTO;
use CoreDomain\DTO\Student\Education\ExerciseStatusDTO;
use CoreDomain\Exception\ValidationException;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class ExerciseController extends Controller
{
    /**
     * @Rest\Get("/exercises")
     * @Rest\View(serializerGroups={"api_grammar_themes_list", "api_grammar_theme_get","api_student_exercise_search","api_exercise_create", "api_image_get", "api_audio_get", "api_video_get", "api_user_exercise_get", "api_dictionary_get", "api_grammar_get"}, statusCode=200)
     */
    public function searchAction(Request $request)
    {
        if ($this->getUser()) {
            /** @var ExerciseSearchDTO $searchDTO */
            $searchDTO = $this->get('serializer')->deserialize(
                json_encode($request->query->all()),
                ExerciseSearchDTO::class,
                'json',
                DeserializationContext::create()->setGroups(['api_student_exercise_search'])
            );

            $user = $this->getUser();
            $searchDTO->userId = $user->getId();
            if ($user->getIsDemo()) {
                $searchDTO->setIsDemo($user->getIsDemo());
            }

            $exercises = $this->get('app.managers.user.student')->getExercises($searchDTO);

            //TODO убрать кастыль
            $res = [];
            foreach ($exercises as $exercise) {
                $res[] = json_decode($this->get('jms_serializer')
                    ->serialize(
                        $exercise, 'json',
                        SerializationContext::create()
                            ->setGroups(array(
                                "api_exercise_create", "api_student_exercise_search",
                                "api_image_get", "api_audio_get", "api_video_get",
                                "api_user_exercise_get", "api_dictionary_get", "api_grammar_get",
                                "api_grammar_themes_list", "api_grammar_theme_get"
                            ))
                    ));
            }
            return new JsonResponse($res);
        }

        throw new \Exception('User is not authorized');
    }

    /**
     * @Rest\PATCH("/exercises/start")
     * @Rest\View(serializerGroups="api_exercise_start", statusCode=200)
     * @ParamConverter(
     *     "exerciseStatusDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_exercise_start"},
     *          "validator"={"groups"={"api_exercise_start"}}
     *     }
     * )
     */
    public function startAction(ExerciseStatusDTO $exerciseStatusDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        if(!$user = $this->getUser()) {
            throw new \Exception('Пользователь не авторизован');
        }

        $this->get('app.education.exercise')->start($exerciseStatusDTO, $user);
    }

    /**
     * @Rest\PATCH("/exercises/finish")
     * @Rest\View(serializerGroups="api_exercise_finish", statusCode=200)
     * @ParamConverter(
     *     "exerciseStatusDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_exercise_finish"},
     *          "validator"={"groups"={"api_exercise_finish"}}
     *     }
     * )
     */
    public function finishAction(ExerciseStatusDTO $exerciseStatusDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        if(!$user = $this->getUser()) {
            throw new \Exception('Пользователь не авторизован');
        }

        $this->get('app.education.exercise')->finish($exerciseStatusDTO, $user);
    }
}