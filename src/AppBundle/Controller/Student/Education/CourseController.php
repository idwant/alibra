<?php

namespace AppBundle\Controller\Student\Education;

use CoreDomain\DTO\Education\SearchUserProgressDTO;
use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use CoreDomain\Model\Education\Course;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

class CourseController extends Controller
{
    /**
     * @Rest\Get("/courses")
     * @Rest\View(serializerGroups="api_student_course_search", statusCode=200)
     */
    public function listAction(Request $request)
    {
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            CourseSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_student_course_search'])
        );

        $user = $this->getUser();
        $searchDTO->userId = $user->getId();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $courses = $this->get('app.managers.user.student')->getCourses($searchDTO);

        // Костылище!! Для демо пользователей по быстрому вырубаем флаг фонетики
        if ($user->getIsDemo()) {
            /** @var Course $course */
            foreach ($courses as $course) {
                $course->setIsPhonetic(false);
            }
        }

        return $courses;
    }

    /**
     * @Rest\GET("/courses/{id}", requirements={"id": "\d+"})
     * @Rest\View(serializerGroups="api_course_get", statusCode=200)
     */
    public function viewAction($id)
    {
        $searchDTO = new CourseSearchDTO();
        $searchDTO->setId($id);

        $user = $this->getUser();
        $searchDTO->userId = $user->getId();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $courses = $this->get('app.managers.user.student')->getCourses($searchDTO);

        // Костылище!! Для демо пользователей по быстрому вырубаем флаг фонетики
        if ($user->getIsDemo()) {
            /** @var Course $course */
            foreach ($courses as $course) {
                $course->setIsPhonetic(false);
            }
        }

        if (count($courses) > 0) {
            return array_shift($courses);
        }

        throw new LogicException('Course not found');
    }

    /**
     * @Rest\Get("/courses/stat")
     * @Rest\View(serializerGroups={"api_student_course_stat"}, statusCode=200)
     */
    public function statAction(Request $request)
    {
        /** @var SearchUserProgressDTO $searchDTO */
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            SearchUserProgressDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_student_course_stat'])
        );

        $user = $this->getUser();
        $searchDTO->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $courseManager = $this->get('app.education.course');

        return $courseManager->getStatResponse($searchDTO);
    }

    /**
     * @Rest\Get("/courses/stat/{destination}")
     * @Rest\View(serializerGroups={"api_student_course_stat"}, statusCode=200)
     */
    public function statExportAction(Request $request, $destination)
    {
        /** @var SearchUserProgressDTO $searchProgressDTO */
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            SearchUserProgressDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_student_course_stat'])
        );
        $user = $this->getUser();
        $searchDTO->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $courseManager = $this->get('app.education.course');

        return $courseManager->getStatResponse($searchDTO, $destination);
    }
}