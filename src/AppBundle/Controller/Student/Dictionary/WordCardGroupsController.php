<?php

namespace AppBundle\Controller\Student\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;
use CoreDomain\Exception\LogicException;
use JMS\Serializer\DeserializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class WordCardGroupsController extends Controller
{
    /**
     * @Rest\Get("/word-card-groups")
     * @Rest\View("statusCode=200")
     */
    public function listAction(Request $request)
    {
        /** @var SearchWordCardGroupDTO $searchDTO */
        $searchDTO = $this->get('jms_serializer')->deserialize(
            json_encode($request->query->all()),
            SearchWordCardGroupDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_word_card_group_search'])
        );
        $user = $this->getUser();
        $searchDTO->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this->get('app.managers.user.user')->getWordCardGroups($searchDTO);
    }

    /**
     * @Rest\Get("/word-card-groups/{id}", requirements={"id" = "\d+"})
     * @Rest\View(serializerGroups={"api_word_card_group_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        $searchDTO = new SearchWordCardGroupDTO();
        $searchDTO->setId($id);

        $user = $this->getUser();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $groups = $this->get('app.managers.user.user')->getWordCardGroups($searchDTO);

        if (count($groups) > 0) {
            return array_shift($groups);
        }

        throw new LogicException('Word card group not found');
    }
}