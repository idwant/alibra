<?php

namespace AppBundle\Controller\Student\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardCategoryDTO;
use CoreDomain\Exception\LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;

class WordCardCategoriesController extends Controller
{
    /**
     * @Rest\Get("/word-card-categories")
     * @Rest\View(serializerGroups={"api_user_word_card_category_get", "api_image_get"}, statusCode=200)
     */
    public function listAction()
    {
        $searchDTO = new SearchWordCardCategoryDTO();

        $user = $this->getUser();
        $searchDTO->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this->get('app.managers.user.user')->getWordCardCategories($searchDTO);
    }

    /**
     * @Rest\Get("/word-card-categories/{id}", requirements={"id" = "\d+"})
     * @Rest\View(serializerGroups={"api_user_word_card_category_get", "api_image_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        $searchDTO = new SearchWordCardCategoryDTO();
        $searchDTO->setId($id);

        $user = $this->getUser();
        $searchDTO->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $categories = $this->get('app.managers.user.user')->getWordCardCategories($searchDTO);

        if (count($categories) > 0) {
            return array_shift($categories);
        }

        throw new LogicException('Word card category not found');
    }
}