<?php

namespace AppBundle\Controller\Student\Dictionary;

use CoreDomain\DTO\Education\SearchUserWordCardDTO;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Education\UserWordCard;
use JMS\Serializer\DeserializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class WordCardController extends Controller
{
    /**
     * @Rest\Get("/word-cards/learn")
     * @Rest\View(serializerGroups={"api_user_word_card_learn", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function listLearnAction(Request $request)
    {
        /** @var SearchUserWordCardDTO $searchDTO */
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            SearchUserWordCardDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_user_word_card_learn'])
        );

        $user = $this->getUser();
        $searchDTO->setCurrentUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this
            ->get('app.managers.user.user')
            ->getAllWordCardForLearn($searchDTO);
    }
    
    /**
     * @Rest\Get("/word-cards/repeat")
     * @Rest\View(serializerGroups={"api_user_word_card_repeat", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function listRepeatAction(Request $request)
    {
        /** @var SearchUserWordCardDTO $searchDTO */
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            SearchUserWordCardDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_user_word_card_repeat'])
        );

        $user = $this->getUser();
        $searchDTO->setCurrentUserId($user->getId())
            ->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this
            ->get('app.managers.user.user')
            ->getAllWordCardForRepeat($searchDTO);
    }

    /**
     * @Rest\Get("/word-cards/review")
     * @Rest\View(serializerGroups={"api_user_word_card_repeat", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function listReviewAction(Request $request)
    {
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            SearchUserWordCardDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_user_word_card_repeat'])
        );

        $user = $this->getUser();
        $searchDTO->setCurrentUserId($user->getId())
            ->setUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this
            ->get('app.managers.user.user')
            ->getAllWordCardForReview($searchDTO);
    }

    /**
     * @Rest\Get("/word-cards/stat")
     * @Rest\View("statusCode=200")
     */
    public function statAction(Request $request)
    {
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            SearchUserWordCardDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_user_word_card_stat'])
        );

        $user = $this->getUser();
        $searchDTO->setCurrentUserId($user->getId());
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this
            ->get('app.managers.user.user')
            ->getStat($searchDTO);
    }

    /**
     * @Rest\Patch("/word-cards/learn/{id}")
     * @Rest\View(serializerGroups={"api_user_word_card_learn", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function learnAction(WordCard $wordCard)
    {
        return $this->get('app.managers.user.user')->learnWordCard($this->getUser(), $wordCard);
    }

    /**
     * @Rest\Patch("/word-cards/postpone-learn/{id}")
     * @Rest\View(serializerGroups={"api_user_word_card_learn", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function learnPostponeAction(WordCard $wordCard)
    {
        return $this->get('app.managers.user.user')->learnPostponeWordCard($this->getUser(), $wordCard);
    }

    /**
     * @Rest\Patch("/word-cards/repeat/{id}")
     * @Rest\View(serializerGroups={"api_user_word_card_repeat", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function repeatAction(UserWordCard $userWordCard)
    {
        return $this->get('app.managers.user.user')->repeatWordCard($this->getUser(), $userWordCard);
    }

    /**
     * @Rest\Patch("/word-cards/disable/{id}")
     * @Rest\View(serializerGroups={"api_user_word_card_repeat", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function disableAction(WordCard $wordCard)
    {
        return $this->get('app.managers.user.user')->disableWordCard($this->getUser(), $wordCard);
    }
}