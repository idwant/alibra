<?php

namespace AppBundle\Controller\Student\Grammar;

use CoreDomain\DTO\Grammar\GrammarSearchDTO;
use CoreDomain\Exception\LogicException;
use JMS\Serializer\DeserializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class GrammarController extends Controller
{
    /**
     * @Rest\Get("/grammars")
     * @Rest\View(serializerGroups={"api_grammar_get", "api_grammar_theme_get", "api_grammar_section_get", "api_video_get"}, statusCode=200)
     */
    public function searchAction(Request $request)
    {
        /** @var GrammarSearchDTO $searchDTO */
        $searchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            GrammarSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_grammar_search'])
        );

        $user = $this->getUser();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        return $this->get('app.grammar')->searchGrammars($searchDTO);
    }

    /**
     * @Rest\GET("/grammars/{id}")
     * @Rest\View(serializerGroups={"api_grammar_get", "api_grammar_section_get", "api_grammar_theme_get", "api_video_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        $searchDTO = new GrammarSearchDTO();
        $searchDTO->setId($id);

        $user = $this->getUser();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $grammars = $this->get('app.grammar')->searchGrammars($searchDTO);

        if (count($grammars) > 0) {
            return array_shift($grammars);
        }

        throw new LogicException('Grammar not found');
    }
}