<?php

namespace AppBundle\Controller\Student\Grammar;

use CoreDomain\DTO\Grammar\GrammarThemeSearchDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GrammarThemeController extends Controller
{
    /**
     * @Rest\GET("/grammar-themes")
     * @Rest\View(serializerGroups="api_grammar_themes_list", statusCode=200)
     */
    public function listAction(Request $request)
    {
        /** @var GrammarThemeSearchDTO $searchDTO */
        $searchDTO= $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            GrammarThemeSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_grammar_theme_search'])
        );

        $user = $this->getUser();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $grammarThemes = $this->get('app.grammar.grammar_theme')->listGrammarThemes($searchDTO);

        return View::create($grammarThemes['data'], 200, ['X-Total-Count' => $grammarThemes['count']])
            ->setSerializationContext(
                SerializationContext::create()->setGroups(array('api_grammar_themes_list'))
            );
    }
}