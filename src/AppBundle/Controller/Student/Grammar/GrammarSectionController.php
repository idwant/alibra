<?php

namespace AppBundle\Controller\Student\Grammar;

use CoreDomain\DTO\Grammar\GrammarSectionSearchDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GrammarSectionController extends Controller
{
    /**
     * Возвращает список секций грамматики
     *
     * @Rest\GET("/grammar-sections")
     * @Rest\View(serializerGroups="api_grammar_section_list", statusCode=200)
     */
    public function listAction(Request $request)
    {
        /** @var GrammarSectionSearchDTO $searchDTO */
        $searchDTO= $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            GrammarSectionSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_grammar_section_search'])
        );

        $user = $this->getUser();
        if ($user->getIsDemo()) {
            $searchDTO->setIsDemo($user->getIsDemo());
        }

        $grammarSections = $this->get('app.grammar.grammar_section')->getAllGrammarSections($searchDTO);
        return View::create($grammarSections['data'], 200, ['X-Total-Count' => $grammarSections['count']]);
    }
}