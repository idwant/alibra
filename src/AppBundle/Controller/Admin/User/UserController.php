<?php

namespace AppBundle\Controller\Admin\User;

use CoreDomain\DTO\User\ProfileDTO;
use CoreDomain\DTO\User\SearchDTO;
use CoreDomain\DTO\User\UserRegisterDTO;
use CoreDomain\Exception\ValidationException;
use CoreDomain\Model\User\User;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class UserController extends Controller
{
    /**
     * Возвращает список пользователей отфильтрованных по роли
     *
     * @Rest\Get("/users")
     */
    public function listAction(Request $request)
    {
        $searchDTO = $this->get('jms_serializer')->deserialize(
            json_encode($request->query->all()),
            SearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_user_search'])
        );

        $userManager = $this->get('app.managers.user.user');

        $users = $userManager->searchUser($searchDTO);

        $content = $this->get('serializer')->serialize(
            $users,
            'json',
            SerializationContext::create()->setGroups('api_user_get')
        );

        $response = new Response($content);
        $response->headers->set('X-total-count', $userManager->searchUserCount($searchDTO));

        return $response;
    }

    /**
     * @Rest\Post("/users")
     * @Rest\View(serializerGroups="api_user_get", statusCode=201)
     * @ParamConverter(
     *      "userDTO",
     *      converter="fos_rest.request_body",
     *      options={
     *          "deserializationContext"={"groups"="api_user_post"},
     *          "validator"={"groups"={"api_user_post"}}
     *      }
     * )
     */
    public function createAction(UserRegisterDTO $userDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        return $this->get('app.managers.user.user')->register($userDTO);
    }

    /**
     * @Rest\Get("/users/{id}")
     * @Rest\View(serializerGroups="api_user_get", statusCode=200)
     */
    public function viewAction(User $user)
    {
        return $user;
    }

    /**
     * @Rest\Patch("/users/{id}")
     * @Rest\View(serializerGroups="api_user_get", statusCode=200)
     * @ParamConverter(
     *      "profileDTO",
     *      converter="fos_rest.request_body",
     *      options={
     *          "deserializationContext"={"groups"="api_user_patch"},
     *          "validator"={"groups"={"api_user_patch"}}
     *      }
     * )
     */
    public function updateAction(User $user, ProfileDTO $profileDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        return $this->get('app.managers.user.user')->updateProfile($user, $profileDTO);
    }

    /**
     * @Rest\Delete("/users/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction()
    {

    }

    /**
     * @Rest\Post("/users/{id}/courses")
     * @Rest\View(statusCode=204)
     */
    public function addCourseAction(Request $request, User $user)
    {
        $this->get('app.managers.user.user')
            ->addCourses($user, $request->request->get('course_id'));
    }

    /**
     * @Rest\Delete("/users/{id}/courses")
     * @Rest\View("statusCode=204")
     */
    public function removeCourseAction(Request $request, User $user)
    {
        $this->get('app.managers.user.user')
            ->removeCourses($user, $request->query->get('course_id'));
    }
}
