<?php

namespace AppBundle\Controller\Admin\Education;

use CoreDomain\DTO\Education\ExerciseTemplateDTO;
use CoreDomain\Exception\ValidationException;
use CoreDomain\Model\Education\ExerciseTemplate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ExerciseTemplateController extends Controller
{
    /**
     * @Rest\Get("/exercise-templates")
     * @Rest\View(serializerGroups={"api_exercise_template_get"}, statusCode=200)
     */
    public function getExerciseTemplatesAction()
    {
        return $this->get('app.repository.education.exercise_template')
            ->findAll();
    }

    /**
     * @Rest\Get("/exercise-templates/{type}")
     * @Rest\View(serializerGroups={"api_exercise_template_get"}, statusCode=200)
     */
    public function getExerciseTemplateAction($type)
    {
        return $this->get('app.repository.education.exercise_template')
            ->findOneByType($type);
    }
}