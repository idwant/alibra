<?php

namespace AppBundle\Controller\Admin\Education;


use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonBindDTO;
use CoreDomain\DTO\Education\LessonDTO;
use CoreDomain\DTO\Education\LessonSortDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use CoreDomain\Exception\ValidationException;


class LessonController extends Controller
{
    /**
     * @Rest\GET("/lessons")
     * @Rest\View(statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $lessonSearchDTO = new LessonSearchDTO();

        $lessonSearchDTO->query = $request->get('query', NULL);
        $lessonSearchDTO->unitId = $request->get('unit_id', NULL);

        $lessons = $this->get('app.education.lesson')->searchLessons($lessonSearchDTO);
        $lessonsCount = $this->get('app.education.lesson')->searchLessonsCount($lessonSearchDTO);

        $view =  View::create($lessons, 200, ['X-Total-Count' => $lessonsCount]);
        $view->setSerializationContext(SerializationContext::create()->setGroups(array('api_lesson_list')));

        return $view;
    }

    /**
     * @Rest\GET("/lessons/{id}")
     * @Rest\View(serializerGroups="api_lesson_get", statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.education.lesson')->getLessonById($id);
    }

    /**
     * @Rest\Post("/lessons")
     * @Rest\View(serializerGroups="api_lesson_create", statusCode=201)
     * @ParamConverter(
     *     "lessonDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_create"},
     *          "validator"={"groups"={"api_lesson_create"}}
     *     }
     * )
     */
    public function createAction(LessonDTO $lessonDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.lesson')->addLesson($lessonDTO);
    }

    /**
     * @Rest\Post("/lessons/bind")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "lessonBindDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_bind"},
     *          "validator"={"groups"={"api_lesson_bind"}}
     *     }
     * )
     */
    public function bindAction(LessonBindDTO $lessonBindDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        $this->get('app.education.lesson')->bindByDTO($lessonBindDTO);
    }

    /**
     * @Rest\Post("/lessons/unbind")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "lessonBindDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_bind"},
     *          "validator"={"groups"={"api_lesson_bind"}}
     *     }
     * )
     */
    public function unbindAction(LessonBindDTO $lessonBindDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        $this->get('app.education.lesson')->unbindByDTO($lessonBindDTO);
    }

    /**
     * @Rest\Patch("/lessons/{id}", requirements={"id" = "\d+"})
     * @Rest\View(serializerGroups={"api_lesson_create", "api_lesson_list"}, statusCode=200)
     * @ParamConverter(
     *     "lessonDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_create"},
     *          "validator"={"groups"={"api_lesson_create"}}
     *     }
     * )
     */
    public function updateAction(LessonDTO $lessonDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.lesson')->updateLesson($lessonDTO, $id);
    }

    /**
     * @Rest\Delete("/lessons/{id}", requirements={"id" = "\d+"})
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.education.lesson')->deleteLesson($id);
    }

    /**
     * @Rest\Patch("/lessons/sort")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "lessonSortDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_sort"},
     *          "validator"={"groups"={"api_lesson_sort"}}
     *     }
     * )
     */
    public function sortAction(LessonSortDTO $lessonSortDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        $this->get('app.education.lesson')->sortByDTO($lessonSortDTO);
    }
}