<?php

namespace AppBundle\Controller\Admin\Education;


use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\UnitBindDTO;
use CoreDomain\DTO\Education\UnitSortDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use CoreDomain\DTO\Education\UnitDTO;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use CoreDomain\Exception\ValidationException;


class UnitController extends Controller
{
    /**
     * @Rest\GET("/units")
     * @Rest\View(serializerGroups="api_unit_list", statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $unitSearchDTO = new UnitSearchDTO();

        $unitSearchDTO->query = $request->get('query', NULL);
        $unitSearchDTO->courseId = $request->get('course_id', NULL);
        $unitSearchDTO->limit = $request->get('limit', $this->getParameter('unit_limit'));
        $unitSearchDTO->offset = $request->get('offset', 0);

        $units = $this->get('app.education.unit')->searchUnits($unitSearchDTO);
        $unitsCount = $this->get('app.education.unit')->searchUnitsCount($unitSearchDTO);

        $view = View::create($units, 200, ['X-Total-Count' => $unitsCount]);
        $view->setSerializationContext(SerializationContext::create()->setGroups(array('api_unit_list')));

        return $view;
    }

    /**
     * @Rest\GET("/units/{id}")
     * @Rest\View(serializerGroups="api_unit_get", statusCode=200)
     */
    public function viewAction($id)         
    {
        return $this->get('app.education.unit')->getUnitById($id);
    }

    /**
     * @Rest\Post("/units")
     * @Rest\View(serializerGroups="api_unit_create", statusCode=201)
     * @ParamConverter(
     *     "unitDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_unit_create"},
     *          "validator"={"groups"={"api_unit_create"}}
     *     }
     * )
     */
    public function createAction(UnitDTO $unitDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.unit')->addUnit($unitDTO);
    }

    /**
     * @Rest\Patch("/units/{id}", requirements={"id" = "\d+"})
     * @Rest\View(serializerGroups={"api_unit_create", "api_unit_get"}, statusCode=200)
     * @ParamConverter(
     *     "unitDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_unit_create"},
     *          "validator"={"groups"={"api_unit_create"}}
     *     }
     * )
     */
    public function updateAction(UnitDTO $unitDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.unit')->updateUnit($unitDTO, $id);
    }

    /**
     * @Rest\Delete("/units/{id}", requirements={"id" = "\d+"})
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.education.unit')->deleteUnit($id);
    }

    /**
     * @Rest\Post("/units/bind")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "unitBindDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_unit_bind"},
     *          "validator"={"groups"={"api_unit_bind"}}
     *     }
     * )
     */
    public function bindAction(UnitBindDTO $unitBindDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        
        $this->get('app.education.unit')->bindByDTO($unitBindDTO);
    }

    /**
     * @Rest\Post("/units/unbind")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "unitBindDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_unit_bind"},
     *          "validator"={"groups"={"api_unit_bind"}}
     *     }
     * )
     */
    public function unbindAction(UnitBindDTO $unitBindDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        $this->get('app.education.unit')->unbindByDTO($unitBindDTO);
    }
    
    /**
     * @Rest\Patch("/units/sort")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "unitSortDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_unit_sort"},
     *          "validator"={"groups"={"api_unit_sort"}}
     *     }
     * )
     */
    public function sortAction(UnitSortDTO $unitSortDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.unit')->sortByDTO($unitSortDTO);
    }

}