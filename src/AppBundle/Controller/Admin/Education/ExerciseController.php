<?php

namespace AppBundle\Controller\Admin\Education;


use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\ExerciseBindDTO;
use CoreDomain\DTO\Education\ExerciseRequestDTO;
use CoreDomain\DTO\Education\ExerciseSortDTO;
use CoreDomain\Exception\ValidationException;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;


class ExerciseController extends Controller
{
    /**
     * @Rest\GET("/exercises")
     * @Rest\View(serializerGroups="api_exercises_list", statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $exercises = $this->get('app.education.exercise')->search(
            $request->get('query', NULL),
            $request->get('lesson_id', NULL),
            $request->get('limit', $this->getParameter('exercise_limit')),
            $request->get('offset', 0)
        );

        return View::create($exercises['data'], 200, ['X-Total-Count' => $exercises['count']]);
    }

    /**
     * @Rest\Post("/exercises")
     * @Rest\View(serializerGroups={"api_exercise_create", "api_audio_get", "api_image_get", "api_dictionary_get", "api_grammar_get"}, statusCode=201)
     * @ParamConverter(
     *     "exerciseRequestDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_exercise_request"}
     *     }
     * )
     */
    public function createAction(ExerciseRequestDTO $exerciseRequestDTO)
    {
        return $this->get('app.education.exercise')->addExercise($exerciseRequestDTO);
    }

    /**
     * @Rest\Get("/exercises/{id}")
     * @Rest\View(serializerGroups={"api_exercise_create", "api_audio_get", "api_image_get", "api_dictionary_get", "api_grammar_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.education.exercise')->getExercise($id);
    }

    /**
     * @Rest\Patch("/exercises/{id}", requirements={"id" = "\d+"})
     * @Rest\View(serializerGroups={"api_exercises_create", "api_audio_get", "api_dictionary_get", "api_grammar_get"}, statusCode=200),
     * @ParamConverter(
     *     "exerciseRequestDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_exercise_request"}
     *     }
     * )
     */
    public function updateAction(ExerciseRequestDTO $exerciseRequestDTO, $id)
    {
        return $this->get('app.education.exercise')->updateExercise($id, $exerciseRequestDTO);
    }

    /**
     * @Rest\Delete("/exercises/{id}", requirements={"id" = "\d+"})
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.repository.education.exercise')->deleteById($id);
    }

    /**
     * @Rest\Patch("/exercises/sort")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "exerciseSortDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_exercise_sort"},
     *          "validator"={"groups"={"api_exercise_sort"}}
     *     }
     * )
     */
    public function sortAction(ExerciseSortDTO $exerciseSortDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.exercise')->sortByDTO($exerciseSortDTO);
    }

    /**
     * @Rest\Post("/exercises/bind")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "exerciseBindDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_exercise_bind"},
     *          "validator"={"groups"={"api_exercise_bind"}}
     *     }
     * )
     */
    public function bindAction(ExerciseBindDTO $exerciseBindDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        $this->get('app.education.exercise')->bindByDTO($exerciseBindDTO);
    }

    /**
     * @Rest\Post("/exercises/unbind")
     * @Rest\View(statusCode=201)
     * @ParamConverter(
     *     "exerciseBindDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_exercise_bind"},
     *          "validator"={"groups"={"api_exercise_bind"}}
     *     }
     * )
     */
    public function unbindAction(ExerciseBindDTO $exerciseBindDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        $this->get('app.education.exercise')->unbindByDTO($exerciseBindDTO);
    }

    /**
     * @Rest\Get("/exercises-test-update")
     * @Rest\View(statusCode=200)
     */
    public function testAction(Request $request)
    {
        $this->get('app.education.exercise')->updateExercises();
    }

}