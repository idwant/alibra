<?php

namespace AppBundle\Controller\Admin\Education;

use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonPartDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use CoreDomain\Exception\ValidationException;


class LessonPartController extends Controller
{
    /**
     * @Rest\GET("/lesson-parts")
     * @Rest\View(serializerGroups="api_lesson_parts_list", statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $lessonParts = $this->get('app.education.lesson_part')
            ->getLessonPartsByLesson($request->get('lesson_id', NULL));

        $res = array();
        foreach ($lessonParts as $lessonPart) {
            $res[] = json_decode($this->get('jms_serializer')->serialize($lessonPart, 'json', SerializationContext::create()->setGroups('api_lesson_parts_list')));
        }
        return new JsonResponse($res);
    }

    /**
     * @Rest\GET("/lesson-parts/{id}")
     * @Rest\View(serializerGroups="api_lesson_parts_list", statusCode=200)
     */
    public function getLessonPartByIdAction($id)
    {
        return $this->get('app.education.lesson_part')->getLessonPartById($id);
    }

    /**
     * @Rest\Post("/lesson-parts")
     * @Rest\View(serializerGroups="api_lesson_part_create", statusCode=201)
     * @ParamConverter(
     *     "lessonPartDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_part_create"},
     *          "validator"={"groups"={"api_lesson_part_create"}}
     *     }
     * )
     */
    public function createLessonPartAction(LessonPartDTO $lessonPartDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.lesson_part')->addLessonPart($lessonPartDTO);
    }

    /**
     * @Rest\Post("/lesson-parts/bind")
     * @Rest\View(statusCode=201)
     */
    public function bindLessonPartsAction(Request $request)
    {
        $params = $request->request->all();
        $this->get('app.education.lesson_part')->bindLessonPartsByDTO($params);
    }

    /**
     * @Rest\Patch("lesson-parts/{id}")
     * @Rest\View(serializerGroups={"api_lesson_part_create", "api_exercise_create"}, statusCode=200)
     * @ParamConverter(
     *     "lessonPartDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_lesson_part_create"},
     *          "validator"={"groups"={"api_lesson_part_create"}}
     *     }
     * )
     */
    public function updateLessonPartAction(LessonPartDTO $lessonPartDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.lesson_part')->updateLessonPart($lessonPartDTO, $id);
    }

    /**
     * @Rest\Delete("lesson-parts/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteLessonPartAction($id)
    {
        $this->get('app.education.lesson_part')->deleteLessonPart($id);
    }
}