<?php

namespace AppBundle\Controller\Admin\Education;

use CoreDomain\DTO\Education\CourseDTO;
use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use CoreDomain\Exception\ValidationException;


class CourseController extends Controller
{
    /**
     * @Rest\GET("/courses")
     * @Rest\View(serializerGroups="api_course_list", statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $courseSearchDTO = new CourseSearchDTO();

        $courseSearchDTO->query = $request->get('query', NULL);
        $courseSearchDTO->limit = $request->get('limit', $this->getParameter('course_limit'));
        $courseSearchDTO->offset = $request->get('offset', 0) ;

        $courses = $this->get('app.education.course')->searchCourses($courseSearchDTO);
        $coursesCount = $this->get('app.education.course')->searchCoursesCount($courseSearchDTO);

        $view =  View::create($courses, 200, ['X-Total-Count' => $coursesCount]);
        $view->setSerializationContext(SerializationContext::create()->setGroups(array('api_course_list')));

        return $view;
    }

    /**
     * @Rest\GET("/courses/{id}")
     * @Rest\View(serializerGroups="api_course_get", statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.education.course')->getCourseById($id);
    }

    /**
     * @Rest\Post("/courses")
     * @Rest\View(serializerGroups="api_course_create", statusCode=201)
     * @ParamConverter(
     *     "courseDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_course_create"},
     *          "validator"={"groups"={"api_course_create"}}
     *     }
     * )
     */
    public function createAction(CourseDTO $courseDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.course')->addCourse($courseDTO);
    }

    /**
     * @Rest\Patch("/courses/{id}")
     * @Rest\View(serializerGroups={"api_course_create", "api_course_get"}, statusCode=200)
     * @ParamConverter(
     *     "courseDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_course_create"},
     *          "validator"={"groups"={"api_course_create"}}
     *     }
     * )
     */
    public function updateAction(CourseDTO $courseDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.education.course')->updateCourse($courseDTO, $id);
    }

    /**
     * @Rest\Delete("/courses/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.education.course')->deleteCourse($id);
    }

    /**
     * @Rest\Post("/courses/{id}")
     * @Rest\View(serializerGroups="api_course_create", statusCode=201)
     * )
     */
    public function bindToUserAction()
    {

    }
}