<?php

namespace AppBundle\Controller\Admin\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardCategoryDTO;
use CoreDomain\DTO\Dictionary\WordCardCategoryDTO;
use CoreDomain\DTO\Dictionary\WordCardGroupDTO;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use CoreDomain\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class WordCardCategoryController extends Controller
{
    /**
     * @Rest\Get("/word-card-categories")
     * @Rest\View(statusCode=200)
     */
    public function listAction(Request $request)
    {
        $searchDTO = $this->get('jms_serializer')->deserialize(
            json_encode($request->query->all()),
            SearchWordCardCategoryDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_word_card_category_search'])
        );

        $manager = $this->get('app.dictionary.word_card_category');
        $categories = $manager->searchWordCardCategory($searchDTO);

        $content = $this->get('serializer')->serialize(
            $categories,
            'json',
            SerializationContext::create()->setGroups(['api_word_card_category_search', 'api_image_get'])
        );

        $response = new Response($content);
        $response->headers->set('X-total-count', $manager->searchWordCardCategoryCount($searchDTO));

        return $response;
    }

    /**
     * @Rest\Post("/word-card-categories")
     * @Rest\View(serializerGroups={"api_word_card_category_search", "api_image_get"}, statusCode=201)
     * @ParamConverter(
     *     "categoryDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_word_card_category_create"},
     *         "validator"={"groups"={"api_word_card_category_create"}}
     *     }
     * )
     */
    public function createAction(WordCardCategoryDTO $categoryDTO, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word_card_category')->save($categoryDTO);
    }

    /**
     * @Rest\Get("/word-card-categories/{id}")
     * @Rest\View(statusCode=200)
     */
    public function viewAction(Request $request, $id)
    {
        return $this->get('app.repository.dictionary.word_card_category')->findOneById($id);
    }

    /**
     * @Rest\Patch("/word-card-categories/{id}")
     * @Rest\View(serializerGroups={"api_word_card_category_search", "api_image_get"}, statusCode=201)
     * @ParamConverter(
     *     "categoryDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_word_card_category_create"},
     *         "validator"={"groups"={"api_word_card_category_create"}}
     *     }
     * )
     */
    public function updateAction(WordCardCategoryDTO $categoryDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if(count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word_card_category')->save($categoryDTO, $id);
    }

    /**
     * @Rest\Delete("/word-card-categories/{id}", requirements={"id" = "\d+"})
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.dictionary.word_card_category')->delete($id);
    }
}