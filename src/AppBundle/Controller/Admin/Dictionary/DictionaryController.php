<?php

namespace AppBundle\Controller\Admin\Dictionary;


use CoreDomain\DTO\Dictionary\DictionaryDTO;
use CoreDomain\DTO\Dictionary\WordDTO;
use CoreDomain\Exception\ValidationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DictionaryController extends Controller
{
    /**
     * @Rest\GET("/dictionaries")
     * @Rest\View(serializerGroups="api_dictionary_list", statusCode=200)
     */
    public function listAction(Request $request)
    {
        $dictionaries = $this->get('app.dictionary')->searchDictionaries(
            $request->get('query', ''),
            $request->get('limit', 10),
            $request->get('offset', 0)
        );

        return View::create($dictionaries['data'], 200, ['X-Total-Count' => $dictionaries['count']])
            ->setSerializationContext(
                SerializationContext::create()->setGroups(array('api_dictionary_list')))
            ;
    }

    /**
     * @Rest\GET("/dictionaries/{id}")
     * @Rest\View(serializerGroups="api_dictionary_list", statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.dictionary')->getDictionaryById($id);
    }

    /**
     * @Rest\Post("/dictionaries")
     * @Rest\View(serializerGroups="api_dictionary_create", statusCode=201)
     * @ParamConverter(
     *     "dictionaryDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_dictionary_create"},
     *          "validator"={"groups"={"api_dictionary_create"}}
     *     }
     * )
     */
    public function createAction(DictionaryDTO $dictionaryDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary')->addDictionary($dictionaryDTO);
    }

    /**
     * @Rest\Patch("/dictionaries/{id}")
     * @Rest\View(serializerGroups="api_dictionary_create", statusCode=201)
     * @ParamConverter(
     *     "dictionaryDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_dictionary_create"},
     *          "validator"={"groups"={"api_dictionary_create"}}
     *     }
     * )
     */
    public function updateAction(DictionaryDTO $dictionaryDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary')->updateDictionary($dictionaryDTO, $id);
    }

    /**
     * @Rest\Delete("dictionaries/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.dictionary')->deleteDictionaryById($id);
    }

    /**
     * @Rest\Post("/dictionaries/{dictionaryId}/words/{wordId}")
     * @Rest\View(serializerGroups="api_word_get", statusCode=200)
     */
    public function addWordAction($dictionaryId, $wordId)
    {
        return $this->get('app.dictionary')->addWordToDictionary($wordId, $dictionaryId);
    }

    /**
     * @Rest\Get("/dictionaries/{dictionaryId}/words")
     * @Rest\View(statusCode=200)
     */
    public function getWordsAction($dictionaryId, Request $request)
    {
        $words = $this->get('app.dictionary')->getAllWords(
            $dictionaryId,
            $request->get('word', null),
            $request->get('limit', 10),
            $request->get('offset', 0));

        return View::create($words['data'], 200, ['X-Total-Count' => $words['count']])
            ->setSerializationContext(
                SerializationContext::create()->setGroups(array('api_word_search')));
    }

    /**
     * @Rest\Delete("dictionaries/{dictionaryId}/words/{wordId}")
     * @Rest\View(statusCode=204)
     */
    public function deleteWordAction($dictionaryId, $wordId)
    {
        $this->get('app.dictionary')->deleteWordFromDictionary($wordId, $dictionaryId);
    }
}