<?php

namespace AppBundle\Controller\Admin\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;
use CoreDomain\DTO\Dictionary\WordCardGroupDTO;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use CoreDomain\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class WordCardGroupController extends Controller
{
    /**
     * @Rest\Get("/word-card-groups")
     * @Rest\View(statusCode=200)
     */
    public function listAction(Request $request)
    {
        $searchDTO = $this->get('jms_serializer')->deserialize(
            json_encode($request->query->all()),
            SearchWordCardGroupDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_word_card_group_search'])
        );

        $manager = $this->get('app.dictionary.word_card_group');
        $groups = $manager->searchWordCardGroup($searchDTO);

        $content = $this->get('serializer')->serialize(
            $groups,
            'json',
            SerializationContext::create()->setGroups(['api_word_card_group_get', 'api_image_get'])
        );

        $response = new Response($content);
        $response->headers->set('X-total-count', $manager->searchWordCardGroupCount($searchDTO));

        return $response;
    }

    /**
     * @Rest\Post("/word-card-groups")
     * @Rest\View(serializerGroups={"api_word_card_group_get"}, statusCode=201)
     * @ParamConverter(
     *     "groupDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_word_card_group_create"},
     *         "validator"={"groups"={"api_word_card_group_create"}}
     *     }
     * )
     */
    public function createAction(WordCardGroupDTO $groupDTO, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word_card_group')->save($groupDTO);
    }

    /**
     * @Rest\Delete("/word-card-groups/{id}", requirements={"id" = "\d+"})
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.dictionary.word_card_group')->delete($id);
    }
}