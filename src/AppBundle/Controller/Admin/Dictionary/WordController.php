<?php

namespace AppBundle\Controller\Admin\Dictionary;


use FOS\RestBundle\Controller\Annotations as Rest;
use CoreDomain\DTO\Dictionary\WordDTO;
use CoreDomain\DTO\Dictionary\WordTranslationDTO;
use CoreDomain\Exception\ValidationException;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\DeserializationContext;

class WordController extends Controller
{
    /**
     * @Rest\GET("/words/{id}")
     * @Rest\View(serializerGroups={"api_word_get", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.dictionary.word')->getWordById($id);
    }

    /**
     * @Rest\Post("/words")
     * @Rest\View(serializerGroups={"api_word_create", "api_audio_get", "api_image_get"}, statusCode=200)
     * @ParamConverter(
     *     "wordDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_word_create"},
     *          "validator"={"groups"={"api_word_create"}}
     *     }
     * )
     */
    public function createAction(WordDTO $wordDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word')->addWord($wordDTO);
    }

    /**
     * @Rest\Patch("words/{id}")
     * @Rest\View(serializerGroups={"api_word_update", "api_audio_get", "api_image_get"}, statusCode=200)
     * @ParamConverter(
     *     "wordDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_word_update"},
     *          "validator"={"groups"={"api_word_update"}}
     *     }
     * )
     */
    public function updateAction(WordDTO $wordDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word')->updateWord($wordDTO, $id);
    }

    /**
     * @Rest\Delete("/words/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.dictionary.word')->deleteWord($id);
    }

    /**
     * @Rest\Get("/words")
     * @Rest\View(serializerGroups={"api_word_search", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $wordDTO = $this->get('jms_serializer')->deserialize(
            json_encode($request->query->all()),
            WordDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_word_search'])
        );

        $words = $this->get('app.dictionary.word')->search(
            $wordDTO,
            $request->get('limit', 10),
            $request->get('offset', 0)
        );

        return View::create($words['data'], 200, ['X-Total-Count' => $words['count']])
            ->setSerializationContext(
                SerializationContext::create()->setGroups(array('api_word_search'))
            );
    }

    /**
     * @Rest\Get("/translate")
     * @Rest\View(serializerGroups="api_word_search", statusCode=200)
     */
    public function translateAction(Request $request)
    {
        return $this->get('app.dictionary.word')->translateWord($request->get('word', ''));
    }

    /**
     * @Rest\Post("/word-translations")
     * @Rest\View(serializerGroups={"api_word_translation_get", "api_audio_get"}, statusCode=200)
     * @ParamConverter(
     *     "wordTranslationDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_word_translation_create"},
     *          "validator"={"groups"={"api_word_translation_create"}}
     *     }
     * )
     */
    public function addTranslationAction(WordTranslationDTO $wordTranslationDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        return $this->get('app.dictionary.word')->addTranslation($wordTranslationDTO);
    }


    /**
     * @Rest\Patch("/word-translations/{id}")
     * @Rest\View(serializerGroups={"api_word_translation_get", "api_audio_get"}, statusCode=200)
     * @ParamConverter(
     *     "wordTranslationDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_word_translation_create"},
     *          "validator"={"groups"={"api_word_translation_create"}}
     *     }
     * )
     */
    public function updateTranslationAction(WordTranslationDTO $wordTranslationDTO, $id, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        return $this->get('app.dictionary.word')->updateTranslation($id, $wordTranslationDTO);
    }


    /**
     * @Rest\Delete("/word-translations/{translationId}")
     * @Rest\View(statusCode=204)
     */
    public function deleteTranslationAction($translationId)
    {
        $this->get('app.dictionary.word')->deleteTranslation($translationId);
    }
}