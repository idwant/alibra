<?php

namespace AppBundle\Controller\Admin\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardDTO;
use CoreDomain\DTO\Dictionary\WordCardDTO;
use CoreDomain\Model\Dictionary\WordCard;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use CoreDomain\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class WordCardController extends Controller
{
    /**
     * @Rest\Get("/word-cards")
     * @Rest\View(statusCode=200)
     */
    public function listAction(Request $request)
    {
        $searchDTO = $this->get('jms_serializer')->deserialize(
            json_encode($request->query->all()),
            SearchWordCardDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_word_card_search'])
        );

        $wordCardManager = $this->get('app.dictionary.word_card');
        $cards = $wordCardManager->searchWordCard($searchDTO);

        $content = $this->get('serializer')->serialize(
            $cards,
            'json',
            SerializationContext::create()->setGroups([
                'api_word_card_search',
                'api_word_get',
                'api_audio_get',
                'api_image_get',
                'api_lesson_get'
            ])
        );

        $response = new Response($content);
        $response->headers->set('X-total-count', $wordCardManager->searchWordCardCount($searchDTO));

        return $response;
    }

    /**
     * @Rest\Get("/word-cards/{id}", requirements={"id" = "\d+"})
     * @Rest\View(serializerGroups={"api_word_card_get", "api_word_get", "api_audio_get", "api_image_get", "api_lesson_get"}, statusCode=201)
     */
    public function viewAction($id)
    {
        return $this->get('app.repository.dictionary.word_card')->findOneById($id);
    }

    /**
     * @Rest\Patch("/word-cards/{id}")
     * @Rest\View(serializerGroups={"api_word_card_get", "api_word_get", "api_audio_get", "api_image_get", "api_lesson_get"}, statusCode=200)
     * @ParamConverter(
     *     "cardDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_word_card_create"},
     *         "validator"={"groups"={"api_word_card_create"}}
     *     }
     * )
     */
    public function updateAction(WordCardDTO $cardDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if(count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word_card')->save($cardDTO, $id);
    }

    /**
     * @Rest\Post("/word-cards")
     * @Rest\View(serializerGroups={"api_word_card_get", "api_word_get", "api_audio_get", "api_image_get", "api_lesson_get"}, statusCode=201)
     * @ParamConverter(
     *     "cardDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *         "deserializationContext"={"groups"="api_word_card_create"},
     *         "validator"={"groups"={"api_word_card_create"}}
     *     }
     * )
     */
    public function createAction(WordCardDTO $cardDTO, ConstraintViolationListInterface $validationErrors)
    {
        if(count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.dictionary.word_card')->save($cardDTO);
    }

    /**
     * @Rest\Delete("/word-cards/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.repository.dictionary.word_card')->deleteById($id);
    }

    /**
     * @Rest\Post("/word-cards/{id}/lessons")
     * @Rest\View(statusCode=204)
     */
    public function addLessonAction(Request $request, WordCard $card)
    {
        $this->get('app.dictionary.word_card')
            ->addLessons($card, $request->request->get('lesson_id'));
    }

    /**
     * @Rest\Delete("/word-cards/{id}/lessons")
     * @Rest\View("statusCode=204")
     */
    public function removeLessonAction(Request $request, WordCard $card)
    {
        $this->get('app.dictionary.word_card')
            ->removeLessons($card, $request->query->get('lesson_id'));
    }











    /**
     * @Rest\Get("/users/{id}/word-cards")
     * @Rest\View("statusCode=200")
     */
    public function listWordCardsAction($id)
    {
        return $this->get('app.dictionary.word_card')->getAllWordCards($id);
        
    }

    /**
     * @Rest\Get("/word-cards/learn/{groupId}")
     * @Rest\View(serializerGroups={"api_user_word_card_learn", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function listWordCardsForLearnAction($groupId)
    {
        return $this
            ->get('app.managers.user.user')
            ->getAllWordCardForLearn(
                $this->getUser(),
                $groupId
            );
    }
    
    /**
     * @Rest\Get("/word-cards/repeat/{groupId}")
     * @Rest\View(serializerGroups={"api_user_word_card_repeat", "api_audio_get", "api_image_get"}, statusCode=200)
     */
    public function listWordCardsForRepeatAction($groupId)
    {
        return $this
            ->get('app.managers.user.user')
            ->getAllWordCardForRepeat(
                $this->getUser(),
                $groupId
            );
    }

    /**
     * @Rest\Patch("/word-cards/learn/{id}")
     * @Rest\View("statusCode=200")
     */
    public function setWordCardLearnAction($id)
    {
        $this->get('app.managers.user.user')->learnedWordCard($id);
    }

    /**
     * @Rest\Patch("/word-cards/repeat/{id}")
     * @Rest\View("statusCode=200")
     */
    public function setWordCardRepeatAction($id)
    {
        $this->get('app.managers.user.user')->repeatedWordCard($id);
    }
}