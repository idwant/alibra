<?php

namespace AppBundle\Controller\Admin\Delivery;


use CoreDomain\DTO\Delivery\DeliveryTemplateDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use CoreDomain\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DeliveryTemplateController extends Controller
{
    /**
	 * @Rest\Get("/delivery-templates")
	 * @Rest\View(statusCode=200)
	 */
	public function getAllDeliveryTemplatesAction(Request $request)
	{
		$limit = $request->query->get('limit');
		$offset = $request->query->get('offset');
		$deliveryTemplates = $this->get('app.managers.delivery.delivery_template')
			->getDeliveryTemplateList($limit, $offset);

		$response = new Response($deliveryTemplates['responseData']);
		$response->headers->set('X-total-count', $deliveryTemplates['count']);
		return $response;
	}

	/**
	 * @Rest\Get("/delivery-templates/{id}")
	 * @Rest\View(serializerGroups="api_delivery_template_get", statusCode=200)
	 */
	public function getDeliveryTemplateAction($id)
	{
		return $this->get('app.managers.delivery.delivery_template')->getDeliveryTemplateById($id);
	}


	/**
	 * @Rest\Post("/delivery-templates")
	 * @Rest\View(serializerGroups="api_delivery_template_get", statusCode=200)
	 * @ParamConverter(
	 *     "deliveryTemplateDTO",
	 *     converter="fos_rest.request_body",
	 *     options={
	 *          "deserializationContext"={"groups"="api_delivery_template_create"},
	 *          "validator"={"groups"={"api_delivery_template_create"}}
	 *     }
	 * )
	 */
	public function createDeliveryTemplateAction(DeliveryTemplateDTO $deliveryTemplateDTO,
												 ConstraintViolationListInterface $validationErrors)
	{
		if (count($validationErrors) > 0) {
			throw new ValidationException('Bad request', $validationErrors);
		}
		return $this->get('app.managers.delivery.delivery_template')->addDeliveryTemplate($deliveryTemplateDTO);
	}

	/**
	 * @Rest\Patch("/delivery-templates/{id}")
	 * @Rest\View(serializerGroups="api_delivery_template_get", statusCode=200)
	 * @ParamConverter(
	 *     "deliveryTemplateDTO",
	 *     converter="fos_rest.request_body",
	 *     options={
	 *          "deserializationContext"={"groups"="api_delivery_template_update"},
	 *          "validator"={"groups"={"api_delivery_template_update"}}
	 *     }
	 * )
	 */
	public function updateDeliveryTemplateAction(DeliveryTemplateDTO $deliveryTemplateDTO, $id,
												 ConstraintViolationListInterface $validationErrors)
	{
		if (count($validationErrors) > 0) {
			throw new ValidationException('Bad request', $validationErrors);
		}
		return $this->get('app.managers.delivery.delivery_template')->updateDeliveryTemplate($deliveryTemplateDTO, $id);
	}

	/**
	 * @Rest\Delete("/delivery-templates/{id}")
	 * @Rest\View(statusCode=204)
	 */
	public function deleteDeliveryTemplateAction($id)
	{
		$this->get('app.managers.delivery.delivery_template')->deleteDeliveryTemplateById($id);
	}
}