<?php

namespace AppBundle\Controller\Admin\Grammar;

use CoreDomain\DTO\Grammar\GrammarSectionDTO;
use CoreDomain\DTO\Grammar\GrammarSectionSearchDTO;
use CoreDomain\Exception\ValidationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class GrammarSectionController extends Controller
{
    /**
     * Возвращает список секций грамматики
     *
     * @Rest\GET("/grammar-sections")
     * @Rest\View(serializerGroups="api_grammar_section_list", statusCode=200)
     */
    public function listAction(Request $request)
    {
        $searchDTO= $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            GrammarSectionSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_grammar_section_search'])
        );

        $grammarSections = $this->get('app.grammar.grammar_section')->getAllGrammarSections($searchDTO);
        return View::create($grammarSections['data'], 200, ['X-Total-Count' => $grammarSections['count']]);
    }

    /**
     * Возвращает грамматическую секцию по Id
     *
     * @Rest\GET("/grammar-sections/{id}")
     * @Rest\View(serializerGroups="api_grammar_section_get", statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.grammar.grammar_section')->getGrammarSectionById($id);
    }

    /**
     * Созадёт грамматическую секцию
     *
     * @Rest\Post("/grammar-sections")
     * @Rest\View(serializerGroups="api_grammar_section_get", statusCode=201)
     * @ParamConverter(
     *     "grammarSectionDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_grammar_section_create"},
     *          "validator"={"groups"={"api_grammar_section_create"}}
     *     }
     * )
     */
    public function createAction(GrammarSectionDTO $grammarSectionDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.grammar.grammar_section')->addGrammarSection($grammarSectionDTO);
    }

    /**
     * Обновляет грамматическую секцию
     *
     * @Rest\Patch("/grammar-sections/{id}")
     * @Rest\View(serializerGroups="api_grammar_section_get", statusCode=201)
     * @ParamConverter(
     *     "grammarSectionDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_grammar_section_update"},
     *          "validator"={"groups"={"api_grammar_section_update"}}
     *     }
     * )
     */
    public function updateAction(GrammarSectionDTO $grammarSectionDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.grammar.grammar_section')->updateGrammarSection($grammarSectionDTO, $id);
    }

    /**
     * Удаляет грамматическую секцию
     *
     * @Rest\Delete("/grammar-sections/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.grammar.grammar_section')->deleteGrammarSection($id);
    }

    /**
     * Возвращает список тем закреплённых за грамматической секцией
     *
     * @Rest\GET("/grammar-sections/{id}/themes")
     * @Rest\View(serializerGroups="api_grammar_themes_list", statusCode=200)
     */
    public function listThemesAction($id)
    {
        return $this->get('app.grammar.grammar_theme')->getGrammarThemesBySection($id);
    }
}