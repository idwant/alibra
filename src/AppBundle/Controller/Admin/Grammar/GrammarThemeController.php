<?php

namespace AppBundle\Controller\Admin\Grammar;


use CoreDomain\DTO\Grammar\GrammarThemeDTO;
use CoreDomain\DTO\Grammar\GrammarThemeSearchDTO;
use CoreDomain\Exception\ValidationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GrammarThemeController extends Controller
{
    /**
     * @Rest\GET("/grammar-themes")
     * @Rest\View(serializerGroups={"api_grammar_themes_list", "api_grammar_section_get"}, statusCode=200)
     */
    public function listAction(Request $request)
    {
        $searchDTO= $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            GrammarThemeSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_grammar_theme_search'])
        );

        $grammarThemes = $this->get('app.grammar.grammar_theme')->listGrammarThemes($searchDTO);

        return View::create($grammarThemes['data'], 200, ['X-Total-Count' => $grammarThemes['count']])
            ->setSerializationContext(
                SerializationContext::create()->setGroups(array('api_grammar_themes_list'))
            );
    }

    /**
     * @Rest\GET("/grammar-themes/{id}/grammars")
     * @Rest\View(serializerGroups="api_grammar_search", statusCode=200)
     */
    public function listGrammarsAction($id)
    {
        return $this->get('app.grammar')->getGrammarsByTheme($id);
    }

    /**
     * @Rest\GET("/grammar-themes/{id}")
     * @Rest\View(serializerGroups={"api_grammar_theme_get","api_grammar_section_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.grammar.grammar_theme')->getGrammarThemeById($id);
    }

    /**
     * @Rest\Post("/grammar-themes")
     * @Rest\View(serializerGroups={"api_grammar_theme_get","api_grammar_section_get"}, statusCode=201)
     * @ParamConverter(
     *     "grammarThemeDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_grammar_theme_create"},
     *          "validator"={"groups"={"api_grammar_theme_create"}}
     *     }
     * )
     */
    public function createAction(GrammarThemeDTO $grammarThemeDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.grammar.grammar_theme')->addGrammarTheme($grammarThemeDTO);
    }

    /**
     * @Rest\Patch("/grammar-themes/{id}")
     * @Rest\View(serializerGroups={"api_grammar_theme_get","api_grammar_section_get"}, statusCode=201)
     * @ParamConverter(
     *     "grammarThemeDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_grammar_theme_create"},
     *          "validator"={"groups"={"api_grammar_theme_create"}}
     *     }
     * )
     */
    public function updateAction(GrammarThemeDTO $grammarThemeDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.grammar.grammar_theme')->updateGrammarTheme($grammarThemeDTO, $id);
    }

    /**
     * @Rest\Delete("/grammar-themes/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.grammar.grammar_theme')->deleteGrammarThemeById($id);
    }
}