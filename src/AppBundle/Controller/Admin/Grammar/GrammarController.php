<?php

namespace AppBundle\Controller\Admin\Grammar;


use CoreDomain\DTO\Grammar\GrammarDTO;
use CoreDomain\DTO\Grammar\GrammarSearchDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use CoreDomain\Exception\ValidationException;

class GrammarController extends Controller
{
    /**
     * Возвращает грамматику
     *
     * @Rest\GET("/grammars/{id}")
     * @Rest\View(serializerGroups={"api_grammar_get", "api_grammar_section_get", "api_grammar_theme_get", "api_video_get"}, statusCode=200)
     */
    public function viewAction($id)
    {
        return $this->get('app.grammar')->getGrammarById($id);
    }

    /**
     * Создаёт новую грамматику
     *
     * @Rest\Post("/grammars")
     * @Rest\View(serializerGroups={"api_grammar_get","api_grammar_theme_get","api_grammar_section_get","api_video_get"}, statusCode=201)
     * @ParamConverter(
     *     "grammarDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_grammar_create"},
     *          "validator"={"groups"={"api_grammar_create"}}
     *     }
     * )
     */
    public function createAction(GrammarDTO $grammarDTO, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.grammar')->addGrammar($grammarDTO);
    }

    /**
     * Обновляет грамматику
     *
     * @Rest\Patch("/grammars/{id}")
     * @Rest\View(serializerGroups={"api_grammar_get","api_grammar_theme_get","api_grammar_section_get", "api_video_get"}, statusCode=201)
     * @ParamConverter(
     *     "grammarDTO",
     *     converter="fos_rest.request_body",
     *     options={
     *          "deserializationContext"={"groups"="api_grammar_update"},
     *          "validator"={"groups"={"api_grammar_update"}}
     *     }
     * )
     */
    public function updateAction(GrammarDTO $grammarDTO, ConstraintViolationListInterface $validationErrors, $id)
    {
        if (count($validationErrors) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }

        return $this->get('app.grammar')->updateGrammar($grammarDTO, $id);
    }

    /**
     * Удаляет грамматику
     * 
     * @Rest\Delete("/grammars/{id}")
     * @Rest\View("statusCode=204")
     */
    public function deleteAction($id)
    {
        $this->get('app.grammar')->deleteGrammarById($id);
    }

    /**
     * Поиск грамматики
     * 
     * @Rest\Get("/grammars")
     * @Rest\View(serializerGroups={"api_grammar_search"}, statusCode=200)
     */
    public function searchAction(Request $request)
    {
        $grammarSearchDTO = $this->get('serializer')->deserialize(
            json_encode($request->query->all()),
            GrammarSearchDTO::class,
            'json',
            DeserializationContext::create()->setGroups(['api_grammar_search'])
        );

        $grammars = $this->get('app.grammar')->searchGrammars(
            $grammarSearchDTO,
            $request->get('limit', 50),
            $request->get('offset', 0)
        );

        $grammarsCount = $this->get('app.grammar')->searchGrammarsCount($grammarSearchDTO);

        return View::create($grammars, 200, ['X-Total-Count' => $grammarsCount])
            ->setSerializationContext(
                SerializationContext::create()->setGroups(array('api_grammar_search')))
            ;
    }
}