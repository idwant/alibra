<?php

namespace  AppBundle\Listener;

use CoreDomain\Model\User\User;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ActivityListener
{
	protected $tokenStorage;
	protected $userRepository;

	public function __construct(TokenStorageInterface $tokenStorage, UserRepositoryInterface $userRepository)
	{
		$this->tokenStorage = $tokenStorage;
		$this->userRepository = $userRepository;
	}

	public function onCoreController(FilterControllerEvent $event)
	{
		$token = $this->tokenStorage->getToken();
		if ($token) {
			$user = $token->getUser();
			if ($user instanceof User) {
				$user->setLastActivityDate(new \DateTime());
				$this->userRepository->addAndSave($user);
			}
		}
	}
}