<?php

namespace AppBundle\Listener;


use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class PDOExceptionListener
{
    public function onPdoException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof PDOException || $exception instanceof UniqueConstraintViolationException) {
            throw new LogicException($exception);
        }
    }
}