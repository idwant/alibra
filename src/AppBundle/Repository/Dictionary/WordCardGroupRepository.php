<?php

namespace AppBundle\Repository\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;
use CoreDomain\Model\Dictionary\WordCardGroup;
use CoreDomain\Model\Education\UserWordCard;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Dictionary\WordCardGroupRepositoryInterface;
use Doctrine\ORM\EntityManager;

class WordCardGroupRepository implements WordCardGroupRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('wcg')
            ->from(WordCardGroup::class, 'wcg')
            ->where('wcg.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllByUser(User $user)
    {
        return $this->em->createQueryBuilder()
            ->select('uwc','wcg')
            ->from(UserWordCard::class, 'uwc')
            ->innerJoin('uwc.wordCard','wc')
            ->innerJoin('wc.group','wcg')
            ->where('uwc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findAllWithStatByUser(SearchWordCardGroupDTO $searchDTO)
    {
        $params = [];

        $userWhere = '1=1';
        $user2Where = '1=1';
        if ($searchDTO->userId) {
            $params['userId'] = $searchDTO->userId;
            $userWhere = 'uwc.user_id = :userId';
            $user2Where = 'uwc2.user_id = :userId';
        }

        $categoryWhere = '1=1';
        if ($searchDTO->categoryId) {
            $params['categoryId'] = $searchDTO->categoryId;
            $categoryWhere = 'wcc.id = :categoryId';
        }

        $groupWhere = '1=1';
        if ($searchDTO->groupId) {
            $params['groupId'] = $searchDTO->groupId;
            $categoryWhere = 'wcg.id = :groupId';
        }

        $isDemoWhere = '1=1';
        if ($searchDTO->isDemo != null) {
            $params['isDemo'] = $searchDTO->isDemo;
            $isDemoWhere = 'EXISTS(SELECT lwc.lesson_id
                                   FROM lesson_word_card lwc
                                       INNER JOIN unit_lesson ul ON ul.lesson_id = lwc.lesson_id
                                       INNER JOIN lesson l ON l.id=ul.lesson_id AND l.is_deleted = false
                                       INNER JOIN unit u ON u.id=ul.unit_id AND u.is_deleted = false
                                       INNER JOIN course_unit cu ON cu.unit_id = ul.unit_id
                                       INNER JOIN course c ON c.id=cu.course_id AND c.is_deleted = false ' .
                                       ($searchDTO->userId
                                           ? 'INNER JOIN user_course uc ON uc.course_id = c.id AND uc.user_id = ' . $searchDTO->userId .' '
                                           : ''
                                       ) .
                                       'WHERE lwc.word_card_id = wc.id AND cu.is_demo = :isDemo
                               )';
        }

        $joinsForIsDemo = '
            INNER JOIN lesson_word_card lwc ON lwc.word_card_id = wc2.id
            INNER JOIN unit_lesson ul ON ul.lesson_id = lwc.lesson_id
            INNER JOIN lesson l ON l.id=ul.lesson_id AND l.is_deleted = false
            INNER JOIN unit u ON u.id=ul.unit_id AND u.is_deleted = false
            INNER JOIN course_unit cu ON cu.unit_id = ul.unit_id AND cu.is_demo = :isDemo
            INNER JOIN course c ON c.id=cu.course_id AND c.is_deleted = false ' .
            ($searchDTO->userId
                ? 'INNER JOIN user_course uc ON uc.course_id = c.id AND uc.user_id = ' . $searchDTO->userId . ' '
                : ''
            );

        $sql = sprintf('
              SELECT t.*
              FROM (
                  SELECT
                      wcg.*,
                      
                      (
                          SELECT SUM(CASE WHEN uwc2.known THEN 1 ELSE 0 END)
                          FROM word_card wc2
                              INNER JOIN word_card_word_card_group wcwcg2 ON wcwcg2.word_card_id = wc2.id
                              LEFT JOIN user_word_card uwc2 ON uwc2.word_card_id = wc2.id AND (%4$s)
                              
                              ' . ($searchDTO->isDemo != null ? $joinsForIsDemo : '') . '
                          WHERE wcwcg2.word_card_group_id = wcg.id
                             AND NOT wc2.is_deleted
                             AND (uwc2 IS NULL OR NOT uwc2.disabled)
                          GROUP BY wcwcg2.word_card_group_id
                      ) AS learned_count,
                      
                      (
                          SELECT SUM(CASE WHEN NOT uwc2.known AND uwc2.learn_date > :now THEN 1 ELSE 0 END)
                          FROM word_card wc2
                              INNER JOIN word_card_word_card_group wcwcg2 ON wcwcg2.word_card_id = wc2.id
                              LEFT JOIN user_word_card uwc2 ON uwc2.word_card_id = wc2.id AND (%4$s)

                              ' . ($searchDTO->isDemo != null ? $joinsForIsDemo : '') . '
                              WHERE wcwcg2.word_card_group_id = wcg.id
                             AND NOT wc2.is_deleted
                             AND (uwc2 IS NULL OR NOT uwc2.disabled)
                          GROUP BY wcwcg2.word_card_group_id
                      ) AS postponed_learn_count,
                      
                      (
                          SELECT MIN(CASE WHEN uwc2.repetition_count IS NULL
                                   THEN 0
                                   ELSE uwc2.repetition_count END)
                          FROM word_card wc2
                            INNER JOIN word_card_word_card_group wcwcg2 ON wcwcg2.word_card_id = wc2.id
                            LEFT JOIN user_word_card uwc2 ON uwc2.word_card_id = wc2.id AND (%4$s)
                            
                            ' . ($searchDTO->isDemo != null ? $joinsForIsDemo : '') . '
                            WHERE wcwcg2.word_card_group_id = wcg.id
                              AND NOT wc2.is_deleted
                              AND (uwc2 IS NULL OR NOT uwc2.disabled)
                          GROUP BY wcwcg2.word_card_group_id
                      ) AS min_repetition_step,
                                            
                      (
                          SELECT count(wc2.id)
                          FROM word_card wc2
                              INNER JOIN word_card_word_card_group wcwcg2 ON wcwcg2.word_card_id = wc2.id
                              LEFT JOIN user_word_card uwc2 ON uwc2.word_card_id = wc2.id AND (%4$s)
                              
                              ' . ($searchDTO->isDemo != null ? $joinsForIsDemo : '') . '
                              WHERE wcwcg2.word_card_group_id = wcg.id
                             AND NOT wc2.is_deleted
                             AND (uwc2 IS NULL OR NOT uwc2.disabled)
                          GROUP BY wcwcg2.word_card_group_id
                      ) AS total_count,
                      
                      %5$s as max_repetition_step,
                      
                      COUNT(wc.id) AS group_total_count,
                      
                      SUM(CASE WHEN uwc.known AND uwc.repetition_count < :repetitionCount
                          THEN 1
                          ELSE 0 END) AS total_to_repetition_count,
                          
                      COALESCE(MIN(uwc.repetition_count), 0) AS current_repetition_step,
                      
                      MAX(uwc.repetition_date) AS max_repetition_date,
                      
                      SUM(CASE WHEN uwc.known
                              AND uwc.repetition_date < :now
                              AND uwc.repetition_count < :repetitionCount
                          THEN 1 ELSE 0 END
                      ) AS ready_to_repeatition_count
                  FROM word_card wc
                      INNER JOIN word_card_word_card_group wcwcg ON wcwcg.word_card_id = wc.id
                      INNER JOIN word_card_group wcg ON wcg.id = wcwcg.word_card_group_id
                      INNER JOIN word_card_category wcc ON wcc.id = wcg.word_card_category_id
                      LEFT JOIN user_word_card uwc ON uwc.word_card_id = wc.id AND (%1$s)
                  WHERE NOT wc.is_deleted AND (%2$s) AND (%3$s) AND (%6$s) AND (uwc IS NULL OR NOT uwc.disabled)
                  GROUP BY wcg.id, COALESCE (uwc.repetition_count, 0)
            ) t
            WHERE t.current_repetition_step = t.min_repetition_step
            ORDER BY t.name
        ', $userWhere, $categoryWhere, $groupWhere, $user2Where, UserWordCard::REPETITION_COUNT, $isDemoWhere);

        $statement = $this->em->getConnection()->prepare($sql);
        $statement->bindValue('repetitionCount', UserWordCard::REPETITION_COUNT);
        $statement->bindValue('now', (new \DateTime())->format('Y-m-d'));

        foreach ($params as $name => $value) {
            $statement->bindValue($name, $value);
        }

        $statement->execute();
        return $statement->fetchAll();
    }

    public function search(SearchWordCardGroupDTO $searchDTO, $onlyCount = false)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('wcg')
            ->from(WordCardGroup::class, 'wcg')
            ->where('wcg.isDeleted = :isDeleted');

        $parameters = ['isDeleted' => false];

        if($searchDTO->name) {
            $qb->andWhere(
                $qb->expr()->like($qb->expr()->lower('wcg.name'), ':name')
            );
            $parameters = array_merge($parameters, [
                'name' => '%'.strtolower(trim($searchDTO->name)).'%'
            ]);
        }

        if ($searchDTO->categoryId) {
            $qb->andWhere('wcg.category = :categoryId');
            $parameters['categoryId'] = $searchDTO->categoryId;
        }

        $qb
            ->setParameters($parameters);

        if ($onlyCount) {
            return $qb
                ->select('COUNT(wcg.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } else {
            $qb
                ->addOrderBy('wcg.name', 'ASC')
                ->addOrderBy('wcg.id');

            if ($searchDTO->limit) {
                $qb->setMaxResults($searchDTO->limit);
            }

            if ($searchDTO->offset) {
                $qb->setFirstResult($searchDTO->offset);
            }

            return $qb
                ->getQuery()
                ->getResult();
        }
    }

    public function addAndSave(WordCardGroup $group)
    {
        $this->em->persist($group);
        $this->em->flush($group);
    }
}