<?php

namespace AppBundle\Repository\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardCategoryDTO;
use CoreDomain\Model\Dictionary\WordCardCategory;
use CoreDomain\Repository\Dictionary\WordCardCategoryRepositoryInterface;
use Doctrine\ORM\EntityManager;

class WordCardCategoryRepository implements WordCardCategoryRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('wcc')
            ->from(WordCardCategory::class, 'wcc')
            ->leftJoin('wcc.groups', 'wcg')
            ->where('wcc.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function search(SearchWordCardCategoryDTO $searchDTO, $onlyCount = false)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('wcc', 'COUNT(DISTINCT wcg) as groupCount')
            ->from(WordCardCategory::class, 'wcc')
            ->leftJoin('wcc.groups', 'wcg')
            ->where('wcc.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if($searchDTO->id) {
            $qb->andWhere('wcc.id = :id')
                ->setParameter('id', $searchDTO->id);
        }

        if($searchDTO->name) {
            $qb
                ->andWhere(
                    $qb->expr()->like($qb->expr()->lower('wcc.name'), ':name')
                )
                ->setParameter('name', '%'.strtolower(trim($searchDTO->name)).'%');
        }

        if ($searchDTO->isDemo !== null) {
            $qb->leftJoin('wcg.wordCards', 'wc')
                ->leftJoin('wc.lessons', 'l')
                ->leftJoin('l.unitLesson', 'ul')
                ->leftJoin('ul.unit', 'u')
                ->leftJoin('u.courseUnit', 'cu')
                ->andWhere('cu.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);

            // Привязку к пользователю проверяем только для демо-пользователей
            if ($searchDTO->getUserId()) {
                $qb->innerJoin('cu.course', 'c')
                    ->innerJoin('c.users', 'usr', 'WITH', 'usr.id=:userId')
                    ->setParameter('userId', $searchDTO->getUserId());
            }
        }

        if ($onlyCount) {
            return $qb
                ->select('COUNT(DISTINCT wcc.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } else {
            $qb
                ->groupBy('wcc.id')
                ->addOrderBy('wcc.name', 'ASC')
                ->addOrderBy('wcc.id');

            if ($searchDTO->limit) {
                $qb->setMaxResults($searchDTO->limit);
            }

            if ($searchDTO->offset) {
                $qb->setFirstResult($searchDTO->offset);
            }

            $groups = $qb
                ->getQuery()
                ->getResult();

            $groups = array_map(function ($groupInfo) {
                /** @var WordCardCategory $group */
                $group = $groupInfo[0];
                $group->setGroupsCount($groupInfo['groupCount']);
                return $group;
            }, $groups);

            return $groups;
        }
    }

    public function addAndSave(WordCardCategory $category)
    {
        $this->em->persist($category);
        $this->em->flush($category);
    }
}