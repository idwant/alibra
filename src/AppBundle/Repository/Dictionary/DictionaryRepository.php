<?php

namespace AppBundle\Repository\Dictionary;


use CoreDomain\Model\Dictionary\Dictionary;
use CoreDomain\Repository\Dictionary\DictionaryRepositoryInterface;
use Doctrine\ORM\EntityManager;

class DictionaryRepository implements DictionaryRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function search($query, $onlyCount = false, $limit = null, $offset = null)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->from(Dictionary::class, 'd')
            ->where('d.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if ($query) {
            $queryBuilder->andWhere('LOWER(d.name) LIKE :name')
                ->setParameter('name', '%'.strtolower(trim($query)).'%');
        }

        if ($onlyCount) {
            return $queryBuilder
                ->select('COUNT(d.id)')
                ->getQuery()
                ->getSingleScalarResult();

        }

        return $queryBuilder
            ->select('d')
            ->addOrderBy('d.name', 'ASC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('d')
            ->from(Dictionary::class, 'd')
            ->where('d.id = :id')
            ->andWhere('d.isDeleted = :isDeleted')
            ->setParameters(['id' => $id, 'isDeleted' => false])
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('d')
            ->from(Dictionary::class, 'd')
            ->where('d.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSave(Dictionary $dictionary)
    {
        $this->em->persist($dictionary);
        $this->em->flush();
    }

    public function deleteById($id)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->update(Dictionary::class, 'd')
            ->set('d.isDeleted', ':isDeleted')
            ->set('d.name', $qb->expr()->concat('d.name', ':deleteDate'))
            ->where('d.id = :id')
            ->setParameters([
                'id' => $id,
                'isDeleted' => true,
                'deleteDate' => (new \DateTime())->format('_YmdHis')
            ])
            ->getQuery()
            ->execute();
    }

    public function addWords($dictionary, $group)
    {
        $sql = sprintf('

				INSERT INTO dictionary_word (dictionary_id, word_id)
				(
					SELECT %1$s as d_id, wc.word_id as w_id
						FROM word_card_word_card_group as wcwcg
							INNER JOIN word_card as wc ON wc.id = wcwcg.word_card_id
						WHERE
							wcwcg.word_card_group_id = %2$s
					GROUP BY d_id, w_id
				)', $dictionary->getId(), $group->getId());

        $statement = $this->em->getConnection()->prepare($sql);

        $statement->execute();
        $statement->fetchAll();
    }
}
