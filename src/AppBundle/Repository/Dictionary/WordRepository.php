<?php

namespace AppBundle\Repository\Dictionary;


use CoreDomain\DTO\Dictionary\WordDTO;
use CoreDomain\Model\Dictionary\Dictionary;
use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\WordTranslation;
use CoreDomain\Repository\Dictionary\WordRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;

class WordRepository implements WordRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('w')
            ->from(Word::class, 'w')
            ->where('w.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function addAndSave(Word $word)
    {
        $this->em->persist($word);
        $this->em->flush($word);
    }

    public function setDeletedById($id)
    {
        $this->em->createQueryBuilder()
            ->update(Word::class, 'w')
            ->set('w.isDeleted', ':isDeleted')
            ->where('w.id = :id')
            ->setParameters(array(
                'isDeleted' => true,
                'id' => $id
            ))
            ->getQuery()
            ->execute();
    }

    public function search(WordDTO $wordDTO, $onlyCount = false,
                                       $limit = null, $offset = null)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->from(Word::class, 'w')
            ->where('w.isDeleted = :isDeleted');

        $parameters = array(
            'isDeleted' => false
        );

        if ($wordDTO->word) {
            $qb->andWhere(
                $qb->expr()->like($qb->expr()->lower('w.word'), ':word')
            );

            $parameters = array_merge($parameters, array(
                    'word' => '%'.strtolower(trim($wordDTO->word)).'%'
                ));
        }

        if($onlyCount) {
            return $qb
                ->select('COUNT(w.id)')
                ->setParameters($parameters)
                ->getQuery()
                ->getSingleScalarResult();
        }
        $result = $qb
            ->select('w')
            ->addOrderBy('w.word', 'ASC')
            ->setParameters($parameters)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function findAllByDictionary(Dictionary $dictionary, $word = null, $onlyCount = false, $limit = null, $offset = null)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->from(Dictionary::class, 'd')
            ->innerJoin('d.words', 'w')
            ->where('d.id = :id')
            ->andWhere('w.isDeleted = :isDeleted')
            ->setParameters(array(
                'id' => $dictionary->getId(),
                'isDeleted' => false
            ));

        if ($word) {
            $qb->andWhere(
                $qb->expr()->like($qb->expr()->lower('w.word'), ':word')
            );
            $qb->setParameter('word', '%'.strtolower(trim($word)).'%');
        }

        if($onlyCount) {
            return $qb->select('COUNT (w.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        try {
            $dictionaryWords =
                $qb->select('d','w')
                    ->addOrderBy('w.word', 'ASC')
                    ->setFirstResult($offset)
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getSingleResult();
        }
        catch(NoResultException $e) {
            return [];
        }

        return $dictionaryWords->getWords();
    }

    public function findDictionaryWord($dictionaryId, $wordId)
    {
        return $this->em->createQueryBuilder()
            ->select('d', 'w')
            ->from(Dictionary::class, 'd')
            ->innerJoin('d.words', 'w')
            ->where('d.id = :dictionaryId')
            ->andWhere('w.id = :wordId')
            ->setParameters(array(
                'dictionaryId' => $dictionaryId,
                'wordId' => $wordId
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findWordTranslationById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('wt')
            ->from(WordTranslation::class, 'wt')
            ->where('wt.id = :id')
            ->andWhere('wt.isDeleted = :isDeleted')
            ->setParameters(array(
                'id' => $id,
                'isDeleted' => false
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function translateWord($word)
    {
        return $this->em->createQueryBuilder()
            ->select('w')
            ->from(Word::class, 'w')
            ->where('w.isDeleted = :isDeleted')
            ->andWhere('LOWER(w.word) = :word')
            ->setParameters(array(
                'isDeleted' => false,
                'word' => $word
            ))
            ->getQuery()
            ->getResult();
    }

    public function addAndSaveWordTranslation(WordTranslation $wordTranslation)
    {
        $this->em->persist($wordTranslation);
        $this->em->flush($wordTranslation);
    }

    public function deleteWordTranslation($wordTranslationId)
    {
        $this->em->createQueryBuilder()
            ->update(WordTranslation::class, 'wt')
            ->set('wt.isDeleted', ':isDeleted')
            ->where('wt.id = :id')
            ->setParameters(array(
                'isDeleted' => true,
                'id' => $wordTranslationId
            ))
            ->getQuery()
            ->execute();
    }

    public function getRandomWords($num = 3, $wordId = null)
    {
        $qb = $this->em->createQueryBuilder();
        $count = $qb
            ->select($qb->expr()->count('w.id'))
            ->from(Word::class, 'w')
            ->where('w.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false)
            ->getQuery()
            ->getSingleScalarResult();

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('w')
            ->from(Word::class, 'w')
            ->where('w.isDeleted = :isDeleted');

        if ($wordId) {
            $qb
                ->andWhere('w.id != :id')
                ->setParameter('id', $wordId);
        }

        $qb
            ->setParameter('isDeleted', false)
            ->setFirstResult(($count > $num) ? rand(0, $count - $num) : 0)
            ->setMaxResults($num);

        return $qb->getQuery()->getResult();
    }
}