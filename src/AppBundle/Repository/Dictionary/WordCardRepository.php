<?php

namespace AppBundle\Repository\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardDTO;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Repository\Dictionary\WordCardRepositoryInterface;
use Doctrine\ORM\EntityManager;

class WordCardRepository implements WordCardRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findAll()
    {
        $result = $this->em->createQueryBuilder()
            ->select('wc', 'w')
            ->from(WordCard::class, 'wc')
            ->innerJoin('wc.word', 'w')
            ->where('wc.isDeleted = :isDeleted')
            ->setParameters(array('isDeleted' => false))
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('wc', 'w')
            ->from(WordCard::class, 'wc')
            ->innerJoin('wc.word', 'w')
            ->where('wc.isDeleted = :isDeleted')
            ->andWhere('wc.id = :id')
            ->setParameters(array(
                'isDeleted' => false,
                'id' => $id
            ))
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function addAndSave(WordCard $card)
    {
        $this->em->persist($card);
        $this->em->flush($card);
    }

    public function deleteById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->update(WordCard::class, 'wc')
            ->set('wc.isDeleted', ':isDeleted')
            ->where('wc.id = :id')
            ->setParameters(array(
                ':id' => $id,
                ':isDeleted' => true
            ))
            ->getQuery()
            ->getResult();
    }

    public function search(SearchWordCardDTO $searchDTO, $onlyCount = false)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('wc', 'w')
            ->from(WordCard::class, 'wc')
            ->innerJoin('wc.word', 'w')
            ->where('wc.isDeleted = :isDeleted');

        $parameters = ['isDeleted' => false];

        if($searchDTO->word){
            $qb->andWhere(
                    $qb->expr()->like($qb->expr()->lower('w.word'), ':word')
                );
                $parameters = array_merge($parameters, [
                    'word' => '%'.strtolower(trim($searchDTO->word)).'%'
            ]);
        }

        $qb
            ->setParameters($parameters);

        if ($onlyCount) {
            return $qb
                ->select('COUNT(wc.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } else {
            $qb
                ->addOrderBy('w.word', 'ASC')
                ->addOrderBy('wc.id');

            return $qb
                ->setMaxResults($searchDTO->limit)
                ->setFirstResult($searchDTO->offset)
                ->getQuery()
                ->getResult();
        }
    }

    public function addLesson(WordCard $card, Lesson $lesson)
    {
        $card->getLessons()->add($lesson);
        $lesson->getWordCards()->add($card);
        $this->em->flush();
    }

    public function deleteLesson(WordCard $card, Lesson $lesson)
    {
        $card->getLessons()->removeElement($lesson);
        $lesson->getWordCards()->removeElement($card);
        $this->em->flush();
    }

    public function findByCategoryAndTheme($theme, $category)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('wc')
            ->from(WordCard::class, 'wc')
            ->leftJoin('wc.groups', 'g')
            ->leftJoin('g.category', 'c')
            ->where('g.name LIKE :gName')
            ->setParameter('gName', '%'.$theme.'%');

        if($category != '') {
            $qb
                ->andWhere('c.name LIKE :cName')
                ->setParameter('cName', '%'.$category.'%');
        }

        return $qb->getQuery()
            ->getResult();
    }
}