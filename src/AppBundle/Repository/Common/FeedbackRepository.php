<?php

namespace AppBundle\Repository\Common;

use CoreDomain\Model\Common\Feedback;
use CoreDomain\Repository\Common\FeedbackRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class FeedbackRepository implements FeedbackRepositoryInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function addAndSave(Feedback $feedback)
    {
        $this->em->persist($feedback);
        $this->em->flush($feedback);
    }
}