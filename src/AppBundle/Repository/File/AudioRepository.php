<?php

namespace AppBundle\Repository\File;

use CoreDomain\Model\File\Audio;

class AudioRepository extends FileRepository
{
    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('a')
            ->from(Audio::class, 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}