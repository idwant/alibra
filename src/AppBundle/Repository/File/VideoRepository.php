<?php

namespace AppBundle\Repository\File;

use CoreDomain\Model\File\Video;

class VideoRepository extends FileRepository
{
    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('v')
            ->from(Video::class, 'v')
            ->where('v.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}