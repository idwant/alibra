<?php

namespace AppBundle\Repository\Delivery;

use CoreDomain\Repository\Delivery\DeliveryTemplateRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use CoreDomain\Model\Delivery\DeliveryTemplate;

class DeliveryTemplateRepository implements DeliveryTemplateRepositoryInterface
{
	private $em;

	public function __construct(EntityManagerInterface $em)
	{
		$this->em = $em;
	}

	public function findAll($limit = null, $offset = null)
	{
		$result = $this->em->createQueryBuilder()
			->select('dt')
			->from(DeliveryTemplate::class, 'dt')
			->setMaxResults($limit)
			->setFirstResult($offset)
			->getQuery()
			->getResult();

		return $result;
	}

	public function findOneById($id)
	{
		return $this->em->createQueryBuilder()
			->select('dt')
			->from(DeliveryTemplate::class, 'dt')
			->where('dt.id = :id')
			->setParameter('id', $id)
			->getQuery()
			->getOneOrNullResult();
	}

	public function addAndSave(DeliveryTemplate $deliveryTemplate)
	{
		$this->em->persist($deliveryTemplate);
		$this->em->flush($deliveryTemplate);
	}

	public function deleteById($id)
	{
		$this->em->createQueryBuilder()
			->delete(DeliveryTemplate::class, 'dt')
			->where('dt.id = :id')
			->setParameter('id', $id)
			->getQuery()
			->execute();
	}
}