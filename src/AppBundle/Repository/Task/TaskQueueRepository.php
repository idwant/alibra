<?php

namespace AppBundle\Repository\Task;

use CoreDomain\Repository\Task\TaskQueueRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use CoreDomain\Model\Task\TaskQueue;
use CoreDomain\Model\Task\TaskStatus;

class TaskQueueRepository implements TaskQueueRepositoryInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    
    public function findOneForExecute()
    {
        $blockId = uniqid('alibra', true);
        
        $blockQuery = "UPDATE task_queue tq
                          SET block_id = :blockId, status_id = :status_changed, attempts = tq.attempts - 1
                          FROM (
                              SELECT id, status_id, service, params, block_id, attempts, created, date_start 
                              FROM task_queue t
                              WHERE t.status_id = :status AND t.block_id IS NULL AND t.date_start < :now AND t.attempts > 0
                              ORDER BY t.date_start DESC 
                              LIMIT 1
                          ) sub
                       WHERE tq.id = sub.id   
                       ";

        $statement = $this->em->getConnection()->prepare($blockQuery);
        $statement->bindValue('blockId', $blockId);
        $statement->bindValue('status_changed', TaskStatus::TASK_IN_PROCESS);
        $statement->bindValue('status', TaskStatus::TASK_WAITE);
        $statement->bindValue('now', (new \DateTime('now'))->format('Y-m-d H:i:s'));
        $statement->execute();

        return $this->em->createQueryBuilder()
            ->select('tq')
            ->from(TaskQueue::class, 'tq')
            ->where('tq.blockId = :blockId')
            ->setParameter('blockId', $blockId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSave(TaskQueue $task)
    {
        $this->em->persist($task);
        $this->em->flush();
    }

    public function unlock(TaskQueue $task)
    {
        $this->em->createQueryBuilder()
            ->update(TaskQueue::class, 'tq')
            ->set('tq.blockId', ':blockId')
            ->where('tq.id = :id')
            ->setParameter('blockId', null)
            ->setParameter('id', $task->getId())
            ->getQuery()
            ->execute();
    }
}