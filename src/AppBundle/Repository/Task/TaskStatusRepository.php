<?php

namespace AppBundle\Repository\Task;

use Doctrine\ORM\EntityManagerInterface;
use CoreDomain\Repository\Task\TaskStatusRepositoryInterface;
use CoreDomain\Model\Task\TaskStatus;

class TaskStatusRepository implements TaskStatusRepositoryInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('ts')
            ->from(TaskStatus::class ,'ts')
            ->where('ts.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}