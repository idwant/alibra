<?php

namespace AppBundle\Repository\Education;


use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\UnitBindDTO;
use CoreDomain\DTO\Education\UnitSortDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Unit;
use CoreDomain\Model\Education\CourseUnit;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use Doctrine\ORM\EntityManager;

class UnitRepository implements UnitRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findAllByDTO(UnitSearchDTO $unitSearchDTO, $onlyCount = false)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->from(Unit::class, 'u')
            ->where('u.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if($unitSearchDTO->query) {
            $queryBuilder
                ->andWhere("lower(u.name) LIKE lower(:query)")
                ->setParameter('query', "%{$unitSearchDTO->query}%");
        }

        if($unitSearchDTO->courseId) {
            $queryBuilder
                ->leftJoin('u.courseUnit', 'cu')
                ->leftJoin('cu.course', 'c', 'WITH', 'c.id = cu.course AND c.isDeleted = :isDeleted')
                ->andWhere('cu.course = :courseId')
                ->setParameter('courseId', $unitSearchDTO->courseId);
        }

        if($onlyCount) {
            return $queryBuilder
                ->select('COUNT(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        else {
            if($unitSearchDTO->limit) {
                $queryBuilder->setMaxResults($unitSearchDTO->limit);
            }
            if($unitSearchDTO->offset) {
                $queryBuilder->setFirstResult($unitSearchDTO->offset);
            }
            if($unitSearchDTO->courseId) {
                $queryBuilder->orderBy('cu.position');
            }

            return $queryBuilder
                ->select('u')
                ->getQuery()
                ->getResult();
        }
    }

    public function findOneById($id)
    {
         return $this->em->createQueryBuilder()
            ->select('u', 'cu', 'c', 'ul', 'l')
            ->from(Unit::class, 'u')
            ->leftJoin('u.courseUnit', 'cu')
            ->leftJoin('cu.course', 'c', 'WITH', 'c.id = cu.course AND c.isDeleted = :isDeleted')
            ->leftJoin('u.unitLesson', 'ul')
            ->leftJoin('ul.lesson', 'l', 'WITH', 'l.id = ul.lesson AND l.isDeleted = :isDeleted')
            ->where('u.id = :id')
            ->andWhere('u.isDeleted = :isDeleted')
            ->setParameters(['id' => $id, 'isDeleted' => false])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('u')
            ->from(Unit::class, 'u')
            ->where('u.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findCourseUnitByDTO(UnitBindDTO $unitBindDTO)
    {
        $result = $this->em->createQueryBuilder()
            ->select('cu')
            ->from(CourseUnit::class, 'cu')
            ->where('cu.course = :courseId')
            ->andWhere('cu.unit = :unitId')
            ->setParameters(array(
                'courseId' => $unitBindDTO->courseId,
                'unitId' => $unitBindDTO->unitId
            ))
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function addAndSaveCourseUnit(CourseUnit $courseUnit)
    {
        $this->em->persist($courseUnit);
        $this->em->flush($courseUnit);
    }

    public function deleteCourseUnits(array $courseUnits)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->delete(CourseUnit::class, 'cu')
            ->where(
                $qb->expr()->notIn('cu', ':courseUnits')
            )
            ->setParameter('courseUnits', array_values($courseUnits))
            ->getQuery()
            ->execute();
    }

    public function addAndSave(Unit $unit)
    {
        $this->em->persist($unit);
        $this->em->flush($unit);
    }

    public function setDeletedById($id)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->update(Unit::class, 'u')
            ->set('u.isDeleted', ':isDeleted')
            ->set('u.name', $qb->expr()->concat('u.name', ':deleteDate'))
            ->where('u.id = :id')
            ->setParameters(array(
                'isDeleted' => true,
                'id' => $id,
                'deleteDate' => (new \DateTime())->format('_YmdHis')
            ))
            ->getQuery()
            ->execute();
    }

    public function findMaxPosition($courseId)
    {
        return $this->em->createQueryBuilder()
            ->select('MAX(cu.position)')
            ->from(CourseUnit::class, 'cu')
            ->where('cu.course = :courseId')
            ->setParameter('courseId', $courseId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function sort(UnitSortDTO $unitSortDTO)
    {
        $this->em->beginTransaction();
        try {
            $unitSortDTO->position++;
            $curPos = $this->em->createQueryBuilder()
                ->select('cu.position')
                ->from(CourseUnit::class, 'cu')
                ->where('cu.course = :courseId')
                ->andWhere('cu.unit = :unitId')
                ->setParameters(['courseId' => $unitSortDTO->courseId, 'unitId' => $unitSortDTO->unitId])
                ->getQuery()
                ->getSingleScalarResult();
            
            $queryBuilder = $this->em->createQueryBuilder()
                ->update(CourseUnit::class, 'cu');
            if ($curPos < $unitSortDTO->position) {
                $queryBuilder->set('cu.position', 'cu.position - 1')
                    ->where('cu.position BETWEEN :pos AND :newPos');
            } else {
                $queryBuilder->set('cu.position', 'cu.position + 1')
                    ->where('cu.position BETWEEN :newPos AND :pos');
            }
            $queryBuilder
                ->andWhere('cu.course = :courseId')
                ->setParameters(['courseId' => $unitSortDTO->courseId, 'pos' => $curPos, 'newPos' => $unitSortDTO->position])
                ->getQuery()
                ->execute();

            $this->em->createQueryBuilder()
                ->update(CourseUnit::class, 'cu')
                ->set('cu.position', $unitSortDTO->position)
                ->where('cu.unit = :unitId')
                ->andWhere('cu.course = :courseId')
                ->setParameters(['unitId' => $unitSortDTO->unitId, 'courseId' => $unitSortDTO->courseId])
                ->getQuery()
                ->execute();

            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function decrementPositions(Course $course, $from = NULL) {
        $queryBuilder = $this->em->createQueryBuilder()
            ->update(CourseUnit::class, 'cu')
            ->set('cu.position', 'cu.position-1')
            ->where('cu.course = :courseId')
            ->setParameter('courseId', $course->getId());
        if($from) {
            $queryBuilder
                ->andWhere('cu.position > :from')
                ->setParameter('from', $from);
        }
        $queryBuilder->getQuery()->execute();
    }

    public function search(UnitSearchDTO $searchDTO)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('u')
            ->from(Unit::class, 'u', 'u.id')
            ->innerJoin('u.courseUnit','cu')
            ->innerJoin('cu.course','c')
            ->innerJoin('c.users','usr')
            ->where('u.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if ($searchDTO->id) {
            $qb
                ->andWhere('u.id = :id')
                ->setParameter('id', $searchDTO->id);
        }

        if ($searchDTO->courseId) {
            $qb
                ->andWhere('c.id = :courseId')
                ->setParameter('courseId', $searchDTO->courseId);
        }

        if ($searchDTO->query) {
            $qb
                ->andWhere(
                $qb->expr()->like(
                    $qb->expr()->lower('u.name'),
                    $qb->expr()->lower(':name')
                    )
                )
                ->setParameter('name', "%$searchDTO->query%");
        }

        if ($searchDTO->userId) {
            $qb
                ->andWhere('usr.id = :userId')
                ->setParameter('userId', $searchDTO->userId);
        }

        if ($searchDTO->isDemo !== null) {
            $qb
                ->andWhere('cu.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }
}