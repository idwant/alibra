<?php

namespace AppBundle\Repository\Education;


use CoreDomain\Model\Education\Hint;
use CoreDomain\Model\Education\UserHint;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\HintRepositoryInterface;
use Doctrine\ORM\EntityManager;

class HintRepository implements HintRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findWithUser(User $user)
    {
        return $this->em->createQueryBuilder()
            ->select('h, uh')
            ->from(Hint::class, 'h')
            ->leftJoin('h.userHint', 'uh', 'WITH', 'h.id = uh.hint AND uh.user = :userId')
            ->setParameters(['userId' => $user->getId()])
            ->getQuery()
            ->getResult();
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('h')
            ->from(Hint::class, 'h')
            ->where('h.name = :name')
            ->setParameters(['name' => $name])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findUserHint(User $user, $name)
    {
        return $this->em->createQueryBuilder()
            ->select('uh')
            ->from(UserHint::class, 'uh')
            ->leftJoin('uh.hint', 'h')
            ->where('uh.user = :userId')
            ->andWhere('h.name = :hintName')
            ->setParameters([
                'userId' => $user->getId(),
                'hintName' => $name
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSaveUserHint(UserHint $userHint)
    {
        $this->em->persist($userHint);
        $this->em->flush();
    }
}