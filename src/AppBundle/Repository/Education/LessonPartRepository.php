<?php

namespace AppBundle\Repository\Education;

use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonPartDTO;
use CoreDomain\Model\Education\LessonExercise;
use CoreDomain\Model\Education\LessonLessonPart;
use CoreDomain\Model\Education\LessonPart;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\UnitLesson;
use CoreDomain\Repository\Education\LessonPartRepositoryInterface;
use Doctrine\ORM\EntityManager;

class LessonPartRepository implements LessonPartRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findAllByDTO(EducationRequestDTO $dto, $limit = null, $offset = null)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('lp')
            ->from(LessonPart::class, 'lp')
            ->where('lp.isDeleted = :isDeleted');

        $parameters = array(
            'isDeleted' => false
        );

        if($dto->course && $dto->unit && $dto->lesson) {
            $queryBuilder
                ->leftJoin('lp.lessonLessonPart', 'llp')
                ->leftJoin('llp.unitLesson', 'ul')
                ->leftJoin('ul.courseUnit', 'cu')
                ->andWhere('cu.unit = :unitId')
                ->andWhere('ul.lesson = :lessonId')
                ->andWhere('cu.course = :courseId');
            $parameters = array_merge($parameters, array(
                'courseId' => $dto->course,
                'unitId' => $dto->unit,
                'lessonId' => $dto->lesson
            ));
        }

        $result = $queryBuilder
            ->setParameters($parameters)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('lp, l')
            ->from(LessonPart::class, 'lp')
            ->leftJoin('lp.lesson', 'l')
            ->where('lp.id = :lessonPartId')
            ->setParameter('lessonPartId', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }
    
    public function findByLessonId($lessonId)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('lp, le, e')
            ->from(LessonPart::class, 'lp')
            ->leftJoin('lp.lessonExercise', 'le')
            ->leftJoin('le.exercise', 'e', 'WITH', 'e.id = le.exercise AND e.isDeleted = :isDeleted')
            ->where('lp.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if($lessonId) {
            $queryBuilder->andWhere('lp.lesson = :lessonId')
                ->setParameter('lessonId', $lessonId);
        }

        return $queryBuilder->addOrderBy('le.position')
            ->getQuery()
            ->getResult();
    }

    public function findByName(LessonPartDTO $lessonPartDTO)
    {
        return $this->em->createQueryBuilder()
            ->select('lp')
            ->from(LessonPart::class, 'lp')
            ->where('lp.lesson = :lesson')
            ->andWhere('lp.name = :name')
            ->andWhere('lp.isDeleted = :isDeleted')
            ->setParameters([
                'lesson' => $lessonPartDTO->lessonId,
                'name' => $lessonPartDTO->name,
                'isDeleted' => false
            ])
            ->getQuery()
            ->getResult();
    }


    public function addAndSave(LessonPart $lessonPart)
    {
        $this->em->persist($lessonPart);
        $this->em->flush($lessonPart);
    }
}