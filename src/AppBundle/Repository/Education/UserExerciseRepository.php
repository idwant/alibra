<?php

namespace AppBundle\Repository\Education;

use CoreDomain\Model\Education\UserExercise;
use CoreDomain\Repository\Education\UserExerciseRepositoryInterface;
use Doctrine\ORM\EntityManager;

class UserExerciseRepository implements UserExerciseRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findOneByUserAndLessonAndExercise($userId, $lessonId, $exerciseId)
    {
        return $this->em->createQueryBuilder()
            ->select('ue')
            ->from(UserExercise::class, 'ue')
            ->innerJoin('ue.userLesson', 'ul', 'WITH', 'ul.user = :user')
            ->andWhere('ue.user = :user AND ue.exercise = :exercise AND ul.lesson=:lesson')
            ->setParameters([
                'user' => $userId,
                'exercise' => $exerciseId,
                'lesson' => $lessonId
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}