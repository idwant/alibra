<?php

namespace AppBundle\Repository\Education;


use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\CourseUnit;
use CoreDomain\Model\Education\UserCourse;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\CourseRepositoryInterface;
use Doctrine\ORM\EntityManager;

class CourseRepository implements CourseRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findAllByDTO(CourseSearchDTO $courseSearchDTO, $onlyCount = false)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->from(Course::class, 'c')
            ->where('c.isDeleted = :isDeleted')
            ->setParameter('isDeleted', 0);

        if($courseSearchDTO->query) {
            $queryBuilder
                ->andWhere("lower(c.name) LIKE lower(:query)")
                ->setParameter('query', "%{$courseSearchDTO->query}%");
        }

        if($onlyCount) {
            return $queryBuilder
                ->select('COUNT(c.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        else {
            if($courseSearchDTO->offset) {
                $queryBuilder->setFirstResult($courseSearchDTO->offset);

            }
            if($courseSearchDTO->limit) {
                $queryBuilder->setMaxResults($courseSearchDTO->limit);
            }

            return $queryBuilder
                ->select('c')
                ->getQuery()
                ->getResult();
        }
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('c, cu, u')
            ->from(Course::class, 'c')
            ->leftJoin('c.courseUnit', 'cu')
            ->leftJoin('cu.unit', 'u', 'WITH', 'u.id = cu.unit AND u.isDeleted = :isDeleted')
            ->where('c.id = :id')
            ->andWhere('c.isDeleted = :isDeleted')
            ->setParameters(['id' => $id, 'isDeleted' => false])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('c')
            ->from(Course::class, 'c')
            ->where('c.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSave(Course $course)
    {
        $this->em->persist($course);
        $this->em->flush($course);
    }

    public function setDeletedById($id)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->update(Course::class, 'c')
            ->set('c.isDeleted', ':isDeleted')
            ->set('c.name', $qb->expr()->concat('c.name', ':deleteDate'))
            ->where('c.id = :id')
            ->setParameters(array(
                'isDeleted' => true,
                'id' => $id,
                'deleteDate' => (new \DateTime())->format('_YmdHis')
            ))
            ->getQuery()
            ->execute();
    }

    public function search(CourseSearchDTO $searchDTO)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('c')
            ->from(Course::class, 'c', 'c.id')
            ->innerJoin('c.users','u')
            ->where('1 = 1');

        if ($searchDTO->userId) {
            $qb
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $searchDTO->userId);
        }

        if ($searchDTO->isDemo !== null) {
            $qb->leftJoin('c.courseUnit', 'cu')
                ->andWhere('cu.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        if ($searchDTO->query) {
            $qb
                ->andWhere(
                    $qb->expr()->like(
                        $qb->expr()->lower('c.name'),
                        $qb->expr()->lower(':name')
                    )
                )
                ->setParameter('name', "%{$searchDTO->query}%");
        }

        if ($searchDTO->id) {
            $qb
                ->andWhere('c.id = :id')
                ->setParameter('id', $searchDTO->id);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }
}