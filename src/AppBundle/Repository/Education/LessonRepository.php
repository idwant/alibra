<?php

namespace AppBundle\Repository\Education;

use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonBindDTO;
use CoreDomain\DTO\Education\LessonSortDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\Unit;
use CoreDomain\Model\Education\UnitLesson;
use CoreDomain\Model\Education\UserLesson;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use Doctrine\ORM\EntityManager;

class LessonRepository implements LessonRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('l, ul, u')
            ->from(Lesson::class, 'l')
            ->leftJoin('l.unitLesson', 'ul')
            ->leftJoin('ul.unit', 'u')
            ->where('l.id = :lessonId')
            ->setParameter('lessonId', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('l')
            ->from(Lesson::class, 'l')
            ->where('l.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllByDTO(LessonSearchDTO $lessonSearchDTO, $onlyCount = false)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->from(Lesson::class, 'l')
            ->where('l.isDeleted = :isDeleted')
            ->setParameter('isDeleted', 0);

        if($lessonSearchDTO->query) {
            $queryBuilder
                ->andWhere("lower(l.name) LIKE lower(:query)")
                ->setParameter('query', "%{$lessonSearchDTO->query}%");
        }

        if($lessonSearchDTO->unitId) {
            $queryBuilder
                ->andWhere('ul.unit = :unitId')
                ->leftJoin('l.unitLesson', 'ul')
                ->setParameter('unitId', $lessonSearchDTO->unitId);
        }

        if($onlyCount) {
            return $queryBuilder
                ->select('COUNT(l.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        else {
            if($lessonSearchDTO->limit) {
                $queryBuilder->setMaxResults($lessonSearchDTO->limit);
            }
            if($lessonSearchDTO->offset) {
                $queryBuilder->setFirstResult($lessonSearchDTO->offset);
            }
            if($lessonSearchDTO->unitId) {
                $queryBuilder->orderBy('ul.position');
            }

            return $queryBuilder
                ->select('l')
                ->getQuery()
                ->getResult();
        }
    }

    public function findByDTO(EducationRequestDTO $dto, $limit = null, $offset = null)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('l')
            ->from(Lesson::class, 'l')
            ->where('l.isDeleted = :isDeleted');

        $parameters = array(
            'isDeleted' => false
        );

        if($dto->unit && $dto->course) {
            $queryBuilder
                ->leftJoin('l.unitLesson', 'ul')
                ->leftJoin('ul.courseUnit', 'cu')
                ->andWhere('cu.unit = :unitId')
                ->andWhere('cu.course = :courseId');
            $parameters = array_merge($parameters, array(
                'courseId' => $dto->course,
                'unitId' => $dto->unit
            ));
        }

        $result = $queryBuilder
            ->setParameters($parameters)
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function addUnitLesson(UnitLesson $unitLesson)
    {
        $this->em->persist($unitLesson);
        $this->em->flush($unitLesson);
    }

    public function deleteUnitLessons(array $unitLessons)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->delete(UnitLesson::class, 'ul')
            ->where(
                $qb->expr()->notIn('ul', ':unitLessons')
            )
            ->setParameter('unitLessons', array_values($unitLessons))
            ->getQuery()
            ->execute();
    }

    public function addAndSave(Lesson $lesson)
    {
        $this->em->persist($lesson);
        $this->em->flush($lesson);
    }

    public function setDeletedById($id)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->update(Lesson::class, 'l')
            ->set('l.isDeleted', ':isDeleted')
            ->set('l.name', $qb->expr()->concat('l.name', ':deleteDate'))
            ->where('l.id = :id')
            ->setParameters(array(
                'isDeleted' => true,
                'id' => $id,
                'deleteDate' => (new \DateTime())->format('_YmdHis')
            ))
            ->getQuery()
            ->execute();
    }

    public function addAndSaveUnitLesson(UnitLesson $unitLesson)
    {
        $this->em->persist($unitLesson);
        $this->em->flush($unitLesson);
    }

    public function addAndSaveUserLesson(UserLesson $userLesson)
    {
        $this->em->persist($userLesson);
        $this->em->flush($userLesson);
    }

    public function findMaxPosition($unitId)
    {
        return $this->em->createQueryBuilder()
            ->select('MAX(ul.position)')
            ->from(UnitLesson::class, 'ul')
            ->where('ul.unit = :unitId')
            ->setParameter('unitId', $unitId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function sort(LessonSortDTO $lessonSortDTO)
    {
        $this->em->beginTransaction();
        try {
            $lessonSortDTO->position++;
            $curPos = $this->em->createQueryBuilder()
                ->select('ul.position')
                ->from(UnitLesson::class, 'ul')
                ->where('ul.lesson = :lessonId')
                ->andWhere('ul.unit = :unitId')
                ->setParameters(['lessonId' => $lessonSortDTO->lessonId, 'unitId' => $lessonSortDTO->unitId])
                ->getQuery()
                ->getSingleScalarResult();

            $queryBuilder = $this->em->createQueryBuilder()
                ->update(UnitLesson::class, 'ul');
            if ($curPos < $lessonSortDTO->position) {
                $queryBuilder->set('ul.position', 'ul.position - 1')
                    ->where('ul.position BETWEEN :pos AND :newPos');
            } else {
                $queryBuilder->set('ul.position', 'ul.position + 1')
                    ->where('ul.position BETWEEN :newPos AND :pos');
            }
            $queryBuilder
                ->andWhere('ul.unit = :unitId')
                ->setParameters(['unitId' => $lessonSortDTO->unitId, 'pos' => $curPos, 'newPos' => $lessonSortDTO->position])
                ->getQuery()
                ->execute();

            $this->em->createQueryBuilder()
                ->update(UnitLesson::class, 'ul')
                ->set('ul.position', $lessonSortDTO->position)
                ->where('ul.unit = :unitId')
                ->andWhere('ul.lesson = :lessonId')
                ->setParameters(['unitId' => $lessonSortDTO->unitId, 'lessonId' => $lessonSortDTO->lessonId])
                ->getQuery()
                ->execute();

            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function decrementPositions(Unit $unit, $from = NULL) {
        $queryBuilder = $this->em->createQueryBuilder()
            ->update(UnitLesson::class, 'ul')
            ->set('ul.position', 'ul.position-1')
            ->where('ul.unit = :unitId')
            ->setParameter('unitId', $unit->getId());
        if($from) {
            $queryBuilder
                ->andWhere('ul.position > :from')
                ->setParameter('from', $from);
        }
        $queryBuilder->getQuery()->execute();
    }

    public function findUnitLessonByDTO(LessonBindDTO $lessonBindDTO)
    {
        $result = $this->em->createQueryBuilder()
            ->select('ul')
            ->from(UnitLesson::class, 'ul')
            ->where('ul.lesson = :lessonId')
            ->andWhere('ul.unit = :unitId')
            ->setParameters(array(
                'lessonId' => $lessonBindDTO->lessonId,
                'unitId' => $lessonBindDTO->unitId
            ))
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function search(LessonSearchDTO $searchDTO)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('l', 'le', 'e', 'lp')
            ->from(Lesson::class, 'l', 'l.id')
            ->innerJoin('l.unitLesson', 'ul')
            ->innerJoin('ul.unit','u')
            ->innerJoin('u.courseUnit','cu')
            ->innerJoin('cu.course','c')
            ->innerJoin('c.users','usr')
            ->leftJoin('l.lessonPart','lp')
            ->leftJoin('lp.lessonExercise','le')
            ->leftJoin('le.exercise','e')
            ->where('l.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if ($searchDTO->id) {
            $qb
                ->andWhere('l.id = :id')
                ->setParameter('id', $searchDTO->id);
        }

        if ($searchDTO->query) {
            $qb
                ->andWhere(
                    $qb->expr()->like(
                        $qb->expr()->lower('l.name'),
                        $qb->expr()->lower(':name')
                    )
                )
                ->setParameter('name', "%$searchDTO->query%");
        }

        if ($searchDTO->courseId) {
            $qb
                ->andWhere('c.id = :courseId')
                ->setParameter('courseId', $searchDTO->courseId);
        }

        if ($searchDTO->unitId) {
            $qb
                ->andWhere('u.id = :unitId')
                ->setParameter('unitId', $searchDTO->unitId);
        }

        if ($searchDTO->userId) {
            $qb
                ->andWhere('usr.id = :userId')
                ->setParameter('userId', $searchDTO->userId);
        }

        if ($searchDTO->isDemo !== null) {
            $qb
                ->andWhere('cu.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        return $qb->getQuery()->getResult();

    }

    public function findUserLesson(Lesson $lesson, User $user)
    {
        return $this->em->createQueryBuilder()
            ->select('ul')
            ->from(UserLesson::class, 'ul')
            ->where('ul.lesson = :lessonId')
            ->andWhere('ul.user = :userId')
            ->setParameters(['lessonId' => $lesson->getId(), 'userId' => $user->getId()])
            ->getQuery()
            ->getOneOrNullResult();
    }
}