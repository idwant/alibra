<?php

namespace AppBundle\Repository\Education;

use CoreDomain\DTO\Education\SearchUserWordCardDTO;
use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Dictionary\WordCardGroup;
use CoreDomain\Model\Education\UserWordCard;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\UserWordCardRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;

class UserWordCardRepository implements UserWordCardRepositoryInterface
{
    private $em;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('uwc')
            ->from(UserWordCard::class, 'uwc')
            ->where('uwc.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function searchWordCards(SearchUserWordCardDTO $searchDTO)
    {
        $qb = $this->searchQueryBuilder($searchDTO);

        return $qb->select('wc')
            ->getQuery()
            ->getResult();
    }

    public function searchUserWordCards(SearchUserWordCardDTO $searchDTO)
    {
        $qb = $this->searchQueryBuilder($searchDTO);

        return $qb->select('uwc')
            ->andWhere('uwc.id IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    private function searchQueryBuilder(SearchUserWordCardDTO $searchDTO)
    {
        $qb = $this->em->createQueryBuilder()
            ->from(WordCard::class, 'wc')
            ->leftJoin('wc.groups','g')
            ->leftJoin('wc.word', 'w')
            ->leftJoin('wc.lessons', 'l')
            ->leftJoin(UserWordCard::class, 'uwc',
                'WITH',
                'uwc.wordCard=wc AND ' . ($searchDTO->currentUserId ? 'uwc.user = ' . $searchDTO->currentUserId : '1=1')
            )
            ->where('wc.isDeleted=false');

        if ($searchDTO->userId) {
            $qb
                ->andWhere('uwc.user = :user')
                ->setParameter('user', $searchDTO->userId);

            if ($searchDTO->repetitionCount || $searchDTO->repetitionDate) {
                if ($searchDTO->repetitionCount) {
                    $qb
                        ->andWhere('uwc.repetitionCount < :repetitionCount')
                        ->setParameter('repetitionCount', $searchDTO->repetitionCount);
                }

                if ($searchDTO->repetitionDate) {
                    $qb
                        ->andWhere('uwc.repetitionDate <= :repetitionDate')
                        ->setParameter('repetitionDate', $searchDTO->repetitionDate);
                }

                $qb->andWhere(
                    $qb->expr()->not(
                        $qb->expr()->exists("
                            SELECT uwc2
                            FROM CoreDomain\Model\Dictionary\WordCard wc2
                                INNER JOIN wc2.groups wcg2
                                INNER JOIN CoreDomain\Model\Education\UserWordCard uwc2
                                    WITH " . ($searchDTO->currentUserId ? "uwc2.user = " . $searchDTO->currentUserId : "1=1") . "
                                        AND uwc2.wordCard = wc2.id AND (
                                        uwc2.repetitionCount < uwc.repetitionCount OR
                                        (uwc2.repetitionCount = uwc.repetitionCount
                                            AND uwc2.repetitionDate > uwc.repetitionDate
                                            AND uwc2.repetitionDate > :repetitionDate
                                        )
                                    )
                            WHERE wcg2.id = g.id AND wc2.id != wc.id AND uwc2.disabled = false AND wc2.isDeleted = false
                        ")
                    )
                );
            }
        }

        if ($searchDTO->known !== null) {
            // Если known = false, то выбираем WordCard без связи с UserWordCard
            $qb
                ->andWhere((new Expr\Orx())
                    ->add('uwc.known = :known')
                    ->add(($searchDTO->known === false) ? 'uwc.id IS NULL' : '1=0'))
                ->setParameter('known', $searchDTO->known);

            if (($searchDTO->known === false) && $searchDTO->learnDate) {
                $qb
                    ->andWhere((new Expr\Orx())
                        ->add('uwc.learnDate <= :learnDate')
                        ->add('uwc.learnDate IS NULL')
                        ->add('uwc.id IS NULL'))
                    ->setParameter('learnDate', $searchDTO->learnDate);
            }
        }

        if ($searchDTO->disabled !== null) {
            $qb
                ->andWhere((new Expr\Orx())
                    ->add('uwc.disabled = :disabled')
                    ->add(($searchDTO->disabled === false) ? 'uwc.id IS NULL' : '1=0'))
                ->setParameter('disabled', $searchDTO->disabled);
        }

        if ($searchDTO->wordId) {
            $qb
                ->andWhere('w.id = :wordId')
                ->setParameter('wordId', $searchDTO->wordId);
        }

        if ($searchDTO->wordCardId) {
            $qb
                ->andWhere('wc.id = :wordCardId')
                ->setParameter('wordCardId', $searchDTO->wordCardId);
        }

        if ($searchDTO->groupId) {
            $qb
                ->andWhere('g.id = :groupId')
                ->setParameter('groupId', $searchDTO->groupId);
        }

        if ($searchDTO->lessonId) {
            $qb
                ->andWhere('l.id = :lessonId')
                ->setParameter('lessonId', $searchDTO->lessonId);
        }

        if ($searchDTO->isDemo !== null) {
            $qb->leftJoin('l.unitLesson', 'ul')
                ->leftJoin('ul.unit', 'u')
                ->leftJoin('u.courseUnit', 'cu')
                ->andWhere('cu.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);

            if ($searchDTO->userId) {
                $qb
                    ->innerJoin('cu.course', 'c')
                    ->innerJoin('c.users', 'usr', 'WITH', 'usr.id=:user')
                    ->setParameter('user', $searchDTO->userId);
            }
        }

        return $qb;
    }

    public function statWordCards(SearchUserWordCardDTO $searchDTO)
    {
        $qb = $this->searchQueryBuilder($searchDTO);

        $qb->select(
            'COUNT(wc.id) AS total',
            'SUM(CASE WHEN uwc.known=true THEN 1 ELSE 0 END) AS learned',
            'SUM(CASE WHEN uwc.disabled=true THEN 1 ELSE 0 END) AS disabled',
            'SUM(CASE WHEN uwc.repetitionDate < :repetitionDate AND uwc.repetitionCount < :repetitionCount THEN 1 ELSE 0 END) AS repeat',
            'SUM(CASE WHEN uwc.repetitionCount >= :repetitionCount THEN 1 ELSE 0 END) AS complete'
        );

        return $qb
            ->setParameter('repetitionCount', UserWordCard::REPETITION_COUNT)
            ->setParameter('repetitionDate', (new \DateTime('now'))->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllByUser(User $user)
    {
        return $this->em->createQueryBuilder()
            ->select('uwc','wc','w','l')
            ->from(UserWordCard::class, 'uwc')
            ->leftJoin('uwc.wordCard', 'wc')
            ->leftJoin('wc.word', 'w')
            ->leftJoin('wc.lessons','l')
            ->where('uwc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function save(UserWordCard $userWordCard)
    {
        $this->em->flush($userWordCard);
    }

    public function addAndSave(UserWordCard $userWordCard)
    {
        $this->em->persist($userWordCard);
        $this->em->flush($userWordCard);
    }
}