<?php

namespace AppBundle\Repository\Education;


use CoreDomain\DTO\Dashboard\DashboardSearchDTO;
use CoreDomain\DTO\Education\ExerciseBindDTO;
use CoreDomain\DTO\Education\ExerciseSortDTO;
use CoreDomain\DTO\Education\SearchUserProgressDTO;
use CoreDomain\DTO\Student\Education\ExerciseSearchDTO;
use CoreDomain\Model\Education\Exercise;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\LessonExercise;
use CoreDomain\Model\Education\LessonPart;
use CoreDomain\Model\Education\UnitLesson;
use CoreDomain\Model\Education\UserExercise;
use CoreDomain\Model\Education\CourseUnit;
use CoreDomain\Model\Education\UserLesson;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\ExerciseRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

class ExerciseRepository implements ExerciseRepositoryInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addAndSave(Exercise $entity)
    {
        $this->em->persist($entity);
        $this->em->flush($entity);
    }

    public function findAll($query, $lessonId, $onlyCount = false, $limit = null, $offset = null)
    {
        $queryBuilder = $this->em->createQueryBuilder()
            ->from(Exercise::class, 'e')
            ->where('e.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if($query) {
            $queryBuilder
                ->andWhere("lower(e.name) LIKE lower(:query)")
                ->setParameter('query', "%".addslashes($query)."%");
        }

        if($lessonId) {
            $queryBuilder
                ->leftJoin('e.lessonExercise', 'le')
                ->andWhere('le.lesson = :lessonId')
                ->setParameter('lessonId', $lessonId);
        }

        if($onlyCount) {
            return $queryBuilder
                ->select('COUNT(e.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        else {
            if($limit) {
                $queryBuilder->setMaxResults($limit);
            }
            if($offset) {
                $queryBuilder->setFirstResult($offset);
            }
            if($lessonId) {
                $queryBuilder->orderBy('le.position');
            }
            else{
                $queryBuilder->orderBy('e.name');
            }

            $data =  $queryBuilder
                ->select('e.name, e.id')
                ->getQuery()
                ->getResult();
            return $data;
        }
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('e', 't', 'g', 'd')
            ->from(Exercise::class, 'e')
            ->innerJoin('e.template', 't')
            ->leftJoin('e.grammar', 'g', 'WITH', 'e.grammar = g.id AND g.isDeleted = :isDeleted')
            ->leftJoin('e.dictionary', 'd', 'WITH', 'e.dictionary = d.id AND d.isDeleted = :isDeleted')
            ->where('e.id = :id')
            ->setParameters(['id' => $id, 'isDeleted' => false])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('e')
            ->from(Exercise::class, 'e')
            ->where('e.name = :name')
            ->setParameter(':name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function deleteById($id)
    {
        $qb = $this->em->createQueryBuilder();
        return $qb->update(Exercise::class, 'e')
            ->set('e.isDeleted', ':isDeleted')
            ->set('e.name', $qb->expr()->concat('e.name', ':deleteDate'))
            ->where('e.id = :id')
            ->setParameters([
                'id' => $id,
                'isDeleted' => true,
                'deleteDate' => (new \DateTime())->format('_YmdHis')
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSaveLessonExercise(LessonExercise $lessonExercise)
    {
        $this->em->persist($lessonExercise);
        $this->em->flush($lessonExercise);
    }

    public function findMaxPosition($lessonId)
    {
        return $this->em->createQueryBuilder()
            ->select('MAX(le.position)')
            ->from(LessonExercise::class, 'le')
            ->where('le.lesson = :lessonId')
            ->setParameter('lessonId', $lessonId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function sort(ExerciseSortDTO $exerciseSortDTO)
    {
        $this->em->beginTransaction();
        try {
            $exerciseSortDTO->position++;
            $curPos = $this->em->createQueryBuilder()
                ->select('le.position')
                ->from(LessonExercise::class, 'le')
                ->where('le.lesson = :lessonId')
                ->andWhere('le.exercise = :exerciseId')
                ->setParameters(['lessonId' => $exerciseSortDTO->lessonId, 'exerciseId' => $exerciseSortDTO->exerciseId])
                ->getQuery()
                ->getSingleScalarResult();

            $queryBuilder = $this->em->createQueryBuilder()
                ->update(LessonExercise::class, 'le');
            if ($curPos < $exerciseSortDTO->position) {
                $queryBuilder->set('le.position', 'le.position - 1')
                    ->where('le.position BETWEEN :pos AND :newPos');
            } else {
                $queryBuilder->set('le.position', 'le.position + 1')
                    ->where('le.position BETWEEN :newPos AND :pos');
            }
            $queryBuilder
                ->andWhere('le.lesson = :lessonId')
                ->setParameters(['lessonId' => $exerciseSortDTO->lessonId, 'pos' => $curPos, 'newPos' => $exerciseSortDTO->position])
                ->getQuery()
                ->execute();

            $this->em->createQueryBuilder()
                ->update(LessonExercise::class, 'le')
                ->set('le.position', $exerciseSortDTO->position)
                ->where('le.lesson = :lessonId')
                ->andWhere('le.exercise = :exerciseId')
                ->setParameters(['lessonId' => $exerciseSortDTO->lessonId, 'exerciseId' => $exerciseSortDTO->exerciseId])
                ->getQuery()
                ->execute();

            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function decrementPositions(Lesson $lesson, $from = NULL) {
        $queryBuilder = $this->em->createQueryBuilder()
            ->update(LessonExercise::class, 'le')
            ->set('le.position', 'le.position-1')
            ->where('le.lesson = :lessonId')
            ->setParameter('lessonId', $lesson->getId());
        if($from) {
            $queryBuilder
                ->andWhere('le.position > :from')
                ->setParameter('from', $from);
        }
        $queryBuilder->getQuery()->execute();
    }

    public function findLessonExerciseByDTO(ExerciseBindDTO $exerciseBindDTO)
    {
        return $this->em->createQueryBuilder()
            ->select('le')
            ->from(LessonExercise::class, 'le')
            ->where('le.lessonPart = :lessonPartId')
            ->andWhere('le.exercise = :exerciseId')
            ->setParameters(array(
                'lessonPartId' => $exerciseBindDTO->lessonPartId,
                'exerciseId' => $exerciseBindDTO->exerciseId
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findLessonExercise(Lesson $lesson, Exercise $exercise)
    {
        return $this->em->createQueryBuilder()
            ->select('le')
            ->from(LessonExercise::class, 'le')
            ->where('le.lesson = :lessonId')
            ->andWhere('le.exercise = :exerciseId')
            ->setParameters(array(
                'lessonId' => $lesson->getId(),
                'exerciseId' => $exercise->getId()
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findLessonExerciseByLessonPart(LessonPart $lessonPart)
    {
        return $this->em->createQueryBuilder()
            ->select('le')
            ->from(LessonExercise::class, 'le')
            ->where('le.lessonPart = :lessonPartId')
            ->andWhere('le.lesson = :lessonId')
            ->setParameters(array(
                'lessonPartId' => $lessonPart->getId(),
                'lessonId' => $lessonPart->getLesson()->getId()
            ))
            ->getQuery()
            ->getResult();
    }

    public function search(ExerciseSearchDTO $searchDTO)
    {
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('l, ul, u, cu, c, usr, le, e')
            ->from(Lesson::class, 'l')
            ->leftJoin('l.unitLesson', 'ul')
            ->leftJoin('ul.unit', 'u')
            ->leftJoin('u.courseUnit', 'cu')
            ->leftJoin('cu.course', 'c')
            ->leftJoin('c.users', 'usr')
            ->leftJoin('l.lessonExercise', 'le')
            ->leftJoin('le.exercise', 'e')
            ->orderBy('le.position')
            ->where('e.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if ($searchDTO->id) {
            $qb
                ->andWhere('e.id = :id')
                ->setParameter('id', $searchDTO->id);
        }

        if ($searchDTO->query) {
            $qb
                ->andWhere(
                    $qb->expr()->like(
                        $qb->expr()->lower('e.name'),
                        $qb->expr()->lower(':name')
                    )
                )
                ->setParameter('name', "%$searchDTO->query%");
        }

        if ($searchDTO->userId) {
            $qb
                ->andWhere('usr.id = :userId')
                ->setParameter('userId', $searchDTO->userId);
        }

        if ($searchDTO->courseId) {
            $qb
                ->andWhere('c.id = :courseId')
                ->setParameter('courseId', $searchDTO->courseId);
        }

        if ($searchDTO->unitId) {
            $qb
                ->andWhere('u.id = :unitId')
                ->setParameter('unitId', $searchDTO->unitId);
        }

        if ($searchDTO->lessonId) {
            $qb
                ->andWhere('l.id = :lessonId')
                ->setParameter('lessonId', $searchDTO->lessonId);
        }

        if ($searchDTO->lessonPartId) {
            $qb
                ->innerJoin('l.lessonPart', 'lp')
                ->andWhere('lp.id = :lessonPartId and le.lessonPart = :lessonPartId')
                ->setParameter('lessonPartId', $searchDTO->lessonPartId);
        }

        if ($searchDTO->exerciseType) {
            $qb
                ->andWhere('e.type = :exerciseType')
                ->setParameter('exerciseType', $searchDTO->exerciseType);
        }

        if ($searchDTO->isDemo !== null) {
            $qb
                ->andWhere('cu.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        return $qb->getQuery()->getResult();
    }

    public function findCoursesStats(DashboardSearchDTO $searchDTO)
    {
        return $this
            ->getStatsQueryBuilder($searchDTO)
            ->addSelect('c.id')
            ->addGroupBy('c.id')
            ->orderBy('c.id')
            ->getQuery()
            ->getResult();
    }

    public function findUnitsStats(DashboardSearchDTO $searchDTO)
    {
        return $this
            ->getStatsQueryBuilder($searchDTO)
            ->addSelect('u.id')
            ->andWhere('c.id = :courseId')
            ->setParameter('courseId', $searchDTO->courseId)
            ->addGroupBy('c.id, u.id, cu.position')
            ->orderBy('c.id, cu.position')
            ->getQuery()
            ->getResult();
    }

    public function findLessonsStats(DashboardSearchDTO $searchDTO)
    {
        return $this
            ->getStatsQueryBuilder($searchDTO)
            ->addSelect('l.id')
            ->andWhere('c.id = :courseId AND u.id = :unitId')
            ->setParameter('courseId', $searchDTO->courseId)
            ->setParameter('unitId', $searchDTO->unitId)
            ->addGroupBy('c.id, u.id, l.id, ul.position')
            ->orderBy('c.id, u.id, ul.position')
            ->getQuery()
            ->getResult();
    }

    public function findLessonPartsStats(DashboardSearchDTO $searchDTO)
    {
        $qb = $this
            ->getStatsQueryBuilder($searchDTO)
            ->addSelect('lp.id')
            ->andWhere('c.id = :courseId AND u.id = :unitId')
            ->setParameter('courseId', $searchDTO->courseId)
            ->setParameter('unitId', $searchDTO->unitId)
            ->addGroupBy('c.id, u.id, l.id, lp.id');

        if ($searchDTO->lessonId) {
            $qb
                ->andWhere('l.id = :lessonId')
                ->setParameter('lessonId', $searchDTO->lessonId);
        }

        if ($searchDTO->exerciseType) {
            $qb
                ->andWhere('e.type = :exerciseType')
                ->setParameter('exerciseType', $searchDTO->exerciseType);
        }

        return $qb
            ->orderBy('c.id, u.id, l.id, lp.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * Базовый QueryBuilder для построения запроса на получение статистики
     * @param DashboardSearchDTO $searchDTO
     * @return QueryBuilder
     */
    private function getStatsQueryBuilder(DashboardSearchDTO $searchDTO)
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select(   'SUM(CASE WHEN (usr_e.status IN (:statusNotStarted) OR usr_e.status IS NULL) THEN 1 ELSE 0 END) AS wait')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess)                          THEN 1 ELSE 0 END) AS success')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusFail)                             THEN 1 ELSE 0 END) AS fail')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess, :statusFail)             THEN 1 ELSE 0 END) AS complete')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusInProgress)                       THEN 1 ELSE 0 END) AS in_progress')

            ->addSelect('SUM(CASE WHEN (usr_e.status IN (:statusNotStarted) OR usr_e.status IS NULL) AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_wait')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess)                          AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_success')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusFail)                             AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_fail')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess, :statusFail)             AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_complete')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusInProgress)                       AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_in_progress')

            ->addSelect('SUM(CASE WHEN (usr_e.status IN (:statusNotStarted) OR usr_e.status IS NULL) AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_wait')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess)                          AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_success')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusFail)                             AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_fail')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess, :statusFail)             AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_complete')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusInProgress)                       AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_in_progress')

            ->addSelect('SUM(CASE WHEN (usr_e.status IN (:statusNotStarted) OR usr_e.status IS NULL) AND e.type=:testType THEN 1 ELSE 0 END) AS test_wait')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess)                          AND e.type=:testType THEN 1 ELSE 0 END) AS test_success')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusFail)                             AND e.type=:testType THEN 1 ELSE 0 END) AS test_fail')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusSuccess, :statusFail)             AND e.type=:testType THEN 1 ELSE 0 END) AS test_complete')
            ->addSelect('SUM(CASE WHEN  usr_e.status IN (:statusInProgress)                       AND e.type=:testType THEN 1 ELSE 0 END) AS test_in_progress')

            ->from(User::class, 'usr')
            ->innerJoin('usr.courses', 'c', 'WITH', 'c.isDeleted = :isDeleted')
            ->leftJoin('c.courseUnit', 'cu')
            ->innerJoin('cu.unit', 'u', 'WITH', 'u.isDeleted = :isDeleted')
            ->leftJoin('u.unitLesson', 'ul')
            ->innerJoin('ul.lesson', 'l', 'WITH', 'l.isDeleted=:isDeleted')
            ->leftJoin('l.lessonExercise', 'le')
            ->leftJoin('le.lessonPart', 'lp', 'WITH', 'lp.isDeleted = :isDeleted')
            ->innerJoin('le.exercise', 'e', 'WITH', 'e.isDeleted = :isDeleted')
            
            ->leftJoin(UserLesson::class, 'usr_l', 'WITH', 'usr_l.lesson=l AND usr_l.user=usr')
            ->leftJoin(UserExercise::class, 'usr_e', 'WITH', 'usr_e.userLesson=usr_l AND usr_e.exercise=e AND usr_e.user=usr')

            ->where('usr.id = :userId')
            ->setParameters([
                'isDeleted' => false,
                'userId' => $searchDTO->userId,

                'statusNotStarted' => UserExercise::STATUS_WAIT,
                'statusSuccess' => UserExercise::STATUS_SUCCESS,
                'statusFail' => UserExercise::STATUS_FAIL,
                'statusInProgress' => UserExercise::STATUS_IN_PROGRESS,

                'groupTaskType' => Exercise::TYPE_GROUP_TASK,
                'homeworkType' => Exercise::TYPE_HOMEWORK,
                'testType' => Exercise::TYPE_TEST
            ]);

            if ($searchDTO->isDemo !== null) {
                $qb
                    ->andWhere('cu.isDemo = :isDemo')
                    ->setParameter('isDemo', $searchDTO->isDemo);
            }

            return $qb;
    }

    public function getUserProgresStat(SearchUserProgressDTO $searchDTO, $onlyCount = false)
    {
        $params = [
            'userId' => $searchDTO->getUserId(),
            'groupTaskType' => Exercise::TYPE_GROUP_TASK,
            'homeworkType' => Exercise::TYPE_HOMEWORK
        ];

        $courseWhere = '1=1';
        if ($searchDTO->courseId) {
            $courseWhere = 'c.id = :courseId';
            $params['courseId'] = $searchDTO->courseId;
        }

        if ($searchDTO->isDemo !== null) {
            $params['isDemo'] = $searchDTO->isDemo;
        }

        $groupTaskParams = [];
        $groupTaskSql = sprintf('
            %s
            WHERE
                usr.id = :userId
                AND %s
                AND e.type = :groupTaskType
                AND (%s)
            GROUP BY c.id, u.id, l.id, lp.id'
            , $this->getStatsQuery(Exercise::TYPE_GROUP_TASK, true, $groupTaskParams)
            , $courseWhere
            , ($searchDTO->isDemo !== null ? 'cu.is_demo = :isDemo' : '1=1')
        );

        $homeWorkParams = [];
        $homeWorkSql = sprintf('
            %s
            WHERE
                usr.id = :userId
                AND %s
                AND e.type = :homeworkType
                AND (%s)
            GROUP BY c.id, u.id, l.id'
            , $this->getStatsQuery(Exercise::TYPE_HOMEWORK, false, $homeWorkParams)
            , $courseWhere
            , ($searchDTO->isDemo !== null ? 'cu.is_demo = :isDemo' : '1=1')
        );

        if ($onlyCount) {
            $sql = sprintf('SELECT COUNT(*) FROM (%s UNION ALL %s) t', $groupTaskSql, $homeWorkSql);
        } else {
            $sql = sprintf('
                SELECT
                    DENSE_RANK() over (ORDER BY t.course_id) AS course_number,
                    DENSE_RANK() over (PARTITION BY t.course_id ORDER BY t.unit_id) AS unit_number,
                    DENSE_RANK() over (PARTITION BY t.unit_id ORDER BY t.lesson_id) AS lesson_number,
                    DENSE_RANK() over (PARTITION BY t.lesson_id ORDER BY t.lesson_part_id) AS lesson_part_number,
                    t.*
                FROM (%s UNION ALL %s) t
                ORDER BY t.course_id, t.unit_id, t.lesson_id', $groupTaskSql, $homeWorkSql);

            if ($searchDTO->limit) {
                $sql .= ' LIMIT ' . $searchDTO->limit;
            }

            if ($searchDTO->offset) {
                $sql .= ' OFFSET ' . $searchDTO->offset;
            }
        }

        $statement = $this->em->getConnection()->prepare($sql);

        $params = array_merge($params, $groupTaskParams, $homeWorkParams);
        foreach ($params as $name => $value) {
            $statement->bindValue($name, $value);
        }

        $statement->execute();
        return $onlyCount ? $statement->fetchColumn() : $statement->fetchAll();
    }

    private function getStatsQuery($exerciseType, $groupByLessonPart, &$params)
    {
        $sql = 'SELECT
                    c.id as course_id,
                    u.id as unit_id,
                    l.id as lesson_id,
                    ' . ($groupByLessonPart ? 'lp.id' : '0' ) . ' AS lesson_part_id,
                    
                    \'' . $exerciseType . '\' as exercise_type,
                    SUM(CASE WHEN (ue.status IN (:statusNotStarted) OR ue.status IS NULL) THEN 1 ELSE 0 END) AS wait,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess)                          THEN 1 ELSE 0 END) AS success,
                    SUM(CASE WHEN  ue.status IN (:statusFail)                             THEN 1 ELSE 0 END) AS fail,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess, :statusFail)             THEN 1 ELSE 0 END) AS complete,
                    SUM(CASE WHEN  ue.status IN (:statusInProgress)                       THEN 1 ELSE 0 END) AS in_progress,
                    
                    SUM(CASE WHEN (ue.status IN (:statusNotStarted) OR ue.status IS NULL) AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_wait,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess)                          AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_success,
                    SUM(CASE WHEN  ue.status IN (:statusFail)                             AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_fail,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess, :statusFail)             AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_complete,
                    SUM(CASE WHEN  ue.status IN (:statusInProgress)                       AND e.type=:groupTaskType THEN 1 ELSE 0 END) AS group_task_in_progress,
                    
                    SUM(CASE WHEN (ue.status IN (:statusNotStarted) OR ue.status IS NULL) AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_wait,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess)                          AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_success,
                    SUM(CASE WHEN  ue.status IN (:statusFail)                             AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_fail,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess, :statusFail)             AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_complete,
                    SUM(CASE WHEN  ue.status IN (:statusInProgress)                       AND e.type=:homeworkType THEN 1 ELSE 0 END) AS homework_in_progress,
                    
                    SUM(CASE WHEN (ue.status IN (:statusNotStarted) OR ue.status IS NULL) AND e.type=:testType THEN 1 ELSE 0 END) AS test_wait,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess)                          AND e.type=:testType THEN 1 ELSE 0 END) AS test_success,
                    SUM(CASE WHEN  ue.status IN (:statusFail)                             AND e.type=:testType THEN 1 ELSE 0 END) AS test_fail,
                    SUM(CASE WHEN  ue.status IN (:statusSuccess, :statusFail)             AND e.type=:testType THEN 1 ELSE 0 END) AS test_complete,
                    SUM(CASE WHEN  ue.status IN (:statusInProgress)                       AND e.type=:testType THEN 1 ELSE 0 END) AS test_in_progress
                FROM users usr
                    INNER JOIN user_course uc ON uc.user_id = usr.id
                    INNER JOIN course c ON c.id = uc.course_id AND NOT c.is_deleted
                    LEFT JOIN course_unit cu ON cu.course_id = c.id
                    INNER JOIN unit u ON u.id = cu.unit_id AND NOT u.is_deleted
                    LEFT JOIN unit_lesson ul ON ul.unit_id = u.id
                    INNER JOIN lesson l ON l.id = ul.lesson_id AND NOT l.is_deleted
                    LEFT JOIN lesson_exercise le ON le.lesson_id = l.id
                    LEFT JOIN lesson_part lp ON lp.id = le.lesson_part_id
                    INNER JOIN exercise e ON e.id = le.exercise_id AND NOT e.is_deleted
                    
                    LEFT JOIN user_lesson usr_l on usr_l.lesson_id = l.id and usr_l.user_id=usr.id
                    LEFT JOIN user_exercise ue ON ue.user_lesson_id=usr_l.id AND ue.exercise_id=e.id AND ue.user_id=usr.id';

                $params = [
                    'statusNotStarted' => UserExercise::STATUS_WAIT,
                    'statusSuccess' => UserExercise::STATUS_SUCCESS,
                    'statusFail' => UserExercise::STATUS_FAIL,
                    'statusInProgress' => UserExercise::STATUS_IN_PROGRESS,

                    'groupTaskType' => Exercise::TYPE_GROUP_TASK,
                    'homeworkType' => Exercise::TYPE_HOMEWORK,
                    'testType' => Exercise::TYPE_TEST
                ];

        return $sql;
    }

    public function findUserExercise(Exercise $exercise, User $user, UserLesson $userLesson)
    {
        return $this->em->createQueryBuilder()
            ->select('ue')
            ->from(UserExercise::class, 'ue')
            ->where('ue.exercise = :exerciseId')
            ->andWhere('ue.user = :userId')
            ->andWhere('ue.userLesson = :userLessonId')
            ->setParameters([
                'exerciseId' => $exercise->getId(),
                'userId' => $user->getId(),
                'userLessonId' => $userLesson->getId(),
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSaveUserExercise(UserExercise $userExercise)
    {
        $this->em->persist($userExercise);
        $this->em->flush($userExercise);
    }
}