<?php

namespace AppBundle\Repository\Education;

use CoreDomain\Model\Education\ExerciseTemplate;
use CoreDomain\Repository\Education\ExerciseTemplateRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class ExerciseTemplateRepository implements ExerciseTemplateRepositoryInterface
{
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findAll() {
        return $this->em->createQueryBuilder()
            ->select('et')
            ->from(ExerciseTemplate::class, 'et')
            ->orderBy('et.id')
            ->getQuery()
            ->getResult();
    }

    public function addAndSave(ExerciseTemplate $entity)
    {
        $this->em->persist($entity);
        $this->em->flush($entity);
    }

    public function findOneByType($type)
    {
        return $this->em->createQueryBuilder()
            ->select('et')
            ->from(ExerciseTemplate::class, 'et')
            ->where('et.type = :type')
            ->setParameter(':type', $type)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneById($id)
    {
        return $this->em->createQueryBuilder()
            ->select('et', 'ett')
            ->from(ExerciseTemplate::class, 'et')
            ->where('et.id = :id')
            ->setParameter(':id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
