<?php

namespace AppBundle\Repository\User;

use CoreDomain\DTO\User\SearchDTO;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Exercise;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\UserLesson;
use CoreDomain\Model\User\UserEmployer;
use CoreDomain\Model\User\UserTeacher;
use Doctrine\ORM\EntityManagerInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use CoreDomain\Model\User\User;

class UserRepository implements UserRepositoryInterface
{
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $email
     * @return User
     */
    public function findOneByEmail($email)
    {
        return $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function findOneByToken($token, $expirePeriod = 'P7D')
    {
        $validCreatedAt = new \DateTime();
        $validCreatedAt->sub(new \DateInterval($expirePeriod));

        return $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->innerJoin('u.sessions', 's')
            ->where('s.token = :token')
            ->andWhere('s.createdAt > :date')
            ->setParameter('token', $token)
            ->setParameter('date', $validCreatedAt)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByAuthToken($token)
    {
        return $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.authenticationToken = :token')
            ->setParameters(['token' => $token])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function search(SearchDTO $searchDTO, $onlyCount = false)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
            ->from(User::class, 'u');

        // фильтруем по ФИО
        if ($searchDTO->fio) {
            $qb
                ->orWhere($qb->expr()->like(
                    $qb->expr()->lower('u.lastName'),
                    $qb->expr()->lower(':searchStr')
                ))
                ->orWhere($qb->expr()->like(
                    $qb->expr()->lower('u.firstName'),
                    $qb->expr()->lower(':searchStr')
                ))
                ->orWhere($qb->expr()->like(
                    $qb->expr()->lower('u.middleName'),
                    $qb->expr()->lower(':searchStr')
                ))
                ->setParameter('searchStr', '%' . trim($searchDTO->fio) . '%');
        }

        // фильтруем по роли
        if ($searchDTO->role) {
            $qb
                ->andWhere($qb->expr()->like('u.roles', ':role'))
                ->setParameter('role', '%' . $searchDTO->role . '%');
        }

        if ($onlyCount) {
            return $qb
                ->select('COUNT(u.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } else {
            $qb
                ->addOrderBy('u.lastName', 'ASC')
                ->addOrderBy('u.firstName', 'ASC')
                ->addOrderBy('u.middleName', 'ASC')
                ->addOrderBy('u.id');

            return $qb
                ->setMaxResults($searchDTO->limit)
                ->setFirstResult($searchDTO->offset)
                ->getQuery()
                ->getResult();
        }
    }

    public function getUserCourseByLesson(Lesson $lesson, User $user)
    {
        return $this->em->createQueryBuilder()
            ->select('c')
            ->from(Course::class, 'c')
            ->leftJoin('c.users', 'usr')
            ->leftJoin('c.courseUnit', 'cu')
            ->leftJoin('cu.unit', 'u')
            ->leftJoin('u.unitLesson', 'ul')
            ->leftJoin('ul.lesson', 'l')
            ->where('usr.id = :userId')
            ->andWhere('l.id = :lessonId')
            ->setParameters(['userId' => $user->getId(), 'lessonId' => $lesson->getId()])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     */
    public function add(User $user)
    {
        $this->em->persist($user);
    }

    public function addAndSave(User $user)
    {
        $this->em->persist($user);
        $this->em->flush($user);
    }

    public function addTeacherToUser(User $user, User $teacher)
    {
        $user->getTeachers()->add($teacher);
        $teacher->getStudents()->add($user);
        $this->em->flush();
    }

    public function addEmployerToUser(User $user, User $employer)
    {
        $user->getEmployers()->add($employer);
        $employer->getEmployees()->add($user);
        $this->em->flush();
    }

    public function addStudentToTeacher(User $teacher, User $student)
    {
        $teacher->getStudents()->add($student);
        $student->getTeachers()->add($teacher);
        $this->em->flush();
    }

    public function addEmployeeToEmployer(User $employer, User $employee)
    {
        $employer->getEmployees()->add($employee);
        $employee->getEmployers()->add($employer);
        $this->em->flush();
    }

    public function addCourse(User $user, Course $course)
    {
        $user->getCourses()->add($course);
        $this->em->flush();
    }

    public function deleteTeacherByUser(User $user, User $teacher)
    {
        $user->getTeachers()->removeElement($teacher);
        $teacher->getStudents()->removeElement($user);
        $this->em->flush();
    }

    public function deleteEmployerByUser(User $user, User $employer)
    {
        $user->getEmployers()->removeElement($employer);
        $employer->getEmployees()->removeElement($user);
        $this->em->flush();
    }

    public function deleteStudentByTeacher(User $teacher, User $student)
    {
        $teacher->getStudents()->removeElement($student);
        $student->getTeachers()->removeElement($teacher);
        $this->em->flush();
    }

    public function deleteEmployeeByEmployer(User $employer, User $employee)
    {
        $employer->getEmployees()->removeElement($employee);
        $employee->getEmployers()->removeElement($employer);
        $this->em->flush();
    }

    public function deleteCourse(User $user, Course $course)
    {
        $user->getCourses()->removeElement($course);
        $this->em->flush();
    }
}
