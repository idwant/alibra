<?php

namespace AppBundle\Repository\Grammar;


use CoreDomain\DTO\Grammar\GrammarThemeSearchDTO;
use CoreDomain\Model\Grammar\GrammarTheme;
use CoreDomain\Model\Grammar\GrammarSection;
use CoreDomain\Repository\Grammar\GrammarThemeRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class GrammarThemeRepository implements GrammarThemeRepositoryInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function search(GrammarThemeSearchDTO $searchDTO, $onlyCount = false){
        $qb = $this->em->createQueryBuilder()
            ->from(GrammarTheme::class, 'gt');

        if ($searchDTO->query) {
            $qb->andWhere(
                $qb->expr()->like(
                    $qb->expr()->lower('gt.name'),
                    $qb->expr()->lower(':name')
                )
            )
                ->setParameter('name', '%'.trim($searchDTO->query).'%');
        }
        if ($searchDTO->section) {
            $qb->andWhere('gt.section = :section')
                ->setParameter('section', $searchDTO->section);
        }

        if ($searchDTO->isDemo !== null) {
            $qb
                ->leftJoin('gt.grammars', 'g')
                ->andWhere('g.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        if ($onlyCount) {
            return $qb->select('COUNT(gt.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }

        return $qb
            ->select('gt')
            ->addOrderBy('gt.name', 'ASC')
            ->setFirstResult($searchDTO->offset)
            ->setMaxResults($searchDTO->limit)
            ->getQuery()
            ->getResult();
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('gt')
            ->from(GrammarTheme::class, 'gt')
            ->where('gt.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function findAllBySection(GrammarSection $grammarSection)
    {
        $result = $this->em->createQueryBuilder()
            ->select('gt')
            ->from(GrammarTheme::class, 'gt')
            ->innerJoin('gt.section', 'gs')
            ->where('gs.id = :grammarSection')
            ->setParameter('grammarSection', $grammarSection)
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function addAndSave(GrammarTheme $grammarTheme)
    {
        $this->em->persist($grammarTheme);
        $this->em->flush();
    }

    public function deleteById($id)
    {
        $this->em->createQueryBuilder()
            ->delete(GrammarTheme::class, 'gt')
            ->where('gt.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}