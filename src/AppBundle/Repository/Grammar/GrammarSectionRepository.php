<?php

namespace AppBundle\Repository\Grammar;


use CoreDomain\DTO\Grammar\GrammarSectionSearchDTO;
use CoreDomain\Model\Grammar\GrammarSection;
use CoreDomain\Repository\Grammar\GrammarSectionRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class GrammarSectionRepository implements GrammarSectionRepositoryInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function search(GrammarSectionSearchDTO $searchDTO, $onlyCount = false)
    {
        $qb = $this->em->createQueryBuilder()
            ->from(GrammarSection::class, 'gs');
        if ($searchDTO->query) {
            $qb->where(
                $qb->expr()->like(
                    $qb->expr()->lower('gs.name'),
                    $qb->expr()->lower(':name')
                )
            )
                ->setParameter('name', '%'.trim($searchDTO->query).'%');
        }

        if ($searchDTO->isDemo !== null) {
            $qb
                ->leftJoin('gs.themes', 'gt')
                ->leftJoin('gt.grammars', 'g')
                ->andWhere('g.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        if ($onlyCount) {
            return $qb->select('COUNT(gs.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        else {
            $qb
                ->addOrderBy('gs.name', 'ASC')
                ->setFirstResult($searchDTO->offset)
                ->setMaxResults($searchDTO->limit);

            return $qb
                ->select('gs')
                ->getQuery()
                ->getResult();
        }
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('gs')
            ->from(GrammarSection::class, 'gs')
            ->where('gs.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
        return $result;
    }

    public function findOneByName($name)
    {
        return $this->em->createQueryBuilder()
            ->select('gs')
            ->from(GrammarSection::class, 'gs')
            ->where('gs.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function addAndSave(GrammarSection $grammarSection)
    {
        $this->em->persist($grammarSection);
        $this->em->flush();
    }

    public function deleteById($id)
    {
        $this->em->createQueryBuilder()
            ->delete(GrammarSection::class, 'gs')
            ->where('gs.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }
}