<?php

namespace AppBundle\Repository\Grammar;


use CoreDomain\DTO\Grammar\GrammarSearchDTO;
use CoreDomain\Model\Grammar\Grammar;
use CoreDomain\Model\Grammar\GrammarTheme;
use CoreDomain\Repository\Grammar\GrammarRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class GrammarRepository implements GrammarRepositoryInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findOneById($id)
    {
        $result = $this->em->createQueryBuilder()
            ->select('g')
            ->from(Grammar::class, 'g')
            ->where('g.id = :id')
            ->andWhere('g.isDeleted = :isDeleted')
            ->setParameters(array(
                'id' => $id,
                'isDeleted' => false
            ))
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }

    public function findAllByTheme(GrammarTheme $grammarTheme)
    {
        $result = $this->em->createQueryBuilder()
            ->select('g')
            ->from(Grammar::class, 'g')
            ->innerJoin('g.theme', 'gt')
            ->where('gt.id = :grammarTheme')
            ->andWhere('g.isDeleted = :isDeleted')
            ->setParameters(array(
                'grammarTheme' => $grammarTheme,
                'isDeleted' => false
            ))
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function addAndSave(Grammar $grammar)
    {
        $this->em->persist($grammar);
        $this->em->flush();
    }

    public function deleteById($id)
    {
        $this->em->createQueryBuilder()
            ->update(Grammar::class, 'g')
            ->set('g.isDeleted', ':isDeleted')
            ->where('g.id = :id')
            ->setParameters(array(
                'isDeleted' => true,
                'id' => $id
            ))
            ->getQuery()
            ->execute();
    }

    public function searchByTitle($title)
    {
        $qb = $this->em->createQueryBuilder();
        $result =
            $qb->select('g')
            ->from(Grammar::class, 'g')
            ->where($qb->expr()->like(
                $qb->expr()->lower('g.title'),
                $qb->expr()->lower(':title')))
            ->andWhere('g.isDeleted = :isDeleted')
            ->addOrderBy('g.title', 'ASC')
            ->setParameters(array(
                'title' => '%'.trim($title).'%',
                'isDeleted' => false
            ))
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function search(GrammarSearchDTO $searchDTO, $onlyCount = false, $limit = null, $offset = null) {
        $qb = $this->em->createQueryBuilder()
            ->from(Grammar::class, 'g')
            ->where('g.isDeleted = :isDeleted')
            ->setParameter('isDeleted', false);

        if ($searchDTO->id) {
            $qb->andWhere('g.id = :id')
                ->setParameter('id', $searchDTO->id);
        }

        if ($searchDTO->query) {
            $qb->andWhere('LOWER(g.title) LIKE LOWER(:query)')
                ->setParameter('query', '%'.trim($searchDTO->query).'%');
        }

        if ($searchDTO->title) {
            $qb->andWhere('LOWER(g.title) LIKE LOWER(:title)')
                ->setParameter('title', '%'.trim($searchDTO->title).'%');
        }

        if ($searchDTO->content) {
            $qb->andWhere('LOWER(g.content) LIKE LOWER(:content)')
                ->setParameter('content', '%'.trim($searchDTO->content).'%');
        }

        if ($searchDTO->theme) {
            $qb->andWhere('g.theme = :theme')
                ->setParameter('theme', $searchDTO->theme);
        }

        if ($searchDTO->isDemo !== null) {
            $qb->andWhere('g.isDemo = :isDemo')
                ->setParameter('isDemo', $searchDTO->isDemo);
        }

        if($onlyCount) {
            return $qb
                ->select('COUNT(g.id)')
                ->getQuery()
                ->getSingleScalarResult();
        }
        else {
            $qb
                ->addOrderBy('g.title', 'ASC')
                ->setFirstResult($offset)
                ->setMaxResults($limit);

            return $qb
                ->select('g')
                ->getQuery()
                ->getResult();
        }
    }
}