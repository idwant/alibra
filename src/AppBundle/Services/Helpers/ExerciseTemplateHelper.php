<?php

namespace AppBundle\Services\Helpers;

use CoreDomain\DTO\Education\ExerciseTemplate;
use CoreDomain\Exception\LogicException;
use Symfony\Component\Config\Definition\Exception\Exception;

class ExerciseTemplateHelper
{
    const TEMPLATE_DIALOG = 'dialog-exercise';
    const TEMPLATE_COMPARE = 'compare';
    const TEMPLATE_FREE_FORM = 'free-form';
    const TEMPLATE_FIND_COUPLE = 'find-couple';
    const TEMPLATE_LISTEN_AND_WRITE = 'listen-write';
    const TEMPLATE_LISTEN_AND_SAY = 'listen-say';
    const TEMPLATE_DRAG_AND_DROP = 'drag-and-drop';
    const TEMPLATE_READ_AND_WRITE = 'read-write';
    const TEMPLATE_PICTURE = 'picture';
    const TEMPLATE_SELECT = 'select';
    const TEMPLATE_FIVE_CARDS = 'five-cards';
    const TEMPLATE_PICK_OUT = 'pick-out';
    const TEMPLATE_SNOWBALL = 'snowball';
    const TEMPLATE_LISTEN_READ_AND_CLICK = 'listen-read-click';
    const TEMPLATE_CROSSWORD = 'crossword';
    const TEMPLATE_DRAG_AND_DROP_AUDIO = 'drag-and-drop-audio';

    public static function getTemplateByName($name)
    {
        switch ($name) {
            case self::TEMPLATE_DIALOG:
                return new ExerciseTemplate\DialogTemplateDTO();
                break;
            case self::TEMPLATE_COMPARE:
                return new ExerciseTemplate\CompareTemplateDTO();
                break;
            case self::TEMPLATE_FREE_FORM:
                return new ExerciseTemplate\FreeFormTemplateDTO();
                break;
            case self::TEMPLATE_FIND_COUPLE:
                return new ExerciseTemplate\FindCoupleTemplateDTO();
                break;
            case self::TEMPLATE_LISTEN_AND_WRITE:
                return new ExerciseTemplate\ListenWriteTemplateDTO();
                break;
            case self::TEMPLATE_LISTEN_AND_SAY:
                return new ExerciseTemplate\ListenSayTemplateDTO();
                break;
            case self::TEMPLATE_DRAG_AND_DROP:
                return new ExerciseTemplate\DragAndDropTemplateDTO();
                break;
            case self::TEMPLATE_READ_AND_WRITE:
                return new ExerciseTemplate\ReadWriteTemplateDTO();
                break;
            case self::TEMPLATE_PICTURE:
                return new ExerciseTemplate\PictureTemplateDTO();
                break;
            case self::TEMPLATE_SELECT:
                return new ExerciseTemplate\SelectTemplateDTO();
                break;
            case self::TEMPLATE_FIVE_CARDS:
                return new ExerciseTemplate\FiveCardsTemplateDTO();
                break;
            case self::TEMPLATE_PICK_OUT:
                return new ExerciseTemplate\PickOutTemplateDTO();
                break;
            case self::TEMPLATE_SNOWBALL:
                return new ExerciseTemplate\SnowBallTemplateDTO();
                break;
            case self::TEMPLATE_LISTEN_READ_AND_CLICK:
                return new ExerciseTemplate\ListenReadClickTemplateDTO();
                break;
            case self::TEMPLATE_CROSSWORD:
                return new ExerciseTemplate\CrosswordTemplateDTO();
                break;
            case self::TEMPLATE_DRAG_AND_DROP_AUDIO:
                return new ExerciseTemplate\DragAndDropAudioTemplateDTO();
                break;
            default:
                throw new LogicException("Unknown type of template");
                break;
        }
    }
}