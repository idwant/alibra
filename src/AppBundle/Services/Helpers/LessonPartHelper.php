<?php

namespace AppBundle\Services\Helpers;

class LessonPartHelper
{
	const LESSON_PART_PHONETICS = 'phonetics';
	const LESSON_PART_WARM_UP = 'warm_up';
	const LESSON_PART_VOCABULARY = 'vocabulary';
	const LESSON_PART_GRAMMAR_INTRO = 'grammar_intro';
	const LESSON_PART_GRAMMAR_GAMES = 'grammar_games';
	const LESSON_PART_SKILL_WORK_GAMES = 'skill_work_games';
	const LESSON_PART_LISTENING = 'listening';
	const LESSON_PART_HOME_WORK = 'home_work';
	const LESSON_PART_TEST = 'test';

	public static function getLessonPartTypeByName($name)
	{
		switch($name) {
			case 'Phonetics':
				return self::LESSON_PART_PHONETICS;
			    break;
			case 'Warm up':
				return self::LESSON_PART_WARM_UP;
			    break;
			case 'Vocabulary':
				return self::LESSON_PART_VOCABULARY;
			    break;
            case 'Grammar intro':
                return self::LESSON_PART_GRAMMAR_INTRO;
                break;
            case 'Grammar games':
                return self::LESSON_PART_GRAMMAR_GAMES;
                break;
            case 'Skill-work games':
                return self::LESSON_PART_SKILL_WORK_GAMES;
                break;
            case 'Listening':
                return self::LESSON_PART_LISTENING;
                break;
            case 'Home work':
                return self::LESSON_PART_HOME_WORK;
                break;
            case 'Test':
                return self::LESSON_PART_TEST;
                break;
			default:
				throw new \Exception("Unknown lesson part type");
				break;
		}
	}
}