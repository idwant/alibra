<?php

namespace AppBundle\Services\Helpers;

class DeliveryTemplateHelper
{
	const DELIVERY_TEMPLATE_EMAIL = 'email';
	const DELIVERY_TEMPLATE_SYSTEM = 'system';
	const DELIVERY_TEMPLATE_SMS = 'sms';

	public static function getDeliveryTemplateTypeByName($name)
	{
		switch($name) {
			case 'email':
				return self::DELIVERY_TEMPLATE_EMAIL;
			case 'system':
				return self::DELIVERY_TEMPLATE_SYSTEM;
			case 'sms':
				return self::DELIVERY_TEMPLATE_SMS;
			default:
				throw new \Exception("Unknown delivery template name");
		}
	}
}