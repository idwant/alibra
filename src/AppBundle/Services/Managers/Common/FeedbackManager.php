<?php

namespace AppBundle\Services\Managers\Common;

use AppBundle\Services\MailSenderService;
use CoreDomain\DTO\Common\FeedbackDTO;
use CoreDomain\Model\Common\Feedback;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Common\FeedbackRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class FeedbackManager
{
    private $em;
    private $feedbackRepository;
    private $mailSenderService;

    public function __construct(
        EntityManagerInterface $em,
        FeedbackRepositoryInterface $feedbackRepository,
        MailSenderService $mailSenderService
    ) {
        $this->em = $em;
        $this->feedbackRepository = $feedbackRepository;
        $this->mailSenderService = $mailSenderService;
    }

    public function save(FeedbackDTO $feedbackDTO, User $user = null)
    {
        $feedback = new Feedback();
        $feedback
            ->setUser($user)
            ->updateInfo($feedbackDTO);

        $this->feedbackRepository->addAndSave($feedback);
        $this->mailSenderService->sendFeedbackMail($feedback);

        return $feedback;
    }
}