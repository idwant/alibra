<?php

namespace AppBundle\Services\Managers\File;

use AppBundle\Repository\File\AudioRepository;
use AppBundle\Repository\File\ImageRepository;
use AppBundle\Repository\File\VideoRepository;
use CoreDomain\Exception\ValidationException;
use CoreDomain\Model\File\Audio;
use CoreDomain\Model\File\File;
use CoreDomain\Model\File\Image;
use CoreDomain\Model\File\Video;
use CoreDomain\Repository\FileRepositoryInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FileManager
{
    private $validator;
    private $audioRepository;
    private $videoRepository;
    private $imageRepository;

    public function __construct(
        ValidatorInterface $validator,
        AudioRepository $audioRepository,
        VideoRepository $videoRepository,
        ImageRepository $imageRepository
    )
    {
        $this->validator = $validator;
        $this->audioRepository = $audioRepository;
        $this->videoRepository = $videoRepository;
        $this->imageRepository = $imageRepository;
    }

    public function upload(UploadedFile $uploadedFile, $type)
    {
        $fileClass = $this->getFileClass($type);
        /** @var File $file */
        $file = new $fileClass($uploadedFile);

        $errors = $this->validator->validate($file, null, $file->getValidationGroup());
        if(count($errors) > 0) {
            throw new ValidationException('File validation error', $errors);
        }

        $uploadedFile->move($file->getUploadRootDir(), $file->getName());
        $repository = $this->getFileRepository($type);
        $repository->add($file);

        return $file;
    }

    public function uploadByConsole(UploadedFile $uploadedFile, $type)
    {
        $fileClass = $this->getFileClass($type);
        /** @var File $file */
        $file = new $fileClass($uploadedFile);

        // TODO: попробовать переделать более красиво
        $fileForMove = new \Symfony\Component\HttpFoundation\File\File($uploadedFile->getPath() . DIRECTORY_SEPARATOR . $uploadedFile->getBasename());
        $fileForMove->move($file->getUploadRootDir(), $file->getName());

        //$uploadedFile->move($file->getUploadRootDir(), $file->getName());
        $repository = $this->getFileRepository($type);
        $repository->add($file);

        return $file;
    }

    /**
     * Загружает файл в базу данных по указанному пути
     * @param $path
     * @param $type
     * @return File|null
     * @throws ContextErrorException
     */
    public function downloadFile($path, $type)
    {
        if (!empty($path)) {
            $tempname = '';
            $attempts = 5;
            while ($attempts > 0) {
                try {
                    $tempname = tempnam('/tmp', 'DOWNLOAD_FILE');
                    file_put_contents($tempname, fopen($path, 'r'));
                    break;
                } catch (ContextErrorException $e) {
                    $attempts--;

                    if($attempts == 0) {
                        throw $e;
                    }
                }
            }

            $file = new UploadedFile($tempname, basename($path));
            return $this->uploadByConsole($file, $type);
        }

        return null;
    }

    /**
     * @param $type
     * @return File
     */
    private function getFileClass($type)
    {
        switch ($type) {
            case File::TYPE_AUDIO:
                return Audio::class;
                break;
            case File::TYPE_VIDEO;
                return Video::class;
                break;
            case File::TYPE_IMAGE:
                return Image::class;
                break;
        }
    }

    /**
     * @param $type
     * @return FileRepositoryInterface
     */
    private function getFileRepository($type)
    {
        switch ($type) {
            case File::TYPE_AUDIO:
                return $this->audioRepository;
                break;
            case File::TYPE_VIDEO;
                return $this->videoRepository;
                break;
            case File::TYPE_IMAGE:
                return $this->imageRepository;
                break;
        }
    }
}