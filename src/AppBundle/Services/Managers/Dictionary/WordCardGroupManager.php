<?php

namespace AppBundle\Services\Managers\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;
use CoreDomain\DTO\Dictionary\WordCardGroupDTO;
use CoreDomain\Model\Dictionary\WordCardGroup;
use CoreDomain\Repository\Dictionary\WordCardCategoryRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordCardGroupRepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use CoreDomain\Exception\LogicException;

class WordCardGroupManager
{
    private $em;
    private $wordCardCategoryRepository;
    private $wordCardGroupRepository;

    public function __construct(
        EntityManager $em,
        WordCardCategoryRepositoryInterface $wordCardCategoryRepository,
        WordCardGroupRepositoryInterface $wordCardGroupRepository
    ) {
        $this->em = $em;
        $this->wordCardGroupRepository = $wordCardGroupRepository;
        $this->wordCardCategoryRepository = $wordCardCategoryRepository;
    }

    public function searchWordCardGroup(SearchWordCardGroupDTO $searchDTO)
    {
        return $this->wordCardGroupRepository->search($searchDTO, false);
    }

    public function searchWordCardGroupCount(SearchWordCardGroupDTO $searchDTO)
    {
        return $this->wordCardGroupRepository->search($searchDTO, true);
    }
    
    public function save(WordCardGroupDTO $groupDTO, $id = null)
    {
        if ($id) {
            /** @var WordCardGroup $group */
            $group = $this->wordCardGroupRepository->findOneById($id);
            if (!$group) {
                throw new Exception('Группа найдена');
            }
        } else {
            $group = new WordCardGroup();
        }

        $category = $this->wordCardCategoryRepository->findOneById($groupDTO->category);

        $this->em->beginTransaction();
        try {
            $group->updateInfo($groupDTO, $category);
            $this->wordCardGroupRepository->addAndSave($group);
            $this->em->commit();
        } catch (UniqueConstraintViolationException $e) {
            $this->em->rollback();

            throw new LogicException('Группа с таким именем уже существует');
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        return $group;
    }

    public function delete($id)
    {
        /** @var WordCardGroup $group */
        $group = $this->wordCardGroupRepository->findOneById($id);
        if($group) {
            $group->setIsDeleted(true);
            $this->wordCardGroupRepository->addAndSave($group);
        }
        else {
            throw new LogicException('Группы не существует');
        }
    }
}