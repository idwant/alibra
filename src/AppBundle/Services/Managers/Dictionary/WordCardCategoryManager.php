<?php

namespace AppBundle\Services\Managers\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardCategoryDTO;
use CoreDomain\DTO\Dictionary\WordCardCategoryDTO;
use CoreDomain\Model\Dictionary\WordCardCategory;
use CoreDomain\Repository\Dictionary\WordCardCategoryRepositoryInterface;
use CoreDomain\Repository\FileRepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use CoreDomain\Exception\LogicException;

class WordCardCategoryManager
{
    private $em;
    private $imageRepository;
    private $wordCardCategoryRepository;

    public function __construct(
        EntityManager $em,
        FileRepositoryInterface $imageRepository,
        WordCardCategoryRepositoryInterface $wordCardCategoryRepository
    ) {
        $this->em = $em;
        $this->imageRepository = $imageRepository;
        $this->wordCardCategoryRepository = $wordCardCategoryRepository;
    }

    public function searchWordCardCategory(SearchWordCardCategoryDTO $searchDTO)
    {
        return $this->wordCardCategoryRepository->search($searchDTO, false);
    }

    public function searchWordCardCategoryCount(SearchWordCardCategoryDTO $searchDTO)
    {
        return $this->wordCardCategoryRepository->search($searchDTO, true);
    }

    public function save(WordCardCategoryDTO $categoryDTO, $id = null)
    {
        if ($id) {
            /** @var WordCardCategory $category */
            $category = $this->wordCardCategoryRepository->findOneById($id);
            if (!$category) {
                throw new Exception('Категория найдена');
            }
        } else {
            $category = new WordCardCategory();
        }

        $image = $this->imageRepository->findOneById($categoryDTO->image);
        $imageBig = $this->imageRepository->findOneById($categoryDTO->image_big);

        $this->em->beginTransaction();
        try {
            $category->updateInfo($categoryDTO, $image, $imageBig);
            $this->wordCardCategoryRepository->addAndSave($category);
            $this->em->commit();
        } catch (UniqueConstraintViolationException $e) {
            $this->em->rollback();

            throw new LogicException('Категория с таким именем уже существует');
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        return $category;
    }

    public function delete($id)
    {
        /** @var WordCardCategory $category */
        $category = $this->wordCardCategoryRepository->findOneById($id);

        if($category) {
            $category->setIsDeleted(true);
            $this->wordCardCategoryRepository->addAndSave($category);
        }
        else {
            throw new LogicException('Категория не найдена');
        }
    }
}