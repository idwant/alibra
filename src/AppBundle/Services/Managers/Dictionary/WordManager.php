<?php

namespace AppBundle\Services\Managers\Dictionary;


use AppBundle\Repository\File\AudioRepository;
use AppBundle\Repository\File\ImageRepository;
use CoreDomain\DTO\Dictionary\WordTranslationDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\WordTranslation;
use CoreDomain\Repository\Dictionary\DictionaryRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordRepositoryInterface;
use CoreDomain\DTO\Dictionary\WordDTO;
use Doctrine\ORM\EntityManagerInterface;

class WordManager
{
    private $em;
    private $dictionaryRepository;
    private $wordRepository;
    private $audioRepository;
    private $imageRepository;

    public function __construct(EntityManagerInterface $em,
                                DictionaryRepositoryInterface $dictionaryRepository,
                                WordRepositoryInterface $wordRepository,
                                AudioRepository $audioRepository,
                                ImageRepository $imageRepository)
    {
        $this->em = $em;
        $this->dictionaryRepository = $dictionaryRepository;
        $this->wordRepository = $wordRepository;
        $this->audioRepository = $audioRepository;
        $this->imageRepository = $imageRepository;
    }

    public function getWordById($id)
    {
        return $this->wordRepository->findOneById($id);
    }

    public function addWord(WordDTO $wordDTO)
    {
        $this->em->beginTransaction();
        try {
            $audio = $this->audioRepository->findOneById($wordDTO->audio);
            $image = $this->imageRepository->findOneById($wordDTO->image);

            $word = new Word();
            $word->updateInfo(
                $wordDTO->word,
                $wordDTO->transcription,
                $image,
                $audio
            );
            $this->wordRepository->addAndSave($word);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $word;
    }

    public function updateWord(WordDTO $wordDTO, $wordId)
    {
        $word = $this->wordRepository->findOneById($wordId);
        if (!$word) {
            throw new LogicException("Такого слова не существует");
        }

        $this->em->beginTransaction();
        try {
            if($word) {
                $audio = $this->audioRepository->findOneById($wordDTO->audio);
                $image = $this->imageRepository->findOneById($wordDTO->image);

                $word->updateInfo(
                    $wordDTO->word,
                    $wordDTO->transcription,
                    $image,
                    $audio
                );
                $this->wordRepository->addAndSave($word);
            }
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $word;
    }

    public function deleteWord($id)
    {
        $this->wordRepository->setDeletedById($id);
    }

    public function search(WordDTO $wordDTO, $limit = null, $offset = null)
    {
        return array(
            'data' => $this->wordRepository->search($wordDTO, false, $limit, $offset),
            'count' => $this->wordRepository->search($wordDTO, true, $limit, $offset)
        );
    }

    public function translateWord($word)
    {
        return $this->wordRepository->translateWord($word);
    }

    public function getAllWords($id)
    {
        $dictionary = $this->dictionaryRepository->findOneById($id);
        if (!$dictionary) {
            throw new \Exception("No such dictionary");
        } 
        return $this->wordRepository->findAllByDictionary($dictionary);
    }

    public function addTranslation(WordTranslationDTO $wordTranslationDTO)
    {
        $word = $this->wordRepository->findOneById($wordTranslationDTO->word);
        if (!$word) {
            throw new LogicException("Такое слово не существует");
        }

        $sampleAudio = $this->audioRepository->findOneById($wordTranslationDTO->sampleAudio);
        $wordTranslation = new WordTranslation();
        $wordTranslation->updateInfo(
            $word,
            $wordTranslationDTO->translation,
            $wordTranslationDTO->sample,
            $sampleAudio
        );
        $this->wordRepository->addAndSaveWordTranslation($wordTranslation);
        return $wordTranslation;
    }

    public function updateTranslation($id, WordTranslationDTO $wordTranslationDTO)
    {
        $wordTranslation = $this->wordRepository->findWordTranslationById($id);
        if (!$wordTranslation) {
            throw new LogicException("Перевод слова не найден");
        }

        $word = $this->wordRepository->findOneById($wordTranslationDTO->word);
        if (!$word) {
            throw new LogicException("Такое слово не существует");
        }

        $sampleAudio = $this->audioRepository->findOneById($wordTranslationDTO->sampleAudio);
        $this->em->beginTransaction();
        try {
            $wordTranslation->updateInfo(
                $word,
                $wordTranslationDTO->translation,
                $wordTranslationDTO->sample,
                $sampleAudio
            );
            $this->wordRepository->addAndSaveWordTranslation($wordTranslation);
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        return $wordTranslation;
    }

    public function deleteTranslation($translationId)
    {
        $this->wordRepository->deleteWordTranslation($translationId);
    }
}