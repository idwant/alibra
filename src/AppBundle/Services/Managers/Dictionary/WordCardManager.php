<?php

namespace AppBundle\Services\Managers\Dictionary;

use CoreDomain\DTO\Dictionary\SearchWordCardDTO;
use CoreDomain\DTO\Dictionary\WordCardDTO;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Dictionary\WordCardGroup;
use CoreDomain\Repository\Dictionary\WordCardGroupRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordCardRepositoryInterface;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\FileRepositoryInterface;
use CoreDomain\Repository\Education\UserWordCardRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use CoreDomain\Exception\LogicException;

class WordCardManager
{
    private $em;
    private $wordCardRepository;
    private $wordCardGroupRepository;
    private $wordRepository;
    private $imageRepository;
    private $audioRepository;
    private $userWordCardRepository;
    private $userRepository;
    private $lessonRepository;

    public function __construct(
        EntityManager $em,
        WordCardRepositoryInterface $wordCardRepository,
        WordCardGroupRepositoryInterface $wordCardGroupRepository,
        WordRepositoryInterface $wordRepository,
        FileRepositoryInterface $imageRepository,
        FileRepositoryInterface $audioRepository,
        UserWordCardRepositoryInterface $userWordCardRepository,
        UserRepositoryInterface $userRepository,
        LessonRepositoryInterface $lessonRepository
    )
    {
        $this->em = $em;
        $this->wordCardRepository = $wordCardRepository;
        $this->wordCardGroupRepository = $wordCardGroupRepository;
        $this->wordRepository = $wordRepository;
        $this->imageRepository = $imageRepository;
        $this->audioRepository = $audioRepository;
        $this->userWordCardRepository = $userWordCardRepository;
        $this->userRepository = $userRepository;
        $this->lessonRepository = $lessonRepository;
    }

    public function searchWordCard(SearchWordCardDTO $searchDTO)
    {
        return $this->wordCardRepository->search($searchDTO, false);
    }

    public function searchWordCardCount(SearchWordCardDTO $searchDTO)
    {
        return $this->wordCardRepository->search($searchDTO, true);
    }

    public function save(WordCardDTO $cardDTO, $id = null)
    {
        if ($id) {
            /** @var WordCard $card */
            $card = $this->wordCardRepository->findOneById($id);
            if (!$card) {
                throw new Exception('Карточка слова не найдена');
            }
        } else {
            $card = new WordCard();
        }

        $word = $this->wordRepository->findOneById($cardDTO->word);
        if (!$word) {
            throw new Exception('Слово в словаре не найдено');
        }

        $audio = $this->audioRepository->findOneById($cardDTO->audio);
        $sampleAudio = $this->audioRepository->findOneById($cardDTO->sampleAudio);
        $image = $this->imageRepository->findOneById($cardDTO->image);

        $this->em->beginTransaction();
        try {
            $card->updateInfo($cardDTO, $word, $audio, $sampleAudio, $image);
            $this->wordCardRepository->addAndSave($card);

            // удаляем все связи
            /** @var WordCardGroup $group */
            foreach ($card->getWordCardGroups() as $group) {
                $card->removeWordCardGroup($group);
                $group->removeWordCard($card);
                $this->wordCardGroupRepository->addAndSave($group);
            }
            $this->wordCardRepository->addAndSave($card);

            // настраиваем новые связи
            foreach ($cardDTO->groups as $groupId) {
                /** @var WordCardGroup $group */
                $group = $this->wordCardGroupRepository->findOneById($groupId);
                if(!$group) {
                    throw new Exception('Не найдена группа');
                }

                $group->addWordCard($card);
                $card->addWordCardGroup($group);
                $this->wordCardGroupRepository->addAndSave($group);
            }
            $this->wordCardRepository->addAndSave($card);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        return $card;
    }

    public function addLessons(WordCard $card, $lessonsIds)
    {
        $lessonsIds = (array)$lessonsIds;

        $this->em->beginTransaction();
        try {
            foreach ($lessonsIds as $lessonId) {
                $lesson = $this->lessonRepository->findOneById($lessonId);
                if (!$lesson) {
                    throw new \Exception("Урок не найден");
                }

                $this->wordCardRepository->addLesson($card, $lesson);
            }
            $this->em->commit();
        }
        catch (UniqueConstraintViolationException $e) {
            $this->em->rollback();

            throw new LogicException('Один или несколько уроков уже закреплены за карточкой слова');
        }
        catch(\Exception $e) {
            $this->em->rollback();

            throw $e;
        }
    }

    public function removeLessons(WordCard $card, $lessonsIds) {
        $lessonsIds = (array)$lessonsIds;

        $this->em->beginTransaction();
        try {
            foreach ($lessonsIds as $lessonId) {
                $lesson = $this->lessonRepository->findOneById($lessonId);
                if (!$lesson) {
                    throw new \Exception("Урок не найден");
                }

                $this->wordCardRepository->deleteLesson($card, $lesson);
            }
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function getAllWordCards($id)
    {
        $user = $this->userRepository->findOneById($id);
        if ($user) {
            return $this->userWordCardRepository->findAllByUser($user);
        }

        return [];
    }
}