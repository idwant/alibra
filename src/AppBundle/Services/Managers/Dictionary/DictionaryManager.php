<?php

namespace AppBundle\Services\Managers\Dictionary;


use CoreDomain\DTO\Dictionary\DictionaryDTO;
use CoreDomain\DTO\Dictionary\WordDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Repository\Dictionary\DictionaryRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use CoreDomain\Model\Dictionary\Word;
use CoreDomain\Model\Dictionary\Dictionary;

class DictionaryManager
{
    private $em;
    private $dictionaryRepository;
    private $wordRepository;

    public function __construct(EntityManagerInterface $em,
                                DictionaryRepositoryInterface $dictionaryRepository,
                                WordRepositoryInterface $wordRepository)
    {
        $this->em = $em;
        $this->dictionaryRepository = $dictionaryRepository;
        $this->wordRepository = $wordRepository;
    }

    public function searchDictionaries($query, $limit = null, $offset = null)
    {
        return array(
            'data' => $this->dictionaryRepository->search($query, false, $limit, $offset),
            'count' => $this->dictionaryRepository->search($query, true, $limit, $offset)
        );
    }

    public function getDictionaryById($id)
    {
        return $this->dictionaryRepository->findOneById($id);
    }

    public function addDictionary(DictionaryDTO $dictionaryDTO)
    {
        if($this->dictionaryRepository->findOneByName($dictionaryDTO->name)) {
            throw new LogicException('Словарь с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $dictionary = new Dictionary();
            $dictionary->updateInfo($dictionaryDTO->name);
            $this->dictionaryRepository->addAndSave($dictionary);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $dictionary;
    }

    public function updateDictionary(DictionaryDTO $dictionaryDTO, $id)
    {
        $dictionary = $this->dictionaryRepository->findOneById($id);
        if (!$dictionary) {
            throw new LogicException("Такого словаря не существует");
        }

        if(
            $dictionary->getName() != $dictionaryDTO->name &&
            $this->dictionaryRepository->findOneByName($dictionaryDTO->name)
        ) {
            throw new LogicException('Словарь с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            if($dictionary) {
                $dictionary->updateInfo($dictionaryDTO->name);
                $this->dictionaryRepository->addAndSave($dictionary);
            }
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $dictionary;
    }

    public function deleteDictionaryById($id)
    {
        $this->dictionaryRepository->deleteById($id);
    }

    public function addWordToDictionary($wordId, $dictionaryId)
    {
        $dictionaryWord = $this->wordRepository->findDictionaryWord($dictionaryId, $wordId);
        if ($dictionaryWord) {
            throw new LogicException("Слово уже есть в словаре");
        }

        $this->em->beginTransaction();
        try {
            $dictionary = $this->dictionaryRepository->findOneById($dictionaryId);
            $word = $this->wordRepository->findOneById($wordId);
            if (!($dictionary && $word)) {
                throw new LogicException("No dictionary or word entry");
            }
            $dictionary->addWord($word);
            $this->dictionaryRepository->addAndSave($dictionary);
            $this->em->commit();
        }
        catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $word;
    }

    public function deleteWordFromDictionary($wordId, $dictionaryId)
    {
        $dictionaryWord = $this->wordRepository->findDictionaryWord($dictionaryId, $wordId);
        if (!$dictionaryWord) {
            throw new LogicException("Такого слова нет в словаре");
        }

        $this->em->beginTransaction();
        try {
            $dictionary = $this->dictionaryRepository->findOneById($dictionaryId);
            $word = $this->wordRepository->findOneById($wordId);
            $dictionary->deleteWord($word);
            $this->dictionaryRepository->addAndSave($dictionary);
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function getAllWords($dictionaryId, $word = null, $limit = null, $offset = null) {
        $dictionary = $this->dictionaryRepository->findOneById($dictionaryId);
        if (!$dictionary) {
            throw new LogicException('Dictionary not found');
        }

        return array(
            'data' => $this->wordRepository->findAllByDictionary($dictionary, $word, false, $limit, $offset),
            'count' => $this->wordRepository->findAllByDictionary($dictionary, $word, true, $limit, $offset)
        );
    }
}