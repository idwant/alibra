<?php

namespace AppBundle\Services\Managers\Delivery;


use CoreDomain\DTO\Delivery\DeliveryTemplateDTO;
use CoreDomain\Model\Delivery\DeliveryTemplate;
use CoreDomain\Repository\Delivery\DeliveryTemplateRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class DeliveryTemplateManager
{
	private $em;
	private $deliveryTemplateRepository;
	private $userRepository;
	private $serializer;

	public function __construct(
		EntityManagerInterface $em,
		DeliveryTemplateRepositoryInterface $deliveryTemplateRepository,
        UserRepositoryInterface $userRepository,
        Serializer $serializer)
	{
		$this->em = $em;
		$this->deliveryTemplateRepository = $deliveryTemplateRepository;
		$this->userRepository = $userRepository;
		$this->serializer = $serializer;
	}

	public function getDeliveryTemplateList($limit = null, $offset = null)
	{
		$deliveryTemplates = $this->deliveryTemplateRepository->findAll($limit, $offset);
		return $this->prepareResponse($deliveryTemplates, array('api_delivery_template_list'));
	}

	public function getDeliveryTemplateById($id)
	{
		return $this->deliveryTemplateRepository->findOneById($id);
	}

	public function addDeliveryTemplate(DeliveryTemplateDTO $dto)
	{
		$this->em->beginTransaction();
		try {
			$user = $this->userRepository->findOneById($dto->user);
			$deliveryTemplate = new DeliveryTemplate();
			$deliveryTemplate->updateInfo($dto, $user);
			$this->deliveryTemplateRepository->addAndSave($deliveryTemplate);
			$this->em->commit();
		}
		catch(\Exception $e) {
			$this->em->rollback();
			throw $e;
		}
		return $deliveryTemplate;
	}

	public function updateDeliveryTemplate(DeliveryTemplateDTO $dto, $id)
	{
		$this->em->beginTransaction();
		try {
			$deliveryTemplate = $this->deliveryTemplateRepository->findOneById($id);
			if ($deliveryTemplate) {
				$user = $this->userRepository->findOneById($dto->user);
				$deliveryTemplate->updateInfo($dto, $user);
				$this->deliveryTemplateRepository->addAndSave($deliveryTemplate);
			}
			$this->em->commit();
		}
		catch(\Exception $e) {
			$this->em->rollback();
			throw $e;
		}
		return $deliveryTemplate;
	}

	public function deleteDeliveryTemplateById($id)
	{
		$this->deliveryTemplateRepository->deleteById($id);
	}

	private function prepareResponse($params, array $serializationGroups)
	{
		$responseData = $this->serializer->serialize(
			$params,
			'json',
			SerializationContext::create()->setGroups($serializationGroups)
		);
		return array(
			'responseData' => $responseData,
			'count' => count($params)
		);
	}
}