<?php

namespace AppBundle\Services\Managers\User;

use AppBundle\Services\MailSenderService;
use AppBundle\Services\Managers\Task\TaskQueueManager;
use CoreDomain\DTO\Dictionary\SearchWordCardCategoryDTO;
use CoreDomain\DTO\Dictionary\SearchWordCardDTO;
use CoreDomain\DTO\Dictionary\SearchWordCardGroupDTO;
use CoreDomain\DTO\Education\SearchUserWordCardDTO;
use CoreDomain\DTO\Education\StatUserWordCardDTO;
use CoreDomain\DTO\Task\TaskQueueDTO;
use CoreDomain\DTO\User\ProfileDTO;
use CoreDomain\DTO\User\SearchDTO;
use CoreDomain\DTO\User\UserRegisterDTO;
use CoreDomain\Exception\EntityNotFoundException;
use CoreDomain\Exception\LogicException;
use CoreDomain\Exception\ValidationException;
use CoreDomain\Model\Dictionary\WordCard;
use CoreDomain\Model\Education\UserWordCard;
use CoreDomain\Model\User\Password;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Dictionary\WordCardCategoryRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordCardGroupRepositoryInterface;
use CoreDomain\Repository\Dictionary\WordRepositoryInterface;
use CoreDomain\Repository\Education\CourseRepositoryInterface;
use CoreDomain\Repository\Education\UserWordCardRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use CoreDomain\Repository\User\UserSessionRepositoryInterface;
use CoreDomain\Security\PasswordStrategyInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Config\Definition\Exception\Exception;

class UserManager
{
    private $em;
    private $userRepository;
    private $userSessionRepository;
    private $courseRepository;
    private $passwordEncoder;
    private $validator;
    private $userWordCardRepository;
    private $wordRepository;
    private $wordCardGroupRepository;
    private $wordCardCategoryRepository;
    private $mailSenderService;

    public function __construct(
        EntityManagerInterface $em,
        UserRepositoryInterface $userRepository,
        UserSessionRepositoryInterface $userSessionRepository,
        CourseRepositoryInterface $courseRepository,
        PasswordStrategyInterface $passwordEncoder,
        RecursiveValidator $validator,
        UserWordCardRepositoryInterface $userWordCardRepository,
        WordRepositoryInterface $wordRepository,
        WordCardGroupRepositoryInterface $wordCardGroupRepository,
        WordCardCategoryRepositoryInterface $wordCardCategoryRepository,
        MailSenderService $mailSenderService

    )
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->userSessionRepository = $userSessionRepository;
        $this->courseRepository = $courseRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
        $this->userWordCardRepository = $userWordCardRepository;
        $this->wordRepository = $wordRepository;
        $this->wordCardGroupRepository = $wordCardGroupRepository;
        $this->wordCardCategoryRepository = $wordCardCategoryRepository;
        $this->mailSenderService = $mailSenderService;
    }

    public function login($email, $password)
    {
        $user = $this->userRepository->findOneByEmail($email);

        if(!$user || !$this->passwordEncoder->isPasswordValid($password, $user->getPassword(), $user->getSalt())) {
            throw new EntityNotFoundException('Неверный логин или пароль');
        }

        if(!$user->getLastAuthDate()) {
            $this->mailSenderService->sendFirstLoginMail($user);
        }

        $userSession = $user->login();
        $this->userSessionRepository->addAndSave($userSession);
        $this->userRepository->addAndSave($user);

        return $userSession;
    }

    public function loginByToken($token)
    {
        $user = $this->userRepository->findOneByAuthToken($token);

        if(!$user) {
            throw new EntityNotFoundException('Неверный токен');
        }

        if(!$user->getLastAuthDate()) {
            $this->mailSenderService->sendFirstLoginMail($user);
        }

        $userSession = $user->login();
        $this->userSessionRepository->addAndSave($userSession);
        $this->userRepository->addAndSave($user);

        return $userSession;
    }

    public function logout(User $user)
    {
        $user->logout();
        $this->userSessionRepository->addAndSave($user->getSession());
    }

    public function getUserById($id)
    {
        return $this->userRepository->findOneById($id);
    }

    public function searchUser(SearchDTO $searchDTO)
    {
        return $this->userRepository->search($searchDTO, false);
    }

    public function searchUserCount(SearchDTO $searchDTO)
    {
        return $this->userRepository->search($searchDTO, true);
    }

    public function register(UserRegisterDTO $userDTO)
    {
        $user = new User(
            $userDTO->getEmail(),
            new Password($this->passwordEncoder, $userDTO->getPassword()),
            $userDTO->getRoles()
        );
        $user
            ->setLastName($userDTO->getLastName())
            ->setFirstName($userDTO->getFirstName())
            ->setMiddleName($userDTO->getMiddleName())
            ->setPhone($userDTO->getPhone())
            ->setCity($userDTO->getCity())
            ->setBirthday($userDTO->getBirthday())
            ->setAgreementNumber($userDTO->getAgreementNumber())
            ->setAuthenticationToken($userDTO->getToken())
            ->setIsDemo($userDTO->getIsDemo());

        if(count($validationErrors = $this->validator->validate($user)) > 0) {
            throw new ValidationException('Bad request', $validationErrors);
        }
        $this->userRepository->addAndSave($user);
        $this->mailSenderService->sendRegisterMail($user);
        return $user;
    }

    public function updateProfile(User $user, ProfileDTO $profileDTO)
    {
        $this->em->beginTransaction();
        try {
            $user->updateProfile($profileDTO);
            if (count($validationErrors = $this->validator->validate($user)) > 0) {
                throw new ValidationException('Bad request', $validationErrors);
            }
            $this->deleteAttachedPersonsByUser($user, $profileDTO);

            // пароль сохраняем только в случае, если он не пустой, иначе оставляем старый пароль
            if (!empty($profileDTO->password)) {
                $password = new Password($this->passwordEncoder, $profileDTO->password);
                $user->setPassword($password->getPassword());
            }

            if ($profileDTO->teachers) {
                $this->addTeachersToUser($user, $profileDTO->teachers);
            }
            if ($profileDTO->employers) {
                $this->addEmployersToUser($user, $profileDTO->employers);
            }
            if ($profileDTO->students) {
                $this->addStudentsToTeacher($user, $profileDTO->students);
            }
            if ($profileDTO->employees) {
                $this->addEmployeesToEmployer($user, $profileDTO->employees);
            }
            $this->userRepository->addAndSave($user);
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $user;
    }

    private function addTeachersToUser(User $user, $teacherIds)
    {
        foreach($teacherIds as $teacherId) {
            $teacher = $this->userRepository->findOneById($teacherId);
            if (!$teacher) {
                throw new LogicException('Учитель не найден');
            }
            if (!$teacher->hasRole(User::ROLE_TEACHER)) {
                throw new LogicException('Пользователь не является учителем');
            }
            $this->userRepository->addTeacherToUser($user, $teacher);
        }
    }

    private function addStudentsToTeacher(User $teacher, $studentIds)
    {
        foreach($studentIds as $studentId) {
            /** @var User $student */
            $student = $this->userRepository->findOneById($studentId);
            if (!$student) {
                throw new LogicException("Студент не найден");
            }
            if (
                !$student->hasRole(User::ROLE_OFFLINE_STUDENT)
                && !$student->hasRole(User::ROLE_ONLINE_STUDENT)
            ) {
                throw new LogicException("Пользователь не является студентом");
            }
            $this->userRepository->addStudentToTeacher($teacher, $student);
        }
    }

    private function addEmployersToUser(User $user, $employerIds)
    {
        foreach($employerIds as $employerId) {
            $employer = $this->userRepository->findOneById($employerId);
            if (!$employer) {
                throw new LogicException("No employer entry");
            }
            if (!$employer->hasRole(User::ROLE_CORPORATE_USER)) {
                throw new LogicException("Пользователь не является корпоративным пользователем");
            }
            $this->userRepository->addEmployerToUser($employer, $user);
        }
    }

    private function addEmployeesToEmployer(User $employer, $employeeIds)
    {
        foreach ($employeeIds as $employeeId) {
            $employee = $this->userRepository->findOneById($employeeId);
            if (!$employee) {
                throw new LogicException("Сотрудник не найден");
            }
            $this->userRepository->addEmployeeToEmployer($employer, $employee);
        }
    }

    public function addCourses(User $user, $coursesIds)
    {
        $coursesIds = (array)$coursesIds;

        $this->em->beginTransaction();
        try {
            foreach ($coursesIds as $courseId) {
                $course = $this->courseRepository->findOneById($courseId);
                if (!$course) {
                    throw new LogicException("Курс не найден");
                }

                $this->userRepository->addCourse($user, $course);
            }
            $this->em->commit();
        }
        catch (UniqueConstraintViolationException $e) {
            $this->em->rollback();

            throw new LogicException('Один или несколько курсов уже закреплены за пользователем');
        }
        catch(\Exception $e) {
            $this->em->rollback();
            
            throw $e;
        }
    }

    public function removeCourses(User $user, $coursesIds) {
        $coursesIds = (array)$coursesIds;

        $this->em->beginTransaction();
        try {
            foreach ($coursesIds as $courseId) {
                $course = $this->courseRepository->findOneById($courseId);
                if (!$course) {
                    throw new LogicException("Курс не найден");
                }

                $this->userRepository->deleteCourse($user, $course);
            }
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    /**
     * @param User $user
     * @param ProfileDTO $profileDTO
     * Удаляет ненужные связи между учителями и учениками, а также между работодателями и работниками
     */
    private function deleteAttachedPersonsByUser(User $user, ProfileDTO $profileDTO)
    {
        foreach ($user->getTeachers() as $teacher) {
            if (!$profileDTO->teachers || !in_array($teacher->getId(), $profileDTO->teachers)) {
                $this->userRepository->deleteTeacherByUser($user, $teacher);
            }
            else {
                $profileDTO->teachers = array_diff($profileDTO->teachers, array($teacher->getId()));
            }
        }

        foreach ($user->getEmployers() as $employer) {
            if (!$profileDTO->employers || !in_array($employer->getId(), $profileDTO->employers)) {
                $this->userRepository->deleteEmployerByUser($user, $employer);
            }
            else {
                $profileDTO->employers = array_diff($profileDTO->employers, array($employer->getId()));
            }
        }

        foreach($user->getStudents() as $student) {
            if (!$profileDTO->students || !in_array($student->getId(), $profileDTO->students)) {
                $this->userRepository->deleteStudentByTeacher($user, $student);
            }
            else {
                $profileDTO->students = array_diff($profileDTO->students, array($student->getId()));
            }
        }

        foreach($user->getEmployees() as $employee) {
            if (!$profileDTO->employees || !in_array($employee->getId(), $profileDTO->employees)) {
                $this->userRepository->deleteEmployeeByEmployer($user, $employee);
            }
            else {
                $profileDTO->employees = array_diff($profileDTO->employees, array($employee->getId()));
            }
        }
    }

    public function getAllWordCardForLearn(SearchUserWordCardDTO $searchDTO)
    {
        $searchDTO
            ->setKnown(false)
            ->setDisabled(false)
            ->setLearnDate((new \DateTime('now'))->format('Y-m-d H:i:s'));
        return $this->userWordCardRepository->searchWordCards($searchDTO);
    }

    public function getAllWordCardForRepeat(SearchUserWordCardDTO $searchDTO)
    {
        $searchDTO
            ->setKnown(true)
            ->setDisabled(false)
            ->setRepetitionCount(UserWordCard::REPETITION_COUNT)
            ->setRepetitionDate((new \DateTime('now'))->format('Y-m-d H:i:s'));

        $userWordCards =  $this->userWordCardRepository
            ->searchUserWordCards($searchDTO);

        foreach ($userWordCards as $userWordCard) {
            $id = $userWordCard->getWordCard()->getId();
            $possibleAnswers = $this->wordRepository->getRandomWords(UserWordCard::NUMBER_POSSIBLE_ANSWERS, $id);
            $userWordCard->setPossibleAnswers($possibleAnswers);
        }

        return $userWordCards;
    }

    public function getAllWordCardForReview(SearchUserWordCardDTO $searchDTO)
    {
        $searchDTO
            ->setKnown(true)
            ->setDisabled(false);

        return $this->userWordCardRepository
            ->searchUserWordCards($searchDTO);
    }

    public function learnWordCard(User $user, WordCard $wordCard)
    {
        $userWordCard = $this->getUserWordCard($user, $wordCard);

        $this->em->beginTransaction();
        try {
            if (!$userWordCard) {
                $userWordCard = new UserWordCard($user, $wordCard);
                $this->userWordCardRepository->addAndSave($userWordCard);
            }

            $userWordCard->setKnown(true);
            $this->userWordCardRepository->save($userWordCard);

            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function learnPostponeWordCard(User $user, WordCard $wordCard) {
        $userWordCard = $this->getUserWordCard($user, $wordCard);

        $this->em->beginTransaction();
        try {
            if (!$userWordCard) {
                $userWordCard = new UserWordCard($user, $wordCard);
                $this->userWordCardRepository->addAndSave($userWordCard);
            }

            $userWordCard->setLearnDate((new \DateTime('now +1 day')));
            $this->userWordCardRepository->save($userWordCard);

            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function disableWordCard(User $user, WordCard $wordCard)
    {
        $userWordCard = $this->getUserWordCard($user, $wordCard);

        $this->em->beginTransaction();
        try {
            if (!$userWordCard) {
                $userWordCard = new UserWordCard($user, $wordCard);
                $this->userWordCardRepository->addAndSave($userWordCard);
            }

            $userWordCard->setDisabled(true);
            $this->userWordCardRepository->save($userWordCard);

            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function repeatWordCard(User $user, UserWordCard $userWordCard)
    {
        $repetitionDate = (new \DateTime('now'))->add(new \DateInterval($userWordCard->getRepetitionRate()));
        $repetitionCount = $userWordCard->getRepetitionCount();
        $userWordCard->setRepetitionDate($repetitionDate)->setRepetitionCount(++$repetitionCount);
        $this->userWordCardRepository->addAndSave($userWordCard);

        return $userWordCard;
    }

    public function getWordCardGroups(SearchWordCardGroupDTO $searchDTO)
    {
        return $this->wordCardGroupRepository->findAllWithStatByUser($searchDTO);
    }

    public function getWordCardCategories(SearchWordCardCategoryDTO $searchDTO)
    {
        return $this->wordCardCategoryRepository->search($searchDTO);
    }

    public function getStat(SearchUserWordCardDTO $searchDTO)
    {
        $stat = $this->userWordCardRepository->statWordCards($searchDTO);

        $statDTO = new StatUserWordCardDTO();
        $statDTO
            ->setTotal($stat['total'])
            ->setLearned($stat['learned'])
            ->setRepeat($stat['repeat'])
            ->setDisabled($stat['disabled'])
            ->setComplete($stat['complete']);

        return $stat;
    }

    /**
     * Подгружает запись UserWordCard по пользователю и WordCard
     * @param User $user
     * @param WordCard $wordCard
     * @return UserWordCard|null
     */
    private function getUserWordCard(User $user, WordCard $wordCard)
    {
        $searchDTO = new SearchUserWordCardDTO();
        $searchDTO
            ->setUserId($user->getId())
            ->setWordCardId($wordCard->getId());
        $userWordCard = $this->userWordCardRepository->searchUserWordCards($searchDTO);
        return count($userWordCard) > 0 ? array_shift($userWordCard) : null;
    }
}