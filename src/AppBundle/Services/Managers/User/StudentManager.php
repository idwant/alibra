<?php

namespace AppBundle\Services\Managers\User;

use AppBundle\Services\Managers\Education\ExerciseManager;
use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use CoreDomain\DTO\Student\Education\ExerciseSearchDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use CoreDomain\Model\Education\Exercise;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\CourseRepositoryInterface;
use CoreDomain\Repository\Education\ExerciseRepositoryInterface;
use CoreDomain\Repository\Education\LessonPartRepositoryInterface;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use CoreDomain\Repository\Education\UserExerciseRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class StudentManager
{
    private $userRepository;
    private $courseRepository;
    private $unitRepository;
    private $lessonRepository;
    private $lessonPartRepository;
    private $exerciseRepository;
    private $exerciseManager;
    private $userExerciseRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        CourseRepositoryInterface $courseRepository,
        UnitRepositoryInterface $unitRepository,
        LessonRepositoryInterface $lessonRepository,
        LessonPartRepositoryInterface $lessonPartRepository,
        ExerciseRepositoryInterface $exerciseRepository,
        UserExerciseRepositoryInterface $userExerciseRepository,
        ExerciseManager $exerciseManager
    ){
        $this->userRepository = $userRepository;
        $this->courseRepository = $courseRepository;
        $this->unitRepository = $unitRepository;
        $this->lessonRepository = $lessonRepository;
        $this->lessonPartRepository = $lessonPartRepository;
        $this->exerciseRepository = $exerciseRepository;
        $this->userExerciseRepository = $userExerciseRepository;
        $this->exerciseManager = $exerciseManager;
    }

    public function getCourses(CourseSearchDTO $courseSearchDTO)
    {
        return $this->courseRepository->search($courseSearchDTO);
    }

    public function getUnits(UnitSearchDTO $unitSearchDTO)
    {
        return $this->unitRepository->search($unitSearchDTO);
    }

    public function getLessons(LessonSearchDTO $lessonSearchDTO)
    {
        return $this->lessonRepository->search($lessonSearchDTO);
    }

    public function getExercises(ExerciseSearchDTO $searchDTO)
    {
        $lessons = $this->exerciseRepository->search($searchDTO);

        $exercises = [];
        /** @var Lesson $lesson */
        foreach ($lessons as $lesson) {
            $exercises = array_merge($lesson->getExercises($exercises), $exercises);
        }

        /** @var Exercise $exercise */
        foreach ($exercises as $exercise) {
            $exercise->setParams($this->exerciseManager->deserializeParams($exercise));

            $userExercise = $this->userExerciseRepository
                ->findOneByUserAndLessonAndExercise(
                    $searchDTO->userId, $exercise->getLesson()->getId(), $exercise->getId()
                );

            if($userExercise) {
                $exercise->setUserExercise($userExercise);
            }
        }

        if ($searchDTO->userId) {
            $lesson = null;
            if ($searchDTO->lessonId) {
                $lesson = $this->lessonRepository->findOneById($searchDTO->lessonId);
            }

            if (!$lesson && $searchDTO->lessonPartId) {
                $lessonPart = $this->lessonPartRepository->findOneById($searchDTO->lessonPartId);
                $lesson = $lessonPart->getLesson();
            }
        }

        return $exercises;
    }
}