<?php

namespace AppBundle\Services\Managers\Task;

use CoreDomain\Model\Task\TaskQueue;
use CoreDomain\Model\Task\TaskStatus;
use CoreDomain\Repository\Task\TaskQueueRepositoryInterface;
use CoreDomain\Repository\Task\TaskStatusRepositoryInterface;
use CoreDomain\DTO\Task\TaskQueueDTO;

class TaskQueueManager
{
    private $taskQueueRepository;
    private $taskStatusRepository;
    
    public function __construct(
        TaskQueueRepositoryInterface $taskQueueRepository,
        TaskStatusRepositoryInterface $taskStatusRepository        
    ) {
        $this->taskQueueRepository = $taskQueueRepository;
        $this->taskStatusRepository = $taskStatusRepository;
    }

    public function getTask()
    {
        return $this->taskQueueRepository->findOneForExecute();
    }

    public function addTask(TaskQueueDTO $taskQueueDTO)
    {
        if(!$taskQueueDTO->service){
           return false; 
        } else {
            $service = $taskQueueDTO->service;
        }
        
        $params = $taskQueueDTO->params ? $taskQueueDTO->params : [];        
        $status = $taskQueueDTO->status ? $taskQueueDTO->status : $this->taskStatusRepository->findOneById(TaskStatus::TASK_WAITE) ;        
        $attempts = $taskQueueDTO->attempts ? $taskQueueDTO->attempts : 1;        
        $taskQueue = new TaskQueue($service, $params, $status, $attempts);
        
        $this->taskQueueRepository->addAndSave($taskQueue);
        return true;
    }

    public function updateTask(TaskQueue $task)
    {
        $this->taskQueueRepository->addAndSave($task);
    }
    
    public function finishTask(TaskQueue $task)
    {
        $task->setStatus($this->taskStatusRepository->findOneById(TaskStatus::TASK_EXECUTED));
        $this->updateTask($task);
    }
    
    public function failTask(TaskQueue $task)
    {
        $task->setStatus($this->taskStatusRepository->findOneById(TaskStatus::TASK_WAITE));
        $this->updateTask($task);
    }
    
    public function unlockTask(TaskQueue $task)
    {
        $this->taskQueueRepository->unlock($task);
    }
}