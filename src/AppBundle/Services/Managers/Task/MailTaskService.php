<?php

namespace AppBundle\Services\Managers\Task;


use CoreDomain\Services\Task\TaskServiceInterface;

class MailTaskService implements TaskServiceInterface
{
    private $serviceName = 'app.service.mail_task';
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function getServiceName()
    {
        return $this->serviceName;
    }

    public function execute(array $params)
    {
        $message = \Swift_Message::newInstance();

        $message->setSubject($params['subject']);
        $message->setFrom($params['from']);
        $message->setTo($params['to']);
        $message->setBody($params['body']);

        if($params['attachment']) {
            $message->attach(\Swift_Attachment::fromPath(
                $params['attachment']['file'],
                $params['attachment']['type']
                )
            );
        }

        $this->mailer->send($message);
    }
}