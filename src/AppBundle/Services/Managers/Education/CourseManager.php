<?php

namespace AppBundle\Services\Managers\Education;

use AppBundle\Services\MailSenderService;
use CoreDomain\DTO\Education\CourseDTO;
use CoreDomain\DTO\Education\LessonPartStatDTO;
use CoreDomain\DTO\Education\ProgressDTO;
use CoreDomain\DTO\Education\SearchUserProgressDTO;
use CoreDomain\DTO\Student\Education\CourseSearchDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\Exercise;
use CoreDomain\Repository\Education\CourseRepositoryInterface;
use CoreDomain\Repository\Education\ExerciseRepositoryInterface;
use CoreDomain\Repository\Education\LessonPartRepositoryInterface;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

use Spraed\PDFGeneratorBundle\PDFGenerator\PDFGenerator;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Validator\Constraints\DateTime;

class CourseManager
{
    const STAT_DESTINATION_DISPLAY = 'display';
    const STAT_DESTINATION_CSV = 'csv';
    const STAT_DESTINATION_PDF = 'pdf';
    const STAT_DESTINATION_EMAIL = 'email';

    private $em;
    private $serializer;
    private $userRepository;
    private $courseRepository;
    private $unitRepository;
    private $lessonRepository;
    private $lessonPartRepository;
    private $exerciseRepository;
    private $mailSenderService;
    private $pdfGenerator;

    public function __construct(
        EntityManagerInterface $em,
        Serializer $serializer,
        UserRepositoryInterface $userRepository,
        CourseRepositoryInterface $courseRepository,
        UnitRepositoryInterface $unitRepository,
        LessonRepositoryInterface $lessonRepository,
        LessonPartRepositoryInterface $lessonPartRepository,
        ExerciseRepositoryInterface $exerciseRepository,
        MailSenderService $mailSenderService,
        PDFGenerator $pdfGenerator
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->userRepository = $userRepository;
        $this->courseRepository = $courseRepository;
        $this->unitRepository = $unitRepository;
        $this->lessonRepository = $lessonRepository;
        $this->lessonPartRepository = $lessonPartRepository;
        $this->exerciseRepository = $exerciseRepository;
        $this->mailSenderService = $mailSenderService;
        $this->pdfGenerator = $pdfGenerator;
    }

    public function searchCourses(CourseSearchDTO $courseSearchDTO)
    {
        return $this->courseRepository->findAllByDTO($courseSearchDTO, false);
    }

    public function searchCoursesCount(CourseSearchDTO $courseSearchDTO)
    {
        return $this->courseRepository->findAllByDTO($courseSearchDTO, true);
    }

    public function getCourseById($id)
    {
        return $this->courseRepository->findOneById($id);
    }

    public function addCourse(CourseDTO $courseDTO)
    {
        if($this->courseRepository->findOneByName($courseDTO->name)) {
            throw new LogicException('Курс с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $course = new Course();
            $course->updateInfo($courseDTO->name);
            $this->courseRepository->addAndSave($course);
            $this->em->commit();
            return $course;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function deleteCourse($id)
    {
        $this->courseRepository->setDeletedById($id);
    }

    public function updateCourse(CourseDTO $courseDTO, $id)
    {
        $course = $this->courseRepository->findOneById($id);
        if(!$course) {
            throw new LogicException('Такого курса не существует.');
        }

        if(
            $course->getName() != $courseDTO->name &&
            $this->courseRepository->findOneByName($courseDTO->name)
        ) {
            throw new LogicException('Курс с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $course->updateInfo($courseDTO->name);
            $this->courseRepository->addAndSave($course);

            $this->em->commit();

            return $course;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function stat(SearchUserProgressDTO $searchDTO)
    {
        $stats = $this->exerciseRepository->getUserProgresStat($searchDTO);

        $progressItems = [];
        foreach ($stats as $stat) {
            $progressDto = new ProgressDTO();
            $progressDto
                ->setCourseNumber($stat['course_number'])
                ->setUnitNumber($stat['unit_number'])
                ->setLessonNumber($stat['lesson_number'])
                ->setLessonPartNumber($stat['lesson_part_number'])
            ;


            if ((int)$stat['course_id'] > 0) {
                $progressDto->setCourse($this->courseRepository->findOneById($stat['course_id']));
            }

            if ((int)$stat['unit_id'] > 0) {
                $progressDto->setUnit($this->unitRepository->findOneById($stat['unit_id']));
            }

            if ((int)$stat['lesson_id'] > 0) {
                $progressDto->setLesson($this->lessonRepository->findOneById($stat['lesson_id']));
            }

            if ((int)$stat['lesson_part_id'] > 0) {
                $progressDto->setLessonPart($this->lessonPartRepository->findOneById($stat['lesson_part_id']));
            }

            $lessonPartStat = new LessonPartStatDTO();
            $lessonPartStat->updateInfo($stat);

            $progressDto
                ->setLessonPartStat($lessonPartStat)
                ->setExerciseType($stat['exercise_type']);

            $progressItems[] = $progressDto;
        }

        return $progressItems;
    }

    public function statCount(SearchUserProgressDTO $searchDTO)
    {
        return $this->exerciseRepository->getUserProgresStat($searchDTO, true);
    }

    public function getStatResponse(SearchUserProgressDTO $searchDTO, $destination = self::STAT_DESTINATION_DISPLAY)
    {
        switch($destination) {
            case self::STAT_DESTINATION_DISPLAY:
                return $this->getStatDisplayResponse($searchDTO);
            case self::STAT_DESTINATION_CSV:
                return $this->getStatCsvResponse($searchDTO);
            case self::STAT_DESTINATION_PDF:
                return $this->getStatPdfResponse($searchDTO);
            case self::STAT_DESTINATION_EMAIL:
                return $this->getStatEmailResponse($searchDTO);
        }

        return $this->getStatDisplayResponse($searchDTO);
    }

    private function getStatDisplayResponse(SearchUserProgressDTO $searchDTO)
    {
        $progressItems = $this->stat($searchDTO);

        $content = $this->serializer->serialize(
            $progressItems,
            'json',
            SerializationContext::create()->setGroups(['api_student_course_stat'])
        );

        $response = new Response($content);
        $response->headers->set('X-total-count', $this->statCount($searchDTO));

        return $response;
    }

    private function getStatCsvResponse(SearchUserProgressDTO $searchDTO)
    {
        $items = $this->stat($searchDTO);

        $fileName = tempnam('/tmp', 'Progress');
        $handler = fopen($fileName, 'a');

        fputcsv($handler, ['Unit', 'Lesson', 'Процент готовности', 'Осталось выполнить'], ';');

        /** @var ProgressDTO $progressDto */
        foreach ($items as $progressDto) {
            $lessonPartStat = $progressDto->getLessonPartStat();

            fputcsv($handler, [
                'Unit ' . $progressDto->unitNumber,
                'Lesson ' . $progressDto->lessonNumber . ' ' . ($progressDto->exerciseType == Exercise::TYPE_HOMEWORK ? 'Homework' : '- ' . $progressDto->lessonPartNumber),
                $lessonPartStat->getCommonCompletedPercent() . '%',
                $lessonPartStat->remainingExercises() . ' упражнений'
            ], ';');
        }

        $response = new BinaryFileResponse($fileName);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            basename($fileName),
            iconv('UTF-8', 'ASCII//TRANSLIT', 'progress.csv')
        );

        return $response;
    }

    private function getStatPdfResponse(SearchUserProgressDTO $searchDTO)
    {
        return new Response($this->generatePdf($searchDTO), 200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'attachment; filename="stat.pdf"'
            )
        );
    }

    private function getStatEmailResponse(SearchUserProgressDTO $searchDTO)
    {
        $pdf = $this->generatePdf($searchDTO, true);
        $user = $this->userRepository->findOneById($searchDTO->getUserId());

        $this->mailSenderService->sendStatMail($user, $pdf);
    }

    private function generatePdf(SearchUserProgressDTO $searchDTO, $toFile = false)
    {
        $items = $this->stat($searchDTO);

        $html = "<table>";
        $html .= "<tr><td>Unit</td><td>Lesson</td><td>Процент готовности</td><td>Осталось выполнить</td></tr>";

        /** @var ProgressDTO $progressDto */
        foreach ($items as $progressDto) {
            $lessonPartStat = $progressDto->getLessonPartStat();
            $html .= "<tr>
                        <td>Unit {$progressDto->unitNumber}</td>
                        <td>Lesson {$progressDto->lessonNumber} ".($progressDto->exerciseType == Exercise::TYPE_HOMEWORK ? 'Homework' : '- ' . $progressDto->lessonPartNumber)."</td>
                        <td>{$lessonPartStat->getCommonCompletedPercent()}%</td>
                        <td>{$lessonPartStat->remainingExercises()} упражнений</td>
                        </tr>";
        }
        $html .= "</table>";

        $pdf =  $this->pdfGenerator->generatePDF($html);

        if($toFile) {
            $path = 'files/pdf/stat_'.$searchDTO->getUserId().'_'.time().'.pdf';
            file_put_contents($path, $pdf);
            return $path;
        }
        else {
            return $pdf;
        }
    }
}