<?php

namespace AppBundle\Services\Managers\Education;


use AppBundle\Services\Helpers\ExerciseTemplateHelper;
use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\ExerciseBindDTO;
use CoreDomain\DTO\Education\ExerciseRequestDTO;
use CoreDomain\DTO\Education\ExerciseSortDTO;
use CoreDomain\DTO\Education\ExerciseTemplate\ExerciseTemplateDTO;
use CoreDomain\DTO\Education\ExerciseTemplate\InputElementDTO;
use CoreDomain\DTO\Education\ExerciseTemplate\ReadWriteTemplateDTO;
use CoreDomain\DTO\Student\Education\ExerciseStatusDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Exception\ValidationException;
use CoreDomain\Model\Education\Exercise;
use CoreDomain\Model\Education\LessonExercise;
use CoreDomain\Model\Education\UserExercise;
use CoreDomain\Model\Education\UserLesson;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Dictionary\DictionaryRepositoryInterface;
use CoreDomain\Repository\Education\ExerciseRepositoryInterface;
use CoreDomain\Repository\Education\LessonPartRepositoryInterface;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use CoreDomain\Repository\Education\ExerciseTemplateRepositoryInterface;
use CoreDomain\Repository\Grammar\GrammarRepositoryInterface;
use CoreDomain\Repository\FileRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ExerciseManager
{
    private $em;
    public $exerciseRepository;
    public $lessonPartRepository;
    public $userRepository;
    public $unitRepository;
    public $lessonRepository;
    public $exerciseTemplateRepository;
    public $audioRepository;
    public $videoRepository;
    public $imageRepository;
    public $dictionaryRepository;
    public $grammarRepository;
    public $serializer;
    public $validator;

    public function __construct(
        EntityManagerInterface $em,
        ExerciseRepositoryInterface $exerciseRepository,
        LessonPartRepositoryInterface $lessonPartRepository,
        UserRepositoryInterface $userRepository,
        UnitRepositoryInterface $unitRepository,
        LessonRepositoryInterface $lessonRepository,
        ExerciseTemplateRepositoryInterface $exerciseTemplateRepository,
        FileRepositoryInterface $audioRepository,
        FileRepositoryInterface $videoRepository,
        FileRepositoryInterface $imageRepository,
        DictionaryRepositoryInterface $dictionaryRepository,
        GrammarRepositoryInterface $grammarRepository,
        Serializer $serializer,
        ValidatorInterface $validator
    )
    {
        $this->em = $em;
        $this->exerciseRepository = $exerciseRepository;
        $this->lessonPartRepository = $lessonPartRepository;
        $this->userRepository = $userRepository;
        $this->unitRepository = $unitRepository;
        $this->lessonRepository = $lessonRepository;
        $this->exerciseTemplateRepository = $exerciseTemplateRepository;
        $this->audioRepository = $audioRepository;
        $this->videoRepository = $videoRepository;
        $this->imageRepository = $imageRepository;
        $this->dictionaryRepository = $dictionaryRepository;
        $this->grammarRepository = $grammarRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function search($query = NULL, $lessonId = NULL, $limit = NULL, $offset = NULL)
    {
        return array(
            'data' => $this->exerciseRepository->findAll($query, $lessonId, false, $limit, $offset),
            'count' => $this->exerciseRepository->findAll($query, $lessonId, true, $limit, $offset)
        );
    }

    public function addExercise(ExerciseRequestDTO $exerciseRequestDTO)
    {
        if($this->exerciseRepository->findOneByName($exerciseRequestDTO->name)) {
            throw new LogicException('Упражнение с таким именем уже существует.');
        }
        /** @var ExerciseTemplateDTO $exerciseParamsDTO */
        $exerciseParamsDTO = $this->prepareParams($exerciseRequestDTO);

        $this->em->beginTransaction();
        try {
            $template = $this->exerciseTemplateRepository->findOneByType($exerciseRequestDTO->templateType);
            if($template) {
                $exercise = new Exercise();
                $audio = $this->audioRepository->findOneById($exerciseRequestDTO->taskAudio);
                $grammar = $this->grammarRepository->findOneById($exerciseRequestDTO->grammar);
                $dictionary = $this->dictionaryRepository->findOneById($exerciseRequestDTO->dictionary);

                $exercise->updateInfo(
                    $exerciseRequestDTO,
                    $audio, $grammar, $dictionary,
                    $exerciseParamsDTO,
                    $template
                );
                $this->exerciseRepository->addAndSave($exercise);
                $exercise->setParams($this->deserializeParams($exercise));
                $this->em->commit();

                return $exercise;
            }
            else {
                throw new LogicException('Тип шаблона упражнения не найден.');
            }
        } catch (\Exception $e) {
            $this->em->rollback();
            throw new LogicException($e->getMessage());
        }
    }

    public function getExercise($id)
    {
        $exercise = $this->exerciseRepository->findOneById($id);
        if(!$exercise) {
            throw new LogicException('No exercise entry');
        }

        $exercise->setParams($this->deserializeParams($exercise));
        return $exercise;
    }

    public function updateExercise($id, ExerciseRequestDTO $exerciseRequestDTO)
    {
        $exercise = $this->exerciseRepository->findOneById($id);
        if(!$exercise) {
            throw new Exception('No exercise template entry');
        }

        if(
            $exercise->getName() != $exerciseRequestDTO->name &&
            $this->exerciseRepository->findOneByName($exerciseRequestDTO->name)
        ) {
            throw new LogicException('Упражнение с таким именем уже существует.');
        }

        /** @var ExerciseTemplateDTO $exerciseParamsDTO */
        $exerciseParamsDTO = $this->prepareParams($exerciseRequestDTO);

        $this->em->beginTransaction();
        try {
            $template = $this->exerciseTemplateRepository->findOneByType($exerciseRequestDTO->templateType);
            if($template) {
                $audio = $this->audioRepository->findOneById($exerciseRequestDTO->taskAudio);
                $grammar = $this->grammarRepository->findOneById($exerciseRequestDTO->grammar);
                $dictionary = $this->dictionaryRepository->findOneById($exerciseRequestDTO->dictionary);

                $exercise->updateInfo(
                    $exerciseRequestDTO,
                    $audio, $grammar, $dictionary,
                    $exerciseParamsDTO,
                    $template
                );
                $this->exerciseRepository->addAndSave($exercise);
                $exercise->setParams($this->deserializeParams($exercise));
                $this->em->commit();

                return $exercise;
            }
        } catch (\Exception $e) {
            $this->em->rollback();
            throw new LogicException($e->getMessage());
        }
    }

    /**
     * Подготовка параметров шаблона для сохранения(валидация, сериализация)
     *
     * @param $exerciseRequestDTO
     * @return array<ExerciseTemplateDTO>
     * @throws ValidationException
     */
    public function prepareParams(ExerciseRequestDTO $exerciseRequestDTO)
    {
        $template = ExerciseTemplateHelper::getTemplateByName($exerciseRequestDTO->templateType);

        $errors = new ConstraintViolationList();
        $exerciseParams = [];
        foreach($exerciseRequestDTO->params as $params) {
            $paramsDTO = $this->serializer->deserialize(
                json_encode($params),
                $template->getClass(),
                'json',
                DeserializationContext::create()->setGroups(call_user_func(array($template->getClass(), 'getDeserializationGroup')))
            );

            $exerciseParams[] = $this->serializer->serialize(
                $paramsDTO,
                'json',
                SerializationContext::create()->setGroups(call_user_func(array($template->getClass(), 'getSerializationGroup'))
            ));
        }

        $errorsExercise = $this->validator->validate(
            $exerciseRequestDTO,
            null,
            call_user_func(array($template->getClass(), 'getValidationGroup'))
        );
        $errors->addAll($errorsExercise);

        if (count($errors) > 0) {
            throw new ValidationException('DTO validation error', $errors);
        }
        return $exerciseParams;
    }

    public function deserializeParams(Exercise $exercise) {
        $template = ExerciseTemplateHelper::getTemplateByName($exercise->template->getType());

        $exerciseParams = [];
        foreach ($exercise->getParams() as $param) {
            $param = $this->serializer->deserialize(
                $param,
                $template->getClass(),
                'json',
                DeserializationContext::create()->setGroups(
                    call_user_func(array($template->getClass(), 'getDeserializationGroup'))
                )
            );

            $exerciseParams[] = $this->collectDependency($param);
        }

        return $exerciseParams;
    }

    /**
     * Достает зависимости DTO и привязывает данные из БД в соответсвующие поля
     *
     * @param ExerciseTemplateDTO $paramsDTO
     * @return ExerciseTemplateDTO
     */
    public function collectDependency(ExerciseTemplateDTO $paramsDTO)
    {
        // TODO - витя обещал зарефакторить копипаст
        foreach ($paramsDTO->getDependencyFields() as $dependencyName => $dependency) {
            if(isset($dependency['included']) && $dependency['included']) {
                foreach ($dependency['field'] as &$field) {
                    foreach ($field->getDependencyFields() as $incDependencyName => $incDependency) {
                        if(is_array($incDependency['value']) && isset($incDependency['type']) && $incDependency['type'] == 'array') {
                            $depValues = array();
                            foreach ($incDependency['value'] as $depValue) {
                                $depValues[] =
                                    $this->getDependency(
                                        $incDependency['repository'],
                                        $depValue
                                    );
                            }
                            $field->$incDependencyName = $depValues;
                        }
                        else {
                            $field->$incDependencyName =
                                $this->getDependency(
                                    $incDependency['repository'],
                                    $incDependency['value']
                                );
                        }
                    }
                }
            } else {
                if(is_array($dependency['value']) && isset($dependency['type']) && $dependency['type'] == 'array') {
                    $depValues = array();
                    foreach ($dependency['value'] as $depValue) {
                        $depValues[] =
                            $this->getDependency(
                                $dependency['repository'],
                                $depValue
                            );
                    }
                    $paramsDTO->$dependencyName = $depValues;
                }
                else {
                    $paramsDTO->$dependencyName =
                        $this->getDependency(
                            $dependency['repository'],
                            $dependency['value']
                        );
                }
            }
        }

        return $paramsDTO;
    }

    /**
     * Поиск зависимости в базе
     *
     * @param $repository
     * @param $value
     * @return mixed
     */
    public function getDependency($repository, $value)
    {
        $repository = $this->getDependencyRepository($repository);
        return $repository->findOneById($value);
    }

    /**
     * Возвращает репозиторий по имени зависимости
     *
     * @param $dependency
     * @return DictionaryRepositoryInterface|FileRepositoryInterface|GrammarRepositoryInterface
     */
    public function getDependencyRepository($dependency)
    {
        switch ($dependency) {
            case 'audio':
                return $this->audioRepository;
                break;
            case 'video':
                return $this->videoRepository;
                break;
            case 'image':
                return $this->imageRepository;
                break;
            case 'dictionary':
                return $this->dictionaryRepository;
                break;
            case 'grammar':
                return $this->grammarRepository;
                break;
            default:
                throw new Exception('No ' . $dependency . ' dependency repository');
                break;
        }
    }

    public function bindByDTO(ExerciseBindDTO $exerciseBindDTO)
    {
        $lessonPart = $this->lessonPartRepository->findOneById($exerciseBindDTO->lessonPartId);
        $exercise = $this->exerciseRepository->findOneById($exerciseBindDTO->exerciseId);
        $lessonExercise = $this->exerciseRepository->findLessonExercise($lessonPart->getLesson(), $exercise);
        if($lessonExercise) {
            throw new LogicException("Связь урок-упражнение уже существует.");
        }

        $this->em->beginTransaction();
        try {
            $lessonExercise = new LessonExercise();

            $exercise = $this->exerciseRepository->findOneById($exerciseBindDTO->exerciseId);
            $lessonPart = $this->lessonPartRepository->findOneById($exerciseBindDTO->lessonPartId);
            $lesson = $lessonPart->getLesson();
            $maxPosition = $this->exerciseRepository->findMaxPosition($lesson->getId());

            if($exercise && $lesson && $lessonPart) {
                $lessonExercise->updateInfo($lesson, $exercise, $lessonPart, $maxPosition+1);
                $this->exerciseRepository->addAndSaveLessonExercise($lessonExercise);

                $this->em->commit();
            }
            else {
                throw new LogicException("Lesson, exercise or lesson part doesn't exists.");
            }
        } catch(\Exception $e) {
            $this->em->rollback();
            throw new LogicException($e->getMessage());
        }
    }

    public function unbindByDTO(ExerciseBindDTO $exerciseBindDTO)
    {
        $this->em->beginTransaction();
        try {
            $lessonExercise = $this->exerciseRepository->findLessonExerciseByDTO($exerciseBindDTO);

            if($lessonExercise) {
                $this->exerciseRepository->decrementPositions($lessonExercise->getLesson(), $lessonExercise->getPosition());
                $this->em->remove($lessonExercise);
                $this->em->flush();

                $this->em->commit();
            }
            else {
                throw new Exception("Relation doesn't exists.");
            }
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function sortByDTO(ExerciseSortDTO $exerciseSortDTO)
    {
        $this->exerciseRepository->sort($exerciseSortDTO);
    }

    public function start(ExerciseStatusDTO $exerciseStatusDTO, User $user)
    {
        $exercise = $this->exerciseRepository->findOneById($exerciseStatusDTO->exerciseId);
        $lesson = $this->lessonRepository->findOneById($exerciseStatusDTO->lessonId);
        $lessonExercise = $this->exerciseRepository->findLessonExercise($lesson, $exercise);
        if(!$lessonExercise) {
            throw new LogicException('Упражнение не принадлежит уроку!');
        }

        $course = $this->userRepository->getUserCourseByLesson($lesson, $user);
        if(!$course) {
            throw new LogicException('Упражнение не доступно пользователю!');
        }

        $userLesson = $this->lessonRepository->findUserLesson($lesson, $user);
        if(!$userLesson) {
            $userLesson = new UserLesson();
            $userLesson->setStartDate(new \DateTime());
            $userLesson->updateInfo($user, $lesson);
        }
        $userExercise = $this->exerciseRepository->findUserExercise($exercise, $user, $userLesson);
        if(!$userExercise) {
            $userExercise = new UserExercise();
            $userExercise->updateInfo($user, $exercise, $userLesson);
        }

        $this->em->beginTransaction();
        try {
            $userExercise->setStartDate(new \DateTime());
            $userExercise->setEndDate();
            $userExercise->setCompleted();
            $userExercise->updateStatus(UserExercise::STATUS_IN_PROGRESS);

            $userLesson->updateStatus(UserExercise::STATUS_IN_PROGRESS);

            $this->lessonRepository->addAndSaveUserLesson($userLesson);
            $this->exerciseRepository->addAndSaveUserExercise($userExercise);
            $this->em->commit();
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function finish(ExerciseStatusDTO $exerciseStatusDTO, User $user)
    {
        $exercise = $this->exerciseRepository->findOneById($exerciseStatusDTO->exerciseId);
        $lesson = $this->lessonRepository->findOneById($exerciseStatusDTO->lessonId);
        $lessonExercise = $this->exerciseRepository->findLessonExercise($lesson, $exercise);
        if(!$lessonExercise) {
            throw new LogicException('Упражнение не принадлежит уроку!');
        }

        $course = $this->userRepository->getUserCourseByLesson($lesson, $user);
        if(!$course) {
            throw new LogicException('Упражнение не доступно пользователю!');
        }
        $userLesson = $this->lessonRepository->findUserLesson($lesson, $user);
        if(!$userLesson) {
            throw new LogicException('Пользователь не начинал урок');
        }
        $userExercise = $this->exerciseRepository->findUserExercise($exercise, $user, $userLesson);
        if(!$userExercise) {
            throw new LogicException('Пользователь не начинал упражнение');
        }

        $this->em->beginTransaction();
        try {
            $userExercise->setCompleted($exerciseStatusDTO->completed);
            $userExercise->setEndDate(new \DateTime());
            $userExercise->updateStatus(UserExercise::STATUS_SUCCESS);

            $userLesson->setEndDate(new \DateTime());
            $userLesson->updateStatus(UserExercise::STATUS_SUCCESS);

            $this->lessonRepository->addAndSaveUserLesson($userLesson);
            $this->exerciseRepository->addAndSaveUserExercise($userExercise);
            $this->em->commit();
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function updateExercises()
    {
        $this->em->beginTransaction();
        try {
            $exercises = $this->em->createQueryBuilder()
                ->select('e')
                ->from(Exercise::class, 'e')
                ->where('e.template = 8')
                ->getQuery()->getResult();
            $template = new ReadWriteTemplateDTO();

            foreach ($exercises as $exercise ) {
                $params = $this->deserializeParams($exercise);
                $newParams = [];
                foreach ($params as $param) {
                    $param->answerBank = "";

                    $param = $this->serializer->serialize(
                        $param,
                        'json',
                        SerializationContext::create()->setGroups(call_user_func(array($template->getClass(), 'getSerializationGroup'))
                        ));

                    $newParams[] = $param;
                }

                $exercise->setParams($newParams);

                $this->exerciseRepository->addAndSave($exercise);
            }
            $this->em->commit();
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }
}