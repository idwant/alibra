<?php

namespace AppBundle\Services\Managers\Education;


use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\UnitBindDTO;
use CoreDomain\DTO\Education\UnitDTO;
use CoreDomain\DTO\Education\UnitSortDTO;
use CoreDomain\DTO\Student\Education\UnitSearchDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Exception\ValidationException;
use CoreDomain\Model\Education\Course;
use CoreDomain\Model\Education\CourseUnit;
use CoreDomain\Model\Education\Unit;
use CoreDomain\Repository\Education\CourseRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\Config\Definition\Exception\Exception;

class UnitManager
{
    private $em;
    private $unitRepository;
    private $courseRepository;
    private $userRepository;
    private $serializer;

    public function __construct(
        EntityManagerInterface $em,
        UnitRepositoryInterface $unitRepository,
        CourseRepositoryInterface $courseRepository,
        UserRepositoryInterface $userRepository,
        Serializer $serializer
    )
    {
        $this->em = $em;
        $this->unitRepository = $unitRepository;
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->serializer = $serializer;
    }

    public function searchUnits(UnitSearchDTO $unitSearchDTO)
    {
        return $this->unitRepository->findAllByDTO($unitSearchDTO, false);
    }

    public function searchUnitsCount(UnitSearchDTO $unitSearchDTO)
    {
        return $this->unitRepository->findAllByDTO($unitSearchDTO, true);
    }

    public function getUnitById($id)
    {
        return $this->unitRepository->findOneById($id);
    }

    public function bindUnitsByDTO($params)
    {
        $this->em->beginTransaction();
        try {
            $educationRequestDTOs = [];
            $courseUnits = [];
            foreach ($params as $param) {
                $educationRequestDTOs [] = $this->prepareEducationRequestDTO($param);
            }
            foreach($educationRequestDTOs as $educationRequestDTO) {
                $courseUnits[] = $this->bindUnit($educationRequestDTO);
            }
            $this->unitRepository->deleteCourseUnits($courseUnits);
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function addUnit(UnitDTO $unitDTO)
    {
        if($this->unitRepository->findOneByName($unitDTO->name)) {
            throw new LogicException('Юнит с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $unit = new Unit();
            $unit->updateInfo($unitDTO->name);
            $this->unitRepository->addAndSave($unit);
            $this->em->commit();
            
            return $unit;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function updateUnit(UnitDTO $unitDTO, $id)
    {
        $unit = $this->unitRepository->findOneById($id);
        if(!$unit) {
            throw new LogicException('Такого юнита не существует.');
        }

        if(
            $unit->getName() != $unitDTO->name &&
            $this->unitRepository->findOneByName($unitDTO->name)
        ) {
            throw new LogicException('Юнит с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $unit->updateInfo($unitDTO->name);
            $this->unitRepository->addAndSave($unit);

            $this->em->commit();
            
            return $unit;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function deleteUnit($id)
    {
        $this->unitRepository->setDeletedById($id);
    }

    private function prepareEducationRequestDTO($params)
    {
        $educationRequestDTO = $this->serializer->deserialize(
            json_encode($params),
            EducationRequestDTO::class,
            'json',
            DeserializationContext::create()->setGroups(array('api_education_request'))
        );

        return $educationRequestDTO;
    }

    private function prepareResponse($params, array $serializationGroups)
    {
        $responseData = $this->serializer->serialize(
            $params,
            'json',
            SerializationContext::create()->setGroups($serializationGroups)
        );
        return array(
            'responseData' => $responseData,
            'count' => count($params)
        );
    }
    
    public function bindByDTO(UnitBindDTO $unitBindDTO)
    {
        $courseUnit = $this->unitRepository->findCourseUnitByDTO($unitBindDTO);
        if($courseUnit) {
            throw new LogicException("Связь курс-юнит уже существует.");
        }

        $this->em->beginTransaction();
        try {
            $maxPosition = $this->unitRepository->findMaxPosition($unitBindDTO->courseId);
            $courseUnit = new CourseUnit();

            $unit = $this->unitRepository->findOneById($unitBindDTO->unitId);
            $course = $this->courseRepository->findOneById($unitBindDTO->courseId);

            if($unit && $course) {
                $courseUnit->updateInfo($course, $unit, $maxPosition+1);
                $this->unitRepository->addAndSaveCourseUnit($courseUnit);
            }
            else {
                throw new LogicException("Course or unit doesn't exists.");
            }

            $this->em->commit();
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function unbindByDTO(UnitBindDTO $unitBindDTO)
    {
        $this->em->beginTransaction();
        try {
            $courseUnit = $this->unitRepository->findCourseUnitByDTO($unitBindDTO);
            if($courseUnit) {
                $this->unitRepository->decrementPositions($courseUnit->getCourse(), $courseUnit->getPosition());
                $this->em->remove($courseUnit);
                $this->em->flush();

                $this->em->commit();
            }
            else {
                throw new Exception("Relation doesn't exists.");
            }
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function sortByDTO(UnitSortDTO $unitSortDTO)
    {
        $this->unitRepository->sort($unitSortDTO);
    }
}