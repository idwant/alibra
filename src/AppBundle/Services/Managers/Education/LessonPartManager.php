<?php

namespace AppBundle\Services\Managers\Education;

use AppBundle\Services\Helpers\LessonPartHelper;
use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\ExerciseBindDTO;
use CoreDomain\DTO\Education\LessonPartDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\Education\LessonLessonPart;
use CoreDomain\Model\Education\LessonPart;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Repository\Education\ExerciseRepositoryInterface;
use CoreDomain\Repository\Education\LessonPartRepositoryInterface;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class LessonPartManager
{
    private $em;
    private $lessonPartRepository;
    private $lessonRepository;
    private $unitRepository;
    private $exerciseRepository;
    private $serializer;

    public function __construct(
        EntityManagerInterface $em,
        LessonPartRepositoryInterface $lessonPartRepository,
        LessonRepositoryInterface $lessonRepository,
        UnitRepositoryInterface $unitRepository,
        ExerciseRepositoryInterface $exerciseRepository,
        ExerciseManager $exerciseManager,
        Serializer $serializer
    )
    {
        $this->em = $em;
        $this->lessonPartRepository = $lessonPartRepository;
        $this->lessonRepository = $lessonRepository;
        $this->unitRepository = $unitRepository;
        $this->exerciseRepository = $exerciseRepository;
        $this->exerciseManager = $exerciseManager;
        $this->serializer = $serializer;
    }

    public function getLessonPartsByDTO($params, $limit = null, $offset = null)
    {
        $dto = $this->prepareEducationRequestDTO($params);
        $lessonParts = $this->lessonPartRepository->findAllByDTO($dto, $limit, $offset);
        return $this->prepareResponse($lessonParts, array('api_lesson_parts_list'));
    }

    public function getLessonPartById($id)
    {
        return $this->lessonPartRepository->findOneById($id);
    }

    public function getLessonPartsByLesson($lessonId)
    {
        return $this->lessonPartRepository->findByLessonId($lessonId);
    }

    public function bindLessonPartsByDTO($params)
    {
        $this->em->beginTransaction();
        try {
            $educationRequestDTOs = [];
            $lessonLessonParts = [];

            foreach ($params as $param) {
                $educationRequestDTOs [] = $this->prepareEducationRequestDTO($param);
            }
            foreach ($educationRequestDTOs as $educationRequestDTO) {
                $lessonLessonParts[] = $this->bindLessonPart($educationRequestDTO);
            }
            $this->lessonPartRepository->deleteLessonLessonParts($lessonLessonParts);
            $this->em->commit();
        }
        catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function addLessonPart(LessonPartDTO $lessonPartDTO)
    {
        $lessonPart = $this->lessonPartRepository->findByName($lessonPartDTO);
        if($lessonPart) {
            throw new LogicException('Lesson Part с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $lesson = $this->lessonRepository->findOneById($lessonPartDTO->lessonId);

            if($lesson) {
                $lessonPart = new LessonPart();
                $lessonPart->updateInfo(
                    $lessonPartDTO->name,
                    $lessonPartDTO->type,
                    //LessonPartHelper::getLessonPartTypeByName($lessonPartDTO->type),
                    $lesson
                );

                $this->lessonPartRepository->addAndSave($lessonPart);
                $this->em->commit();
                
                return $lessonPart;
            }
            else {
                throw new LogicException('Урока не существует.');
            }
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function updateLessonPart(LessonPartDTO $lessonPartDTO, $id)
    {
        $this->em->beginTransaction();
        try {
            $lessonPart = $this->lessonPartRepository->findOneById($id);
            $lesson = $this->lessonRepository->findOneById($lessonPartDTO->lessonId);

            if($lessonPart && $lesson) {
                $lessonPart->updateInfo(
                    $lessonPartDTO->name,
                    $lessonPartDTO->type,
                    //LessonPartHelper::getLessonPartTypeByName($lessonPartDTO->type),
                    $lesson
                );
                $this->lessonPartRepository->addAndSave($lessonPart);
            }
            $this->em->commit();

            return $lessonPart;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function deleteLessonPart($id)
    {
        $lessonPart = $this->lessonPartRepository->findOneById($id);
        $lessonExercises = $this->exerciseRepository->findLessonExerciseByLessonPart($lessonPart);

        if($lessonExercises) {
            foreach ($lessonExercises as $lessonExercise) {
                $exerciseBindDTO = new ExerciseBindDTO();
                $exerciseBindDTO->exerciseId = $lessonExercise->getExercise()->getId();
                $exerciseBindDTO->lessonPartId = $lessonExercise->getLessonPart()->getId();

                $this->exerciseManager->unbindByDTO($exerciseBindDTO);
            }
        }

        $lessonPart->delete();
        $this->lessonPartRepository->addAndSave($lessonPart);
    }

    private function prepareEducationRequestDTO($params)
    {
        $educationRequestDTO = $this->serializer->deserialize(
            json_encode($params),
            EducationRequestDTO::class,
            'json',
            DeserializationContext::create()->setGroups(array('api_education_request'))
        );

        return $educationRequestDTO;
    }

    private function prepareResponse($params, array $serializationGroups)
    {
        $responseData = $this->serializer->serialize(
            $params,
            'json',
            SerializationContext::create()->setGroups($serializationGroups)
        );
        return array(
            'responseData' => $responseData,
            'count' => count($params)
        );
    }
}