<?php

namespace AppBundle\Services\Managers\Education;

use CoreDomain\DTO\Dashboard\DashboardItemDTO;
use CoreDomain\DTO\Dashboard\DashboardSearchDTO;
use CoreDomain\Repository\Education\CourseRepositoryInterface;
use CoreDomain\Repository\Education\ExerciseRepositoryInterface;
use CoreDomain\Repository\Education\LessonPartRepositoryInterface;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class DashboardManager
{
    private $em;
    private $userRepository;
    private $courseRepository;
    private $unitRepository;
    private $lessonRepository;
    private $lessonPartRepository;
    private $exerciseRepository;

    public function __construct(EntityManagerInterface $em,
                                UserRepositoryInterface $userRepository,
                                CourseRepositoryInterface $courseRepository,
                                UnitRepositoryInterface $unitRepository,
                                LessonRepositoryInterface $lessonRepository,
                                LessonPartRepositoryInterface $lessonPartRepository,
                                ExerciseRepositoryInterface $exerciseRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->courseRepository = $courseRepository;
        $this->unitRepository = $unitRepository;
        $this->lessonRepository = $lessonRepository;
        $this->lessonPartRepository = $lessonPartRepository;
        $this->exerciseRepository = $exerciseRepository;
    }

    public function getCourses(DashboardSearchDTO $searchDTO)
    {
        $stats = $this->exerciseRepository->findCoursesStats($searchDTO);

        $courses = [];
        foreach ($stats as $stat) {
            $course = $this->courseRepository->findOneById($stat['id']);
            
            $dashboard = new DashboardItemDTO();
            $dashboard->updateInfo($stat);

            $course->setDashboard($dashboard);

            $courses[] = $course;
        }

        return $courses;
    }

    public function getUnits(DashboardSearchDTO $searchDTO)
    {
        $stats = $this->exerciseRepository->findUnitsStats($searchDTO);

        $units = [];
        foreach ($stats as $stat) {
            $unit = $this->unitRepository->findOneById($stat['id']);

            $dashboard = new DashboardItemDTO();
            $dashboard->updateInfo($stat);

            $unit->setDashboard($dashboard);

            $units[] = $unit;
        }

        return $units;
    }

    public function getLessons(DashboardSearchDTO $searchDTO)
    {
        $stats = $this->exerciseRepository->findLessonsStats($searchDTO);

        $lessons = [];
        foreach ($stats as $stat) {
            $lesson = $this->lessonRepository->findOneById($stat['id']);

            $dashboard = new DashboardItemDTO();
            $dashboard->updateInfo($stat);

            $lesson->setDashboard($dashboard);

            $lessons[] = $lesson;
        }

        return $lessons;
    }

    public function getLessonParts(DashboardSearchDTO $searchDTO)
    {
        $stats = $this->exerciseRepository->findLessonPartsStats($searchDTO);

        $lessonParts = [];
        foreach ($stats as $stat) {
            $lessonPart = $this->lessonPartRepository->findOneById($stat['id']);

            $dashboard = new DashboardItemDTO();
            $dashboard->updateInfo($stat);

            $lessonPart->setDashboard($dashboard);

            $lessonParts[] = $lessonPart;
        }

        return $lessonParts;
    }
}
