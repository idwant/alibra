<?php

namespace AppBundle\Services\Managers\Education;

use AppBundle\Services\Managers\User\UserManager;
use CoreDomain\DTO\Education\EducationRequestDTO;
use CoreDomain\DTO\Education\LessonBindDTO;
use CoreDomain\DTO\Education\LessonDTO;
use CoreDomain\DTO\Education\LessonSortDTO;
use CoreDomain\DTO\Student\Education\LessonSearchDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\Education\Lesson;
use CoreDomain\Model\Education\Unit;
use CoreDomain\Model\Education\UnitLesson;
use CoreDomain\Repository\Education\LessonRepositoryInterface;
use CoreDomain\Repository\Education\UnitRepositoryInterface;
use CoreDomain\Repository\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\Config\Definition\Exception\Exception;

class LessonManager
{

    private $em;
    private $lessonRepository;
    private $unitRepository;
    private $userRepository;
    private $serializer;
    private $userManager;

    public function __construct(
        EntityManagerInterface $em,
        LessonRepositoryInterface $lessonRepository,
        UnitRepositoryInterface $unitRepository,
        UserRepositoryInterface $userRepository,
        Serializer $serializer,
        UserManager $userManager
    )
    {
        $this->em = $em;
        $this->lessonRepository = $lessonRepository;
        $this->unitRepository = $unitRepository;
        $this->userRepository = $userRepository;
        $this->serializer = $serializer;
        $this->userManager = $userManager;
    }

    public function searchLessons(LessonSearchDTO $lessonSearchDTO)
    {
        return $this->lessonRepository->findAllByDTO($lessonSearchDTO, false);
    }

    public function searchLessonsCount(LessonSearchDTO $lessonSearchDTO)
    {
        return $this->lessonRepository->findAllByDTO($lessonSearchDTO, true);
    }

    public function getLessonById($id)
    {
        return $this->lessonRepository->findOneById($id);
    }

    public function addLesson(LessonDTO $lessonDTO)
    {
        if($this->lessonRepository->findOneByName($lessonDTO->name)) {
            throw new LogicException('Урок с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $lesson = new Lesson();
            $lesson->updateInfo($lessonDTO->name);
            $this->lessonRepository->addAndSave($lesson);
            $this->em->commit();
            
            return $lesson;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function updateLesson(LessonDTO $lessonDTO, $id)
    {
        $lesson = $this->lessonRepository->findOneById($id);
        if(!$lesson) {
            throw new LogicException('Такого урока не существует.');
        }

        if(
            $lesson->getName() != $lessonDTO->name &&
            $this->lessonRepository->findOneByName($lessonDTO->name)
        ) {
            throw new LogicException('Урок с таким именем уже существует.');
        }

        $this->em->beginTransaction();
        try {
            $lesson->updateInfo($lessonDTO->name);
            $this->lessonRepository->addAndSave($lesson);

            $this->em->commit();
            
            return $lesson;
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function deleteLesson($id)
    {
        $this->lessonRepository->setDeletedById($id);
    }

    private function prepareEducationRequestDTO($params)
    {
        $educationRequestDTO = $this->serializer->deserialize(
            json_encode($params),
            EducationRequestDTO::class,
            'json',
            DeserializationContext::create()->setGroups(array('api_education_request'))
        );

        return $educationRequestDTO;
    }

    private function prepareResponse($params, array $serializationGroups)
    {
        $responseData = $this->serializer->serialize(
            $params,
            'json',
            SerializationContext::create()->setGroups($serializationGroups)
        );
        return array(
            'responseData' => $responseData,
            'count' => count($params)
        );
    }

    public function bindByDTO(LessonBindDTO $lessonBindDTO)
    {
        $unitLesson = $this->lessonRepository->findUnitLessonByDTO($lessonBindDTO);
        if($unitLesson) {
            throw new LogicException("Связь юнит-урок уже существует.");
        }

        $this->em->beginTransaction();
        try {
            $maxPosition = $this->lessonRepository->findMaxPosition($lessonBindDTO->unitId);
            $unitLesson = new UnitLesson();

            $unit = $this->unitRepository->findOneById($lessonBindDTO->unitId);
            $lesson = $this->lessonRepository->findOneById($lessonBindDTO->lessonId);

            if($unit && $lesson) {
                $unitLesson->updateInfo($unit, $lesson, $maxPosition+1);
                $this->lessonRepository->addAndSaveUnitLesson($unitLesson);
            }
            else {
                throw new Exception("Unit or lesson doesn't exists.");
            }

            $this->em->commit();
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function unbindByDTO(LessonBindDTO $lessonBindDTO)
    {
        $this->em->beginTransaction();
        try {
            $unitLesson = $this->lessonRepository->findUnitLessonByDTO($lessonBindDTO);
            if($unitLesson) {
                $this->lessonRepository->decrementPositions($unitLesson->getUnit(), $unitLesson->getPosition());
                $this->em->remove($unitLesson);
                $this->em->flush();

                $this->em->commit();
            }
            else {
                throw new Exception("Relation doesn't exists.");
            }
        } catch(\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function sortByDTO(LessonSortDTO $lessonSortDTO)
    {
        $this->lessonRepository->sort($lessonSortDTO);
    }
}