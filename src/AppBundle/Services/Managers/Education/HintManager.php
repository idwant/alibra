<?php

namespace AppBundle\Services\Managers\Education;


use CoreDomain\DTO\Education\HintDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\Education\Hint;
use CoreDomain\Model\Education\UserHint;
use CoreDomain\Model\User\User;
use CoreDomain\Repository\Education\HintRepositoryInterface;

class HintManager
{
    const START_EXERCISE_HINT = 'start_exercise';
    const RECORD_AUDIO_HINT = 'record_audio';

    private $hintRepository;

    public function __construct(HintRepositoryInterface $hintRepository)
    {
        $this->hintRepository = $hintRepository;
    }

    public function getHintsWithUser(User $user)
    {
        return $this->hintRepository->findWithUser($user);
    }

    public function updateUserHint(User $user, HintDTO $hintDTO)
    {
        /** @var Hint $hint */
        $hint = $this->hintRepository->findOneByName($hintDTO->name);

        if($hint) {
            $userHint = $this->hintRepository->findUserHint($user, $hintDTO->name);
            if(!$userHint) {
                $userHint = new UserHint($user, $hint);
            }
            $userHint->incrementAttempts();
            if($hint->getTimeout()) {
                $userHint->setShowAgainDate();
            }
            $this->hintRepository->addAndSaveUserHint($userHint);
        }
        else {
            throw new LogicException('Нет подсказки с таким именем');
        }
    }
}