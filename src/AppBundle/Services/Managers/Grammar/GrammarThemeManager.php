<?php

namespace AppBundle\Services\Managers\Grammar;


use CoreDomain\DTO\Grammar\GrammarThemeDTO;
use CoreDomain\DTO\Grammar\GrammarThemeSearchDTO;
use CoreDomain\Model\Grammar\GrammarTheme;
use CoreDomain\Repository\Grammar\GrammarSectionRepositoryInterface;
use CoreDomain\Repository\Grammar\GrammarThemeRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class GrammarThemeManager
{
    private $em;
    private $grammarThemeRepository;
    private $grammarSectionRepository;

    public function __construct(EntityManagerInterface $em,
                                GrammarThemeRepositoryInterface $grammarThemeRepository,
                                GrammarSectionRepositoryInterface $grammarSectionRepository)
    {
        $this->em = $em;
        $this->grammarThemeRepository = $grammarThemeRepository;
        $this->grammarSectionRepository = $grammarSectionRepository;
    }

    public function listGrammarThemes(GrammarThemeSearchDTO $searchDTO) {
        return array(
            'data' => $this->grammarThemeRepository->search($searchDTO, false),
            'count' => $this->grammarThemeRepository->search($searchDTO, true)
        );
    }

    public function getGrammarThemeById($id)
    {
        return $this->grammarThemeRepository->findOneById($id);
    }

    public function getGrammarThemesBySection($sectionId)
    {
        $grammarSection = $this->grammarSectionRepository->findOneById($sectionId);

        $grammarThemes = [];
        if($grammarSection) {
            $grammarThemes = $this->grammarThemeRepository->findAllBySection($grammarSection);
        }
        return $grammarThemes;
    }

    public function addGrammarTheme(GrammarThemeDTO $grammarThemeDTO)
    {
        $this->em->beginTransaction();
        try {
            $section = $this->grammarSectionRepository->findOneById($grammarThemeDTO->section);

            if(!$section) {
                throw new \Exception("No Grammar Section entry!");
            }

            $grammarTheme = new GrammarTheme();
            $grammarTheme->updateInfo(
                $grammarThemeDTO->name,
                $section
            );
            $this->grammarThemeRepository->addAndSave($grammarTheme);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $grammarTheme;
    }

    public function updateGrammarTheme(GrammarThemeDTO $grammarThemeDTO, $id)
    {
        $this->em->beginTransaction();
        try {
            $section = $this->grammarSectionRepository->findOneById($grammarThemeDTO->section);
            $grammarTheme = $this->grammarThemeRepository->findOneById($id);

            if(!($section && $grammarTheme)) {
                throw new \Exception("No GrammarSection or GrammarTheme entry");
            }

            $grammarTheme->updateInfo(
                $grammarThemeDTO->name,
                $section
            );
            $this->grammarThemeRepository->addAndSave($grammarTheme);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $grammarTheme;
    }

    public function deleteGrammarThemeById($id)
    {
        $this->grammarThemeRepository->deleteById($id);
    }
}