<?php

namespace AppBundle\Services\Managers\Grammar;


use CoreDomain\DTO\Grammar\GrammarSearchDTO;
use CoreDomain\Model\Grammar;
use CoreDomain\DTO\Grammar\GrammarDTO;
use CoreDomain\Repository\FileRepositoryInterface;
use CoreDomain\Repository\Grammar\GrammarRepositoryInterface;
use CoreDomain\Repository\Grammar\GrammarThemeRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class GrammarManager
{
    private $em;
    private $grammarRepository;
    private $grammarThemeRepository;
    private $videoRepository;

    public function __construct(EntityManagerInterface $em, GrammarRepositoryInterface $grammarRepository,
                                GrammarThemeRepositoryInterface $grammarThemeRepository,
                                FileRepositoryInterface $videoRepository)
    {
        $this->em = $em;
        $this->grammarRepository = $grammarRepository;
        $this->grammarThemeRepository = $grammarThemeRepository;
        $this->videoRepository = $videoRepository;
    }

    public function getGrammarById($id)
    {
        return $this->grammarRepository->findOneById($id);
    }

    public function getGrammarsByTheme($themeId)
    {
        $theme = $this->grammarThemeRepository->findOneById($themeId);

        $grammars = [];
        if($theme) {
            $grammars = $this->grammarRepository->findAllByTheme($theme);
        }

        return $grammars;
    }

    public function addGrammar(GrammarDTO $grammarDTO)
    {
        $this->em->beginTransaction();
        try {
            $theme = $this->grammarThemeRepository->findOneById($grammarDTO->theme);
            $video = $this->videoRepository->findOneById($grammarDTO->video);

            if(!$theme) {
                throw new \Exception("No GrammarTheme entry");
            }

            $grammar = new Grammar\Grammar();
            $grammar->updateInfo(
                $grammarDTO->title,
                $grammarDTO->content,
                $theme,
                $video
            );
            $this->grammarRepository->addAndSave($grammar);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $grammar;
    }

    public function updateGrammar(GrammarDTO $grammarDTO, $id)
    {
        $this->em->beginTransaction();
        try {
            $theme = $this->grammarThemeRepository->findOneById($grammarDTO->theme);
            /** @var Grammar\Grammar $grammar */
            $grammar = $this->grammarRepository->findOneById($id);
            $video = $this->videoRepository->findOneById($grammarDTO->video);

            if(!($theme && $grammar)) {
                throw new \Exception("No GrammarTheme or Grammar entry");
            }

            $grammar->updateInfo(
                $grammarDTO->title,
                $grammarDTO->content,
                $theme,
                $video,
                $grammar->getIsDeleted(),
                $grammarDTO->isDemo
            );
            $this->grammarRepository->addAndSave($grammar);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $grammar;
    }

    public function deleteGrammarById($id)
    {
        $this->grammarRepository->deleteById($id);
    }

    public function searchGrammars(GrammarSearchDTO $grammarSearchDTO, $limit = null, $offset = null)
    {
        return $this->grammarRepository->search($grammarSearchDTO, false, $limit, $offset);
    }
    
    public function searchGrammarsCount(GrammarSearchDTO $grammarSearchDTO, $limit = null, $offset = null) {
        return $this->grammarRepository->search($grammarSearchDTO, true, $limit, $offset);
    }
}