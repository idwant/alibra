<?php

namespace AppBundle\Services\Managers\Grammar;


use AppBundle\Repository\Grammar\GrammarSectionRepository;
use CoreDomain\DTO\Grammar\GrammarSectionDTO;
use CoreDomain\DTO\Grammar\GrammarSectionSearchDTO;
use CoreDomain\Exception\LogicException;
use CoreDomain\Model\Grammar\GrammarSection;
use Doctrine\ORM\EntityManagerInterface;

class GrammarSectionManager
{
    private $em;
    private $grammarSectionRepository;

    public function __construct(EntityManagerInterface $em,
                                GrammarSectionRepository $grammarSectionRepository)
    {
        $this->em = $em;
        $this->grammarSectionRepository = $grammarSectionRepository;
    }

    public function getAllGrammarSections(GrammarSectionSearchDTO $searchDTO)
    {
        return array(
            'data' => $this->grammarSectionRepository->search($searchDTO, false),
            'count' => $this->grammarSectionRepository->search($searchDTO, true)
        );
    }

    public function getGrammarSectionById($id)
    {
        return $this->grammarSectionRepository->findOneById($id);
    }

    public function addGrammarSection(GrammarSectionDTO $grammarSectionDTO)
    {
        if ($this->grammarSectionRepository->findOneByName($grammarSectionDTO->name)) {
            throw new LogicException("Секция с таким именем уже существует");
        }
        $this->em->beginTransaction();
        try {
            $grammarSection = new GrammarSection();
            $grammarSection->updateInfo(
                $grammarSectionDTO->name
            );
            $this->grammarSectionRepository->addAndSave($grammarSection);
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $grammarSection;
    }

    public function updateGrammarSection(GrammarSectionDTO $grammarSectionDTO, $id)
    {
        $this->em->beginTransaction();
        try {
            $grammarSection = $this->grammarSectionRepository->findOneById($id);
            if($grammarSection) {
                $grammarSection->updateInfo(
                    $grammarSectionDTO->name
                );
                $this->grammarSectionRepository->addAndSave($grammarSection);
            }
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
        return $grammarSection;
    }

    public function deleteGrammarSection($id)
    {
        $this->grammarSectionRepository->deleteById($id);
    }
}