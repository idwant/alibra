<?php

namespace AppBundle\Services\Daemon;

use CoreDomain\Services\Daemon\Worker;
use AppBundle\Services\Managers\Task\TaskQueueManager;
use CoreDomain\Model\Task\TaskQueue;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use CoreDomain\Services\Task\TaskServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class TaskWorker extends Worker
{
    protected $workerName = 'myWorker';
    
    private $taskQueueManager;
    private $container;
    private $em;

    public function __construct($appDir, TaskQueueManager $taskQueueManager, Container $container, EntityManagerInterface $em)
    {
        $this->appDir = $appDir;
        $this->taskQueueManager = $taskQueueManager;
        $this->container = $container;
        $this->em = $em;
    }

    protected function getTasksToLaunch($count)
    {
        $tasks = [];
        for ($i = 0; $i < $count; $i++) {
            $task = $this->taskQueueManager->getTask();
            if($task){
                $tasks[] = $task;
            } else {
                return $tasks;
            }
        }
        return $tasks;
    }

    /**
     * @param $task TaskQueue
     */
    protected function runTask($task)
    {
        if($this->container->has($task->getService())) {
            $service = $this->container->get($task->getService());
            if($service instanceof TaskServiceInterface) {
                try{
                    /**@var $service TaskServiceInterface*/
                    $service->execute($task->getParams());
                    $this->taskQueueManager->finishTask($task);
                } catch (\Exception $e){
                    $this->taskQueueManager->failTask($task);
                }
            }
        }  
    }

    /**
     * @param $task TaskQueue
     * @param $status
     */
    protected function finishTask($task, $status)
    {
        $this->taskQueueManager->unlockTask($task);
    }
    
    protected function openResource()
    {
        $this->em->getConnection()->connect();
    }
    
    protected function closeResource()
    {
        $this->em->getConnection()->close();
    }
    
    protected function initWorkerPause()
    {
        if (count($this->runningProcesses) == 0 && count($this->finishedProcesses) == 0) {
            exit;
        }
    }
}