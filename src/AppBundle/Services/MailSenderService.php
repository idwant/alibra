<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Services\Managers\Task\TaskQueueManager;
use AppBundle\Services\Managers\Task\MailTaskService;
use CoreDomain\DTO\Task\TaskQueueDTO;
use CoreDomain\Model\User\User;
use CoreDomain\Model\Common\Feedback;

class MailSenderService
{
    private $container;
    private $taskQueueManager;
    private $mailTaskService;

    public function __construct(ContainerInterface $container, TaskQueueManager $taskQueueManager, MailTaskService $mailTaskService)
    {
        $this->container = $container;
        $this->taskQueueManager = $taskQueueManager;
        $this->mailTaskService = $mailTaskService;
    }

    public function sendRegisterMail(User $user)
    {
        $taskParams = new TaskQueueDTO();
        $taskParams->params = [
            'to' => $user->getEmail(),
            'from' => $this->container->getParameter('register_mail'),
            'subject' => 'User registration',
            'body' => 'Test'
        ];
        $taskParams->service = $this->mailTaskService->getServiceName();
        $this->taskQueueManager->addTask($taskParams);
    }

    public function sendFirstLoginMail(User $user)
    {
        $taskParams = new TaskQueueDTO();
        $taskParams->params = [
            'to' => $user->getEmail(),
            'from' => $this->container->getParameter('register_mail'),
            'subject' => 'User first login',
            'body' => 'Test'
        ];
        $taskParams->service = $this->mailTaskService->getServiceName();
        $this->taskQueueManager->addTask($taskParams);
    }

    public function sendFeedbackMail(Feedback $feedback)
    {
        $taskParams = new TaskQueueDTO();
        $taskParams->params = [
            'to' => $this->container->getParameter('feedback_mail'),
            'from' => $feedback->getEmail(),
            'subject' => $feedback->getTitle(),
            'body' => $feedback->getMessage()
        ];
        $taskParams->service = $this->mailTaskService->getServiceName();
        $this->taskQueueManager->addTask($taskParams);
    }

    public function sendStatMail(User $user, $path)
    {
        $taskParams = new TaskQueueDTO();
        $taskParams->params = [
            'to' => $user->getEmail(),
            'from' => $this->container->getParameter('stat_mail'),
            'subject' => 'Your statistic',
            'body' => 'Test',
            'attachment' => [
                'file' => $path,
                'type' => 'application/pdf'
            ]
        ];
        $taskParams->service = $this->mailTaskService->getServiceName();
        $this->taskQueueManager->addTask($taskParams);
    }
}