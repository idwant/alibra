Грамматический справочник (GrammarSection)
==========================================
GET - /api/grammar-sections - список всех грамматических справочников
    
    {query} - название справочника (для поиска)
    {limit}
    {offset} 

GET - /api/grammar-sections/{$id} - получить грамматический справочник по _id_

POST - /api/grammar-sections - создать грамматический справочник

PATCH - /api/grammar-sections/{$id} - обновить грамматический справочник

DELETE - /api/grammar-sections/{$id} - удалить словарь

* Добавление/обновление (пример тела запроса)


```
{
  "name" : "Verbs"
}

```
_name_ - обязательное поле.

Тема грамматики (GrammarTheme)
==============================
GET - /api/grammar-themes - получить все темы грамматики
 
    {query} - название темы (для поиска)
    {section} - id грамматического справочника (GrammarSection)
    {limit}
    {offset}

GET - /api/grammar-themes/{id} - получить тему грамматики по _id_

POST - /api/grammar-themes - добавить грамматическую темы

PATCH - /api/grammar-themes/{id} - обновить данные по теме грамматики

DELETE - /api/grammar-themes/{id} - удалить тему грамматики

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "To be verbs",
  "section" : 1
}

```
где _section_ - id на таблицу GrammarSection

Грамматика (Grammar)
=========================
GET - /api/grammars/{id} - получить грамматику по _id_
 
GET - /api/grammars - поиск грамматик

    {query} - пока что поиск по названию грамматики
    {title} - название грамматики
    {content} - строка для поиска по содержанию
    {theme} - id темы грамматики (GrammarTheme)
    {limit}
    {offset}

POST - /api/grammars - добавить грамматику

PATCH - /api/grammars/{id} - обновить данные по грамматике

DELETE - /api/grammars/{id} - удалить грамматику

* Добавление/обновление (пример тела запроса)

```
{
  "title" : "Was/were",
  "content" : "Some content",
  "theme" : 1,
  "video" : 1
}

```
где _theme_ - id на таблицу GrammarTheme
    _video_ - id на таблицу Video