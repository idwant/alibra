Словарь (Dictionary)
====
GET - /api/dictionaries - список всех словарей
    
    {name} - название словаря (для поиска)
    {limit}
    {offset}

GET - /api/dictionaries/{$id} - получить словарь по _id_

POST - /api/dictionaries - создать словарь

PATCH - /api/dictionaries/{$id} - обновить словарь

DELETE - /api/dictionaries/{$id} - удалить словарь

POST - /api/dictionaries/{dictionaryId}/words/{wordId} - добавление слова в словарь
  
  {dictionaryId} - id словаря
  {wordId} - id слова

GET - /api/dictionaries/{dictionaryId}/words?word={word} - список слов в словаре
 
  {dictionaryId} - id cловаря
  {word} - строка для поиска слова
  {limit}
  {offset}
  
DELETE - /api/dictionaries/{dictionaryId}/words/{wordId} - удаление слова из словаря
  
* Добавление/обновление (пример тела запроса)


```
{
  "name" : "English-Russian"
}

```
_name_ - обязательное поле.

Слово (Word)
====
GET - /api/words/{$id} - получить слово по _id_

POST - /api/words - добавить слово в словарь

GET - /api/words?word={word}&dictionary={dictionaryId} - поиск слов в словаре
    
    {word} - строка для поиска
    {dictionaryId} - id словаря, в котором искать слова

Если параметр _word_ не указан, вернутся все слова в словаре.
Если параметр _dictionary_ не указан, вернутся совпадения слов из всех словарей.

PATCH - /api/words/{$id} - обновить данные по слову

DELETE - /api/words/{$id} - удалить слово из словоря

* Добавление/обновление (пример тела запроса)

```
{
  "word" : "go",
  "transctiption" : "transcription_example",
  "image" : 1,
  "audio" : 1
}

```
где _image_, _audio_ - _id_ на таблицы соответственно image, audio.
_word_, _transcription_, - обязательные поля.

POST - /api/word-translations - добавление перевода слова

* Пример тела запроса (добавление перевода)
```
{
  "word" : 1,
  "translation" : "идти"
  "sample" : "To go for a walk",
  "sample_audio" : 1,
  "translation__audio" : 2
}

```
где _word_, _sample_audio_, _translation_audio_ - id на таблицы word, audio, audio
_word_, _translation_ - обязательные поля.

DELETE - /api/word-translations/{translationId} - удаление перевода по id

Словарная карточка (Word Card)
==============================

GET - /students/word-cards/groups - получить темы пользователя со статистикой
Вернет массив тем пользователя со статистикой
```
[
    {
        "id": 2,
        "name": "animals",
        "learned": 0,
        "need_repeated": 0,
        "words_in_group": 2
    },
    { 
        ....
    },
    {
        ....
    }
]
```
где:
 _id_ - id темы,
 _name_ - название темы, 
 _learned_ - количество карточек выученых пользователем, 
 _need_repeat_ - количество карточек, которые нужно повторить, 
 _words_in_group_ - всего карточек в теме у пользователя

GET - /students/word-cards/learn/{groupId} - получить карточки для изучения
где: _groupId_ - id темы
Вернет массив моделй UserWordCard

GET - /students/word-cards/repeat/{groupId} - получить карточки для повторения
где: _groupId_ - id темы
Вернет массив моделейUserWordCard

PATCH - /students/word-cards/learn/{userWordCardId} - пользователь изучил карточку

PATCH - /students/word-cards/repeat/{userWordCardId} - пользователь повторил карточку