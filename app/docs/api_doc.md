Шаблоны упражнений
=
POST - /api/exercises

GET - /api/exercises/{$id}

PATCH - /api/exercises/{$id}

DELETE - /api/exercises/{$id}
___
1. Диалог
* Добавление/обновление (пример тела запроса)
```
{
  "type" : "dialog",
  "params" : {
    "name" : "te123s2342",

    "audio" : 13,
    "possible_answers" : ["13","123"],
    "answer_bank" : ["13","123"],
    "answer" : "123",
    "audio_hint" : 13,
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, grammar, dictionary, audioHint - id на соответсвующие таблицы
* Выборка

{
    "name": "te123s2342",
    "audio": {
        "path": "/files/audio/",
        "name": "263052b2a15188ebe5219f4845145b57.mp3"
    },
    "audio_hint": {
        "path": "/files/audio/",
        "name": "263052b2a15188ebe5219f4845145b57.mp3"
    },
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "possible_answers": [
        "13",
        "123"
    ],
    "answer_bank": [
        "13",
        "123"
    ],
    "answer": "123"
}

___
2. Прочитать за диктором и сравнить
* Добавление/обновление (пример тела запроса)
```
{
  "type" : "compare",
  "params" : {
    "name" : "te123st7weqe123dsa",

    "audio" : 1,
    "word" : 1,
    "transcription" : "213",
    "answer" : "123",
    "audio_hint" : 1,
    "audio_answer" : 1
  }
}
```
audio, audioHint, audioAnswer - id на соответсвующие таблицы
* Выборка
```
{
    "name": "te123st7weqe123dsa",
    "audio": {
        "path": "/file/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    },
    "audio_hint": {
        "path": "/file/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    },
    "audio_answer": {
        "path": "/file/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    },
    "word": "1",
    "answer": "123",
    "transcription": "213"
}
```
___
3. Ответ в свободной форме
* Добавление/обновление (пример тела запроса)
```
{
  "type" : "free_form",
  "params" : {
    "name" : "te213st8",

    "question" : "21",
    "possible_answer" : ["213"],
    "hint" : "123",
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, dictionary, grammar - id на соответсвующие таблицы
* Выборка
```
{
    "name": "te213st8",
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "question": "21",
    "possible_answer": [
        "213"
    ],
    "hint": "123"
}
```
___
4. Найти пару
* Добавление/обновление (пример тела запроса)
```
{
  "type" : "find_couple",
  "params" : {
    "name" : "test12311",
    
    "couples" : ["123","123"]
  }
}
```
audio - id на соответсвующие таблицы
couples - массив пар
* Выборка
```
{
    "name": "test12311",
    "couples": [
        "123",
        "123"
    ]
}
```
___
5. Послушать и впечатать
* Добавление/обновление (пример тела запроса)
```
{
  "type" : "listen_write",
  "params" : {
    "name" : "test11423532243",

    
    "answer" : "asdasd",
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, dictionary, grammar - id на соответсвующие таблицы

* Выборка
```
{
    "name": "teswet11423532243",
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "answer": "asdasd"
}
```
___
6. Послушать, ответить устно и проверить
* Добавление/обновление (пример тела запроса)
```
{
  "type" : "listen_say",
  "params" : {
    "name" : "tqweest13",

    
    "answer" : "asdasd",
    "audio_hint" : 13,
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, audioHint, dictionary, grammar - id на соответсвующие таблицы
* Выборка
```
{
    "name": "tqweest13",
    "audio_hint": {
        "path": "/files/audio/",
        "name": "263052b2a15188ebe5219f4845145b57.mp3"
    },
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "answer": "asdasd"
}
```
___
7. Drag and Drop
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "drag_and_drop",
  "params" : {
    "name" : "test12315",
    
    "words" : "wqeqwe",
    "answer" : "asdasd",
    "audio_hint" : 1,
    "dictionary" : 1,
    "grammar" : 1
  }
}
```

audio, audioHint, dictionary, grammar - id на соответсвующие таблицы
* Выборка
```
{
    "name": "test12315",
    "audio_hint": {
        "path": "/files/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    },
    "words": "wqeqwe",
    "answer": "asdasd"
}

```
___
8. Прочитать и впечатать 
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "read_write",
  "params" : {
    "name" : "tasdasdasest24",
    
    "elements" : [
      {
        "element" : "asdas",
        "is_input" : false
      },
      {
        "element" : "asdas",
        "is_input" : true
      }
    ],
    "hint" : "asdasd",
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, dictionary, grammar - id на соответсвующие таблицы
elements - массив символов (isInput - флаг необходимо вписывать элемент или нет)
* Выборка
```
{
    "name": "tasdasdasest24",
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "elements": [
        {
            "element": "asdas",
            "is_input": false
        },
        {
            "element": "asdas",
            "is_input": true
        }
    ],
    "hint": "asdasd"
}
```
___
9. Назвать и проверить
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "picture",
  "params" : {
    "name" : "tewqeqwweqst333",

    
    "elements" : [
      {
        "image" : 1,
        "answer" : "123",
        "audio_hint" : 13
      },
      {
        "image" : 1,
        "answer" : "123",
        "audio_hint" : 13
      }
    ],
    "audioHint" : 1,
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, audioHint, dictionary, grammar - id на соответсвующие таблицы
elements - массив карточек
* Выборка
```
{
    "name": "tewqeqwweqst333",
    "elements": [
        {
            "image": {
                "path": "/file/image/",
                "name": "213.jpg"
            },
            "audio_hint": {
                "path": "/files/audio/",
                "name": "263052b2a15188ebe5219f4845145b57.mp3"
            },
            "answer": "123"
        },
        {
            "image": {
                "path": "/file/image/",
                "name": "213.jpg"
            },
            "audio_hint": {
                "path": "/files/audio/",
                "name": "263052b2a15188ebe5219f4845145b57.mp3"
            },
            "answer": "123"
        }
    ]
}

```
___
10. Выбрать из выпадающего списка
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "select",
  "params" : {
    "name" : "tsefsasddfst25",

    
    "elements" : [
      {
        "element":"123",
        "is_select" : false
      },
      {
        "element":"asd",
        "is_select" : false
      }
    ],
    "possible_answers" : ["123"],
    "answer" : "123",
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, dictionary, grammar - id на соответсвующие таблицы
elements - массив элементов (isSelect - флаг необходимо выбирать значение элемента или нет)
* Выборка
```
{
    "name": "tsefsasddfst25",
    "elements": [
        {
            "element": "123",
            "is_select": false
        },
        {
            "element": "asd",
            "is_select": false
        }
    ],
    "possible_answers": [
        "123"
    ],
    "answer": "123"
}

```
___
11. 5 карт
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "five_cards",
  "params" : {
    "name" : "te123s123wee23w20",
    
    "cards" : [
      	{
          "question" : "qweqwe",
          "possible_answers" : ["sdf","sdf"],
          "answer" : "qew",
          "image" : 1
        },
        {
          "question" : "qweqwe2",
          "possible_answers" : ["sdf","sdf"],
          "answer" : "qwe3",
          "image" : 2
        }
    ],
    "grammar" : 1
  }
}
```
audio, image, grammar - id на соответсвующие таблицы
cards - массив карточек
* Выборка
```
{
    "name": "te123sqwe123wee23w20",
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "cards": [
        {
            "image": {
                "path": "/files/image/",
                "name": "213"
            },
            "question": "qweqwe",
            "possible_answers": [
                "sdf",
                "sdf"
            ],
            "answer": "qew"
        },
        {
            "image": {
                "path": "/files/image/",
                "name": "12"
            },
            "question": "qweqwe2",
            "possible_answers": [
                "sdf",
                "sdf"
            ],
            "answer": "qwe3"
        }
    ]
}

```
___
12. Выделить слово
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "pick_out",
  "params" : {
    "name" : "te123s34t22wer3",
    
    "elements" : [
      {
        "element" : "1",
        "is_select" : true
      },
      {
        "element" : "2",
        "is_select" : false
      }
    ],
    "hint" : "123",
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, dictionary, grammar - id на соответсвующие таблицы
elements - массив элементов (isSelect - флаг необходимо выбирать значение элемента или нет)
* Выборка
```
{
    "name": "te123s34t22wer3",
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "elements": [
        {
            "element": "1",
            "is_select": true
        },
        {
            "element": "2",
            "is_select": false
        }
    ]
}

```
___
13. snowball
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "snowball",
  "params" : {
    "name" : "test21q11we",

    
    "exercise_text" : "dede",
    "possible_answers" : ["123"],
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, dictionary, grammar - id на соответсвующие таблицы
* Выборка
```
{
    "name": "test21q11we",
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "exercise_text": "dede",
    "possible_answers": [
        "123"
    ]
}

```
___
14. listen\read and click
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "listen_read_click",
  "params" : {
    "name" : "test32413123",

    
    "exercise_audio" : 1,
    "exercise_image" : 1,
    "exercise_text" : "123",
    "possible_answers" : ["answers"], 
    "answer" : "asdasd",
    "dictionary" : 1,
    "grammar" : 1
  }
}
```
audio, exerciseAudio, exerciseImage, dictionary, grammar - id на соответсвующие таблицы
* Выборка
```
{
    "name": "test32413123",
    "exercise_audio": {
        "path": "/files/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    },
    "exercise_image": {
        "path": "/files/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    },
    "dictionary": {
        "id": 1,
        "name": "test"
    },
    "grammar": {
        "id": 1,
        "title": "test",
        "content": "test"
    },
    "exercise_text": "123",
    "answer": "asdasd"
}
```
___
15. кроссворд
* Добавление/обновление (пример тела запроса)

```
{
  "type" : "crossword",
  "params" : {
    "name" : "test23",
    
    "words" : ["asdasd"]
  }
}
```
audio - id на соответсвующие таблицы
words - массив cлов
* Выборка
```
{
    "name": "test344323",
    "words": [
        "asdasd"
    ]
}
```
___
Привязка упражнений к пользователю

GET - /api/user/{userId}/exercises - все упражнения, открытые юзеру

POST - /api/user/{userId}/exercises - открыть упражнения пользователю

Пример запроса:
```
{
  "params" : [
    {
      "education-request":
    	{
     	 "course" : 1,
     	 "unit" : 5,
     	 "lesson" : 1
    	},
      "lesson-part-types" : ["homework", "lesson"]
    },
    
    {
      "education-request":
   	    {
     	"course" : 1,
      	"unit" : 4,
      	"lesson" : 3
    	},
      "lesson-part-types" : ["test"]
    }
  ]
}
```
lesson-part-types - массив типов lesson-part, открытых пользователю (возможные значения - homework, lesson, test)
___

Файлы
==
POST - /api/{$type}/upload
{$type} - audio/video/image - тип загружаемого файла
тело запроса - файл(название поля в сответствии с типом(audio/video/image ))
возвращает id добавленного файла

Курсы (Course)
==
POST - /api/courses

GET - /api/courses - выборка всех курсов

GET - /api/courses/{$id}

GET - /api/courses/{$id}/units - все юниты на курсе

PATCH - /api/courses/{$id}

DELETE - /api/courses/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Course 1"

```

Юниты (Unit)
==
POST - /api/units

GET - /api/units/{$id}

GET - /api/units?course={id} - список юнитов, привязанных к курсу

GET - /api/users/{userId}/units?course={courseId} - список юнитов, открытых пользователю

GET - /api/units/{$id}/lessons - все уроки в юните

PATCH - /api/units/{$id}

DELETE - /api/units/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Unit 1"
}

```

Уроки (Lesson)
==
POST - /api/lessons

GET - /api/lessons/{$id}

GET - /api/lessons?course={courseId}&unit={unitId} - все уроки, привязанные к юниту

GET - /api/lessons/{$id}/lesson_parts - все lesson part в уроке

GET - /api/users/{userId}/lessons?course={courseId}&unit={unitId} - список уроков, открытых юзеру

PATCH - /api/lessons/{$id}

DELETE - /api/lessons/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Lesson 1"
}

```

Lesson Part
==
POST - /api/lesson-parts

GET - /api/lesson-parts/{$id}

GET - /api/lesson-parts?course={courseId}&unit={unitId}&lesson={lessonId}

PATCH - /api/lesson-parts/{$id}

DELETE - /api/lesson-parts/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Lesson 1",
  "type" : "homework"

```

type = строковое поле, может принимать одно из трех значений:
1) "lesson"
2) "homework"
3) "test"

Карточки слов (DictionaryWordCard)
==
POST - /api/word-cards

GET - /api/word-cards/{$id}
GET - /api/word-cards/{$id}

PATCH - /api/word-cards/{$id}

DELETE - /api/word-cards/{$id}

* Добавление/обновление (пример тела запроса)
```
{
  "dictionaryWord" : 1,
  "audio" : 1,
  "image" : 1,
  "sample" : "afasfs"
}
```
* Выборка
```
{
    "id": 10,
    "sample": "1",
    "dictionaryWord": {
        "word": "table",
        "translation": "teble",
        "transcription": "table",
        "sample": "table",
        "dictionary": {
            "id": 1,
            "name": "test"
        },
        "image": {
            "path": "/files/image/",
            "name": "213"
        },
        "audio": {
            "path": "/files/audio/",
            "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
        }
    },
    "image": {
        "path": "/files/image/",
        "name": "213"
    },
    "audio": {
        "path": "/files/audio/",
        "name": "6fff7593b86e2c0c18bbd09ea5ac54c4.mp3"
    }
}
```


Раздел грамматики (GrammarSection)
==
POST - /api/grammar-sections

GET - /api/grammar-sections - список всех разделов

GET - /api/grammar-sections/{$id}

GET - /api/grammar-sections/{$id}/themes - список тем для данного раздела

PATCH - /api/grammar-sections/{$id}

DELETE - /api/grammar-sections/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Grammar Section 1"
}

```


Темы грамматики (GrammarTheme)
==
POST - /api/grammar-themes

GET - /api/grammar-themes/{$id}

GET - /api/grammar-themes/{$id}/grammars - список грамматик для темы

PATCH - /api/grammar-themes/{$id}

DELETE - /api/grammar-themes/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Grammar Theme 1",
  "grammar_section" : 1
}

```
где grammar_section - id на соответствующую таблицу.

Грамматика (Grammar)
==
POST - /api/grammars

GET - /api/grammars/{$id}

GET - /api/grammars?title={title} - поиск грамматических правил по заголовку

    {title} - строка заголовка, если не указана - вернутся все грамматические правила

PATCH - /api/grammars/{$id}

DELETE - /api/grammars/{$id}

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Grammar 1",
  "grammar_theme" : 1,
  "title" : "grammar title",
  "content" : "grammar content"
}

```
где theme - id на таблицу grammar_theme.


Редактор рассылки (DeliveryTemplate)
====================================
GET - /api/delivery-templates - все шаблоны рассылок

GET - /api/delivery-templates/{id} - шаблон по id

POST - /api/delivery-templates

PATCH - /api/delivery-templates/{id}

* Выборка
```
{
    "id": 7,
    "name": "Test delivery",
    "type": "email",
    "user_types":[
        "ROLE_CORPORATE",
        "ROLE_USER"
    ],
    "start_date": "2016-05-01T12:05:40+0300",
    "end_date": "2016-06-20T12:05:40+0300",
    "repeat_count": 1000,
    "repeat_step_minute": 1130,
    "message": "Test delivery text",
    "user":{
        "id": 1,
        "email": "batman@gmail.com",
        "last_name": "Иванов",
        "first_name": "Иван",
        "middle_name": "Иванович"
        }
}
```

* Добавление/обновление (пример тела запроса)

```
{
  "name" : "Second delivery",
  "type" : "sms",
  "user_types" : ["ROLE_USER", "ROLE_ADMIN"],
  "start_date" : "2016-03-01",
  "end_date" : "2016-03-30",
  "repeat_count" : 400,
  "repeat_step_minute" : 1440,
  "message" : "New course will be opened soon"
}

```
    
    type - тип рассылки (возможные значения - sms, email, system)
    user_types - типы пользователей, для которых предназначена рассылка (ROLE_ADMIN, ROLE_USER, ROLE_CORPORATE, ROLE_TEACHER)
    repeat_step_minute - шаг повторения в минутах
    
DELETE - /api/delivery-templates/{id}

Редактор пользователей
====
GET - /api/users/{id} - получить пользователя по id

GET - /api/users?role={roleName} - все пользователи с указанной ролью (например, teacher)

GET - /api/users/search?fullname={ФИО} - поиск юзера по ФИО

DELETE - /api/users/{id} - удаление пользователя

PATCH - /api/users/{id} - изменить данные пользователя

Пример запроса:

```
{
  "email": "sanek@gmail.com",
  "roles":["ROLE_TEACHER","ROLE_USER", "ROLE_ADMIN", "ROLE_CORPORATE"],
  "last_name": "Буйнов",
  "first_name": "Александр",
  "middle_name": "Александрович",
  "phone": "+37529-104-22-73",
  "city": "Гомель",
  "birthday": "1993-04-23",
  "students": [],
  "employers": [],
  "teachers": [1, 4],
  "employees": [2]
}

```
roles - массив ролей пользователя

students - массив id-шек пользователей из таблицы user, которые будут закреплены за данным юзером
           в качестве учеников (доступно для роли ROLE_TEACHER)

employers - массив id работодателей (доступно для ролей онлайн и оффлайн слушателей, пока что это
            общая роль ROLE_USER)

teachers - массив id учителей данного пользователя (также доступно для ролей онлайн и оффлайн слушателей)

employees - массив id работников (доступно для роли ROLE_CORPORATE)