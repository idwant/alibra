# Exercises
1. GET /exercises - выборка списка упражнений
 Параметры запроса: 
    query(поиск по имени), lesson_id, limit, offset
2. GET /exercises/{id} - выборка одного упражнения
3. PATCH /exercises/{id} - обновление упражнения(параметры см. POST /exercises)
4. DELETE /exercises/{id} - удаление упражнения
5. PATCH /exercises/sort - сортировка упражнения в рамках урока
    {
        "lesson_id",
        "exercise_id",
        "position"
    }
6. POST /exercises/bind - привязка упражнения к уроку
    {
     "exercise_id"
     "lesson_part_id"
    }
7. POST /exercises/unbind - убрать связь упражнения к уроку
    {
     "exercise_id"
     "lesson_part_id"
    }
8. POST /exercises - создание упражнения
    {
        "name",
        "template_type", - тип шаблона упражнения
        "type", - тип упражнения
        "task_en",
        "task_ru",
        "task_audio",
        "dictionary",
        "grammar",
        "params" - массив объектов (объект зависит от типа шаблона упражнения)
     }
     
---
# Типы шаблонов упражнений 

1. dialog-exercise (Диалог)

        "answers" - массив слов для ответов
        "possible_answers", - массив слов для вариантов ответов
        "audio_hint", - id аудио подсказки
        "audio", - id аудио
        "image", - id картинки
        
2. compare (Прочитать за диктором и сравнить)
    
        "transcription", - массив знаков транскрипции
        "answer", - ответ(string)
        "audio_hint", - id аудио подсказки
        "audio_answer", - id аудио ответа
        "image" - id картинки
    
3. free-form (Ответ в свободной форме)

        "question", - ответ(string)
        "possible_answers", - массив ответов
        "hint", - подсказка(string)
        "image" - id картинки
        
4. find-couple (Найти пару)

        "coouples" - массив слов
        
5. listen-write (Послушать и впечатать)

        "question", - вопрос(string)
        "answer", - ответ(string)
        "audio", - id аудио
        "image" - id картинки
        
6. listen-say (Послушать, ответить устно и проверить)

       "answer", - вопрос(string)
       "audio_hint", - id аудио подсказки
       "image" - id картинки
       
7. drag-and-drop

        "words" : [{"text":"text"}], - массив объектов слов
        "answer", - вопрос(string)
        "audio_hint", - id аудио подсказки
        "image" - id картинки

8. read-write (Прочитать и впечатать)

        "image", - id картинки
        "hint", - подсказка(string)
        "elements" : [ - массив элементов
          {
            "text" , - текст(string)
            "skip" - флаг пропуска слова
          }
        ]
        
9. picture (Назвать и проверить)
        
        "elements" : [ - массив элементов
            {
                "image", - id картинки
                "answer", - ответ(string)
                "audio_hint": - id аудио
            }
        ]

10. select (Выбрать из выпадающего списка) 

        "elements" : [ - массив элементов
            {
                "text" , - текст
                "skip", - флаг пропуска слова
                "possible_answers" : [] - массив вариантов ответов
            }
        ]
        
11. five-cards (5 карт)

        "cards" : [ - массив карт
            {
                "question", - вопрос
                "possible_answers", - массив ответов
                "answer", - ответ
                "image" - id картинки
            }
        ]
        
12. pick-out (Выделить слово)

        "elements" : [ - массив элементов
            {
            "text", - текст
            "answer" - ответ
            }
        ],
        "hint" - подсказка
        
13. snowball
    
        "exercise_text", - текст упражнения 
        "possible_answers":[] - массив вариантов ответов
        
14. listen-read-click

        "audio", - id аудио
        "image", - id картинки
        "hint", - подсказка(string)
        "elements" : [ - массив элементов
            {
                "text", - текст 
                "answer" - флаг является ли ответом
            }]
        
15. crossword (Кроссворд)

        "grid" : [],
        "legend" : {},
        "items" : [ - массив элементов кроссворда
            {
                "question", - вопрос (string)
                "answer" - ответ (string)
            }
        ]
    