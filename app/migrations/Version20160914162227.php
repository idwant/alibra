<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160914162227 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE word_card_category ADD image_big_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word_card_category ADD CONSTRAINT FK_AB286DEBF030979E FOREIGN KEY (image_big_id) REFERENCES file_image (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AB286DEBF030979E ON word_card_category (image_big_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE word_card_category DROP CONSTRAINT FK_AB286DEBF030979E');
        $this->addSql('DROP INDEX IDX_AB286DEBF030979E');
        $this->addSql('ALTER TABLE word_card_category DROP image_big_id');
    }
}
