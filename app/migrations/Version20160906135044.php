<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160906135044 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE word_card_category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE word_card_category (id INT NOT NULL, image_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AB286DEB5E237E06 ON word_card_category (name)');
        $this->addSql('CREATE INDEX IDX_AB286DEB3DA5256D ON word_card_category (image_id)');
        $this->addSql('ALTER TABLE word_card_category ADD CONSTRAINT FK_AB286DEB3DA5256D FOREIGN KEY (image_id) REFERENCES file_image (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card_group ADD word_card_category_id INT DEFAULT NULL');

        // Созадём фиктивную категорию, чтобы привязать к ней все темы и сделать поле word_card_category_id NOT NULL
        $this->addSql('INSERT INTO word_card_category (id, image_id, name, description, is_deleted) VALUES (NEXTVAL(\'word_card_category_id_seq\'), NULL, \'Test\', NULL, false)');
        $this->addSql('UPDATE word_card_group SET word_card_category_id = (SELECT id FROM word_card_category WHERE name = \'Test\' LIMIT 1)');
        $this->addSql('ALTER TABLE word_card_group ALTER COLUMN word_card_category_id SET NOT NULL');

        $this->addSql('ALTER TABLE word_card_group ADD CONSTRAINT FK_3C0387A1261047CA FOREIGN KEY (word_card_category_id) REFERENCES word_card_category (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_3C0387A1261047CA ON word_card_group (word_card_category_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE word_card_group DROP CONSTRAINT FK_3C0387A1261047CA');
        $this->addSql('DROP SEQUENCE word_card_category_id_seq CASCADE');
        $this->addSql('DROP TABLE word_card_category');
        $this->addSql('DROP INDEX IDX_3C0387A1261047CA');
        $this->addSql('ALTER TABLE word_card_group DROP word_card_category_id');
    }
}
