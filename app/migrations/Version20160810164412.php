<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160810164412 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE word_card_word_card_group (word_card_group_id INT NOT NULL, word_card_id INT NOT NULL, PRIMARY KEY(word_card_group_id, word_card_id))');
        $this->addSql('CREATE INDEX IDX_14CEBEF15330AD6D ON word_card_word_card_group (word_card_group_id)');
        $this->addSql('CREATE INDEX IDX_14CEBEF140BEEDD3 ON word_card_word_card_group (word_card_id)');
        $this->addSql('ALTER TABLE word_card_word_card_group ADD CONSTRAINT FK_14CEBEF15330AD6D FOREIGN KEY (word_card_group_id) REFERENCES word_card_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card_word_card_group ADD CONSTRAINT FK_14CEBEF140BEEDD3 FOREIGN KEY (word_card_id) REFERENCES word_card (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT fk_1e819b3e5330ad6d');
        $this->addSql('DROP INDEX idx_1e819b3e5330ad6d');
        $this->addSql('ALTER TABLE word_card DROP word_card_group_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE word_card_word_card_group');
        $this->addSql('ALTER TABLE word_card ADD word_card_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT fk_1e819b3e5330ad6d FOREIGN KEY (word_card_group_id) REFERENCES word_card_group (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1e819b3e5330ad6d ON word_card (word_card_group_id)');
    }
}
