<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160731121127 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51C48281F3C');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51C3A3123C7');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CAF5E5B3C');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CB0CA2760');
        $this->addSql('TRUNCATE exercise CASCADE');
        $this->addSql('ALTER TABLE exercise ALTER type SET NOT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51C48281F3C FOREIGN KEY (exercise_template_id) REFERENCES exercise_template (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51C3A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CAF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CB0CA2760 FOREIGN KEY (grammar_id) REFERENCES grammar (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT fk_aedad51c48281f3c');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT fk_aedad51c3a3123c7');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT fk_aedad51caf5e5b3c');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT fk_aedad51cb0ca2760');
        $this->addSql('ALTER TABLE exercise ALTER type DROP NOT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT fk_aedad51c48281f3c FOREIGN KEY (exercise_template_id) REFERENCES exercise_template (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT fk_aedad51c3a3123c7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT fk_aedad51caf5e5b3c FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT fk_aedad51cb0ca2760 FOREIGN KEY (grammar_id) REFERENCES grammar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
