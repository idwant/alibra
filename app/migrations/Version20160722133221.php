<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160722133221 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise ADD audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD dictionary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD grammar_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51C3A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CAF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CB0CA2760 FOREIGN KEY (grammar_id) REFERENCES grammar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AEDAD51C3A3123C7 ON exercise (audio_id)');
        $this->addSql('CREATE INDEX IDX_AEDAD51CAF5E5B3C ON exercise (dictionary_id)');
        $this->addSql('CREATE INDEX IDX_AEDAD51CB0CA2760 ON exercise (grammar_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51C3A3123C7');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CAF5E5B3C');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CB0CA2760');
        $this->addSql('DROP INDEX IDX_AEDAD51C3A3123C7');
        $this->addSql('DROP INDEX IDX_AEDAD51CAF5E5B3C');
        $this->addSql('DROP INDEX IDX_AEDAD51CB0CA2760');
        $this->addSql('ALTER TABLE exercise DROP audio_id');
        $this->addSql('ALTER TABLE exercise DROP dictionary_id');
        $this->addSql('ALTER TABLE exercise DROP grammar_id');
    }
}
