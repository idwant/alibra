<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160715150540 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE course_unit DROP CONSTRAINT FK_1419D155591CC992');
        $this->addSql('ALTER TABLE course_unit DROP CONSTRAINT FK_1419D155F8BD700D');
        $this->addSql('ALTER TABLE course_unit ADD CONSTRAINT FK_1419D155591CC992 FOREIGN KEY (course_id) REFERENCES course (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_unit ADD CONSTRAINT FK_1419D155F8BD700D FOREIGN KEY (unit_id) REFERENCES unit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_exercise DROP CONSTRAINT FK_E69D9F05CDF80196');
        $this->addSql('ALTER TABLE lesson_exercise DROP CONSTRAINT FK_E69D9F05E934951A');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT FK_E69D9F05CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT FK_E69D9F05E934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT FK_6F2D9F05CDF80196');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT FK_6F2D9F05F8BD700D');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT FK_6F2D9F05CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT FK_6F2D9F05F8BD700D FOREIGN KEY (unit_id) REFERENCES unit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE lesson_exercise DROP CONSTRAINT fk_e69d9f05cdf80196');
        $this->addSql('ALTER TABLE lesson_exercise DROP CONSTRAINT fk_e69d9f05e934951a');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT fk_e69d9f05cdf80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT fk_e69d9f05e934951a FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_unit DROP CONSTRAINT fk_1419d155591cc992');
        $this->addSql('ALTER TABLE course_unit DROP CONSTRAINT fk_1419d155f8bd700d');
        $this->addSql('ALTER TABLE course_unit ADD CONSTRAINT fk_1419d155591cc992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_unit ADD CONSTRAINT fk_1419d155f8bd700d FOREIGN KEY (unit_id) REFERENCES unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT fk_6f2d9f05f8bd700d');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT fk_6f2d9f05cdf80196');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT fk_6f2d9f05f8bd700d FOREIGN KEY (unit_id) REFERENCES unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT fk_6f2d9f05cdf80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
