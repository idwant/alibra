<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160803104630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE word_translation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE word_translation (id INT NOT NULL, word_id INT DEFAULT NULL, translation_audio_id INT DEFAULT NULL, sample_audio_id INT DEFAULT NULL, translation VARCHAR(255) NOT NULL, sample VARCHAR(255) NOT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8CD87099E357438D ON word_translation (word_id)');
        $this->addSql('CREATE INDEX IDX_8CD870996923BC0E ON word_translation (translation_audio_id)');
        $this->addSql('CREATE INDEX IDX_8CD87099234D5F7B ON word_translation (sample_audio_id)');
        $this->addSql('ALTER TABLE word_translation ADD CONSTRAINT FK_8CD87099E357438D FOREIGN KEY (word_id) REFERENCES word (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_translation ADD CONSTRAINT FK_8CD870996923BC0E FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_translation ADD CONSTRAINT FK_8CD87099234D5F7B FOREIGN KEY (sample_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT fk_fefd2729af5e5b3c');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT fk_fefd2729232512c');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5AAF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5AE357438D FOREIGN KEY (word_id) REFERENCES word (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_fefd2729af5e5b3c RENAME TO IDX_25D63D5AAF5E5B3C');
        $this->addSql('ALTER INDEX idx_fefd2729232512c RENAME TO IDX_25D63D5AE357438D');
        $this->addSql('ALTER TABLE word DROP CONSTRAINT fk_c3f17511af5e5b3c');
        $this->addSql('ALTER TABLE word DROP CONSTRAINT fk_c3f175116923bc0e');
        $this->addSql('DROP INDEX idx_c3f175116923bc0e');
        $this->addSql('DROP INDEX idx_c3f17511af5e5b3c');
        $this->addSql('ALTER TABLE word DROP dictionary_id');
        $this->addSql('ALTER TABLE word DROP translation_audio_id');
        $this->addSql('ALTER TABLE word DROP translation');
        $this->addSql('ALTER TABLE word DROP sample');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE word_translation_id_seq CASCADE');
        $this->addSql('DROP TABLE word_translation');
        $this->addSql('ALTER TABLE word ADD dictionary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word ADD translation_audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word ADD translation VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE word ADD sample VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT fk_c3f17511af5e5b3c FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT fk_c3f175116923bc0e FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_c3f175116923bc0e ON word (translation_audio_id)');
        $this->addSql('CREATE INDEX idx_c3f17511af5e5b3c ON word (dictionary_id)');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5AAF5E5B3C');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5AE357438D');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT fk_fefd2729af5e5b3c FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT fk_fefd2729232512c FOREIGN KEY (word_id) REFERENCES word (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX idx_25d63d5aaf5e5b3c RENAME TO idx_fefd2729af5e5b3c');
        $this->addSql('ALTER INDEX idx_25d63d5ae357438d RENAME TO idx_fefd2729232512c');
    }
}
