<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151202155048 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE user_lesson_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_exercise_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_lesson (id INT NOT NULL, user_id INT DEFAULT NULL, lesson_id INT DEFAULT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, status INT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9D266FCEA76ED395 ON user_lesson (user_id)');
        $this->addSql('CREATE INDEX IDX_9D266FCECDF80196 ON user_lesson (lesson_id)');
        $this->addSql('CREATE TABLE user_exercise (id INT NOT NULL, user_id INT DEFAULT NULL, exercise_id INT DEFAULT NULL, user_lesson_id INT DEFAULT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, status INT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4E57311CA76ED395 ON user_exercise (user_id)');
        $this->addSql('CREATE INDEX IDX_4E57311CE934951A ON user_exercise (exercise_id)');
        $this->addSql('CREATE INDEX IDX_4E57311CCD188C72 ON user_exercise (user_lesson_id)');
        $this->addSql('ALTER TABLE user_lesson ADD CONSTRAINT FK_9D266FCEA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_lesson ADD CONSTRAINT FK_9D266FCECDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_exercise ADD CONSTRAINT FK_4E57311CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_exercise ADD CONSTRAINT FK_4E57311CE934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_exercise ADD CONSTRAINT FK_4E57311CCD188C72 FOREIGN KEY (user_lesson_id) REFERENCES user_lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_exercise DROP CONSTRAINT FK_4E57311CCD188C72');
        $this->addSql('DROP SEQUENCE user_lesson_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_exercise_id_seq CASCADE');
        $this->addSql('DROP TABLE user_lesson');
        $this->addSql('DROP TABLE user_exercise');
    }
}
