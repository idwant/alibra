<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160712151533 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE grammar ADD video_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE grammar ADD is_deleted BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE grammar ADD CONSTRAINT FK_7A6B627929C1004E FOREIGN KEY (video_id) REFERENCES file_video (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7A6B627929C1004E ON grammar (video_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE grammar DROP CONSTRAINT FK_7A6B627929C1004E');
        $this->addSql('DROP INDEX IDX_7A6B627929C1004E');
        $this->addSql('ALTER TABLE grammar DROP video_id');
        $this->addSql('ALTER TABLE grammar DROP is_deleted');
    }
}
