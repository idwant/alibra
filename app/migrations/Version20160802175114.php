<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160802175114 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE dictionary_word_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE word_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE word (id INT NOT NULL, dictionary_id INT DEFAULT NULL, image_id INT DEFAULT NULL, sample_audio_id INT DEFAULT NULL, translation_audio_id INT DEFAULT NULL, word VARCHAR(255) NOT NULL, translation VARCHAR(255) NOT NULL, transcription VARCHAR(255) NOT NULL, sample VARCHAR(255) DEFAULT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C3F17511AF5E5B3C ON word (dictionary_id)');
        $this->addSql('CREATE INDEX IDX_C3F175113DA5256D ON word (image_id)');
        $this->addSql('CREATE INDEX IDX_C3F17511234D5F7B ON word (sample_audio_id)');
        $this->addSql('CREATE INDEX IDX_C3F175116923BC0E ON word (translation_audio_id)');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F17511AF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F175113DA5256D FOREIGN KEY (image_id) REFERENCES file_image (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F17511234D5F7B FOREIGN KEY (sample_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F175116923BC0E FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT fk_1e819b3e232512c');
        $this->addSql('DROP INDEX idx_1e819b3e232512c');
        $this->addSql('TRUNCATE word_card CASCADE');
        $this->addSql('ALTER TABLE word_card RENAME COLUMN dictionary_word_id TO word_id');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3EE357438D FOREIGN KEY (word_id) REFERENCES word (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1E819B3EE357438D ON word_card (word_id)');

        $this->addSql('DROP TABLE dictionary_dictionary_word');
        $this->addSql('DROP TABLE dictionary_word');

        $this->addSql('CREATE TABLE dictionary_word (dictionary_id INT NOT NULL, word_id INT NOT NULL, PRIMARY KEY(dictionary_id, word_id))');
        $this->addSql('CREATE INDEX IDX_FEFD2729AF5E5B3C ON dictionary_word (dictionary_id)');
        $this->addSql('CREATE INDEX IDX_FEFD2729232512C ON dictionary_word (word_id)');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_FEFD2729AF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_FEFD2729232512C FOREIGN KEY (word_id) REFERENCES word (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
