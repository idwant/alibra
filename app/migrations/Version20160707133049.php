<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160707133049 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise ADD task_en TEXT NOT NULL');
        $this->addSql('ALTER TABLE exercise ADD task_ru TEXT NOT NULL');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT fk_25d63d5a3a3123c7');
        $this->addSql('DROP INDEX idx_25d63d5a3a3123c7');
        $this->addSql('ALTER TABLE dictionary_word ADD translation_audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dictionary_word RENAME COLUMN audio_id TO sample_audio_id');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5A234D5F7B FOREIGN KEY (sample_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5A6923BC0E FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_25D63D5A234D5F7B ON dictionary_word (sample_audio_id)');
        $this->addSql('CREATE INDEX IDX_25D63D5A6923BC0E ON dictionary_word (translation_audio_id)');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT fk_1e819b3e3a3123c7');
        $this->addSql('DROP INDEX idx_1e819b3e3a3123c7');
        $this->addSql('ALTER TABLE word_card ADD translation_audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word_card RENAME COLUMN audio_id TO sample_audio_id');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E234D5F7B FOREIGN KEY (sample_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E6923BC0E FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1E819B3E234D5F7B ON word_card (sample_audio_id)');
        $this->addSql('CREATE INDEX IDX_1E819B3E6923BC0E ON word_card (translation_audio_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E234D5F7B');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E6923BC0E');
        $this->addSql('DROP INDEX IDX_1E819B3E234D5F7B');
        $this->addSql('DROP INDEX IDX_1E819B3E6923BC0E');
        $this->addSql('ALTER TABLE word_card ADD audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word_card DROP sample_audio_id');
        $this->addSql('ALTER TABLE word_card DROP translation_audio_id');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT fk_1e819b3e3a3123c7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1e819b3e3a3123c7 ON word_card (audio_id)');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5A234D5F7B');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5A6923BC0E');
        $this->addSql('DROP INDEX IDX_25D63D5A234D5F7B');
        $this->addSql('DROP INDEX IDX_25D63D5A6923BC0E');
        $this->addSql('ALTER TABLE dictionary_word ADD audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dictionary_word DROP sample_audio_id');
        $this->addSql('ALTER TABLE dictionary_word DROP translation_audio_id');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT fk_25d63d5a3a3123c7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_25d63d5a3a3123c7 ON dictionary_word (audio_id)');
        $this->addSql('ALTER TABLE exercise DROP task_en');
        $this->addSql('ALTER TABLE exercise DROP task_ru');
    }
}
