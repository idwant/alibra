<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160629163357 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE task_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE task_queue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE task_status (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE task_queue (id INT NOT NULL, status_id INT NOT NULL, service VARCHAR(255) NOT NULL, params JSON DEFAULT NULL, block_id VARCHAR(255) DEFAULT NULL, attempts INT DEFAULT 0 NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B85964436BF700BD ON task_queue (status_id)');
        $this->addSql('COMMENT ON COLUMN task_queue.date_start IS \'Дата, когда нужно начать выполнять задачу\'');
        $this->addSql('ALTER TABLE task_queue ADD CONSTRAINT FK_B85964436BF700BD FOREIGN KEY (status_id) REFERENCES task_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE task_queue DROP CONSTRAINT FK_B85964436BF700BD');
        $this->addSql('DROP SEQUENCE task_status_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE task_queue_id_seq CASCADE');
        $this->addSql('DROP TABLE task_status');
        $this->addSql('DROP TABLE task_queue');
    }
}
