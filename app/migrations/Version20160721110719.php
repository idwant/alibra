<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160721110719 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise_template ADD type VARCHAR(255)');
        $this->addSql('UPDATE exercise_template SET type = name');
        $this->addSql('UPDATE exercise_template SET name = ett.name FROM exercise_template_type ett WHERE ett.id = type_id');
        $this->addSql('ALTER TABLE exercise_template DROP CONSTRAINT fk_910cbf6bc54c8c93');
        $this->addSql('DROP SEQUENCE exercise_template_type_id_seq CASCADE');
        $this->addSql('DROP TABLE exercise_template_type');
        $this->addSql('DROP INDEX uniq_910cbf6b5e237e06');
        $this->addSql('DROP INDEX idx_910cbf6b3a3123c7');
        $this->addSql('DROP INDEX idx_910cbf6bc54c8c93');
        $this->addSql('ALTER TABLE exercise_template DROP audio_id');
        $this->addSql('ALTER TABLE exercise_template DROP type_id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_910CBF6B8CDE5729 ON exercise_template (type)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE exercise_template_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE exercise_template_type (id INT NOT NULL, name VARCHAR(255) NOT NULL, name_en VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_1b487d9d3d773ac4 ON exercise_template_type (name_en)');
        $this->addSql('DROP INDEX UNIQ_910CBF6B8CDE5729');
        $this->addSql('ALTER TABLE exercise_template ADD audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise_template ADD type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise_template DROP type');
        $this->addSql('ALTER TABLE exercise_template ADD CONSTRAINT fk_910cbf6b3a3123c7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise_template ADD CONSTRAINT fk_910cbf6bc54c8c93 FOREIGN KEY (type_id) REFERENCES exercise_template_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_910cbf6b5e237e06 ON exercise_template (name)');
        $this->addSql('CREATE INDEX idx_910cbf6b3a3123c7 ON exercise_template (audio_id)');
        $this->addSql('CREATE INDEX idx_910cbf6bc54c8c93 ON exercise_template (type_id)');
    }
}
