<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160803114501 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE word DROP CONSTRAINT fk_c3f17511234d5f7b');
        $this->addSql('DROP INDEX idx_c3f17511234d5f7b');
        $this->addSql('ALTER TABLE word RENAME COLUMN sample_audio_id TO audio_id');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT FK_C3F175113A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C3F175113A3123C7 ON word (audio_id)');
        $this->addSql('ALTER TABLE word_translation ALTER sample DROP NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE word DROP CONSTRAINT FK_C3F175113A3123C7');
        $this->addSql('DROP INDEX IDX_C3F175113A3123C7');
        $this->addSql('ALTER TABLE word RENAME COLUMN audio_id TO sample_audio_id');
        $this->addSql('ALTER TABLE word ADD CONSTRAINT fk_c3f17511234d5f7b FOREIGN KEY (sample_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_c3f17511234d5f7b ON word (sample_audio_id)');
        $this->addSql('ALTER TABLE word_translation ALTER sample SET NOT NULL');
    }
}
