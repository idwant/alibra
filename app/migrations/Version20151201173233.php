<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151201173233 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE dictionary_word_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE feedback_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE exercise_template_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lesson_part_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE word_card_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE file_audio_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_settings_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE grammar_theme_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE dictionary_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE file_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE grammar_section_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE file_video_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE faq_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE course_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE exercise_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lesson_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE grammar_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE dictionary_word (id INT NOT NULL, dictionary_id INT DEFAULT NULL, image_id INT DEFAULT NULL, audio_id INT DEFAULT NULL, word VARCHAR(255) NOT NULL, translation VARCHAR(255) NOT NULL, transcription VARCHAR(255) NOT NULL, sample VARCHAR(255) DEFAULT NULL, isDeleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_25D63D5AAF5E5B3C ON dictionary_word (dictionary_id)');
        $this->addSql('CREATE INDEX IDX_25D63D5A3DA5256D ON dictionary_word (image_id)');
        $this->addSql('CREATE INDEX IDX_25D63D5A3A3123C7 ON dictionary_word (audio_id)');
        $this->addSql('CREATE TABLE feedback (id INT NOT NULL, user_id INT DEFAULT NULL, message TEXT DEFAULT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D2294458A76ED395 ON feedback (user_id)');
        $this->addSql('CREATE TABLE user_teacher (user_id INT NOT NULL, teacher_id INT NOT NULL, PRIMARY KEY(user_id, teacher_id))');
        $this->addSql('CREATE INDEX IDX_E8FBB8DFA76ED395 ON user_teacher (user_id)');
        $this->addSql('CREATE INDEX IDX_E8FBB8DF41807E1D ON user_teacher (teacher_id)');
        $this->addSql('CREATE TABLE user_employer (user_id INT NOT NULL, employer_id INT NOT NULL, PRIMARY KEY(user_id, employer_id))');
        $this->addSql('CREATE INDEX IDX_3EC11466A76ED395 ON user_employer (user_id)');
        $this->addSql('CREATE INDEX IDX_3EC1146641CD9E7A ON user_employer (employer_id)');
        $this->addSql('CREATE TABLE exercise_template (id INT NOT NULL, audio_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, task_en TEXT NOT NULL, task_ru TEXT NOT NULL, params JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_910CBF6B5E237E06 ON exercise_template (name)');
        $this->addSql('CREATE INDEX IDX_910CBF6B3A3123C7 ON exercise_template (audio_id)');
        $this->addSql('CREATE TABLE lesson_part (id INT NOT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_707EB6A25E237E06 ON lesson_part (name)');
        $this->addSql('CREATE TABLE word_card (id INT NOT NULL, dictionary_word_id INT DEFAULT NULL, image_id INT DEFAULT NULL, audio_id INT DEFAULT NULL, sample VARCHAR(255) DEFAULT NULL, isDeleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1E819B3E232512C ON word_card (dictionary_word_id)');
        $this->addSql('CREATE INDEX IDX_1E819B3E3DA5256D ON word_card (image_id)');
        $this->addSql('CREATE INDEX IDX_1E819B3E3A3123C7 ON word_card (audio_id)');
        $this->addSql('CREATE TABLE file_audio (id INT NOT NULL, path VARCHAR(255) NOT NULL, original_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_settings (id INT NOT NULL, user_id INT DEFAULT NULL, language CHAR(2) DEFAULT \'ru\' NOT NULL, word_repetition_type VARCHAR(255) DEFAULT NULL, allow_email_word_repetition_notify BOOLEAN DEFAULT \'true\' NOT NULL, allow_email_administrative_notify BOOLEAN DEFAULT \'true\' NOT NULL, allow_sms_word_repetition_notify BOOLEAN DEFAULT \'true\' NOT NULL, allow_sms_administrative_notify BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5C844C5A76ED395 ON user_settings (user_id)');
        $this->addSql('CREATE TABLE grammar_theme (id INT NOT NULL, grammar_section_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_67D3EE348B33918 ON grammar_theme (grammar_section_id)');
        $this->addSql('CREATE TABLE dictionary (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1FA0E5265E237E06 ON dictionary (name)');
        $this->addSql('CREATE TABLE file_image (id INT NOT NULL, path VARCHAR(255) NOT NULL, original_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE grammar_section (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_275C4B125E237E06 ON grammar_section (name)');
        $this->addSql('CREATE TABLE file_video (id INT NOT NULL, path VARCHAR(255) NOT NULL, original_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE faq (id INT NOT NULL, question VARCHAR(255) NOT NULL, answer VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE unit (id INT NOT NULL, name VARCHAR(255) NOT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DCBB0C535E237E06 ON unit (name)');
        $this->addSql('CREATE TABLE course (id INT NOT NULL, name VARCHAR(255) NOT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_169E6FB95E237E06 ON course (name)');
        $this->addSql('CREATE TABLE exercise (id INT NOT NULL, exercise_template_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, params JSON NOT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AEDAD51C5E237E06 ON exercise (name)');
        $this->addSql('CREATE INDEX IDX_AEDAD51C48281F3C ON exercise (exercise_template_id)');
        $this->addSql('CREATE TABLE lesson (id INT NOT NULL, name VARCHAR(255) NOT NULL, is_deleted BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F87474F35E237E06 ON lesson (name)');
        $this->addSql('CREATE TABLE grammar (id INT NOT NULL, grammar_theme_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, content TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7A6B62799A04D517 ON grammar (grammar_theme_id)');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5AAF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5A3DA5256D FOREIGN KEY (image_id) REFERENCES file_image (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_word ADD CONSTRAINT FK_25D63D5A3A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE feedback ADD CONSTRAINT FK_D2294458A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_teacher ADD CONSTRAINT FK_E8FBB8DFA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_teacher ADD CONSTRAINT FK_E8FBB8DF41807E1D FOREIGN KEY (teacher_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_employer ADD CONSTRAINT FK_3EC11466A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_employer ADD CONSTRAINT FK_3EC1146641CD9E7A FOREIGN KEY (employer_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise_template ADD CONSTRAINT FK_910CBF6B3A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E232512C FOREIGN KEY (dictionary_word_id) REFERENCES dictionary_word (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E3DA5256D FOREIGN KEY (image_id) REFERENCES file_image (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E3A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_settings ADD CONSTRAINT FK_5C844C5A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE grammar_theme ADD CONSTRAINT FK_67D3EE348B33918 FOREIGN KEY (grammar_section_id) REFERENCES grammar_section (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51C48281F3C FOREIGN KEY (exercise_template_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE grammar ADD CONSTRAINT FK_7A6B62799A04D517 FOREIGN KEY (grammar_theme_id) REFERENCES grammar_theme (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users ADD first_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD middle_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD username VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE users ADD city VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD birthday DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD status VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD last_activity_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD last_auth_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE users RENAME COLUMN fullname TO last_name');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON users (username)');
        $this->addSql('ALTER TABLE user_session ADD logout_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE user_session RENAME COLUMN created_at TO login_date');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E232512C');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5A3A3123C7');
        $this->addSql('ALTER TABLE exercise_template DROP CONSTRAINT FK_910CBF6B3A3123C7');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E3A3123C7');
        $this->addSql('ALTER TABLE grammar DROP CONSTRAINT FK_7A6B62799A04D517');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5AAF5E5B3C');
        $this->addSql('ALTER TABLE dictionary_word DROP CONSTRAINT FK_25D63D5A3DA5256D');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E3DA5256D');
        $this->addSql('ALTER TABLE grammar_theme DROP CONSTRAINT FK_67D3EE348B33918');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51C48281F3C');
        $this->addSql('DROP SEQUENCE dictionary_word_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE feedback_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE exercise_template_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lesson_part_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE word_card_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE file_audio_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_settings_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE grammar_theme_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE dictionary_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE file_image_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE grammar_section_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE file_video_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE faq_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE unit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE course_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE exercise_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lesson_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE grammar_id_seq CASCADE');
        $this->addSql('DROP TABLE dictionary_word');
        $this->addSql('DROP TABLE feedback');
        $this->addSql('DROP TABLE user_teacher');
        $this->addSql('DROP TABLE user_employer');
        $this->addSql('DROP TABLE exercise_template');
        $this->addSql('DROP TABLE lesson_part');
        $this->addSql('DROP TABLE word_card');
        $this->addSql('DROP TABLE file_audio');
        $this->addSql('DROP TABLE user_settings');
        $this->addSql('DROP TABLE grammar_theme');
        $this->addSql('DROP TABLE dictionary');
        $this->addSql('DROP TABLE file_image');
        $this->addSql('DROP TABLE grammar_section');
        $this->addSql('DROP TABLE file_video');
        $this->addSql('DROP TABLE faq');
        $this->addSql('DROP TABLE unit');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE exercise');
        $this->addSql('DROP TABLE lesson');
        $this->addSql('DROP TABLE grammar');
        $this->addSql('DROP INDEX UNIQ_1483A5E9F85E0677');
        $this->addSql('ALTER TABLE users ADD fullname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users DROP last_name');
        $this->addSql('ALTER TABLE users DROP first_name');
        $this->addSql('ALTER TABLE users DROP middle_name');
        $this->addSql('ALTER TABLE users DROP username');
        $this->addSql('ALTER TABLE users DROP city');
        $this->addSql('ALTER TABLE users DROP birthday');
        $this->addSql('ALTER TABLE users DROP status');
        $this->addSql('ALTER TABLE users DROP last_activity_date');
        $this->addSql('ALTER TABLE users DROP last_auth_date');
        $this->addSql('ALTER TABLE user_session DROP logout_date');
        $this->addSql('ALTER TABLE user_session RENAME COLUMN login_date TO created_at');
    }
}
