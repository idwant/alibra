<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160303144247 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("TRUNCATE exercise_template CASCADE;");
        $this->addSql("
          INSERT INTO exercise_template(id, audio_id, name, task_en, task_ru, params, type_id)
            VALUES
            (1, NULL, 'dialog', 'Dialog', 'Диалог', '{}', 1), 
            (2, NULL, 'compar', 'Read and compar', 'Прочитать за диктофоном и сравнить', '{}', 2), 
            (3, NULL, 'free_form', 'Free form answer', 'Ответ в свободной форме', '{}', 3), 
            (4, NULL, 'find_coupl', 'Find coupl', 'Найти пару', '{}', 4), 
            (5, NULL, 'listen_writ', 'Listen and writ', 'Послушать и впечатать', '{}', 5), 
            (6, NULL, 'listen_say', 'Listen and say', 'Послушать,  ответить устно и сравнить', '{}', 6), 
            (7, NULL, 'drag_and_drop', 'Drag and drop', 'Drag and Drop', '{}', 7), 
            (8, NULL, 'read_writ', 'Read and writ', 'Прочитать и впечатать', '{}', 8), 
            (9, NULL, 'pictur', 'Pictur', 'Назвать и проверить', '{}', 9), 
            (10, NULL, 'select', 'Select', 'Выбрать из выпадающего списка', '{}', 10), 
            (11, NULL, 'five_card', 'Five card', '5 карт', '{}', 11), 
            (12, NULL, 'pick_out', 'Pick out word', 'Выделить слово', '{}', 12), 
            (13, NULL, 'snowball', 'Snowball', 'Snowball', '{}', 13), 
            (14, NULL, 'listen_read_click', 'Listen,  read and click', 'Listen,  read and click', '{}', 14), 
            (15, NULL, 'crossword', 'Crossword', 'Кроссворд', '{}', 15);"
        );
        // this up() migration is auto-generated,  please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated,  please modify it to your needs

    }
}
