<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160325101004 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("UPDATE exercise_template SET name = 'dialog-exercise' WHERE id = 1");
        $this->addSql("UPDATE exercise_template SET name = 'compare' WHERE id = 2");
        $this->addSql("UPDATE exercise_template SET name = 'free-form' WHERE id = 3");
        $this->addSql("UPDATE exercise_template SET name = 'find-couple' WHERE id = 4");
        $this->addSql("UPDATE exercise_template SET name = 'listen-write' WHERE id = 5");
        $this->addSql("UPDATE exercise_template SET name = 'listen-say' WHERE id = 6");
        $this->addSql("UPDATE exercise_template SET name = 'drag-and-drop' WHERE id = 7");
        $this->addSql("UPDATE exercise_template SET name = 'read-write' WHERE id = 8");
        $this->addSql("UPDATE exercise_template SET name = 'picture' WHERE id = 9");
        $this->addSql("UPDATE exercise_template SET name = 'select' WHERE id = 10");
        $this->addSql("UPDATE exercise_template SET name = 'five-card' WHERE id = 11");
        $this->addSql("UPDATE exercise_template SET name = 'pick-out' WHERE id = 12");
        $this->addSql("UPDATE exercise_template SET name = 'snowball' WHERE id = 13");
        $this->addSql("UPDATE exercise_template SET name = 'listen-read-click' WHERE id = 14");
        $this->addSql("UPDATE exercise_template SET name = 'crossword' WHERE id = 15");

        $this->addSql("UPDATE exercise_template_type SET name_en = 'dialog-exercise' WHERE id = 1");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'compare' WHERE id = 2");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'free-form' WHERE id = 3");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'find-couple' WHERE id = 4");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'listen-write' WHERE id = 5");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'listen-say' WHERE id = 6");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'drag-and-drop' WHERE id = 7");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'read-write' WHERE id = 8");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'picture' WHERE id = 9");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'select' WHERE id = 10");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'five-card' WHERE id = 11");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'pick-out' WHERE id = 12");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'snowball' WHERE id = 13");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'listen-read-click' WHERE id = 14");
        $this->addSql("UPDATE exercise_template_type SET name_en = 'crossword' WHERE id = 15");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
