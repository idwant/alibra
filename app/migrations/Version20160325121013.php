<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160325121013 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE delivery_template ALTER user_types TYPE TEXT');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types DROP DEFAULT');
        $this->addSql('ALTER TABLE file_audio ALTER name SET NOT NULL');
        $this->addSql('ALTER TABLE file_image ALTER name SET NOT NULL');
        $this->addSql('ALTER TABLE file_video ALTER name SET NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE file_video ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE file_audio ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE file_image ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types DROP DEFAULT');
    }
}
