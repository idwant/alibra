<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160712095512 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE lesson_part_exercise DROP CONSTRAINT fk_eda44381f74fb60c');
        $this->addSql('DROP SEQUENCE lesson_lesson_part_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE lesson_exercise_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE lesson_exercise (id INT NOT NULL, lesson_id INT DEFAULT NULL, exercise_id INT DEFAULT NULL, sort INT DEFAULT 100 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E69D9F05CDF80196 ON lesson_exercise (lesson_id)');
        $this->addSql('CREATE INDEX IDX_E69D9F05E934951A ON lesson_exercise (exercise_id)');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT FK_E69D9F05CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT FK_E69D9F05E934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE lesson_lesson_part');
        $this->addSql('ALTER TABLE course_unit ADD sort INT DEFAULT 100 NOT NULL');
        $this->addSql('DROP INDEX idx_eda44381f74fb60c');
        $this->addSql('ALTER TABLE lesson_part_exercise DROP lesson_lesson_part_id');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT fk_6f2d9f0547680510');
        $this->addSql('DROP INDEX idx_6f2d9f0547680510');
        $this->addSql('ALTER TABLE unit_lesson ADD sort INT DEFAULT 100 NOT NULL');
        $this->addSql('ALTER TABLE unit_lesson DROP number');
        $this->addSql('ALTER TABLE unit_lesson RENAME COLUMN unit_course_id TO unit_id');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT FK_6F2D9F05F8BD700D FOREIGN KEY (unit_id) REFERENCES unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6F2D9F05F8BD700D ON unit_lesson (unit_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE lesson_exercise_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE lesson_lesson_part_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE lesson_lesson_part (id INT NOT NULL, unit_lesson_id INT DEFAULT NULL, lesson_part_id INT DEFAULT NULL, number INT DEFAULT 0 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_81a75da9b99f194b ON lesson_lesson_part (lesson_part_id)');
        $this->addSql('CREATE INDEX idx_81a75da9d38ccd14 ON lesson_lesson_part (unit_lesson_id)');
        $this->addSql('ALTER TABLE lesson_lesson_part ADD CONSTRAINT fk_81a75da9d38ccd14 FOREIGN KEY (unit_lesson_id) REFERENCES unit_lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_lesson_part ADD CONSTRAINT fk_81a75da9b99f194b FOREIGN KEY (lesson_part_id) REFERENCES lesson_part (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE lesson_exercise');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT FK_6F2D9F05F8BD700D');
        $this->addSql('DROP INDEX IDX_6F2D9F05F8BD700D');
        $this->addSql('ALTER TABLE unit_lesson ADD number INT NOT NULL');
        $this->addSql('ALTER TABLE unit_lesson DROP sort');
        $this->addSql('ALTER TABLE unit_lesson RENAME COLUMN unit_id TO unit_course_id');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT fk_6f2d9f0547680510 FOREIGN KEY (unit_course_id) REFERENCES course_unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_6f2d9f0547680510 ON unit_lesson (unit_course_id)');
        $this->addSql('ALTER TABLE course_unit DROP sort');
        $this->addSql('ALTER TABLE lesson_part_exercise ADD lesson_lesson_part_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson_part_exercise ADD CONSTRAINT fk_eda44381f74fb60c FOREIGN KEY (lesson_lesson_part_id) REFERENCES lesson_lesson_part (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_eda44381f74fb60c ON lesson_part_exercise (lesson_lesson_part_id)');
    }
}
