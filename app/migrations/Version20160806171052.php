<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160806171052 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT fk_1e819b3e6923bc0e');
        $this->addSql('DROP INDEX idx_1e819b3e6923bc0e');
        $this->addSql('ALTER TABLE word_card RENAME COLUMN translation_audio_id TO audio_id');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E3A3123C7 FOREIGN KEY (audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1E819B3E3A3123C7 ON word_card (audio_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E3A3123C7');
        $this->addSql('DROP INDEX IDX_1E819B3E3A3123C7');
        $this->addSql('ALTER TABLE word_card RENAME COLUMN audio_id TO translation_audio_id');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT fk_1e819b3e6923bc0e FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_1e819b3e6923bc0e ON word_card (translation_audio_id)');
    }
}
