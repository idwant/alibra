<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160806160415 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE word_translation DROP CONSTRAINT fk_8cd870996923bc0e');
        $this->addSql('DROP INDEX idx_8cd870996923bc0e');
        $this->addSql('ALTER TABLE word_translation DROP translation_audio_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE word_translation ADD translation_audio_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word_translation ADD CONSTRAINT fk_8cd870996923bc0e FOREIGN KEY (translation_audio_id) REFERENCES file_audio (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_8cd870996923bc0e ON word_translation (translation_audio_id)');
    }
}
