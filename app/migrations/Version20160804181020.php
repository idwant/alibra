<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160804181020 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE user_unit_lesson_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_course_unit_id_seq CASCADE');
        $this->addSql('DROP TABLE user_course_unit');
        $this->addSql('DROP TABLE user_unit_lesson');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE user_unit_lesson_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_course_unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_course_unit (id INT NOT NULL, user_id INT DEFAULT NULL, course_unit_id INT DEFAULT NULL, lesson_parts JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_bd0218c0a76ed395 ON user_course_unit (user_id)');
        $this->addSql('CREATE INDEX idx_bd0218c0f07e75e1 ON user_course_unit (course_unit_id)');
        $this->addSql('CREATE TABLE user_unit_lesson (id INT NOT NULL, user_id INT DEFAULT NULL, unit_lesson_id INT DEFAULT NULL, lesson_parts JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_c6365690a76ed395 ON user_unit_lesson (user_id)');
        $this->addSql('CREATE INDEX idx_c6365690d38ccd14 ON user_unit_lesson (unit_lesson_id)');
        $this->addSql('ALTER TABLE user_course_unit ADD CONSTRAINT fk_bd0218c0a76ed395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_course_unit ADD CONSTRAINT fk_bd0218c0f07e75e1 FOREIGN KEY (course_unit_id) REFERENCES course_unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_unit_lesson ADD CONSTRAINT fk_c6365690a76ed395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_unit_lesson ADD CONSTRAINT fk_c6365690d38ccd14 FOREIGN KEY (unit_lesson_id) REFERENCES unit_lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
