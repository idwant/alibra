<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160906175823 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE hint_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_hint_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE hint (id INT NOT NULL, text VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, max_attempts INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_34C603535E237E06 ON hint (name)');
        $this->addSql('CREATE TABLE user_hint (id INT NOT NULL, user_id INT DEFAULT NULL, hint_id INT DEFAULT NULL, attempts INT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4E474F9AA76ED395 ON user_hint (user_id)');
        $this->addSql('CREATE INDEX IDX_4E474F9A519161AB ON user_hint (hint_id)');
        $this->addSql('ALTER TABLE user_hint ADD CONSTRAINT FK_4E474F9AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_hint ADD CONSTRAINT FK_4E474F9A519161AB FOREIGN KEY (hint_id) REFERENCES hint (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('INSERT INTO hint(id, name, text, max_attempts) VALUES(NEXTVAL(\'hint_id_seq\'), \'start_exercise\', \'START HERE\', 1)');
        $this->addSql('INSERT INTO hint(id, name, text, max_attempts) VALUES(NEXTVAL(\'hint_id_seq\'), \'record_audio\', \'Нажмите, чтобы начать запись. Нажмите еще раз, чтобы закончить запись.\', 1)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_hint DROP CONSTRAINT FK_4E474F9A519161AB');
        $this->addSql('DROP SEQUENCE hint_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_hint_id_seq CASCADE');
        $this->addSql('DROP TABLE hint');
        $this->addSql('DROP TABLE user_hint');
    }
}
