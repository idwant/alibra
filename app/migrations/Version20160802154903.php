<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160802154903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE dictionary_dictionary_word (dictionary_id INT NOT NULL, dictionary_word_id INT NOT NULL, PRIMARY KEY(dictionary_id, dictionary_word_id))');
        $this->addSql('CREATE INDEX IDX_FEFD2729AF5E5B3C ON dictionary_dictionary_word (dictionary_id)');
        $this->addSql('CREATE INDEX IDX_FEFD2729232512C ON dictionary_dictionary_word (dictionary_word_id)');
        $this->addSql('ALTER TABLE dictionary_dictionary_word ADD CONSTRAINT FK_FEFD2729AF5E5B3C FOREIGN KEY (dictionary_id) REFERENCES dictionary (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dictionary_dictionary_word ADD CONSTRAINT FK_FEFD2729232512C FOREIGN KEY (dictionary_word_id) REFERENCES dictionary_word (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE dictionary_dictionary_word');
    }
}
