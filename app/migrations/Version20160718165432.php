<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160718165432 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise ADD lesson_part_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CB99F194B FOREIGN KEY (lesson_part_id) REFERENCES lesson_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AEDAD51CB99F194B ON exercise (lesson_part_id)');

        $this->addSql('ALTER TABLE exercise ADD lesson_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CCDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson_exercise (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AEDAD51CCDF80196 ON exercise (lesson_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CB99F194B');
        $this->addSql('DROP INDEX IDX_AEDAD51CB99F194B');
        $this->addSql('ALTER TABLE exercise DROP lesson_part_id');

        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CCDF80196');
        $this->addSql('DROP INDEX IDX_AEDAD51CCDF80196');
        $this->addSql('ALTER TABLE exercise DROP lesson_id');
    }
}
