<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160806153931 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_word_card ALTER repetition_rate TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE user_word_card ALTER repetition_rate SET DEFAULT \'P7D\'');
        $this->addSql('COMMENT ON COLUMN user_word_card.repetition_rate IS \'Частота повторения карточки в днях в формате DateInterval\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE user_word_card ALTER repetition_rate TYPE INT');
        $this->addSql('ALTER TABLE user_word_card ALTER repetition_rate SET DEFAULT 7');
        $this->addSql('COMMENT ON COLUMN user_word_card.repetition_rate IS \'Частота повторения карточки в днях\'');
    }
}
