<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160714131759 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('TRUNCATE course CASCADE');
        $this->addSql('ALTER TABLE course ADD position INT NOT NULL');
        $this->addSql('TRUNCATE course_unit CASCADE');
        $this->addSql('ALTER TABLE course_unit ADD position INT NOT NULL');
        $this->addSql('ALTER TABLE course_unit DROP sort');
        $this->addSql('TRUNCATE lesson_exercise CASCADE');
        $this->addSql('ALTER TABLE lesson_exercise ADD position INT NOT NULL');
        $this->addSql('ALTER TABLE lesson_exercise DROP sort');
        $this->addSql('TRUNCATE unit_lesson CASCADE');
        $this->addSql('ALTER TABLE unit_lesson ADD position INT NOT NULL');
        $this->addSql('ALTER TABLE unit_lesson DROP sort');
        $this->addSql('ALTER TABLE course DROP "position"');
        $this->addSql('CREATE UNIQUE INDEX course_unit_u ON course_unit (course_id, unit_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE lesson_exercise ADD sort INT DEFAULT 100 NOT NULL');
        $this->addSql('ALTER TABLE lesson_exercise DROP position');
        $this->addSql('ALTER TABLE course DROP position');
        $this->addSql('ALTER TABLE course_unit ADD sort INT DEFAULT 100 NOT NULL');
        $this->addSql('ALTER TABLE course_unit DROP position');
        $this->addSql('ALTER TABLE unit_lesson ADD sort INT DEFAULT 100 NOT NULL');
        $this->addSql('ALTER TABLE unit_lesson DROP position');
        $this->addSql('ALTER TABLE course ADD "position" INT NOT NULL');
        $this->addSql('DROP INDEX course_unit_u');
    }
}
