<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151202141142 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE user_word_card_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_unit_lesson_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_course_unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_word_card (id INT NOT NULL, user_id INT DEFAULT NULL, word_card_id INT DEFAULT NULL, known BOOLEAN DEFAULT \'false\' NOT NULL, repetition_count INT DEFAULT 0 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1E6116DAA76ED395 ON user_word_card (user_id)');
        $this->addSql('CREATE INDEX IDX_1E6116DA40BEEDD3 ON user_word_card (word_card_id)');
        $this->addSql('CREATE TABLE user_unit_lesson (id INT NOT NULL, user_id INT DEFAULT NULL, unit_lesson_id INT DEFAULT NULL, lesson_parts VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C6365690A76ED395 ON user_unit_lesson (user_id)');
        $this->addSql('CREATE INDEX IDX_C6365690D38CCD14 ON user_unit_lesson (unit_lesson_id)');
        $this->addSql('CREATE TABLE user_course_unit (id INT NOT NULL, user_id INT DEFAULT NULL, course_unit_id INT DEFAULT NULL, lesson_parts VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BD0218C0A76ED395 ON user_course_unit (user_id)');
        $this->addSql('CREATE INDEX IDX_BD0218C0F07E75E1 ON user_course_unit (course_unit_id)');
        $this->addSql('ALTER TABLE user_word_card ADD CONSTRAINT FK_1E6116DAA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_word_card ADD CONSTRAINT FK_1E6116DA40BEEDD3 FOREIGN KEY (word_card_id) REFERENCES word_card (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_unit_lesson ADD CONSTRAINT FK_C6365690A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_unit_lesson ADD CONSTRAINT FK_C6365690D38CCD14 FOREIGN KEY (unit_lesson_id) REFERENCES unit_lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_course_unit ADD CONSTRAINT FK_BD0218C0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_course_unit ADD CONSTRAINT FK_BD0218C0F07E75E1 FOREIGN KEY (course_unit_id) REFERENCES course_unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE user_word_card_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_unit_lesson_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_course_unit_id_seq CASCADE');
        $this->addSql('DROP TABLE user_word_card');
        $this->addSql('DROP TABLE user_unit_lesson');
        $this->addSql('DROP TABLE user_course_unit');
    }
}
