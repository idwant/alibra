<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160705181230 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_unit_lesson ALTER lesson_parts TYPE JSON USING (lesson_parts::json)');
        $this->addSql('ALTER TABLE user_unit_lesson ALTER lesson_parts DROP DEFAULT');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types TYPE JSON USING (user_types::json)');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types DROP DEFAULT');
        $this->addSql('ALTER TABLE user_course_unit ALTER lesson_parts TYPE JSON USING (lesson_parts::json)');
        $this->addSql('ALTER TABLE user_course_unit ALTER lesson_parts DROP DEFAULT');
        $this->addSql('DROP INDEX uniq_707eb6a25e237e06');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE UNIQUE INDEX uniq_707eb6a25e237e06 ON lesson_part (name)');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types TYPE TEXT');
        $this->addSql('ALTER TABLE delivery_template ALTER user_types DROP DEFAULT');
        $this->addSql('ALTER TABLE user_unit_lesson ALTER lesson_parts TYPE TEXT');
        $this->addSql('ALTER TABLE user_unit_lesson ALTER lesson_parts DROP DEFAULT');
        $this->addSql('ALTER TABLE user_course_unit ALTER lesson_parts TYPE TEXT');
        $this->addSql('ALTER TABLE user_course_unit ALTER lesson_parts DROP DEFAULT');
    }
}
