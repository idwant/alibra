<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160804150546 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE word_card_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE word_card_group (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE word_card ADD word_card_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE word_card ADD CONSTRAINT FK_1E819B3E5330AD6D FOREIGN KEY (word_card_group_id) REFERENCES word_card_group (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1E819B3E5330AD6D ON word_card (word_card_group_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE word_card DROP CONSTRAINT FK_1E819B3E5330AD6D');
        $this->addSql('DROP SEQUENCE word_card_group_id_seq CASCADE');
        $this->addSql('DROP TABLE word_card_group');
        $this->addSql('DROP INDEX IDX_1E819B3E5330AD6D');
        $this->addSql('ALTER TABLE word_card DROP word_card_group_id');
    }
}
