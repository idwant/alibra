<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161013000142 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE hint ADD timeout INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_hint ADD show_again_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');

        $this->addSql('INSERT INTO hint (id, text, name, max_attempts, timeout) VALUES (NEXTVAL(\'hint_id_seq\'),\'У вас %availableGroups% незавершенных повторений\', \'group_notice\', -1, 86400)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE hint DROP timeout');
        $this->addSql('ALTER TABLE user_hint DROP show_again_date');
    }
}
