<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160119173119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE exercise_template_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE exercise_template_type (id INT NOT NULL, name VARCHAR(255) NOT NULL, name_en VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1B487D9D3D773AC4 ON exercise_template_type (name_en)');
        $this->addSql('ALTER TABLE exercise_template ADD type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise_template ADD CONSTRAINT FK_910CBF6BC54C8C93 FOREIGN KEY (type_id) REFERENCES exercise_template_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_910CBF6BC54C8C93 ON exercise_template (type_id)');

        $this->addSql("
          INSERT INTO exercise_template_type(id, name, name_en) VALUES
            (1, 'Диалог', 'dialog'),
            (2, 'Прочитать за диктором и сравнить', 'compare'),
            (3, 'Ответ в свободной форме', 'free_form'),
            (4, 'Найти пару', 'find_couple'),
            (5, 'Послушать и впечатать', 'listen_write'),
            (6, 'Послушать, ответить устно и проверить', 'listen_say'),
            (7, 'Drag and Drop', 'drag_and_drop'),
            (8, 'Прочитать и впечатать', 'read_write'),
            (9, 'Назвать и проверить', 'picture'),
            (10, 'Выбрать из выпадающего списка', 'select'),
            (11, '5 карт', 'five_cards'),
            (12, 'Выделить слово', 'pick_out'),
            (13, 'Snowball', 'snowball'),
            (14, 'Listen\\read and click', 'listen_read_click'),
            (15, 'Кроссворд', 'crossword');"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE exercise_template DROP CONSTRAINT FK_910CBF6BC54C8C93');
        $this->addSql('DROP SEQUENCE exercise_template_type_id_seq CASCADE');
        $this->addSql('DROP TABLE exercise_template_type');
        $this->addSql('DROP INDEX IDX_910CBF6BC54C8C93');
        $this->addSql('ALTER TABLE exercise_template DROP type_id');
    }
}
