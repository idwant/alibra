<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160722111228 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE exercise_word_card_id_seq CASCADE');
        $this->addSql('ALTER TABLE exercise_word_card DROP id');
        $this->addSql('ALTER TABLE exercise_word_card ALTER exercise_id SET NOT NULL');
        $this->addSql('ALTER TABLE exercise_word_card ALTER word_card_id SET NOT NULL');
        $this->addSql('ALTER TABLE exercise_word_card ADD PRIMARY KEY (exercise_id, word_card_id)');
        $this->addSql('ALTER TABLE exercise_template ALTER type SET NOT NULL');
        $this->addSql('ALTER TABLE exercise_template ALTER type SET NOT NULL');

        $this->addSql('TRUNCATE unit_lesson CASCADE');
        $this->addSql('TRUNCATE unit_lesson_word_card CASCADE');
        $this->addSql('TRUNCATE lesson_exercise CASCADE');

        $this->addSql('CREATE UNIQUE INDEX lesson_exercise_u ON lesson_exercise (lesson_id, exercise_id)');
        $this->addSql('CREATE UNIQUE INDEX unit_lesson_u ON unit_lesson (unit_id, lesson_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE exercise_word_card_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP INDEX exercise_word_card_pkey');
        $this->addSql('ALTER TABLE exercise_word_card ADD id INT NOT NULL');
        $this->addSql('ALTER TABLE exercise_word_card ALTER exercise_id DROP NOT NULL');
        $this->addSql('ALTER TABLE exercise_word_card ALTER word_card_id DROP NOT NULL');
        $this->addSql('ALTER TABLE exercise_word_card ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE exercise_template ALTER type DROP NOT NULL');
        $this->addSql('DROP INDEX lesson_exercise_u');
        $this->addSql('DROP INDEX unit_lesson_u');
    }
}
