<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151202134513 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE delivery_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE delivery_template_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE unit_lesson_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE course_unit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lesson_lesson_part_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE lesson_part_exercise_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE delivery (id INT NOT NULL, user_id INT DEFAULT NULL, delivery_template_id INT DEFAULT NULL, "type" VARCHAR(255) NOT NULL, user_types VARCHAR(255) NOT NULL, delivery_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, sended_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, message TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3781EC10A76ED395 ON delivery (user_id)');
        $this->addSql('CREATE INDEX IDX_3781EC1039C7AEFE ON delivery (delivery_template_id)');
        $this->addSql('CREATE TABLE delivery_template (id INT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, user_types VARCHAR(255) NOT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, repeat_count INT DEFAULT 1000000 NOT NULL, repeat_step_minute INT DEFAULT 1440 NOT NULL, message TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F601FEEF5E237E06 ON delivery_template (name)');
        $this->addSql('CREATE INDEX IDX_F601FEEFA76ED395 ON delivery_template (user_id)');
        $this->addSql('COMMENT ON COLUMN delivery_template.repeat_step_minute IS \'шаг повторения рассылки в минутах\'');
        $this->addSql('CREATE TABLE unit_lesson (id INT NOT NULL, unit_course_id INT DEFAULT NULL, lesson_id INT DEFAULT NULL, "number" INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6F2D9F0547680510 ON unit_lesson (unit_course_id)');
        $this->addSql('CREATE INDEX IDX_6F2D9F05CDF80196 ON unit_lesson (lesson_id)');
        $this->addSql('CREATE TABLE unit_lesson_word_card (unit_lesson_id INT NOT NULL, word_card_id INT NOT NULL, PRIMARY KEY(unit_lesson_id, word_card_id))');
        $this->addSql('CREATE INDEX IDX_BF953703D38CCD14 ON unit_lesson_word_card (unit_lesson_id)');
        $this->addSql('CREATE INDEX IDX_BF95370340BEEDD3 ON unit_lesson_word_card (word_card_id)');
        $this->addSql('CREATE TABLE course_unit (id INT NOT NULL, course_id INT DEFAULT NULL, unit_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1419D155591CC992 ON course_unit (course_id)');
        $this->addSql('CREATE INDEX IDX_1419D155F8BD700D ON course_unit (unit_id)');
        $this->addSql('CREATE TABLE exercise_grammar (exercise_id INT NOT NULL, grammar_id INT NOT NULL, PRIMARY KEY(exercise_id, grammar_id))');
        $this->addSql('CREATE INDEX IDX_A07E953CE934951A ON exercise_grammar (exercise_id)');
        $this->addSql('CREATE INDEX IDX_A07E953CB0CA2760 ON exercise_grammar (grammar_id)');
        $this->addSql('CREATE TABLE lesson_lesson_part (id INT NOT NULL, unit_lesson_id INT DEFAULT NULL, lesson_part_id INT DEFAULT NULL, "number" INT DEFAULT 0 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_81A75DA9D38CCD14 ON lesson_lesson_part (unit_lesson_id)');
        $this->addSql('CREATE INDEX IDX_81A75DA9B99F194B ON lesson_lesson_part (lesson_part_id)');
        $this->addSql('CREATE TABLE lesson_part_exercise (id INT NOT NULL, lesson_lesson_part_id INT DEFAULT NULL, exercise_id INT DEFAULT NULL, "number" INT DEFAULT 0 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EDA44381F74FB60C ON lesson_part_exercise (lesson_lesson_part_id)');
        $this->addSql('CREATE INDEX IDX_EDA44381E934951A ON lesson_part_exercise (exercise_id)');
        $this->addSql('ALTER TABLE delivery ADD CONSTRAINT FK_3781EC10A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE delivery ADD CONSTRAINT FK_3781EC1039C7AEFE FOREIGN KEY (delivery_template_id) REFERENCES delivery_template (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE delivery_template ADD CONSTRAINT FK_F601FEEFA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT FK_6F2D9F0547680510 FOREIGN KEY (unit_course_id) REFERENCES course_unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson ADD CONSTRAINT FK_6F2D9F05CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson_word_card ADD CONSTRAINT FK_BF953703D38CCD14 FOREIGN KEY (unit_lesson_id) REFERENCES unit_lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE unit_lesson_word_card ADD CONSTRAINT FK_BF95370340BEEDD3 FOREIGN KEY (word_card_id) REFERENCES word_card (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_unit ADD CONSTRAINT FK_1419D155591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE course_unit ADD CONSTRAINT FK_1419D155F8BD700D FOREIGN KEY (unit_id) REFERENCES unit (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise_grammar ADD CONSTRAINT FK_A07E953CE934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise_grammar ADD CONSTRAINT FK_A07E953CB0CA2760 FOREIGN KEY (grammar_id) REFERENCES grammar (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_lesson_part ADD CONSTRAINT FK_81A75DA9D38CCD14 FOREIGN KEY (unit_lesson_id) REFERENCES unit_lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_lesson_part ADD CONSTRAINT FK_81A75DA9B99F194B FOREIGN KEY (lesson_part_id) REFERENCES lesson_part (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_part_exercise ADD CONSTRAINT FK_EDA44381F74FB60C FOREIGN KEY (lesson_lesson_part_id) REFERENCES lesson_lesson_part (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_part_exercise ADD CONSTRAINT FK_EDA44381E934951A FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_teacher DROP CONSTRAINT FK_E8FBB8DFA76ED395');
        $this->addSql('ALTER TABLE user_teacher DROP CONSTRAINT FK_E8FBB8DF41807E1D');
        $this->addSql('ALTER TABLE user_teacher ADD CONSTRAINT FK_E8FBB8DFA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_teacher ADD CONSTRAINT FK_E8FBB8DF41807E1D FOREIGN KEY (teacher_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_employer DROP CONSTRAINT FK_3EC11466A76ED395');
        $this->addSql('ALTER TABLE user_employer DROP CONSTRAINT FK_3EC1146641CD9E7A');
        $this->addSql('ALTER TABLE user_employer ADD CONSTRAINT FK_3EC11466A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_employer ADD CONSTRAINT FK_3EC1146641CD9E7A FOREIGN KEY (employer_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE delivery DROP CONSTRAINT FK_3781EC1039C7AEFE');
        $this->addSql('ALTER TABLE unit_lesson_word_card DROP CONSTRAINT FK_BF953703D38CCD14');
        $this->addSql('ALTER TABLE lesson_lesson_part DROP CONSTRAINT FK_81A75DA9D38CCD14');
        $this->addSql('ALTER TABLE unit_lesson DROP CONSTRAINT FK_6F2D9F0547680510');
        $this->addSql('ALTER TABLE lesson_part_exercise DROP CONSTRAINT FK_EDA44381F74FB60C');
        $this->addSql('DROP SEQUENCE delivery_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE delivery_template_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE unit_lesson_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE course_unit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lesson_lesson_part_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE lesson_part_exercise_id_seq CASCADE');
        $this->addSql('DROP TABLE delivery');
        $this->addSql('DROP TABLE delivery_template');
        $this->addSql('DROP TABLE unit_lesson');
        $this->addSql('DROP TABLE unit_lesson_word_card');
        $this->addSql('DROP TABLE course_unit');
        $this->addSql('DROP TABLE exercise_grammar');
        $this->addSql('DROP TABLE lesson_lesson_part');
        $this->addSql('DROP TABLE lesson_part_exercise');
        $this->addSql('ALTER TABLE user_teacher DROP CONSTRAINT fk_e8fbb8dfa76ed395');
        $this->addSql('ALTER TABLE user_teacher DROP CONSTRAINT fk_e8fbb8df41807e1d');
        $this->addSql('ALTER TABLE user_teacher ADD CONSTRAINT fk_e8fbb8dfa76ed395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_teacher ADD CONSTRAINT fk_e8fbb8df41807e1d FOREIGN KEY (teacher_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_employer DROP CONSTRAINT fk_3ec11466a76ed395');
        $this->addSql('ALTER TABLE user_employer DROP CONSTRAINT fk_3ec1146641cd9e7a');
        $this->addSql('ALTER TABLE user_employer ADD CONSTRAINT fk_3ec11466a76ed395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_employer ADD CONSTRAINT fk_3ec1146641cd9e7a FOREIGN KEY (employer_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
