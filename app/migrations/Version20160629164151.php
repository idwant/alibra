<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160629164151 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            INSERT INTO task_status VALUES (1, 'wait', 'Ожидает выполнения', 'Задача ожидает выполнения');
            INSERT INTO task_status VALUES (2, 'in process', 'Выполняется', 'Задача выполняется');
            INSERT INTO task_status VALUES (3, 'executed', 'Выполнена', 'Задача выполнена');
        ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
