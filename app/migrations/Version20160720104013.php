<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160720104013 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT FK_AEDAD51CCDF80196');
        $this->addSql('DROP INDEX IDX_AEDAD51CCDF80196');
        $this->addSql('ALTER TABLE exercise DROP lesson_id');

        $this->addSql('DROP SEQUENCE lesson_part_exercise_id_seq CASCADE');
        $this->addSql('DROP TABLE lesson_part_exercise');
        $this->addSql('ALTER TABLE lesson_part ADD lesson_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson_part ADD CONSTRAINT FK_707EB6A2CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_707EB6A2CDF80196 ON lesson_part (lesson_id)');
        
        $this->addSql('ALTER TABLE exercise DROP CONSTRAINT fk_aedad51cb99f194b');
        $this->addSql('DROP INDEX idx_aedad51cb99f194b');
        $this->addSql('ALTER TABLE exercise DROP lesson_part_id');
        $this->addSql('ALTER TABLE lesson_exercise ADD lesson_part_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson_exercise ADD CONSTRAINT FK_E69D9F05B99F194B FOREIGN KEY (lesson_part_id) REFERENCES lesson_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E69D9F05B99F194B ON lesson_exercise (lesson_part_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE exercise ADD lesson_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT FK_AEDAD51CCDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson_exercise (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AEDAD51CCDF80196 ON exercise (lesson_id)');

        $this->addSql('CREATE SEQUENCE lesson_part_exercise_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE lesson_part_exercise (id INT NOT NULL, exercise_id INT DEFAULT NULL, number INT DEFAULT 0 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_eda44381e934951a ON lesson_part_exercise (exercise_id)');
        $this->addSql('ALTER TABLE lesson_part_exercise ADD CONSTRAINT fk_eda44381e934951a FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_part DROP CONSTRAINT FK_707EB6A2CDF80196');
        $this->addSql('DROP INDEX IDX_707EB6A2CDF80196');
        $this->addSql('ALTER TABLE lesson_part DROP lesson_id');
        
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE lesson_exercise DROP CONSTRAINT FK_E69D9F05B99F194B');
        $this->addSql('DROP INDEX IDX_E69D9F05B99F194B');
        $this->addSql('ALTER TABLE lesson_exercise DROP lesson_part_id');
        $this->addSql('ALTER TABLE exercise ADD lesson_part_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exercise ADD CONSTRAINT fk_aedad51cb99f194b FOREIGN KEY (lesson_part_id) REFERENCES lesson_part (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_aedad51cb99f194b ON exercise (lesson_part_id)');
    }
}
