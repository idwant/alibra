<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160804100832 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE lesson_word_card (lesson_id INT NOT NULL, word_card_id INT NOT NULL, PRIMARY KEY(lesson_id, word_card_id))');
        $this->addSql('CREATE INDEX IDX_7AA274B4CDF80196 ON lesson_word_card (lesson_id)');
        $this->addSql('CREATE INDEX IDX_7AA274B440BEEDD3 ON lesson_word_card (word_card_id)');
        $this->addSql('ALTER TABLE lesson_word_card ADD CONSTRAINT FK_7AA274B4CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE lesson_word_card ADD CONSTRAINT FK_7AA274B440BEEDD3 FOREIGN KEY (word_card_id) REFERENCES word_card (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE exercise_word_card');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE exercise_word_card (exercise_id INT NOT NULL, word_card_id INT NOT NULL, PRIMARY KEY(exercise_id, word_card_id))');
        $this->addSql('CREATE INDEX idx_b0569dd440beedd3 ON exercise_word_card (word_card_id)');
        $this->addSql('CREATE INDEX idx_b0569dd4e934951a ON exercise_word_card (exercise_id)');
        $this->addSql('ALTER TABLE exercise_word_card ADD CONSTRAINT fk_b0569dd4e934951a FOREIGN KEY (exercise_id) REFERENCES exercise (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE exercise_word_card ADD CONSTRAINT fk_b0569dd440beedd3 FOREIGN KEY (word_card_id) REFERENCES word_card (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE lesson_word_card');
    }
}
